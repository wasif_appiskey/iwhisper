//
//  BLLUser.h
//  The Club App
//
//  Created by Wasif Iqbal on 8/10/15.
//  Copyright (c) 2015 Create Media Group. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BLLUser : NSObject

-(BOOL)isUserLoggedIn;

-(NSManagedObject*)getLoggedInUser;

-(void)LogOutUser;

+(BLLUser*)manager;

@end

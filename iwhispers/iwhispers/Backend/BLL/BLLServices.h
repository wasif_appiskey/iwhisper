//
//  BLLAuthentication.h
//  danone
//
//  Created by Wasif Iqbal on 3/20/15.
//  Copyright (c) 2015 Appiskey.inc All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseBLL.h"


@interface BLLServices : BaseBLL <AFNetworkingWrapperDelegate>
{
    
}

-(void)userLogin:(NSMutableDictionary *)params;
-(void)userVerify:(NSMutableDictionary *)params;
-(void)syncContact:(NSMutableDictionary *)params;
-(void)accountDelete:(NSMutableDictionary *)params;
-(void)profileUpdate:(NSMutableDictionary *)params image:(NSData *)imageData;
-(void)getProfile:(NSMutableDictionary *)params;
-(void)sendChatMessage:(NSMutableDictionary *)params;
-(void)sendChatImage:(NSMutableDictionary *)params image:(NSData *)imageData;
-(void)createGroup:(NSMutableDictionary *)params image:(NSData *)imageData;
-(void)addGroupMember:(NSMutableDictionary *)params;
-(void)getContactDetail:(NSMutableDictionary *)params;
-(void)removeGroupMember:(NSMutableDictionary *)params;
-(void)getGroupDetail:(NSMutableDictionary *)params;
-(void)sendGroupMessage:(NSMutableDictionary *)params;
-(void)messageSeen:(NSMutableDictionary *)params;
-(void)messageRecieved:(NSMutableDictionary *)params;
-(void)sendGroupChatImage:(NSMutableDictionary *)params image:(NSData *)imageData;

@end

//
//  BLLAuthentication.m
//  Alef
//
//  Created by Salman on 8/2/15.
//  Copyright (c) 2015 Create Media Group. All rights reserved.
//

#import "BLLServices.h"
#import "CategoryParser.h"
#import "Enums.h"
#import "iwhispers-Swift.h"
#import "AppConstants.h"


@implementation BLLServices


-(void)userLogin:(NSMutableDictionary *)params{
    NSURL *url = [NSURL URLWithString:[[NSString stringWithFormat:@"%@%@", kAPIUrl,KLogin] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *strURl = [NSString stringWithFormat:@"%@",url];
    
    AFNetworkingWrapper *afNetworkWrapper = [[AFNetworkingWrapper alloc] initWithURL:strURl andPostParams:params];
    afNetworkWrapper.delegate=self;
    afNetworkWrapper.tagService= Login;
    [afNetworkWrapper startAsynchronousPost];
}



-(void)userVerify:(NSMutableDictionary *)params{
    NSURL *url = [NSURL URLWithString:[[NSString stringWithFormat:@"%@%@", kAPIUrl,KVerify] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *strURl = [NSString stringWithFormat:@"%@",url];
    
    AFNetworkingWrapper *afNetworkWrapper = [[AFNetworkingWrapper alloc] initWithURL:strURl andPostParams:params];
    afNetworkWrapper.delegate=self;
    afNetworkWrapper.tagService= Verify;
    [afNetworkWrapper startAsynchronousPut];
}


-(void)syncContact:(NSMutableDictionary *)params{
    NSURL *url = [NSURL URLWithString:[[NSString stringWithFormat:@"%@%@", kAPIUrl,KSyncContact] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *strURl = [NSString stringWithFormat:@"%@",url];
    
    AFNetworkingWrapper *afNetworkWrapper = [[AFNetworkingWrapper alloc] initWithURL:strURl andPostParams:params];
    afNetworkWrapper.delegate=self;
    afNetworkWrapper.tagService= SyncContact;
    [afNetworkWrapper startAsynchronousPost];
}


-(void)getProfile:(NSMutableDictionary *)params{
    NSURL *url = [NSURL URLWithString:[[NSString stringWithFormat:@"%@%@", kAPIUrl,KLogin] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *strURl = [NSString stringWithFormat:@"%@",url];
    
    AFNetworkingWrapper *afNetworkWrapper = [[AFNetworkingWrapper alloc] initWithURL:strURl andPostParams:params];
    afNetworkWrapper.delegate=self;
    afNetworkWrapper.tagService= GetProfile;
    [afNetworkWrapper startAsynchronous];
}


-(void)getContactDetail:(NSMutableDictionary *)params{
    NSURL *url = [NSURL URLWithString:[[NSString stringWithFormat:@"%@%@/%@", kAPIUrl,KLogin,[params valueForKey:@"contactID"]] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    [params removeObjectForKey:@"contactID"];
    NSString *strURl = [NSString stringWithFormat:@"%@",url];
    
    AFNetworkingWrapper *afNetworkWrapper = [[AFNetworkingWrapper alloc] initWithURL:strURl andPostParams:params];
    afNetworkWrapper.delegate=self;
    afNetworkWrapper.tagService= GetContactDetail;
    [afNetworkWrapper startAsynchronous];
}


-(void)profileUpdate:(NSMutableDictionary *)params image:(NSData *)imageData{
    NSURL *url = [NSURL URLWithString:[[NSString stringWithFormat:@"%@%@", kAPIUrl,KLogin] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *strURl = [NSString stringWithFormat:@"%@",url];
    
    AFNetworkingWrapper *afNetworkWrapper = [[AFNetworkingWrapper alloc] initWithURL:strURl andPostParams:params];
    afNetworkWrapper.delegate=self;
    afNetworkWrapper.tagService= SetProfile;
    afNetworkWrapper.postParams = params;
    if (imageData != nil) {
        afNetworkWrapper.postKey = @"photo";
        afNetworkWrapper.mimetype = @"image/jpeg";
        afNetworkWrapper.extentionType = @"jpg";
        afNetworkWrapper.postData = imageData;
        [afNetworkWrapper startAsynchronousPostWithData];
    }else{
        [afNetworkWrapper startAsynchronousPut];
    }
}
-(void)sendChatMessage:(NSMutableDictionary *)params{
    NSURL *url = [NSURL URLWithString:[[NSString stringWithFormat:@"%@%@", kAPIUrl,KSendChatMessage] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *strURl = [NSString stringWithFormat:@"%@",url];
    
    AFNetworkingWrapper *afNetworkWrapper = [[AFNetworkingWrapper alloc] initWithURL:strURl andPostParams:params];
    afNetworkWrapper.delegate=self;
    afNetworkWrapper.tagService= SendChatMessage;
    [afNetworkWrapper startAsynchronousPost];
}

-(void)sendGroupMessage:(NSMutableDictionary *)params{
    NSURL *url = [NSURL URLWithString:[[NSString stringWithFormat:@"%@%@", kAPIUrl,KSendGroupMessage] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *strURl = [NSString stringWithFormat:@"%@",url];
    
    AFNetworkingWrapper *afNetworkWrapper = [[AFNetworkingWrapper alloc] initWithURL:strURl andPostParams:params];
    afNetworkWrapper.delegate=self;
    afNetworkWrapper.tagService= SendGroupMessage;
    [afNetworkWrapper startAsynchronousPost];
}


-(void)sendChatImage:(NSMutableDictionary *)params image:(NSData *)imageData{
    NSURL *url = [NSURL URLWithString:[[NSString stringWithFormat:@"%@%@", kAPIUrl,KSendChatMessage] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *strURl = [NSString stringWithFormat:@"%@",url];
    
    AFNetworkingWrapper *afNetworkWrapper = [[AFNetworkingWrapper alloc] initWithURL:strURl andPostParams:params];
    afNetworkWrapper.delegate=self;
    afNetworkWrapper.tagService= SendChatImage;
    afNetworkWrapper.postParams = params;
    if (imageData != nil) {
        if ([[params valueForKey:@"type"] isEqualToString:@"AUDIO"]) {
            afNetworkWrapper.mimetype = @"audio/x-caf";
            afNetworkWrapper.extentionType = @"caf";
        }else if ([[params valueForKey:@"type"] isEqualToString:@"IMAGE"]){
            afNetworkWrapper.mimetype = @"image/jpeg";
            afNetworkWrapper.extentionType = @"jpg";
        }else if ([[params valueForKey:@"type"] isEqualToString:@"VIDEO"]){
         //   afNetworkWrapper.mimetype = @"video/quicktime";
         //   afNetworkWrapper.extentionType = @"mov";
           
            
            afNetworkWrapper.mimetype = @"video/mp4";
            afNetworkWrapper.extentionType = @"mp4";
        }
        afNetworkWrapper.postKey = @"file";
        
        afNetworkWrapper.postData = imageData;
        [afNetworkWrapper startAsynchronousPostWithData];
    }else{
        [afNetworkWrapper startAsynchronousPut];
    }
}


-(void)sendGroupChatImage:(NSMutableDictionary *)params image:(NSData *)imageData{
    NSURL *url = [NSURL URLWithString:[[NSString stringWithFormat:@"%@%@", kAPIUrl,KSendGroupMessage] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *strURl = [NSString stringWithFormat:@"%@",url];
    
    AFNetworkingWrapper *afNetworkWrapper = [[AFNetworkingWrapper alloc] initWithURL:strURl andPostParams:params];
    afNetworkWrapper.delegate=self;
    afNetworkWrapper.tagService= SendChatImage;
    afNetworkWrapper.postParams = params;
    if (imageData != nil) {
        if ([[params valueForKey:@"type"] isEqualToString:@"AUDIO"]) {
            afNetworkWrapper.mimetype = @"audio/x-caf";
            afNetworkWrapper.extentionType = @"caf";
        }else if ([[params valueForKey:@"type"] isEqualToString:@"IMAGE"]){
            afNetworkWrapper.mimetype = @"image/jpeg";
            afNetworkWrapper.extentionType = @"jpg";
        }else if ([[params valueForKey:@"type"] isEqualToString:@"VIDEO"]){
            afNetworkWrapper.mimetype = @"video/quicktime";
            afNetworkWrapper.extentionType = @"mov";
        }
        afNetworkWrapper.postKey = @"file";
        
        afNetworkWrapper.postData = imageData;
        [afNetworkWrapper startAsynchronousPostWithData];
    }else{
        [afNetworkWrapper startAsynchronousPut];
    }
}



-(void)createGroup:(NSMutableDictionary *)params image:(NSData *)imageData{
    NSURL *url = [NSURL URLWithString:[[NSString stringWithFormat:@"%@%@", kAPIUrl,kCreateGroup] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *strURl = [NSString stringWithFormat:@"%@",url];
    
    AFNetworkingWrapper *afNetworkWrapper = [[AFNetworkingWrapper alloc] initWithURL:strURl andPostParams:params];
    afNetworkWrapper.delegate=self;
    afNetworkWrapper.tagService= CreateGroup;
    afNetworkWrapper.postParams = params;
    if (imageData != nil) {
        afNetworkWrapper.postKey = @"photo";
        afNetworkWrapper.mimetype = @"image/jpeg";
        afNetworkWrapper.extentionType = @"jpg";
        afNetworkWrapper.postData = imageData;
        [afNetworkWrapper startAsynchronousPostWithData];
    }else{
        [afNetworkWrapper startAsynchronousPost];
    }
}

-(void)addGroupMember:(NSMutableDictionary *)params{
    NSURL *url = [NSURL URLWithString:[[NSString stringWithFormat:@"%@%@/%@/%@", kAPIUrl,kCreateGroup,[params valueForKey:@"group_id"],KAddGroupMember] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *strURl = [NSString stringWithFormat:@"%@",url];
    
    AFNetworkingWrapper *afNetworkWrapper = [[AFNetworkingWrapper alloc] initWithURL:strURl andPostParams:params];
    afNetworkWrapper.delegate=self;
    afNetworkWrapper.tagService= AddGroupMember;
    [afNetworkWrapper startAsynchronousPost];
}

-(void)removeGroupMember:(NSMutableDictionary *)params{
    NSURL *url = [NSURL URLWithString:[[NSString stringWithFormat:@"%@%@/%@/%@", kAPIUrl,kCreateGroup,[params valueForKey:@"groupID"],kRemoveMember] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *strURl = [NSString stringWithFormat:@"%@",url];
    
    AFNetworkingWrapper *afNetworkWrapper = [[AFNetworkingWrapper alloc] initWithURL:strURl andPostParams:params];
    afNetworkWrapper.delegate=self;
    afNetworkWrapper.tagService= RemoveGroupMember;
    [afNetworkWrapper startAsynchronousPost];
}

-(void)getGroupDetail:(NSMutableDictionary *)params{
    NSURL *url = [NSURL URLWithString:[[NSString stringWithFormat:@"%@%@/%@", kAPIUrl,kCreateGroup,[params valueForKey:@"groupID"]] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    [params removeObjectForKey:@"groupID"];
    NSString *strURl = [NSString stringWithFormat:@"%@",url];
    
    AFNetworkingWrapper *afNetworkWrapper = [[AFNetworkingWrapper alloc] initWithURL:strURl andPostParams:params];
    afNetworkWrapper.delegate=self;
    afNetworkWrapper.tagService= GetGroupDetail;
    [afNetworkWrapper startAsynchronous];
}

-(void)accountDelete:(NSMutableDictionary *)params{
    NSURL *url = [NSURL URLWithString:[[NSString stringWithFormat:@"%@%@", kAPIUrl,KLogin] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    NSString *strURl = [NSString stringWithFormat:@"%@",url];
    
    AFNetworkingWrapper *afNetworkWrapper = [[AFNetworkingWrapper alloc] initWithURL:strURl andPostParams:params];
    afNetworkWrapper.delegate=self;
    afNetworkWrapper.tagService= DeleteAccount;
    [afNetworkWrapper startAsynchronousPost];
}

-(void)messageSeen:(NSMutableDictionary *)params{
    NSURL *url = [NSURL URLWithString:[[NSString stringWithFormat:@"%@%@", kAPIUrl,KMessageSeen] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    NSString *strURl = [NSString stringWithFormat:@"%@",url];
    
    AFNetworkingWrapper *afNetworkWrapper = [[AFNetworkingWrapper alloc] initWithURL:strURl andPostParams:params];
    afNetworkWrapper.delegate=self;
    afNetworkWrapper.tagService= MessageSeen;
    [afNetworkWrapper startAsynchronousPost];
}


-(void)messageRecieved:(NSMutableDictionary *)params{
    NSURL *url = [NSURL URLWithString:[[NSString stringWithFormat:@"%@%@", kAPIUrl,KMessageSeen] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    NSString *strURl = [NSString stringWithFormat:@"%@",url];
    
    AFNetworkingWrapper *afNetworkWrapper = [[AFNetworkingWrapper alloc] initWithURL:strURl andPostParams:params];
    afNetworkWrapper.delegate=self;
    afNetworkWrapper.tagService= MessageRecieved;
    [afNetworkWrapper startAsynchronousPost];
}

#pragma mark - AFNetworkingWrapper delegate methods

//asynchronous request finished
- (void)requestFinished:(NSString *)response tag:(int)tag
{
    if (isDebug) {
        NSLog(@"%d",tag);
    }
    CategoryParser *authenticationParser = [[CategoryParser alloc] init];
    
    if ([authenticationParser parseAuthenticationData:response authType:tag])
    {
        
      
        
        
        [self.delegate successWithData:authenticationParser.arrItems msg:authenticationParser.strMessage tag:tag];
    }
    else
    {
        SBJSON *json = [SBJSON new];
        NSMutableDictionary *responseArrayn = [json objectWithString:response];
        if (responseArrayn != nil) {
            NSMutableDictionary *responseDictionary = [[NSMutableDictionary alloc]init];
            responseDictionary = [responseArrayn objectForKey:@"data"];

            NSString *strStatus =  [responseArrayn valueForKey:@"status"];
            if ([strStatus isEqualToString:@"fail"]) {
                NSArray *aray = [[NSArray alloc]init];
                aray = [responseDictionary allValues];
                authenticationParser.strMessage = @"";
                
                for (int i= 0 ; i<aray.count; i++) {
                    if ([authenticationParser.strMessage isEqualToString:@""]) {
                        authenticationParser.strMessage = [NSString stringWithFormat:@"%@",[aray objectAtIndex:i]];
                    }else{
                        authenticationParser.strMessage = [NSString stringWithFormat:@"%@\r%@",authenticationParser.strMessage,[aray objectAtIndex:i]];
                    }
                }
                
                [self.delegate requestFailure:authenticationParser.strMessage tag:tag];
                
            }
        }else{
            authenticationParser.strMessage= @"";
            
            [self.delegate requestFailure:authenticationParser.strMessage tag:tag];
        }
       
    }
}

//asynchronous request failed
- (void)requestFailed:(NSString *)response tag:(int)tag
{
    if (![response isEqualToString:@"fail"]) {
        SBJSON *json = [SBJSON new];
        NSMutableDictionary *responseArrayn = [json objectWithString:response];
        NSLog(@"%@",responseArrayn);
        if (responseArrayn == nil) {
            if (response == nil) {
                [self.delegate requestFailure:@"fail" tag:tag];
            }else{
                [self.delegate requestFailure:response tag:tag];
            }
        }else{
            NSMutableArray *arrError = [[NSMutableArray alloc]init];
            if ([responseArrayn valueForKey:@"errors"] != nil) {
                arrError = [responseArrayn valueForKey:@"errors"];
                NSString *strError = @"" ;
                for (int i = 0 ; i < arrError.count ; i++) {
                    strError = [NSString stringWithFormat:@"%@%@\r",strError,[[arrError objectAtIndex:i]valueForKey:@"message"]];
                }
                [self.delegate requestFailure:strError tag:tag];
            }
        }
    }else{
        [self.delegate requestFailure:@"fail" tag:tag];

    }
    
}
@end

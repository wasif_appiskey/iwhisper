//
//  BLLUser.m
//  The Club App
//
//  Created by Wasif Iqbal on 8/10/15.
//  Copyright (c) 2015 Create Media Group. All rights reserved.
//

#import "BLLUser.h"

static BLLUser *manager = NULL;

@implementation BLLUser

+(BLLUser*)manager {
    
    @synchronized (self) {
        if (manager == NULL) {
            manager = [[self alloc] init];
            
            //TODO: Should be removed from final release
            //			NSURL *storeUrl = [NSURL fileURLWithPath: [[manager applicationDocumentsDirectory] stringByAppendingPathComponent:kDBName]];
            //			[[NSFileManager defaultManager] removeItemAtURL:storeUrl error:nil];
        }
    }
    
    return manager;
}

-(BOOL)isUserLoggedIn
{
    User *userObj = (User*)[DatabaseHelper getScalarData:@"User"
                                                  offset:0
                                               predicate:[NSPredicate predicateWithFormat:@"isLoggedIn == 1"] sortDescriptor:nil];
    if (userObj) {
        
        return YES;
        
    }else
    {
        return NO;
    }
}

-(User *)getLoggedInUser
{
    User *userObj = (User*)[DatabaseHelper getScalarData:@"User"
                                                  offset:0
                                               predicate:[NSPredicate predicateWithFormat:@"isLoggedIn == 1"] sortDescriptor:nil];
    return userObj;
}




#pragma mark - AFNetworkingWrapper delegate methods

//asynchronous request finished
- (void)requestFinished:(NSString *)response tag:(int)tag
{
        NSLog(@"=======================");
    
}

//asynchronous request failed
- (void)requestFailed:(int)tag
{
}

-(void)LogOutUser
{
    
    [DatabaseHelper deleteAllObjectsOfEntity:@"User" predicate:nil];
    [DatabaseHelper deleteAllObjectsOfEntity:@"Chat" predicate:nil];
    [DatabaseHelper deleteAllObjectsOfEntity:@"Contact" predicate:nil];
    [DatabaseHelper deleteAllObjectsOfEntity:@"Message" predicate:nil];
    [DatabaseHelper commitChanges];
    
}


@end

    //
//  BaseBLL.h
//  danone
//
//  Created by Wasif Iqbal on 3/20/15.
//  Copyright (c) 2015 Amani Systems. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworkingWrapper.h"
#import "JSONKit.h"

// Protocol for the importer to communicate with its delegate.
@protocol BaseBLLDelegate <NSObject>

//when success code returned from server
- (void)successWithData:(id)data msg:(NSString *)successMsg tag:(int)tag;

//when failure returned from server
- (void)requestFailure:(NSString *)failureMsg tag:(int)tag;

//for showing progress of upload items.
-(void)uploadProgress:(float)progress;

@end

@interface BaseBLL : NSObject <BaseBLLDelegate, AFNetworkingWrapperDelegate>
{
    __weak id <BaseBLLDelegate> delegate;
    
    int requestTag;
    
    //    ASIHTTPWrapper *baseRequest;
}

@property (nonatomic, readwrite) int requestTag;
@property (nonatomic, weak) id <BaseBLLDelegate> delegate;


-(void)cancelRequest;


@end

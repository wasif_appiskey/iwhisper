//
//  BaseBLL.m
//  danone
//
//  Created by Wasif Iqbal on 3/20/15.
//  Copyright (c) 2015 Amani Systems. All rights reserved.
//

#import "BaseBLL.h"

@implementation BaseBLL

@synthesize delegate, requestTag;


-(void)cancelRequest
{
    //[baseRequest cancelRequest];
}


- (void)successWithData:(id)data msg:(NSString *)successMsg tag:(int)tag{
    
}

//when failure returned from server
- (void)requestFailure:(NSString *)failureMsg tag:(int)tag{
    
}

//for showing progress of upload items.
-(void)uploadProgress:(float)progress{
    
}


-(void)requestFailed:(NSString *)response tag:(int)tag{
    
}


-(void)requestFinished:(NSString *)response tag:(int)tag{
    
}

@end

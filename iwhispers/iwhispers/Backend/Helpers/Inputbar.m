//
//  Inputbar.h
//  Whatsapp
//
//  Created by Rafael Castro on 7/11/15.
//  Copyright (c) 2015 HummingBird. All rights reserved.
//

#import "Inputbar.h"

@interface Inputbar() <HPGrowingTextViewDelegate,POVoiceHUDDelegate>
@property (nonatomic, strong) UIButton *rightButton;
@property (nonatomic, strong) UIButton *rightButton1;
@property (nonatomic, strong) UIButton *leftButton;

@property (nonatomic, strong) UIButton *voiceCancel;
@property (nonatomic, strong) UIButton *voiceSend;
@property (nonatomic, retain) UILabel *lblVoice;



@property (nonatomic, strong) UIImageView *imgSeperator;
@property (nonatomic, strong) UIView *micView;

@end

#define RIGHT_BUTTON_SIZE 30
#define SEND_BUTTON_SIZE 50


#define VOICE_BUTTON_SIZE 70
#define VOICE_UILABEL_SIZE 150


#define SEPERATOR_SIZE 1

#define LEFT_BUTTON_SIZE 45

@implementation Inputbar

-(id)init
{
    self = [super init];
    if (self)
    {
        [self addContent];
    }
    return self;
}
-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    {
        [self addContent];
    }
    return self;
}
- (id)initWithCoder:(NSCoder *)aDecoder
{
    if ((self = [super initWithCoder:aDecoder]))
    {
        [self addContent];
    }
    return self;
}
-(void)addContent
{
    self.backgroundColor = [UIColor colorWithRed:235.0/255.0 green:235.0/255.0 blue:235.0/255.0 alpha:1.0];
    [self addTextView];
    [self addRightButton];
    [self addRightButton1];
    [self addImageSeperator];
    [self addLeftButton];
    [self addSendButton];
    [self addMicView];
    self.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
}

-(void)addMicView{
    
    _micView = [[UIView alloc]initWithFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width, 0, [[UIScreen mainScreen] bounds].size.width, self.frame.size.height)];
    [_micView setBackgroundColor:[UIColor redColor]];
    
    CGSize size = self.frame.size;

    _voiceSend = [[UIButton alloc]initWithFrame: CGRectMake([[UIScreen mainScreen] bounds].size.width - VOICE_BUTTON_SIZE -5, 5, VOICE_BUTTON_SIZE, size.height-10)];
                                                            
    [_voiceSend setTitle:@"Send" forState:UIControlStateNormal];
    _voiceSend.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin;
    [_voiceSend addTarget:self action:@selector(didPressVoiceSend:) forControlEvents:UIControlEventTouchUpInside];
    [_voiceSend setBackgroundColor:[UIColor colorWithRed:0.0/255.0 green:115.0/255.0 blue:181.0/255.0 alpha:1.0]];
    _voiceSend.layer.cornerRadius = 5;
    _voiceSend.layer.masksToBounds = YES;
    [_voiceSend.titleLabel setFont:[UIFont fontWithName:@"OpenSans" size:12.0]];
    
    _voiceCancel = [[UIButton alloc]initWithFrame: CGRectMake([[UIScreen mainScreen] bounds].size.width - VOICE_BUTTON_SIZE - VOICE_BUTTON_SIZE - 10, 5, VOICE_BUTTON_SIZE, size.height-10)];
    [_voiceCancel setTitle:@"Cancel" forState:UIControlStateNormal];
    [_voiceCancel addTarget:self action:@selector(didPressVoiceCancel:) forControlEvents:UIControlEventTouchUpInside];
    [_voiceCancel setBackgroundColor:[UIColor colorWithRed:0.0/255.0 green:115.0/255.0 blue:181.0/255.0 alpha:1.0]];
    [_voiceCancel.titleLabel setFont:[UIFont fontWithName:@"OpenSans" size:12.0]];


    _voiceCancel.layer.cornerRadius = 5;
    _voiceCancel.layer.masksToBounds = YES;

    _voiceCancel.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin;
    
    _lblVoice = [[UILabel alloc]initWithFrame: CGRectMake(15, 0, VOICE_UILABEL_SIZE, size.height)];
    [_lblVoice setText:@"Recording"];
    
    _lblVoice.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin;
    
    self.voiceHud = [[POVoiceHUD alloc] initWithParentView:_lblVoice];
    
    [self.voiceHud setDelegate:self];
    
    
    [_micView addSubview:_voiceSend];
    [_micView addSubview:_voiceCancel];
    [_micView addSubview:_lblVoice];
    [_micView addSubview:self.voiceHud];
    [self addSubview:_micView];
    [_lblVoice setFont:[UIFont fontWithName:@"OpenSans" size:20.0]];
    [_lblVoice setTextColor:[UIColor redColor]];

    [_micView setBackgroundColor:[UIColor whiteColor]];
    
}

-(void)addTextView
{
    CGSize size = self.frame.size;
    _textView = [[HPGrowingTextView alloc] initWithFrame:CGRectMake(LEFT_BUTTON_SIZE,
                                                                    5,
                                                                    size.width - LEFT_BUTTON_SIZE - RIGHT_BUTTON_SIZE-RIGHT_BUTTON_SIZE-SEPERATOR_SIZE-10,
                                                                    size.height)];
    _textView.isScrollable = NO;
    _textView.contentInset = UIEdgeInsetsMake(0, 5, 0, 5);
    
    _textView.minNumberOfLines = 1;
    _textView.maxNumberOfLines = 6;
    // you can also set the maximum height in points with maxHeight
    // textView.maxHeight = 200.0f;
    _textView.returnKeyType = UIReturnKeyGo; //just as an example
    _textView.font = [UIFont fontWithName:@"OpenSans" size:13.0];
    _textView.delegate = self;
    _textView.internalTextView.scrollIndicatorInsets = UIEdgeInsetsMake(5, 0, 5, 0);
    _textView.backgroundColor = [UIColor whiteColor];
    _textView.placeholder = _placeholder;
    
    //textView.autocapitalizationType = UITextAutocapitalizationTypeSentences;
    _textView.keyboardType = UIKeyboardTypeDefault;
    _textView.returnKeyType = UIReturnKeyDefault;
    _textView.enablesReturnKeyAutomatically = YES;
    //textView.scrollIndicatorInsets = UIEdgeInsetsMake(0.0, -1.0, 0.0, 1.0);
    //textView.textContainerInset = UIEdgeInsetsMake(8.0, 4.0, 8.0, 0.0);
    _textView.layer.cornerRadius = 5.0;
    _textView.layer.borderWidth = 0.5;
    _textView.layer.borderColor =  [UIColor colorWithRed:200.0/255.0 green:200.0/255.0 blue:205.0/255.0 alpha:1.0].CGColor;
    
    _textView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
    // view hierachy
    [self addSubview:_textView];
}
-(void)addRightButton
{
    CGSize size = self.frame.size;
    self.rightButton = [[UIButton alloc] init];
    self.rightButton.frame = CGRectMake(size.width - RIGHT_BUTTON_SIZE, 0, RIGHT_BUTTON_SIZE, size.height);
    self.rightButton.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin;
    [self.rightButton setImage:self.rightButtonImage forState:UIControlStateNormal];
    
    [self.rightButton addTarget:self action:@selector(didPressRightButton:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.rightButton];
    
    [self.rightButton setSelected:YES];
}

-(void)addRightButton1
{
    CGSize size = self.frame.size;
    self.rightButton1 = [[UIButton alloc] init];
    self.rightButton1.frame = CGRectMake(size.width - RIGHT_BUTTON_SIZE - RIGHT_BUTTON_SIZE - 4, 0, RIGHT_BUTTON_SIZE, size.height);
    self.rightButton1.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin;
    [self.rightButton1 setImage:self.rightButtonImage1 forState:UIControlStateNormal];
    
    [self.rightButton1 addTarget:self action:@selector(didPressRightButton1:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.rightButton1];

    [self.rightButton1 setSelected:YES];
}

-(void)addSendButton
{
    CGSize size = self.frame.size;
    self.sendButton = [[UIButton alloc] init];
    self.sendButton.frame = CGRectMake(size.width - RIGHT_BUTTON_SIZE - RIGHT_BUTTON_SIZE - 4, 0, SEND_BUTTON_SIZE, size.height);
    self.sendButton.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin;
    [self.sendButton setTitle:@"Send" forState:UIControlStateNormal];
    [self.sendButton setFont:[UIFont fontWithName:@"OpenSans-Semibold" size:16]];
    [self.sendButton setTitleColor:[UIColor colorWithRed:4.0/255.0 green:151.0/255.0 blue:255.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    [self.sendButton addTarget:self action:@selector(didPressSendButton:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.sendButton];
    
    [self.sendButton setSelected:YES];
    [self.sendButton setHidden:YES];

}

-(void)addImageSeperator
{
    CGSize size = self.frame.size;
    self.imgSeperator = [[UIImageView alloc] init];
    self.imgSeperator.frame = CGRectMake(size.width - SEPERATOR_SIZE - RIGHT_BUTTON_SIZE, 10, SEPERATOR_SIZE, size.height -20);
    self.imgSeperator.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin;

    [self.imgSeperator setBackgroundColor:[UIColor lightGrayColor]];
    [self addSubview:self.imgSeperator];
}

-(void)addLeftButton
{
    CGSize size = self.frame.size;
    self.leftButton = [[UIButton alloc] init];
    self.leftButton.frame = CGRectMake(0, 0, LEFT_BUTTON_SIZE, size.height);
    self.leftButton.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleRightMargin;
    [self.leftButton setImage:self.leftButtonImage forState:UIControlStateNormal];
    
    [self.leftButton addTarget:self action:@selector(didPressLeftButton:) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:self.leftButton];
}
-(void)resignFirstResponder
{
    [_textView resignFirstResponder];
}
-(NSString *)text
{
    return _textView.text;
}


#pragma mark - Delegate

-(void)didPressRightButton:(UIButton *)sender
{
  //  if (self.rightButton.isSelected) return;
    [UIView animateWithDuration:0.5 animations:^{
        self.voiceHud.hidden = YES;
        [_micView setFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    }];
    
    

    [self.delegate inputbarDidPressRightButton:self];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask, YES);

    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.aac",self.textView.text]];
    [self.voiceHud startForFilePath:path];

    //self.textView.text = @"";
}


-(void)didPressVoiceCancel:(UIButton *)sender
{
    [UIView animateWithDuration:0.5 animations:^{
        self.voiceHud.hidden = YES;
        [_micView setFrame:CGRectMake(self.frame.size.width, 0, self.frame.size.width, self.frame.size.height)];
    }];
    
    [self.voiceHud cancelRecording];
    
    [self.delegate inputbarDidPressVoiceCancel:self];
    self.textView.text = @"";
}

-(void)didPressVoiceSend:(UIButton *)sender
{
    [UIView animateWithDuration:0.5 animations:^{
        self.voiceHud.hidden = YES;
        [_micView setFrame:CGRectMake(self.frame.size.width, 0, self.frame.size.width, self.frame.size.height)];
    }];
    
    [self.voiceHud commitRecording];
    
    
    [self.delegate inputbarDidPressVoiceSend:self];
   // self.textView.text = @"";
}


#pragma mark - POVoiceHUD Delegate

- (void)POVoiceHUD:(POVoiceHUD *)voiceHUD voiceRecorded:(NSString *)recordPath length:(float)recordLength {
    @try {
        NSLog(@"Sound recorded with file %@ for %.2f seconds", [recordPath lastPathComponent], recordLength);
    } @catch (NSException *exception) {
        NSLog(@"%@",exception);
    }
}

- (void)voiceRecordCancelledByUser:(POVoiceHUD *)voiceHUD {
    NSLog(@"Voice recording cancelled for HUD: %@", voiceHUD);
}

- (void)voiceRecordTime:(NSString *)recordTime{
    [_lblVoice setText:recordTime];
}

-(void)didPressRightButton1:(UIButton *)sender
{
    //  if (self.rightButton.isSelected) return;
    
    [self.delegate inputbarDidPressRight1Button:self];
    self.textView.text = @"";
}
-(void)didPressLeftButton:(UIButton *)sender
{
    [self.delegate inputbarDidPressLeftButton:self];
}

-(void)didPressSendButton:(UIButton *)sender
{
    [self.delegate inputbarDidPressSendButton:self];
}
#pragma mark - Set Methods

-(void)setPlaceholder:(NSString *)placeholder
{
    _placeholder = placeholder;
    _textView.placeholder = placeholder;
}
-(void)setLeftButtonImage:(UIImage *)leftButtonImage
{
    [self.leftButton setImage:leftButtonImage forState:UIControlStateNormal];
}
-(void)setRightButtonImage:(UIImage *)rightButtonImage
{
    [self.rightButton setImage:rightButtonImage forState:UIControlStateNormal];
}


-(void)setRightButtonImage1:(UIImage *)rightButtonImage1{
    [self.rightButton1 setImage:rightButtonImage1 forState:UIControlStateNormal];
}


#pragma mark - TextViewDelegate

-(void)growingTextView:(HPGrowingTextView *)growingTextView willChangeHeight:(float)height
{
    float diff = (growingTextView.frame.size.height - height);
    
    CGRect r = self.frame;
    r.size.height -= diff;
    r.origin.y += diff;
    self.frame = r;
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(inputbarDidChangeHeight:)])
    {
        [self.delegate inputbarDidChangeHeight:self.frame.size.height];
    }
}
-(void)growingTextViewDidBeginEditing:(HPGrowingTextView *)growingTextView
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(inputbarDidBecomeFirstResponder:)])
    {
        [self.delegate inputbarDidBecomeFirstResponder:self];
    }
}
- (void)growingTextViewDidChange:(HPGrowingTextView *)growingTextView
{
    NSString *text = [growingTextView.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    if ([text isEqualToString:@""]){
        [self.rightButton1 setHidden:NO];
        [self.rightButton setHidden:NO];
        [self.imgSeperator setHidden:NO];
        [self.sendButton setHidden:YES];

    }else{
        [self.rightButton1 setHidden:YES];
        [self.rightButton setHidden:YES];
        [self.imgSeperator setHidden:YES];
        [self.sendButton setHidden:NO];
        if (self.delegate && [self.delegate respondsToSelector:@selector(inputbarDidTextChange:)])
        {
            [self.delegate inputbarDidTextChange:self];
        }
    }
    

}


@end

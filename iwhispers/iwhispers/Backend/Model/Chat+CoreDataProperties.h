//
//  Chat+CoreDataProperties.h
//  
//
//  Created by Wasif Iqbal on 1/12/17.
//
//

#import "Chat.h"


NS_ASSUME_NONNULL_BEGIN

@interface Chat (CoreDataProperties)

+ (NSFetchRequest<Chat *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *chatType;
@property (nullable, nonatomic, copy) NSNumber *contactID;
@property (nullable, nonatomic, copy) NSString *messageID;
@property (nullable, nonatomic, copy) NSString *name;
@property (nullable, nonatomic, copy) NSNumber *numberOfUnreadMessages;
@property (nullable, nonatomic, copy) NSString *typing;

@end

NS_ASSUME_NONNULL_END

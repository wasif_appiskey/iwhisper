//
//  Message+CoreDataProperties.m
//  
//
//  Created by Wasif Iqbal on 5/18/17.
//
//

#import "Message+CoreDataProperties.h"

@implementation Message (CoreDataProperties)

+ (NSFetchRequest<Message *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Message"];
}

@dynamic aesEncrypted;
@dynamic chat_id;
@dynamic date;
@dynamic file_link;
@dynamic heigh;
@dynamic identifier;
@dynamic message_type;
@dynamic sender;
@dynamic senderID;
@dynamic sha;
@dynamic status;
@dynamic strTag;
@dynamic text;
@dynamic message;

@end

//
//  GroupMessageSeen+CoreDataProperties.m
//  
//
//  Created by Apple on 12/5/16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "GroupMessageSeen+CoreDataProperties.h"

@implementation GroupMessageSeen (CoreDataProperties)

@dynamic chat_id;
@dynamic message_id;
@dynamic status;
@dynamic user_id;

@end

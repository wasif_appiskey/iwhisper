//
//  Contact+CoreDataProperties.h
//  
//
//  Created by Wasif Iqbal on 12/27/16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Contact.h"

NS_ASSUME_NONNULL_BEGIN

@interface Contact (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *active;
@property (nullable, nonatomic, retain) NSString *identifier;
@property (nullable, nonatomic, retain) NSString *image_id;
@property (nullable, nonatomic, retain) NSString *last_seen;
@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSString *phone;
@property (nullable, nonatomic, retain) NSString *publicKey;
@property (nullable, nonatomic, retain) NSString *serverName;

@end

NS_ASSUME_NONNULL_END

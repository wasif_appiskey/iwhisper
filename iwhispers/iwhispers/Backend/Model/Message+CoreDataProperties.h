//
//  Message+CoreDataProperties.h
//  
//
//  Created by Wasif Iqbal on 5/18/17.
//
//

#import "Message.h"


NS_ASSUME_NONNULL_BEGIN

@interface Message (CoreDataProperties)

+ (NSFetchRequest<Message *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *aesEncrypted;
@property (nullable, nonatomic, copy) NSString *chat_id;
@property (nullable, nonatomic, copy) NSDate *date;
@property (nullable, nonatomic, copy) NSString *file_link;
@property (nullable, nonatomic, copy) NSNumber *heigh;
@property (nullable, nonatomic, copy) NSString *identifier;
@property (nullable, nonatomic, copy) NSNumber *message_type;
@property (nullable, nonatomic, copy) NSNumber *sender;
@property (nullable, nonatomic, copy) NSNumber *senderID;
@property (nullable, nonatomic, copy) NSString *sha;
@property (nullable, nonatomic, copy) NSNumber *status;
@property (nullable, nonatomic, copy) NSString *strTag;
@property (nullable, nonatomic, copy) NSString *text;
@property (nullable, nonatomic, copy) NSString *message;

@end

NS_ASSUME_NONNULL_END

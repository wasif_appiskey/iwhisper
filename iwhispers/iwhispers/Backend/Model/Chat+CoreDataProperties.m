//
//  Chat+CoreDataProperties.m
//  
//
//  Created by Wasif Iqbal on 1/12/17.
//
//

#import "Chat+CoreDataProperties.h"

@implementation Chat (CoreDataProperties)

+ (NSFetchRequest<Chat *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Chat"];
}

@dynamic chatType;
@dynamic contactID;
@dynamic messageID;
@dynamic name;
@dynamic numberOfUnreadMessages;
@dynamic typing;

@end

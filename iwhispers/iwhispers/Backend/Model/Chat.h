//
//  Chat.h
//  iwhispers
//
//  Created by Apple on 7/12/16.
//  Copyright © 2016 Appiskey. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Message.h"
#import "Contact.h"


NS_ASSUME_NONNULL_BEGIN

//
// This class is responsable to store information
// displayed in ChatController
//

@interface Chat : NSManagedObject
@property (strong, nonatomic) NSString *messageID;
@property (strong, nonatomic) NSNumber *contactID;
@property (assign, nonatomic) NSNumber *numberOfUnreadMessages;
-(NSString *)identifier;
-(void)save;
@end

NS_ASSUME_NONNULL_END

#import "Chat+CoreDataProperties.h"
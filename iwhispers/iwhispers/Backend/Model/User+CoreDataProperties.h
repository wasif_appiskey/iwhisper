//
//  User+CoreDataProperties.h
//  
//
//  Created by Wasif Iqbal on 1/3/17.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "User.h"

NS_ASSUME_NONNULL_BEGIN

@interface User (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *countryCode;
@property (nullable, nonatomic, retain) NSString *fcm;
@property (nullable, nonatomic, retain) NSNumber *isLoggedIn;
@property (nullable, nonatomic, retain) NSNumber *isSilent;
@property (nullable, nonatomic, retain) NSNumber *countryNumber;
@property (nullable, nonatomic, retain) NSNumber *isVerified;
@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSString *phone;
@property (nullable, nonatomic, retain) NSString *publicKey;
@property (nullable, nonatomic, retain) NSNumber *saveMedia;
@property (nullable, nonatomic, retain) NSString *token;
@property (nullable, nonatomic, retain) NSNumber *userID;
@property (nullable, nonatomic, retain) NSString *userImage;
@property (nullable, nonatomic, retain) NSString *countryName;

@end

NS_ASSUME_NONNULL_END

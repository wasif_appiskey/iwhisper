//
//  User+CoreDataProperties.m
//  
//
//  Created by Wasif Iqbal on 1/3/17.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "User+CoreDataProperties.h"

@implementation User (CoreDataProperties)

@dynamic countryCode;
@dynamic fcm;
@dynamic isLoggedIn;
@dynamic isSilent;
@dynamic isVerified;
@dynamic name;
@dynamic phone;
@dynamic publicKey;
@dynamic saveMedia;
@dynamic token;
@dynamic userID;
@dynamic userImage;
@dynamic countryName;
@dynamic countryNumber;

@end

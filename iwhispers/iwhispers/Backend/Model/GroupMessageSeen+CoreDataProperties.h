//
//  GroupMessageSeen+CoreDataProperties.h
//  
//
//  Created by Apple on 12/5/16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "GroupMessageSeen.h"

NS_ASSUME_NONNULL_BEGIN

@interface GroupMessageSeen (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *chat_id;
@property (nullable, nonatomic, retain) NSString *message_id;
@property (nullable, nonatomic, retain) NSNumber *status;
@property (nullable, nonatomic, retain) NSString *user_id;

@end

NS_ASSUME_NONNULL_END

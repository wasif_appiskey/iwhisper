//
//  Group+CoreDataProperties.m
//  
//
//  Created by Apple on 8/23/16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Group+CoreDataProperties.h"

@implementation Group (CoreDataProperties)

@dynamic identifier;
@dynamic name;
@dynamic image;

@end

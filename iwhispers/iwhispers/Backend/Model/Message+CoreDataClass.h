//
//  Message+CoreDataClass.h
//  
//
//  Created by Wasif Iqbal on 5/18/17.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Message : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Message+CoreDataProperties.h"

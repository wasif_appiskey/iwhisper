//
//  GroupMember+CoreDataProperties.h
//  
//
//  Created by Apple on 9/21/16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "GroupMember.h"

NS_ASSUME_NONNULL_BEGIN

@interface GroupMember (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *contactID;
@property (nullable, nonatomic, retain) NSNumber *groupID;
@property (nullable, nonatomic, retain) NSString *isAdmin;

@end

NS_ASSUME_NONNULL_END

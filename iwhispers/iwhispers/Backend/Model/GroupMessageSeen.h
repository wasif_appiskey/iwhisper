//
//  GroupMessageSeen.h
//  
//
//  Created by Apple on 12/5/16.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface GroupMessageSeen : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "GroupMessageSeen+CoreDataProperties.h"

//
//  Contact+CoreDataProperties.m
//  
//
//  Created by Wasif Iqbal on 12/27/16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Contact+CoreDataProperties.h"

@implementation Contact (CoreDataProperties)

@dynamic active;
@dynamic identifier;
@dynamic image_id;
@dynamic last_seen;
@dynamic name;
@dynamic phone;
@dynamic publicKey;
@dynamic serverName;

@end

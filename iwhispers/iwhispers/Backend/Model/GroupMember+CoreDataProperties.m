//
//  GroupMember+CoreDataProperties.m
//  
//
//  Created by Apple on 9/21/16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "GroupMember+CoreDataProperties.h"

@implementation GroupMember (CoreDataProperties)

@dynamic contactID;
@dynamic groupID;
@dynamic isAdmin;

@end

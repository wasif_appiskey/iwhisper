//
//  Message.h
//  Whatsapp
//
//  Created by Rafael Castro on 6/16/15.
//  Copyright (c) 2015 HummingBird. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>


NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, MessageStatus)
{
    MessageStatusSending,
    MessageStatusSent,
    MessageStatusReceived,
    MessageStatusRead,
    MessageStatusFailed
};

typedef NS_ENUM(NSInteger, MessageSender)
{
    MessageSenderMyself,
    MessageSenderSomeone
};

//
// This class is the message object itself
//
@interface Message : NSManagedObject

@property (assign, nonatomic) NSNumber *sender;
@property (assign, nonatomic) NSNumber *status;
@property (strong, nonatomic) NSString *identifier;
@property (strong, nonatomic) NSString *chat_id;
@property (strong, nonatomic) NSString *text;
@property (strong, nonatomic) NSDate *date;
@property (assign, nonatomic) NSNumber *heigh;
@property (strong, nonatomic) NSNumber *message_type;
@property (strong, nonatomic) NSString *file_link;
@property (strong, nonatomic) NSString *strTag;
@property (strong, nonatomic) NSString *aesEncrypted;
@property (strong, nonatomic) NSString *sha;
@property (strong, nonatomic) NSString *message;



@end

NS_ASSUME_NONNULL_END

#import "Message+CoreDataProperties.h"


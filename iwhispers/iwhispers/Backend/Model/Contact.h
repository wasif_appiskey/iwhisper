//
//  Contact.h
//  Whatsapp
//
//  Created by Magneto on 2/12/16.
//  Copyright © 2016 HummingBird. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Contact : NSManagedObject
@property (nonatomic, strong) NSString *identifier;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *image_id;
@property (nonatomic, retain) NSString *phone;
@property (nonatomic, retain) NSString *publicKey;

@end

NS_ASSUME_NONNULL_END

#import "Contact+CoreDataProperties.h"
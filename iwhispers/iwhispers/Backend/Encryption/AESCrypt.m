//
//  AESCrypt.m
//  Gurpartap Singh
//
//  Created by Gurpartap Singh on 06/05/12.
//  Copyright (c) 2012 Gurpartap Singh
// 
// 	MIT License
// 
// 	Permission is hereby granted, free of charge, to any person obtaining
// 	a copy of this software and associated documentation files (the
// 	"Software"), to deal in the Software without restriction, including
// 	without limitation the rights to use, copy, modify, merge, publish,
// 	distribute, sublicense, and/or sell copies of the Software, and to
// 	permit persons to whom the Software is furnished to do so, subject to
// 	the following conditions:
// 
// 	The above copyright notice and this permission notice shall be
// 	included in all copies or substantial portions of the Software.
// 
// 	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// 	EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// 	MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// 	NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// 	LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// 	OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// 	WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

#import "AESCrypt.h"
#import "NSData+Base64.h"
#import "NSString+Base64.h"
#import "NSData+CommonCrypto.h"
#import "AES.h"
#import "RSA.h"

@implementation AESCrypt

+ (NSString *)encrypt:(NSString *)message password:(NSString *)password {
    
    NSData *encryptedData = [[message dataUsingEncoding:NSUTF8StringEncoding] AES256EncryptedDataUsingKey:[[password dataUsingEncoding:NSUTF8StringEncoding] SHA256Hash] error:nil];
    NSString *base64EncodedString = [NSString base64StringFromData:encryptedData length:[encryptedData length]];
    return base64EncodedString;
    
}


+ (NSData *)encryptData:(NSData *)message password:(NSString *)password {
    
    NSData *encryptedData = [message AES256EncryptedDataUsingKey:[[password dataUsingEncoding:NSUTF8StringEncoding] SHA256Hash] error:nil];
    return message;
    
}


+ (NSString *)decrypt:(NSString *)base64EncodedString password:(NSString *)password {
  NSData *encryptedData = [NSData base64DataFromString:base64EncodedString];
  NSData *decryptedData = [encryptedData decryptedAES256DataUsingKey:[[password dataUsingEncoding:NSUTF8StringEncoding] SHA256Hash] error:nil];
  return [[NSString alloc] initWithData:decryptedData encoding:NSUTF8StringEncoding];
}

+ (NSString *)generateSha:(NSString *)message {
    NSData *shaData = [[message dataUsingEncoding:NSUTF8StringEncoding] SHA1Hash];
    NSString *base64EncodedString = [NSString stringWithFormat:@"%@",shaData];
    base64EncodedString = [base64EncodedString stringByReplacingOccurrencesOfString:@" " withString:@""];
    base64EncodedString = [base64EncodedString stringByReplacingOccurrencesOfString:@"<" withString:@""];
    base64EncodedString = [base64EncodedString stringByReplacingOccurrencesOfString:@">" withString:@""];
    return base64EncodedString;
}



+ (NSData *)generateShaData:(NSData *)message {
    NSData *shaData = [message SHA1Hash];
    return shaData;
}



+(NSString *)encryptAESKey:(NSString *)aesKey publicKey:(NSString *)publicKey{
    
    return  [RSA encryptString:aesKey publicKey:publicKey];
}


+(NSString *)encryptKey:(NSString *)Key privateKey:(NSString *)privateKey{
    
    return  [RSA encryptString:Key privateKey:privateKey];
}


+(NSString *)decrptyAESKey:(NSString *)aesKey privateKey:(NSString *)privateKey{
    return [RSA decryptString:aesKey privateKey:privateKey];;
}

+(NSString *)generateAESRandomKey{
        unsigned char buf[16];
        arc4random_buf(buf, sizeof(buf));
        NSData *shaData =  [NSData dataWithBytes:buf length:sizeof(buf)];
        NSString *strAES = [NSString base64StringFromData:shaData length:[shaData length]];
        NSLog( @"length %ld", (unsigned long)[strAES length]);
        return strAES;
}

@end

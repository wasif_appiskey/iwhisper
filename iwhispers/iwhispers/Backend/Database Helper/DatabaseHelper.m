//
//  DatabaseHelper.m
//  QuizTrail
//
//  Created by Ammad on 8/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DatabaseHelper.h"
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@implementation DatabaseHelper

//@synthesize context;
//@synthesize arrRecords;

+(NSManagedObjectContext *)managedObjectContext
{
    id appDelegate = (id)[[UIApplication sharedApplication] delegate];
	return [appDelegate managedObjectContext];
}

+(NSManagedObjectModel *)managedObjectModel
{
    id appDelegate = (id)[[UIApplication sharedApplication] delegate];
    return [appDelegate managedObjectModel];
}

+ (NSManagedObject *)getObjectIfExist:(NSString *)entityName predicate:(NSPredicate *)predicate
{
	NSManagedObject *mo = nil;
    NSArray *dictionaryList =  [self getAllData:entityName offset:0 predicate:predicate sortDescriptor:nil];

    //if object doesnt exist then create a new one
    if (![dictionaryList count]) {
//        mo = (LanguageDictionary *)[NSEntityDescription insertNewObjectForEntityForName:entity inManagedObjectContext:[self managedObjectContext]];
        mo = [NSEntityDescription insertNewObjectForEntityForName:entityName inManagedObjectContext:[self managedObjectContext]];
    }
    else {
        mo = [dictionaryList objectAtIndex:0];
    }
    
    return mo;
}
+ (NSManagedObject *)getNewObject:(NSString *)entityName
{
    return [NSEntityDescription insertNewObjectForEntityForName:entityName inManagedObjectContext:[self managedObjectContext]];
}

+(NSManagedObject *)getScalarData:(NSString *)entityName offset:(NSInteger)offset predicate:(NSPredicate *)pred sortDescriptor:(NSSortDescriptor *)sDesc
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:[self managedObjectContext]];
    fetchRequest.entity = entity;
    fetchRequest.fetchOffset = offset;
    fetchRequest.fetchLimit = 10000;
    
    if (pred != nil) {
        fetchRequest.predicate = pred;
    }
    if (sDesc != nil) {
        fetchRequest.sortDescriptors = [NSArray arrayWithObject:sDesc];
    }
    
    NSError *error;
    NSArray *arrRecords = [[self managedObjectContext] executeFetchRequest:fetchRequest error:&error];
    
	
	if (arrRecords.count > 0)
	{
		return [arrRecords objectAtIndex:0];
	}
	else
	{
		return nil;
	}
}

//get objects of an Entity
+(NSArray *)getAllData:(NSString *)entityName offset:(NSInteger)offset predicate:(NSPredicate *)pred sortDescriptor:(NSSortDescriptor *)sDesc
{
	
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:[self managedObjectContext]];
	fetchRequest.entity = entity;
	fetchRequest.fetchOffset = offset;
	fetchRequest.fetchLimit = 10000;
	
	if (pred != nil) {
		fetchRequest.predicate = pred;
	}
	if (sDesc != nil) {
		fetchRequest.sortDescriptors = [NSArray arrayWithObject:sDesc];
	}
	
    NSError *error;
    NSArray *arrRecords = [[self managedObjectContext] executeFetchRequest:fetchRequest error:&error];
	
	return arrRecords;
}

//get count of objects of an Entity
+(NSInteger)getCountOfAllData:(NSString *)entityName predicate:(NSPredicate *)pred
{
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	[fetchRequest setEntity: [NSEntityDescription entityForName: entityName inManagedObjectContext:[self managedObjectContext]]];
	
	if (pred != nil)
		fetchRequest.predicate = pred;
	
	NSError *error = nil;
	NSUInteger count = [[self managedObjectContext] countForFetchRequest: fetchRequest error: &error];
		
	return count;
}

//to delete all entries in a core data table.
+(void)deleteAllObjectsOfEntity:(NSString *)entityName  predicate:(NSPredicate *)pred
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:[NSEntityDescription entityForName:entityName inManagedObjectContext:[self managedObjectContext]]];
    if (pred != nil)
        fetchRequest.predicate = pred;
    
    NSError *error;
    NSArray *items = [[self managedObjectContext] executeFetchRequest:fetchRequest error:&error];
	
	for (NSManagedObject *managedObject in items) 
	    [[self managedObjectContext] deleteObject:managedObject];
    
    [DatabaseHelper commitChanges];
	
}

+(void)deleteObject:(NSManagedObject *)object
{
	[[self managedObjectContext] deleteObject:object];
	
	[DatabaseHelper commitChanges];
}

+(void)commitChanges
{
	NSError *error;
    @try {
        if ([[self managedObjectContext] hasChanges] && ![[self managedObjectContext] save:&error])
        {
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            
        }
    } @catch (NSException *exception) {
        NSLog(@"%@",exception);
    }
}


+(void)migrateToiCloud{
    NSURL *documentsDirectory = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
    
    //This is the path to the new store. Note it has a different file name
    NSURL *storeURL = [documentsDirectory URLByAppendingPathComponent:@"TestRemote.sqlite"];
    
    //This is the path to the existing store
    NSURL *seedStoreURL = [documentsDirectory URLByAppendingPathComponent:@"SingleViewCoreData.sqlite"];
    
    //You should create a new store here instead of using the one you presumably already have access to
    NSPersistentStoreCoordinator *coord = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    
    NSError *seedStoreError;
    NSDictionary *seedStoreOptions = @{ NSReadOnlyPersistentStoreOption: @YES };
    NSPersistentStore *seedStore = [coord addPersistentStoreWithType:NSSQLiteStoreType
                                                       configuration:nil
                                                                 URL:seedStoreURL
                                                             options:seedStoreOptions
                                                               error:&seedStoreError];
    
    NSDictionary *iCloudOptions = @{ NSPersistentStoreUbiquitousContentNameKey: @"MyiCloudStore" };
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    //This is using an operation queue because this happens synchronously
    [queue addOperationWithBlock:^{
        NSError *blockError;
        [coord migratePersistentStore:seedStore
                                toURL:storeURL
                              options:iCloudOptions
                             withType:NSSQLiteStoreType
                                error:&blockError];
        
        NSOperationQueue *mainQueue = [NSOperationQueue mainQueue];
        [mainQueue addOperationWithBlock:^{
            // This will be called when the migration is done
        }];
    }];
}


@end

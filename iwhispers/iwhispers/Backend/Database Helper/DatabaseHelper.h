//
//  DatabaseHelper.h
//  QuizTrail
//
//  Created by Ammad on 8/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface DatabaseHelper : NSObject
{

}

+(NSManagedObjectContext *)managedObjectContext;

+(NSManagedObjectModel *)managedObjectModel;

+ (NSManagedObject *)getObjectIfExist:(NSString *)entityName predicate:(NSPredicate *)predicate;

+ (NSManagedObject *)getNewObject:(NSString *)entityName;

+(NSInteger)getCountOfAllData:(NSString *)entityName predicate:(NSPredicate *)pred;

+(NSArray *)getAllData:(NSString *)entityName offset:(NSInteger)offset predicate:(NSPredicate *)pred sortDescriptor:(NSSortDescriptor *)sDesc;

+(NSManagedObject *)getScalarData:(NSString *)entityName offset:(NSInteger)offset predicate:(NSPredicate *)pred sortDescriptor:(NSSortDescriptor *)sDesc;

+(void)deleteAllObjectsOfEntity:(NSString *)entityName  predicate:(NSPredicate *)pred;

+(void)deleteObject:(NSManagedObject *)object;

+(void)commitChanges;

+(void)migrateToiCloud;



@end

//
//  AuthenticationParser.m
//  danone
//
//  Created by Wasif Iqbal on 3/20/15.
//  Copyright (c) 2015 Amani Systems. All rights reserved.
//

#import "CategoryParser.h"
#import "JSON.h"
#import "AppConstants.h"


@implementation CategoryParser

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code
        self.arrItems = [[NSMutableArray alloc] init];
    }
    return self;
}

-(BOOL)parseAuthenticationData:(NSString *)response authType:(AuthenticationParserType)authType
{
    switch (authType)
    {
            
        case Login:
            return [self parseLogin:response];
            break;
        case Verify:
            return [self parseVerify:response];
            break;
        case SyncContact:
            return [self parseSyncContact:response];
            break;
        case SetProfile:
            return [self parseSetProfile:response];
            break;
        case GetProfile:
            return [self parseGetProfile:response];
            break;
        case SendChatMessage:
            return [self parseSendChatMessage:response];
            break;
        case SendChatImage:
            return [self parseSendChatMessage:response];
            break;
        case CreateGroup:
            return [self parseCreateGroup:response];
            break;
        case AddGroupMember:
            return [self parseAddGroupMember:response];
            break;
        case RemoveGroupMember:
            return [self parseRemoveGroupMember:response];
            break;
        case GetContactDetail:
            return [self parseGetContactDetail:response];
            break;
        case GetGroupDetail:
            return [self parseGetGroupDetail:response];
            break;
        case SendGroupMessage:
            return [self parseSendGroupMessage:response];
            break;
        case DeleteAccount:
            return [self parseDeleteAccount:response];
            break;
        case MessageSeen:
            return [self parseMessageSeen:response];
            break;
        case MessageRecieved:
            return [self parseMessageSeen:response];
            break;
        default:
            break;
    }
    
    return FALSE;
}



-(BOOL)parseMessageSeen:(NSString *)response
{
    @try {
        SBJSON *json = [SBJSON new];
        NSMutableDictionary *responseArrayn = [json objectWithString:response];
        NSString *strStatus = [responseArrayn objectForKey:@"status"];
        
        if (![strStatus isEqualToString:@"fail"]) {
            NSMutableDictionary *responseDictionary = [[NSMutableDictionary alloc]init];
            responseDictionary = [responseArrayn objectForKey:@"data"];
            [self.arrItems addObject:responseDictionary];

            return YES;
            
        }
        else
            return NO;
    }
    @catch (NSException *exception) {
        NSLog(@"parseMessageSeen exception   %@",exception);
    }
}


-(BOOL)parseDeleteAccount:(NSString *)response
{
    @try {
        SBJSON *json = [SBJSON new];
        NSMutableDictionary *responseArrayn = [json objectWithString:response];
        NSString *strStatus = [responseArrayn objectForKey:@"status"];
        
        if (![strStatus isEqualToString:@"fail"]) {
            
            return YES;
            
        }
        else
            return NO;
    }
    @catch (NSException *exception) {
        NSLog(@"parseDeleteAccount exception   %@",exception);
    }
}


-(BOOL)parseGetGroupDetail:(NSString *)response
{
    @try {
        SBJSON *json = [SBJSON new];
        NSMutableDictionary *responseArrayn = [json objectWithString:response];
        NSString *strStatus = [responseArrayn objectForKey:@"status"];
        
        if (![strStatus isEqualToString:@"fail"]) {
            NSMutableDictionary *responseDictionary = [[NSMutableDictionary alloc]init];
            responseDictionary = [responseArrayn objectForKey:@"data"];
            NSMutableDictionary *groupDic = [[NSMutableDictionary alloc]init];
            groupDic = (NSMutableDictionary*)[responseDictionary valueForKey:@"group"];
            NSMutableArray *arrContact = [[NSMutableArray alloc]init];
            arrContact = (NSMutableArray*) [groupDic valueForKey:@"users"];
            NSString *strGroupID = @"";
            
            NSString *strAdminID = @"";

            if ([groupDic valueForKey:@"admin_id"] != nil  && [groupDic valueForKey:@"admin_id"] != [NSNull null]) {
                strAdminID = [NSString stringWithFormat:@"%@",[groupDic valueForKey:@"admin_id"]];

            }
            
            
            if ([groupDic valueForKey:@"id"] != nil  && [groupDic valueForKey:@"id"] != [NSNull null]) {
                strGroupID = [NSString stringWithFormat:@"%@",[groupDic valueForKey:@"id"]];
                Group  *groupObj = (Group *)[DatabaseHelper getObjectIfExist:@"Group" predicate:[NSPredicate predicateWithFormat:@"identifier == %@",strGroupID ]];
                
                if ([groupDic valueForKey:@"name"] != nil  && [groupDic valueForKey:@"name"] != [NSNull null]) {
                    [groupObj setName:[groupDic valueForKey:@"name"]];
                }
                if ([groupDic valueForKey:@"image"] != nil  && [groupDic valueForKey:@"image"] != [NSNull null]) {
                    [groupObj setImage:[NSString stringWithFormat:@"%@%@",kGroupPictureURL,[groupDic valueForKey:@"image"]]];
                }
                [DatabaseHelper commitChanges];
            }
            [DatabaseHelper deleteAllObjectsOfEntity:@"GroupMember" predicate:[NSPredicate predicateWithFormat:@"groupID == %@", strGroupID]];

            for (int i = 0; i<arrContact.count; i++) {
                NSString *strContactID = @"";
                NSMutableDictionary *contactDic = [[NSMutableDictionary alloc]init];
                contactDic = (NSMutableDictionary*)[arrContact objectAtIndex:i];
                
                if ([contactDic valueForKey:@"id"] != nil) {
                    strContactID = [NSString stringWithFormat:@"%@",[contactDic valueForKey:@"id"]];
                }
                Contact  *contactObj = (Contact *)[DatabaseHelper getObjectIfExist:@"Contact" predicate:[NSPredicate predicateWithFormat:@"identifier == %@",strContactID ]];
               
                [contactObj setIdentifier:strContactID];
                if ([contactDic valueForKey:@"name"] != nil && [contactDic valueForKey:@"name"] != [NSNull null]) {
                    [contactObj setServerName:[contactDic valueForKey:@"name"]];
                }
                if ([contactDic valueForKey:@"public_key"] != nil && [contactDic valueForKey:@"public_key"] != [NSNull null]) {
                    [contactObj setPublicKey:[contactDic valueForKey:@"public_key"]];
                }
                if ([contactDic valueForKey:@"phone"] != nil && [contactDic valueForKey:@"phone"] != [NSNull null]) {
                    [contactObj setPhone:[contactDic valueForKey:@"phone"]];
                }
                if ([contactDic valueForKey:@"image"] != nil && [contactDic valueForKey:@"image"] != [NSNull null]) {
                    [contactObj setImage_id:[NSString stringWithFormat:@"%@%@",kProfilePictureURL,[contactDic valueForKey:@"image"]]];
                }
                if ([contactDic valueForKey:@"active"] != nil && [contactDic valueForKey:@"active"] != [NSNull null]) {
                    [contactObj setActive:[NSString stringWithFormat:@"%@",[contactDic valueForKey:@"active"]]];
                }
                [DatabaseHelper commitChanges];
                
                NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
                f.numberStyle = NSNumberFormatterDecimalStyle;
                NSNumber *contactID = [f numberFromString:strContactID];
                NSNumber *groupID = [f numberFromString:strGroupID];
                
                
                GroupMember  *memberObj = (GroupMember *)[DatabaseHelper getObjectIfExist:@"GroupMember" predicate:[NSPredicate predicateWithFormat:@"(contactID == %@) AND (groupID == %@) ",contactID ,groupID]];
                if ([strAdminID isEqualToString:strContactID]) {
                    [memberObj setIsAdmin:@"1"];
                }else{
                    [memberObj setIsAdmin:@"0"];
                }
                [memberObj setContactID:contactID];
                [memberObj setGroupID:groupID];
                [DatabaseHelper commitChanges];
            }
            return YES;
        }else{
            return NO;
        }
    }
    @catch (NSException *exception) {
        NSLog(@"parseGetGroupDetail exception   %@",exception);
    }
}



-(BOOL)parseGetContactDetail:(NSString *)response
{
    @try {
        SBJSON *json = [SBJSON new];
        NSMutableDictionary *responseArrayn = [json objectWithString:response];
        NSString *strStatus = [responseArrayn objectForKey:@"status"];
        
        if (![strStatus isEqualToString:@"fail"]) {
            NSMutableDictionary *responseDictionary = [[NSMutableDictionary alloc]init];
            responseDictionary = [responseArrayn objectForKey:@"data"];
                NSString *strContactID = @"";
                NSMutableDictionary *contactDic = [[NSMutableDictionary alloc]init];
                contactDic = (NSMutableDictionary*)[responseDictionary valueForKey:@"user"];
                
                if ([contactDic valueForKey:@"id"] != nil) {
                    strContactID = [NSString stringWithFormat:@"%@",[contactDic valueForKey:@"id"]];
                }
                Contact  *contactObj = (Contact *)[DatabaseHelper getObjectIfExist:@"Contact" predicate:[NSPredicate predicateWithFormat:@"identifier == %@",strContactID ]];
                [contactObj setIdentifier:strContactID];
                if ([contactDic valueForKey:@"name"] != nil && [contactDic valueForKey:@"name"] != [NSNull null]) {
                    [contactObj setName:[contactDic valueForKey:@"name"]];
                    [contactObj setServerName:[contactDic valueForKey:@"name"]];

                }
                if ([contactDic valueForKey:@"public_key"] != nil && [contactDic valueForKey:@"public_key"] != [NSNull null]) {
                    [contactObj setPublicKey:[contactDic valueForKey:@"public_key"]];
                }
                if ([contactDic valueForKey:@"phone"] != nil && [contactDic valueForKey:@"phone"] != [NSNull null]) {
                    [contactObj setPhone:[contactDic valueForKey:@"phone"]];
                }
                if ([contactDic valueForKey:@"image"] != nil && [contactDic valueForKey:@"image"] != [NSNull null]) {
                    [contactObj setImage_id:[NSString stringWithFormat:@"%@%@",kProfilePictureURL,[contactDic valueForKey:@"image"]]];
                }
            if ([contactDic valueForKey:@"active"] != nil && [contactDic valueForKey:@"active"] != [NSNull null]) {
                [contactObj setActive:[NSString stringWithFormat:@"%@",[contactDic valueForKey:@"active"]]];
            }
                [DatabaseHelper commitChanges];
                
            
            return YES;
            
        }
        else
            return NO;
    }
    @catch (NSException *exception) {
        NSLog(@"parseGetContactDetail exception   %@",exception);
    }
}


-(BOOL)parseRemoveGroupMember:(NSString *)response
{
    @try {
        SBJSON *json = [SBJSON new];
        NSMutableDictionary *responseArrayn = [json objectWithString:response];
        NSString *strStatus = [responseArrayn objectForKey:@"status"];
        
        if (![strStatus isEqualToString:@"fail"]) {
            
            return YES;
            
        }
        else
            return NO;
    }
    @catch (NSException *exception) {
        NSLog(@"parseRemoveGroupMember exception   %@",exception);
    }
}

-(BOOL)parseAddGroupMember:(NSString *)response
{
    @try {
        SBJSON *json = [SBJSON new];
        NSMutableDictionary *responseArrayn = [json objectWithString:response];
        NSString *strStatus = [responseArrayn objectForKey:@"status"];
        
        if (![strStatus isEqualToString:@"fail"]) {
            
            return YES;
            
        }
        else
            return NO;
    }
    @catch (NSException *exception) {
        NSLog(@"parseAddGroupMember exception   %@",exception);
    }
}

-(BOOL)parseCreateGroup:(NSString *)response
{
    @try {
        SBJSON *json = [SBJSON new];
        NSMutableDictionary *responseArrayn = [json objectWithString:response];
        NSString *strStatus = [responseArrayn objectForKey:@"status"];
        
        if (![strStatus isEqualToString:@"fail"]) {
            
            NSMutableDictionary *responseDictionary = [[NSMutableDictionary alloc]init];
            responseDictionary = [responseArrayn objectForKey:@"data"];
            responseDictionary = [[responseDictionary allValues]objectAtIndex:0];
            [self.arrItems addObject:responseDictionary];
            return YES;
            
        }
        else
            return NO;
    }
    @catch (NSException *exception) {
        NSLog(@"parseCreateGroup exception   %@",exception);
    }
}

-(BOOL)parseLogin:(NSString *)response
{
    @try {
        SBJSON *json = [SBJSON new];
        NSMutableDictionary *responseArrayn = [json objectWithString:response];
        NSString *strStatus = [responseArrayn objectForKey:@"status"];

        if (![strStatus isEqualToString:@"fail"]) {
            
                User *userObj = (User *)[DatabaseHelper getObjectIfExist:@"User" predicate:[NSPredicate predicateWithFormat:@"isLoggedIn == 1"]];
                [userObj setIsLoggedIn:[NSNumber numberWithInt:1]];
                [DatabaseHelper commitChanges];
                return YES;
        
        }
        else
            return NO;
    }
    @catch (NSException *exception) {
        NSLog(@"parseLogin exception   %@",exception);
    }
}

-(BOOL)parseSendChatMessage:(NSString *)response
{
    @try {
        SBJSON *json = [SBJSON new];
        NSMutableDictionary *responseArrayn = [json objectWithString:response];
        NSString *strStatus = [responseArrayn objectForKey:@"status"];
        
        if (![strStatus isEqualToString:@"fail"]) {
            NSMutableDictionary *responseDictionary = [[NSMutableDictionary alloc]init];
            responseDictionary = [responseArrayn objectForKey:@"data"];
            [self.arrItems addObject:responseDictionary];
            return YES;
            
        }
        else
            return NO;
    }
    @catch (NSException *exception) {
        NSLog(@"parseSendChatMessage exception   %@",exception);
    }
}
-(BOOL)parseSendGroupMessage:(NSString *)response
{
    @try {
        SBJSON *json = [SBJSON new];
        NSMutableDictionary *responseArrayn = [json objectWithString:response];
        NSString *strStatus = [responseArrayn objectForKey:@"status"];
        
        if (![strStatus isEqualToString:@"fail"]) {
            NSMutableDictionary *responseDictionary = [[NSMutableDictionary alloc]init];
            responseDictionary = [responseArrayn objectForKey:@"data"];
            [self.arrItems addObject:responseDictionary];
            return YES;
            
        }
        else
            return NO;
    }
    @catch (NSException *exception) {
        NSLog(@"parseSendChatMessage exception   %@",exception);
    }
}



-(BOOL)parseVerify:(NSString *)response
{
    @try {
        SBJSON *json = [SBJSON new];
        NSMutableDictionary *responseArrayn = [json objectWithString:response];
        NSString *strStatus = [responseArrayn objectForKey:@"status"];
        
        if (![strStatus isEqualToString:@"fail"]) {
            NSMutableDictionary *responseDictionary = [[NSMutableDictionary alloc]init];
            responseDictionary = [responseArrayn objectForKey:@"data"];

            User *userObj = (User *)[DatabaseHelper getObjectIfExist:@"User" predicate:[NSPredicate predicateWithFormat:@"isLoggedIn == 1"]];
            [userObj setIsVerified:[NSNumber numberWithInt:1]];
            [userObj setToken:[responseDictionary valueForKey:@"token"]];
            [DatabaseHelper commitChanges];
            return YES;
            
        }
        else
            return NO;
    }
    @catch (NSException *exception) {
        NSLog(@"parseVerify exception   %@",exception);
    }
}

-(BOOL)parseSyncContact:(NSString *)response
{
    @try {
        SBJSON *json = [SBJSON new];
        NSMutableDictionary *responseArrayn = [json objectWithString:response];
        NSString *strStatus = [responseArrayn objectForKey:@"status"];
        
        if (![strStatus isEqualToString:@"fail"]) {
          
            NSMutableDictionary *responseDictionary = [[NSMutableDictionary alloc]init];
            responseDictionary = [responseArrayn objectForKey:@"data"];
            NSMutableArray *arrContact = [[NSMutableArray alloc]init];
            arrContact = (NSMutableArray*) [responseDictionary valueForKey:@"contact_list"];
            for (int i = 0; i<arrContact.count; i++) {
                NSString *strContactID = @"";
                NSString *strPhoneNumber = @"";

                NSMutableDictionary *contactDic = [[NSMutableDictionary alloc]init];
                contactDic = (NSMutableDictionary*)[arrContact objectAtIndex:i];
                
                if ([contactDic valueForKey:@"id"] != nil) {
                    strContactID = [NSString stringWithFormat:@"%@",[contactDic valueForKey:@"id"]];
                }
                
                if ([contactDic valueForKey:@"phone"] != nil && [contactDic valueForKey:@"phone"] != [NSNull null]) {
                    strPhoneNumber = [contactDic valueForKey:@"phone"];
                }
                Contact  *contactObj = (Contact *)[DatabaseHelper getObjectIfExist:@"Contact" predicate:[NSPredicate predicateWithFormat:@"phone == %@",strPhoneNumber ]];
                
                [contactObj setIdentifier:strContactID];
                if ([contactDic valueForKey:@"name"] != nil && [contactDic valueForKey:@"name"] != [NSNull null]) {
                    [contactObj setServerName:[contactDic valueForKey:@"name"]];
                }
                if ([contactDic valueForKey:@"public_key"] != nil && [contactDic valueForKey:@"public_key"] != [NSNull null]) {
                    [contactObj setPublicKey:[contactDic valueForKey:@"public_key"]];
                }
                if ([contactDic valueForKey:@"phone"] != nil && [contactDic valueForKey:@"phone"] != [NSNull null]) {
                    [contactObj setPhone:[contactDic valueForKey:@"phone"]];
                }
                if ([contactDic valueForKey:@"image"] != nil && [contactDic valueForKey:@"image"] != [NSNull null]) {
                    [contactObj setImage_id:[NSString stringWithFormat:@"%@%@",kProfilePictureURL,[contactDic valueForKey:@"image"]]];
                }
                if ([contactDic valueForKey:@"active"] != nil && [contactDic valueForKey:@"active"] != [NSNull null]) {
                    [contactObj setActive:[NSString stringWithFormat:@"%@",[contactDic valueForKey:@"active"]]];
                }
                
                
                [DatabaseHelper commitChanges];
                
            }
            return YES;
            
        }
        else
            return NO;
    }
    @catch (NSException *exception) {
        NSLog(@"parseSyncContact exception   %@",exception);
    }
}


-(BOOL)parseSetProfile:(NSString *)response
{
    @try {
        SBJSON *json = [SBJSON new];
        NSMutableDictionary *responseArrayn = [json objectWithString:response];
        NSString *strStatus = [responseArrayn objectForKey:@"status"];
        
        if (![strStatus isEqualToString:@"fail"]) {
            
            NSMutableDictionary *responseDictionary = [[NSMutableDictionary alloc]init];
            responseDictionary = [responseArrayn objectForKey:@"data"];
            
            return YES;
            
        }
        else
            return NO;
    }
    @catch (NSException *exception) {
        NSLog(@"parseSetProfile exception   %@",exception);
    }
}


-(BOOL)parseGetProfile:(NSString *)response
{
    @try {
        SBJSON *json = [SBJSON new];
        NSMutableDictionary *responseArrayn = [json objectWithString:response];
        NSString *strStatus = [responseArrayn objectForKey:@"status"];
        
        if (![strStatus isEqualToString:@"fail"]) {
            
            NSMutableDictionary *responseDictionary = [[NSMutableDictionary alloc]init];
            responseDictionary = [responseArrayn objectForKey:@"data"];
            NSMutableDictionary *dicUser = [[NSMutableDictionary alloc]init];
            dicUser = (NSMutableDictionary*) [responseDictionary valueForKey:@"user"];
            User *userObj = (User *)[DatabaseHelper getObjectIfExist:@"User" predicate:[NSPredicate predicateWithFormat:@"isLoggedIn == 1"]];
            if ([dicUser valueForKey:@"id"] != nil) {
                [userObj setUserID:[NSNumber numberWithInt:[[dicUser valueForKey:@"id"]intValue]]];
            }
            if ([dicUser valueForKey:@"name"] != nil) {
                [userObj setName:[dicUser valueForKey:@"name"]];
            }
            if ([dicUser valueForKey:@"phone"] != nil) {
                [userObj setPhone:[dicUser valueForKey:@"phone"]];
            }
            if ([dicUser valueForKey:@"public_key"] != nil) {
                [userObj setPublicKey:[dicUser valueForKey:@"public_key"]];
            }
            if ([dicUser valueForKey:@"image"] != nil) {
                [userObj setUserImage:[NSString stringWithFormat:@"%@%@",kProfilePictureURL,[dicUser valueForKey:@"image"]]];
            }
            [DatabaseHelper commitChanges];
            return YES;
            
        }
        else
            return NO;
    }
    @catch (NSException *exception) {
        NSLog(@"parseSetProfile exception   %@",exception);
    }
}



@end

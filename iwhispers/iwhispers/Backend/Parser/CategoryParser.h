//
//  AuthenticationParser.h
//  danone
//
//  Created by Wasif Iqbal on 3/20/15.
//  Copyright (c) 2015 Amani Systems. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseParser.h"
#import "Enums.h"


@interface CategoryParser : BaseParser
{
    
}


-(BOOL)parseSetProfile:(NSString *)response;
-(BOOL)parseGetProfile:(NSString *)response;
-(BOOL)parseSendGroupMessage:(NSString *)response;
-(BOOL)parseAuthenticationData:(NSString *)response authType:(AuthenticationParserType)authType;


@end

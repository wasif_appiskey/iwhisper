//
//  BaseParser.h
//  danone
//
//  Created by Wasif Iqbal on 3/20/15.
//  Copyright (c) 2015 Amani Systems. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BaseParser : NSObject
{
    NSMutableArray *arrItems;
    NSString *strMessage;
}

@property (nonatomic, retain) NSString *strMessage;
@property (nonatomic, retain) NSMutableArray *arrItems;

@end

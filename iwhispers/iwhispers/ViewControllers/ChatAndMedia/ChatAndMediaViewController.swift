//
//  ChatAndMediaViewController.swift
//  iwhispers
//
//  Created by Apple on 7/1/16.
//  Copyright © 2016 Appiskey. All rights reserved.
//

import UIKit

class ChatAndMediaViewController: UIViewController,UIAlertViewDelegate {

    @IBOutlet var sliderVibrate: UISwitch!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.settingNavigation()
        self.populateView()
        // Do any additional setup after loading the view.
    }
    
    
    func populateView() {
        let userObj : User = DatabaseHelper.getObjectIfExist("User", predicate: NSPredicate(format: "isLoggedIn == 1")) as! User
        if userObj.saveMedia == nil {
            userObj.saveMedia = NSNumber(bool: true)
        }
        DatabaseHelper.commitChanges()
        if userObj.saveMedia == true {
            sliderVibrate.setOn(true, animated: false)
        }else{
            sliderVibrate.setOn(false, animated: false)
        }
    }
    
    
    @IBAction func didValueChange(sender: AnyObject) {
        let userObj : User = DatabaseHelper.getObjectIfExist("User", predicate: NSPredicate(format: "isLoggedIn == 1")) as! User
        
        if sliderVibrate.on == true {
            (UIApplication.sharedApplication().delegate as! AppDelegate).soundNotification()
            userObj.saveMedia = NSNumber(bool: true)
            DatabaseHelper.commitChanges()
        }else{
            (UIApplication.sharedApplication().delegate as! AppDelegate).silentNotification()
            userObj.saveMedia = NSNumber(bool: false)
            DatabaseHelper.commitChanges()
        }
        
        
    }
    
    /////////////////////**** Setting Up Navigation ******////////////
    
    func settingNavigation(){
        self.settingNavigationBg()
        self.settingNavigationTitle()
        self.settingNavigationLeftButton()
    }
    
    //////**** Setting Navigation Background ****//////
    
    func settingNavigationBg(){
        self.navigationController?.navigationBar.translucent = false
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 0.0/255.0, green: 115.0/255.0, blue: 181.0/255.0, alpha: 1.0)
        
    }
    
    //////**** Setting Navigation Title ****//////
    func settingNavigationTitle(){
        self.title = "Chat & Media"
        self.navigationController!.navigationBar.titleTextAttributes = [
            NSFontAttributeName: UIFont(name: "OpenSans", size: 15.0)!,
            NSForegroundColorAttributeName :  UIColor.whiteColor()
            
            
        ]
    }
    
    
    //////**** Setting Navigation Left Button ****//////
    
    func settingNavigationLeftButton(){
        let button: UIButton = UIButton(type:UIButtonType.Custom)
        button.setImage(UIImage(named: "arrow-white.png"), forState: UIControlState.Normal)
        button.addTarget(self, action: #selector(HomeViewController.btnActionNewMessage), forControlEvents: UIControlEvents.TouchUpInside)
        button.frame = CGRectMake(0, 0, 20, 21)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    
    //////**** Setting New Message Button Action ****//////
    
    func btnActionNewMessage(){
        print("Menu Pressed")
        self.navigationController?.popViewControllerAnimated(true)
        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnActionClearChat(sender: AnyObject) {
        let alert : UIAlertView = UIAlertView(title: "", message: "Are you sure you want to delete your Chats", delegate: self, cancelButtonTitle: "No", otherButtonTitles: "Yes")
        alert.tag = 100
        alert.show()
    }
    
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        if alertView.tag == 100 {
            let buttonTitle = alertView.buttonTitleAtIndex(buttonIndex)
            print("\(buttonTitle) pressed")
            if buttonTitle == "No" {
                
            }else if buttonTitle == "Yes"{
                DatabaseHelper.deleteAllObjectsOfEntity("Message", predicate: nil)
              //  DatabaseHelper.deleteAllObjectsOfEntity("Chat", predicate: nil)
             //   DatabaseHelper.deleteAllObjectsOfEntity("Group", predicate: nil)
             //   DatabaseHelper.deleteAllObjectsOfEntity("GroupMember", predicate: nil)
                let arrChat : NSArray = DatabaseHelper.getAllData("Chat", offset: 0, predicate: nil, sortDescriptor: nil) as! NSArray
                for i in 0  ..< arrChat.count   {
                    var chatObj : Chat = arrChat.objectAtIndex(i) as! Chat
                    chatObj.messageID = "0"
                }

            }
        }
    }
    
    @IBAction func btnActionChatBackup(sender: AnyObject) {
        DatabaseHelper.migrateToiCloud()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

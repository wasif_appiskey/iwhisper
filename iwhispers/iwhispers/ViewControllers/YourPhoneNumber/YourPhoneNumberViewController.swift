//
//  YourPhoneNumberViewController.swift
//  iwhispers
//
//  Created by Apple on 6/20/16.
//  Copyright © 2016 Appiskey. All rights reserved.
//

import UIKit

class YourPhoneNumberViewController: UIViewController, BaseBLLDelegate, UITextFieldDelegate {
    
    @IBOutlet var lblCountryCode: UILabel!
    @IBOutlet var txtPhoneNumber: UITextField!
    @IBOutlet var txtCountry: UITextField!
    
    var countryNumber : NSNumber!
    var strCountryCode : String!
    var strCountryName : String!
    var strPhoneNumber : String!
    
    var isEditNumber : Bool = false
    var arrCountry : NSMutableArray = NSMutableArray()
    @IBOutlet var btnBack: UIButton!
    @IBOutlet weak var countryPickerView: UIView!
    @IBOutlet weak var countryPicker: UIPickerView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ///// Open Keyboard
        self.setupData()
        if strCountryCode != nil {
            self.populateData()
        }
        txtPhoneNumber.becomeFirstResponder()
        // Do any additional setup after loading the view.
    }
    
    func populateData() {
        lblCountryCode.text = strCountryCode
        txtPhoneNumber.text = strPhoneNumber
        txtCountry.text = strCountryCode
    }
    
    func setupData(){
        let dataDic1 : NSMutableDictionary = NSMutableDictionary()
        dataDic1.setValue("Bangladesh", forKey: "name")
        dataDic1.setValue("880", forKey: "code")
        dataDic1.setValue(0, forKey: "number")

        arrCountry.addObject(dataDic1)
        
        let dataDic2 : NSMutableDictionary = NSMutableDictionary()
        dataDic2.setValue("Afghanistan", forKey: "name")
        dataDic2.setValue("93", forKey: "code")
        dataDic2.setValue(1, forKey: "number")

        arrCountry.addObject(dataDic2)
        
        let dataDic3 : NSMutableDictionary = NSMutableDictionary()
        dataDic3.setValue("Canada", forKey: "name")
        dataDic3.setValue("1", forKey: "code")
        dataDic3.setValue(2, forKey: "number")

        arrCountry.addObject(dataDic3)
        
        let dataDic4 : NSMutableDictionary = NSMutableDictionary()
        dataDic4.setValue("Egypt", forKey: "name")
        dataDic4.setValue("20", forKey: "code")
        dataDic4.setValue(3, forKey: "number")

        arrCountry.addObject(dataDic4)
        
        let dataDic5 : NSMutableDictionary = NSMutableDictionary()
        dataDic5.setValue("Germany", forKey: "name")
        dataDic5.setValue("49", forKey: "code")
        dataDic5.setValue(4, forKey: "number")

        arrCountry.addObject(dataDic5)
        
        let dataDic6 : NSMutableDictionary = NSMutableDictionary()
        dataDic6.setValue("New Zealand", forKey: "name")
        dataDic6.setValue("64", forKey: "code")
        dataDic6.setValue(5, forKey: "number")

        arrCountry.addObject(dataDic6)
        
        let dataDic7 : NSMutableDictionary = NSMutableDictionary()
        dataDic7.setValue("Niger", forKey: "name")
        dataDic7.setValue("227", forKey: "code")
        dataDic7.setValue(6, forKey: "number")

        arrCountry.addObject(dataDic7)
        
        let dataDic8 : NSMutableDictionary = NSMutableDictionary()
        dataDic8.setValue("Pakistan", forKey: "name")
        dataDic8.setValue("92", forKey: "code")
        dataDic8.setValue(7, forKey: "number")

        arrCountry.addObject(dataDic8)
        
        
        let dataDic14 : NSMutableDictionary = NSMutableDictionary()
        dataDic14.setValue("Saudi Arabia", forKey: "name")
        dataDic14.setValue("966", forKey: "code")
        dataDic14.setValue(8, forKey: "number")

        arrCountry.addObject(dataDic14)
        
        let dataDic9 : NSMutableDictionary = NSMutableDictionary()
        dataDic9.setValue("South Africa", forKey: "name")
        dataDic9.setValue("27", forKey: "code")
        dataDic9.setValue(9, forKey: "number")

        arrCountry.addObject(dataDic9)
        
        let dataDic10 : NSMutableDictionary = NSMutableDictionary()
        dataDic10.setValue("Turkey", forKey: "name")
        dataDic10.setValue("90", forKey: "code")
        dataDic10.setValue(10, forKey: "number")

        arrCountry.addObject(dataDic10)
        
        let dataDic11 : NSMutableDictionary = NSMutableDictionary()
        dataDic11.setValue("United Kingdom", forKey: "name")
        dataDic11.setValue("44", forKey: "code")
        dataDic11.setValue(11, forKey: "number")

        arrCountry.addObject(dataDic11)
        
        let dataDic13 : NSMutableDictionary = NSMutableDictionary()
        dataDic13.setValue("United States", forKey: "name")
        dataDic13.setValue("1", forKey: "code")
        dataDic13.setValue(12, forKey: "number")

        arrCountry.addObject(dataDic13)
        
        let dataDic12 : NSMutableDictionary = NSMutableDictionary()
        dataDic12.setValue("Yemen", forKey: "name")
        dataDic12.setValue("967", forKey: "code")
        dataDic12.setValue(13, forKey: "number")

        arrCountry.addObject(dataDic12)

    }
    
    ///////////////////******* Populate View *******/////////////////////
    func populateView() {
        
        let userObj : User = DatabaseHelper.getObjectIfExist("User", predicate: NSPredicate(format: "isLoggedIn == 1")) as! User
        if userObj.countryCode != nil {
            lblCountryCode.text = userObj.countryCode
            txtCountry.text = userObj.countryName
            if userObj.countryNumber != nil {
                self.countryPicker.selectRow(userObj.countryNumber!.integerValue, inComponent: 0, animated: false)

            }
        }else{
            /////Getting Your Current Country
            let currentLocale = NSLocale.currentLocale()
            let countryCode = currentLocale.objectForKey(NSLocaleCountryCode) as! String//get the set country name, code of your iphone
            ////Setting Country Code
            lblCountryCode.text = String(format: "+%@",getCountryCallingCode(countryCode))
            ////Setting Country Name
            txtCountry.text = String(format: "%@",getCountryFullName(countryCode))
        }
        
    }
    override func viewWillAppear(animated: Bool) {
        if isEditNumber == true {
            rdv_tabBarController .setTabBarHidden(true, animated: true)
            btnBack.hidden = false
        }else{
            btnBack.hidden = true
        }
        self.populateView()
        self.navigationController?.navigationBarHidden = true
    }
    
    override func viewWillDisappear(animated: Bool) {
        self.navigationController?.navigationBarHidden = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    //////**** Validate Login *****///////
    
    func isValidate() -> Bool {
        
        if txtPhoneNumber.text == "" {
            Common.showAlert("Alert", message: "Please enter your contact number")
            return false
        }
        
        if Common.validateNumber(txtPhoneNumber.text!) == false {
            Common.showAlert("Alert", message: "Please enter valid contact number")
            return false
        }
        if txtPhoneNumber.text?.characters.count < 10 {
            Common.showAlert("Alert", message: "Please enter valid contact number")
            return false
        }
        
        return true
    }
    
    
    ////////****** Button Action for Verify Number *****//////////
    
    @IBAction func btnActionVerifyNumber(sender: AnyObject) {
        
        
        /////Testing
        //        var smsVerifyVC : SMSVerifyViewController = SMSVerifyViewController()
        //        smsVerifyVC = SMSVerifyViewController(nibName: "SMSVerifyViewController", bundle: nil)
        //        self.navigationController?.pushViewController(smsVerifyVC, animated: true)
        let isValidate : Bool = self.isValidate()
        if isValidate == false {
            return
        }
        if InternetCheck.IsConnected() {
            MBProgressHUD.showHUDAddedTo(self.view, animated: true)
            let  bllSerives : BLLServices =  BLLServices()
            bllSerives.delegate = self
            let parameter : NSMutableDictionary =  NSMutableDictionary()
            var strPhone : String =  String()
            var strPhoneLocal : String =  String()
            strPhoneLocal = txtPhoneNumber.text!
            ///// Removing "0" from start of phone number if Exists
            if strPhoneLocal.hasPrefix("0")  {
                strPhoneLocal = strPhoneLocal.substringFromIndex(strPhoneLocal.startIndex.advancedBy(1))
            }
            //// Merging Country code and phone number
            strPhone = String(format: "%@%@",lblCountryCode.text!,strPhoneLocal)
            parameter.setValue(strPhone, forKey: "phone")
            //// Callling Service
            bllSerives.userLogin(parameter)
        }else{
            Common.showAlert("Alert", message: "Please check your internet connection");
        }
    }
    /////////////******** Getting Country Code  from  Shortcode of country *****////////////
    
    func getCountryCallingCode(countryRegionCode:String)->String{
        
        let prefixCodes = ["AF": "93", "AE": "971", "AL": "355", "AN": "599", "AS":"1", "AD": "376", "AO": "244", "AI": "1", "AG":"1", "AR": "54","AM": "374", "AW": "297", "AU":"61", "AT": "43","AZ": "994", "BS": "1", "BH":"973", "BF": "226","BI": "257", "BD": "880", "BB": "1", "BY": "375", "BE":"32","BZ": "501", "BJ": "229", "BM": "1", "BT":"975", "BA": "387", "BW": "267", "BR": "55", "BG": "359", "BO": "591", "BL": "590", "BN": "673", "CC": "61", "CD":"243","CI": "225", "KH":"855", "CM": "237", "CA": "1", "CV": "238", "KY":"345", "CF":"236", "CH": "41", "CL": "56", "CN":"86","CX": "61", "CO": "57", "KM": "269", "CG":"242", "CK": "682", "CR": "506", "CU":"53", "CY":"537","CZ": "420", "DE": "49", "DK": "45", "DJ":"253", "DM": "1", "DO": "1", "DZ": "213", "EC": "593", "EG":"20", "ER": "291", "EE":"372","ES": "34", "ET": "251", "FM": "691", "FK": "500", "FO": "298", "FJ": "679", "FI":"358", "FR": "33", "GB":"44", "GF": "594", "GA":"241", "GS": "500", "GM":"220", "GE":"995","GH":"233", "GI": "350", "GQ": "240", "GR": "30", "GG": "44", "GL": "299", "GD":"1", "GP": "590", "GU": "1", "GT": "502", "GN":"224","GW": "245", "GY": "595", "HT": "509", "HR": "385", "HN":"504", "HU": "36", "HK": "852", "IR": "98", "IM": "44", "IL": "972", "IO":"246", "IS": "354", "IN": "91", "ID":"62", "IQ":"964", "IE": "353","IT":"39", "JM":"1", "JP": "81", "JO": "962", "JE":"44", "KP": "850", "KR": "82","KZ":"77", "KE": "254", "KI": "686", "KW": "965", "KG":"996","KN":"1", "LC": "1", "LV": "371", "LB": "961", "LK":"94", "LS": "266", "LR":"231", "LI": "423", "LT": "370", "LU": "352", "LA": "856", "LY":"218", "MO": "853", "MK": "389", "MG":"261", "MW": "265", "MY": "60","MV": "960", "ML":"223", "MT": "356", "MH": "692", "MQ": "596", "MR":"222", "MU": "230", "MX": "52","MC": "377", "MN": "976", "ME": "382", "MP": "1", "MS": "1", "MA":"212", "MM": "95", "MF": "590", "MD":"373", "MZ": "258", "NA":"264", "NR":"674", "NP":"977", "NL": "31","NC": "687", "NZ":"64", "NI": "505", "NE": "227", "NG": "234", "NU":"683", "NF": "672", "NO": "47","OM": "968", "PK": "92", "PM": "508", "PW": "680", "PF": "689", "PA": "507", "PG":"675", "PY": "595", "PE": "51", "PH": "63", "PL":"48", "PN": "872","PT": "351", "PR": "1","PS": "970", "QA": "974", "RO":"40", "RE":"262", "RS": "381", "RU": "7", "RW": "250", "SM": "378", "SA":"966", "SN": "221", "SC": "248", "SL":"232","SG": "65", "SK": "421", "SI": "386", "SB":"677", "SH": "290", "SD": "249", "SR": "597","SZ": "268", "SE":"46", "SV": "503", "ST": "239","SO": "252", "SJ": "47", "SY":"963", "TW": "886", "TZ": "255", "TL": "670", "TD": "235", "TJ": "992", "TH": "66", "TG":"228", "TK": "690", "TO": "676", "TT": "1", "TN":"216","TR": "90", "TM": "993", "TC": "1", "TV":"688", "UG": "256", "UA": "380", "US": "1", "UY": "598","UZ": "998", "VA":"379", "VE":"58", "VN": "84", "VG": "1", "VI": "1","VC":"1", "VU":"678", "WS": "685", "WF": "681", "YE": "967", "YT": "262","ZA": "27" , "ZM": "260", "ZW":"263"]
        let countryDialingCode = prefixCodes[countryRegionCode]
        return countryDialingCode!
        
    }
    
    /////////////******** Getting Full Name of Country with Country Code *****////////////
    func getCountryFullName(countryRegionCode:String)->String{
        
        let prefixCodes = ["AF": "93", "AE": "971", "AL": "355", "AN": "599", "AS":"1", "AD": "376", "AO": "244", "AI": "1", "AG":"1", "AR": "54","AM": "374", "AW": "297", "AU":"61", "AT": "43","AZ": "994", "BS": "1", "BH":"973", "BF": "226","BI": "257", "BD": "880", "BB": "1", "BY": "375", "BE":"32","BZ": "501", "BJ": "229", "BM": "1", "BT":"975", "BA": "387", "BW": "267", "BR": "55", "BG": "359", "BO": "591", "BL": "590", "BN": "673", "CC": "61", "CD":"243","CI": "225", "KH":"855", "CM": "237", "CA": "1", "CV": "238", "KY":"345", "CF":"236", "CH": "41", "CL": "56", "CN":"86","CX": "61", "CO": "57", "KM": "269", "CG":"242", "CK": "682", "CR": "506", "CU":"53", "CY":"537","CZ": "420", "DE": "49", "DK": "45", "DJ":"253", "DM": "1", "DO": "1", "DZ": "213", "EC": "593", "EG":"20", "ER": "291", "EE":"372","ES": "34", "ET": "251", "FM": "691", "FK": "500", "FO": "298", "FJ": "679", "FI":"358", "FR": "33", "GB":"44", "GF": "594", "GA":"241", "GS": "500", "GM":"220", "GE":"995","GH":"233", "GI": "350", "GQ": "240", "GR": "30", "GG": "44", "GL": "299", "GD":"1", "GP": "590", "GU": "1", "GT": "502", "GN":"224","GW": "245", "GY": "595", "HT": "509", "HR": "385", "HN":"504", "HU": "36", "HK": "852", "IR": "98", "IM": "44", "IL": "972", "IO":"246", "IS": "354", "IN": "91", "ID":"62", "IQ":"964", "IE": "353","IT":"39", "JM":"1", "JP": "81", "JO": "962", "JE":"44", "KP": "850", "KR": "82","KZ":"77", "KE": "254", "KI": "686", "KW": "965", "KG":"996","KN":"1", "LC": "1", "LV": "371", "LB": "961", "LK":"94", "LS": "266", "LR":"231", "LI": "423", "LT": "370", "LU": "352", "LA": "856", "LY":"218", "MO": "853", "MK": "389", "MG":"261", "MW": "265", "MY": "60","MV": "960", "ML":"223", "MT": "356", "MH": "692", "MQ": "596", "MR":"222", "MU": "230", "MX": "52","MC": "377", "MN": "976", "ME": "382", "MP": "1", "MS": "1", "MA":"212", "MM": "95", "MF": "590", "MD":"373", "MZ": "258", "NA":"264", "NR":"674", "NP":"977", "NL": "31","NC": "687", "NZ":"64", "NI": "505", "NE": "227", "NG": "234", "NU":"683", "NF": "672", "NO": "47","OM": "968", "PK": "Pakistan", "PM": "508", "PW": "680", "PF": "689", "PA": "507", "PG":"675", "PY": "595", "PE": "51", "PH": "63", "PL":"48", "PN": "872","PT": "351", "PR": "1","PS": "970", "QA": "974", "RO":"40", "RE":"262", "RS": "381", "RU": "7", "RW": "250", "SM": "378", "SA":"966", "SN": "221", "SC": "248", "SL":"232","SG": "65", "SK": "421", "SI": "386", "SB":"677", "SH": "290", "SD": "249", "SR": "597","SZ": "268", "SE":"46", "SV": "503", "ST": "239","SO": "252", "SJ": "47", "SY":"963", "TW": "886", "TZ": "255", "TL": "670", "TD": "235", "TJ": "992", "TH": "66", "TG":"228", "TK": "690", "TO": "676", "TT": "1", "TN":"216","TR": "90", "TM": "993", "TC": "1", "TV":"688", "UG": "256", "UA": "380", "US": "United States", "UY": "598","UZ": "998", "VA":"379", "VE":"58", "VN": "84", "VG": "1", "VI": "1","VC":"1", "VU":"678", "WS": "685", "WF": "681", "YE": "967", "YT": "262","ZA": "27" , "ZM": "260", "ZW":"263"]
        let countryDialingCode = prefixCodes[countryRegionCode]
        return countryDialingCode!
        
    }
    
    @IBAction func btnActionCountrySelection(sender: AnyObject) {
        self.view.endEditing(true)
        if countryPickerView.alpha == 0 {
            UIView.animateWithDuration(1.0, animations: {
                self.countryPickerView.alpha = 1
            })
        }else{
            UIView.animateWithDuration(1.0, animations: {
                self.countryPickerView.alpha = 0
            })
        }
    }
    
    @IBAction func btnActionPickerDone(sender: AnyObject) {
        UIView.animateWithDuration(1.0, animations: {
            self.countryPickerView.alpha = 0
        })
    }
    /////////////***** Base BLL Delegates ****/////////
    //////***** Success *****////////
    func successWithData(data: AnyObject!, msg successMsg: String!, tag: Int32) {
        ///// Going to View Controller Sms Verification /////////
        MBProgressHUD.hideHUDForView(self.view, animated: true)
        let userObj : User = DatabaseHelper.getObjectIfExist("User", predicate: NSPredicate(format: "isLoggedIn == 1")) as! User
        
        
        
        
        var strPhone : String =  String()
        var strPhoneLocal : String =  String()
        strPhoneLocal = txtPhoneNumber.text!
        ///// Removing "0" from start of phone number if Exists
        if strPhoneLocal.hasPrefix("0")  {
            strPhoneLocal = strPhoneLocal.substringFromIndex(strPhoneLocal.startIndex.advancedBy(1))
        }
        
        strPhone = String(format: "%@%@",lblCountryCode.text!,strPhoneLocal)
        //// Saving Phone number in User Object
        userObj.phone = strPhone
        //// Saving Country in User Object
        userObj.countryCode = lblCountryCode.text!
        userObj.countryName = txtCountry.text!
        userObj.countryNumber = countryNumber
        DatabaseHelper.commitChanges()
        var smsVerifyVC : SMSVerifyViewController = SMSVerifyViewController()
        smsVerifyVC = SMSVerifyViewController(nibName: "SMSVerifyViewController", bundle: nil)
        smsVerifyVC.isEditNumber = isEditNumber
        self.navigationController?.pushViewController(smsVerifyVC, animated: true)
        
    }
    
    
    
    
    //////***** Failure *****////////
    func requestFailure(failureMsg: String!, tag: Int32) {
        MBProgressHUD.hideHUDForView(self.view, animated: true)
        Common.showAlert("Sorry", message: failureMsg)
    }
    
    //////***** Uploading *****////////
    func uploadProgress(progress: Float) {
        
    }
    
    
    @IBAction func btnActionBack(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == countryPicker {
            return arrCountry.count
        }
        return 0
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        if pickerView == countryPicker {
            var dataDic : NSMutableDictionary = NSMutableDictionary()
            dataDic = arrCountry.objectAtIndex(row) as! NSMutableDictionary
            var strCountryName : String = dataDic.valueForKey("name") as! String
            return strCountryName
        }
        return ""
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        if pickerView == countryPicker {
            var dataDic : NSMutableDictionary = NSMutableDictionary()
            dataDic = arrCountry.objectAtIndex(row) as! NSMutableDictionary
            let strCountryName : String = dataDic.valueForKey("name") as! String
            let tempCountryNumber : Int = dataDic.valueForKey("number") as! Int
            txtCountry.text = strCountryName
            countryNumber = NSNumber(integer: tempCountryNumber)
            let strCountryCode: String = dataDic.valueForKey("code") as! String
            lblCountryCode.text = "+"+strCountryCode
            
        }
    }
    
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool{
        if textField == txtPhoneNumber {
            if Common.isNumeric(string) {
                if txtPhoneNumber.text == "" {
                    if lblCountryCode.text != "+966"{
                        if string == "0" {
                            return false
                        }else{
                            return true
                        }
                    }else{
                        return true
                    }
                }
                return true
            }else{
                if string == "" {
                    return true
                }
                return false
            }
        }
        return false
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

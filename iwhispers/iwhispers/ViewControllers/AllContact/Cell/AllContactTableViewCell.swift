//
//  AllContactTableViewCell.swift
//  iwhispers
//
//  Created by Apple on 6/27/16.
//  Copyright © 2016 Appiskey. All rights reserved.
//

import UIKit

class AllContactTableViewCell: UITableViewCell {

    @IBOutlet var lblMessageTime: UILabel!
    @IBOutlet var lblLastMessage: UILabel!
    @IBOutlet var lblName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

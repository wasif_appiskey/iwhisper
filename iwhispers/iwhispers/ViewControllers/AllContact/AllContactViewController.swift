//
//  AllContactViewController.swift
//  iwhispers
//
//  Created by Apple on 6/27/16.
//  Copyright © 2016 Appiskey. All rights reserved.
//

import UIKit
import ContactsUI
import AddressBook
import Contacts

class AllContactViewController: UIViewController,UISearchBarDelegate, BaseBLLDelegate {
    @IBOutlet var tblContact: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    var phonesArray  = Array<String>()
    var nameArray  = Array<String>()
    var contactArray : NSArray = NSArray()
    
    @IBOutlet var lblNoContact: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.intializeTableView()
        self.settingNavigation()
        self.getAddressBookNames()
        self.pulltoRefresh()
        // Do any additional setup after loading the view.
    }
    
    func pulltoRefresh() {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh(_:)), forControlEvents: .ValueChanged)
        tblContact.addSubview(refreshControl)
    }
    func refresh(refreshControl: UIRefreshControl) {
        // Do your job, when done:
        self.getAddressBookNames()
        refreshControl.endRefreshing()
    }
    
    override func viewWillAppear(animated: Bool) {
        self.getAllContact()
        rdv_tabBarController .setTabBarHidden(false, animated: true)
        
    }
  
    
    func getAddressBookNames() {
        let authorizationStatus = ABAddressBookGetAuthorizationStatus()
        if (authorizationStatus == ABAuthorizationStatus.NotDetermined)
        {
            NSLog("requesting access...")
            var emptyDictionary: CFDictionaryRef?
            let addressBook = (ABAddressBookCreateWithOptions(emptyDictionary, nil) == nil)
            ABAddressBookRequestAccessWithCompletion(addressBook,{success, error in
                if success {
                    self.processContactNames();
                }
                else {
                    NSLog("unable to request access")
                }
            })
        }
        else if (authorizationStatus == ABAuthorizationStatus.Denied || authorizationStatus == ABAuthorizationStatus.Restricted) {
            NSLog("access denied")
        }
        else if (authorizationStatus == ABAuthorizationStatus.Authorized) {
            NSLog("access granted")
            processContactNames()
        }

    }
    
    func processContactNames()
    {
        var errorRef: Unmanaged<CFError>?
        let addressBook: ABAddressBookRef? = extractABAddressBookRef(ABAddressBookCreateWithOptions(nil, &errorRef))
        
        let contactList: NSArray = ABAddressBookCopyArrayOfAllPeople(addressBook).takeRetainedValue()
        print("records in the array \(contactList.count)")
        
        for record:ABRecordRef in contactList {
            processAddressbookRecord(record)
        }
        print("All Phones: \(phonesArray)")
        
        for i:Int in 0 ..< phonesArray.count {
            var contactObj : Contact = DatabaseHelper.getObjectIfExist("Contact", predicate: NSPredicate(format: "phone == %@",phonesArray[i])) as! Contact
            contactObj.phone = phonesArray[i]
            contactObj.name = nameArray[i]
            DatabaseHelper.commitChanges()
        }
        self.syncContactService()

        
    }
    
    func processAddressbookRecord(contactPerson: ABRecordRef) {
        let phonesRef: ABMultiValueRef = ABRecordCopyValue(contactPerson, kABPersonPhoneProperty).takeRetainedValue() as ABMultiValueRef
        for i:Int in 0 ..< ABMultiValueGetCount(phonesRef) {
            //  var label: String = ABMultiValueCopyLabelAtIndex(phonesRef, i).takeRetainedValue()  as! String
            var value: String = ABMultiValueCopyValueAtIndex(phonesRef, i).takeRetainedValue()  as! String
            
            //      value = value.stringByReplacingOccurrencesOfString("\u{00a0}", withString: "")
            //      value = value.stringByReplacingOccurrencesOfString(" ", withString: "")
            
            value = self.removeSpecialCharsFromString(value)
            
            let userObj : User = DatabaseHelper.getObjectIfExist("User", predicate: NSPredicate(format: "isLoggedIn == 1")) as! User
            
            
            print("Phone: = \(value)")
            ///// Removing "0" from start of phone number if Exists
            if value.hasPrefix("00")  {
                value = value.substringFromIndex(value.startIndex.advancedBy(1))
                value = value.substringFromIndex(value.startIndex.advancedBy(1))
                value = String(format: "+%@", value)
            }
            
            if value.hasPrefix("0")  {
                value = value.substringFromIndex(value.startIndex.advancedBy(1))
                value = String(format: "%@%@", userObj.countryCode!,value)
            }
            if !value.hasPrefix("+")  {
                value = String(format: "%@%@", userObj.countryCode!,value)
            }
            
            if let firstName = ABRecordCopyValue(contactPerson, kABPersonFirstNameProperty) {
                let fn:String = (firstName.takeRetainedValue() as? String) ?? ""
                nameArray.append(fn as String)
            }else{
                nameArray.append("" as String)
            }
            phonesArray.append(value as String)
        }
        
    }
    
    func removeSpecialCharsFromString(text: String) -> String {
        let okayChars : Set<Character> =
            Set("1234567890+".characters)
        return String(text.characters.filter {okayChars.contains($0) })
    }
    
    func extractABAddressBookRef(abRef: Unmanaged<ABAddressBookRef>!) -> ABAddressBookRef? {
        if let ab = abRef {
            return Unmanaged<NSObject>.fromOpaque(ab.toOpaque()).takeRetainedValue()
        }
        return nil
    }
    
    
    
    /////////////////////****Sync Contact With Service ******////////////////
    func syncContactService(){
        if InternetCheck.IsConnected() {

            let bllService : BLLServices = BLLServices()
            bllService.delegate = self;
            let parameter : NSMutableDictionary = NSMutableDictionary()
            parameter.setValue(phonesArray, forKey: "contacts")
            bllService.syncContact(parameter)
        }else{
            Common.showAlert("Alert", message: "Please check your internet connection");
        }
    }
    
    /////////////////////**** Setting Up Navigation ******////////////
    
    func settingNavigation(){
        self.settingNavigationBg()
        self.settingNavigationTitle()
        self.settingNavigationLeftButton()
        self.settingNavigationRightButton()
    }
    
    //////**** Setting Navigation Background ****//////
    
    func settingNavigationBg(){
        self.navigationController?.navigationBar.translucent = false
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 0.0/255.0, green: 115.0/255.0, blue: 181.0/255.0, alpha: 1.0)
        
    }
    
    //////**** Setting Navigation Title ****//////
    func settingNavigationTitle(){
        self.title = "All Contacts"
        self.navigationController!.navigationBar.titleTextAttributes = [
            NSFontAttributeName: UIFont(name: "OpenSans", size: 15.0)!,
            NSForegroundColorAttributeName :  UIColor.whiteColor()
            
            
        ]
    }
    
    
    //////**** Setting Navigation Left Button ****//////
    
    func settingNavigationLeftButton(){
        let button: UIButton = UIButton(type:UIButtonType.Custom)
        button.setImage(UIImage(named: "new-message-icon.png"), forState: UIControlState.Normal)
        button.addTarget(self, action: #selector(HomeViewController.btnActionNewMessage), forControlEvents: UIControlEvents.TouchUpInside)
        button.frame = CGRectMake(0, 0, 20, 21)
        let barButton = UIBarButtonItem(customView: button)
        //self.navigationItem.leftBarButtonItem = barButton
    }
    
    
    //////**** Setting New Message Button Action ****//////
    
    func btnActionNewMessage(){
        print("Menu Pressed")
        
    }
    
    //////**** Setting Navigation Left Button ****//////
    
    func settingNavigationRightButton(){
        let buttonNewGroup: UIButton = UIButton(type:UIButtonType.Custom)
        buttonNewGroup.setImage(UIImage(named: "new-group-icon.png"), forState: UIControlState.Normal)
        buttonNewGroup.addTarget(self, action: #selector(HomeViewController.btnActionNewGroup), forControlEvents: UIControlEvents.TouchUpInside)
        buttonNewGroup.frame = CGRectMake(42, 0, 40, 48)
        let buttonNewChat: UIButton = UIButton(type:UIButtonType.Custom)
        buttonNewChat.setImage(UIImage(named: "new-chat-icon.png"), forState: UIControlState.Normal)
        buttonNewChat.addTarget(self, action: #selector(HomeViewController.btnActionNewChat), forControlEvents: UIControlEvents.TouchUpInside)
        buttonNewChat.frame = CGRectMake(42, 0, 40, 48)
        
        
        let viewRight: UIView = UIView(frame: CGRectMake(0, 0, 85, 48))
        viewRight.addSubview(buttonNewGroup)
        //viewRight.addSubview(buttonNewChat)
        
        let barButton = UIBarButtonItem(customView: viewRight)
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    //////**** Setting New Group Button Action ****//////
    
    func btnActionNewGroup(){
        rdv_tabBarController .setTabBarHidden(true, animated: true)
        var newGroupVC : NewGroupViewController = NewGroupViewController()
        newGroupVC = NewGroupViewController(nibName: "NewGroupViewController", bundle: nil)
        self.navigationController?.pushViewController(newGroupVC, animated: true)
    }
    
    //////**** Setting New Chat Button Action ****//////
    
    func btnActionNewChat(){
        
    }
    
    
    ////////////////////***** Intialize TableView *******//////////
    
    func intializeTableView(){
        
        let nibOne = UINib(nibName: "AllContactTableViewCell", bundle: nil)
        tblContact.registerNib(nibOne, forCellReuseIdentifier: "AllContactTableViewCellIdentifier")
        
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contactArray.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat{
        return 65
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:AllContactTableViewCell = tblContact.dequeueReusableCellWithIdentifier("AllContactTableViewCellIdentifier") as! AllContactTableViewCell
        cell.selectionStyle = .None
        let contactObj : Contact = contactArray.objectAtIndex(indexPath.row) as! Contact
        if contactObj.name == "" {
            cell.lblName.text = contactObj.serverName
        }else{
            cell.lblName.text = contactObj.name
        }
        
        cell.lblLastMessage.text = ""
        cell.lblMessageTime.text = ""
        
        return cell
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        rdv_tabBarController .setTabBarHidden(true, animated: true)
        
      
        let contactObj : Contact = contactArray.objectAtIndex(indexPath.row) as! Contact
        if contactObj.serverName != nil{
            var messageVC : ChatDetailViewController = ChatDetailViewController()
            messageVC = ChatDetailViewController(nibName: "ChatDetailViewController", bundle: nil)
            messageVC.contactObj = contactObj
            self.navigationController?.pushViewController(messageVC, animated: true)
        }else{
            var inviteVC : InviteViewController = InviteViewController()
            inviteVC = InviteViewController(nibName: "InviteViewController", bundle: nil)
            inviteVC.contactObj = contactObj
            self.navigationController?.pushViewController(inviteVC, animated: true)
        }
        
        
       
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK : searchBar Delegates
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        let strSearch = String(format: "%@*", searchBar.text!)
        contactArray = DatabaseHelper.getAllData("Contact", offset: 0, predicate: NSPredicate(format: "name LIKE[c] %@",strSearch ), sortDescriptor: nil)

        tblContact.reloadData()
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        self.getAllContact()
        
    }
    
    @IBAction func importContacts(sender: AnyObject) {
        // ContactsImporter.importContacts(showContacts(Array<Contacts>, error: nil))
    }
    
    /////////////***** Base BLL Delegates ****/////////
    //////***** Success *****////////
    func successWithData(data: AnyObject!, msg successMsg: String!, tag: Int32) {
        ///// Going to View Controller Sms Verification /////////
        self.getAllContact()
        MBProgressHUD.hideHUDForView(self.view, animated: true)
        // (UIApplication.sharedApplication().delegate as! AppDelegate).setupViewControllers()
    }
    
    
    //////***** Failure *****////////
    func requestFailure(failureMsg: String!, tag: Int32) {
        MBProgressHUD.hideHUDForView(self.view, animated: true)
        Common.showAlert("Sorry", message: failureMsg)
    }
    
    //////***** Uploading *****////////
    func uploadProgress(progress: Float) {
        
    }
    
    func getAllContact() {
        contactArray = DatabaseHelper.getAllData("Contact", offset: 0, predicate: nil, sortDescriptor: nil)
        for i:Int in 0 ..< contactArray.count {
            let contactObj : Contact = contactArray.objectAtIndex(i) as! Contact
            if contactObj.phone == "" {
                DatabaseHelper.deleteObject(contactObj)
            }
            if contactObj.name == "" {
                DatabaseHelper.deleteObject(contactObj)
            }
            if contactObj.active == "1" {
                
            }
            
        }
        contactArray = DatabaseHelper.getAllData("Contact", offset: 0, predicate: nil, sortDescriptor: nil)
        
        var arrMutable : NSMutableArray = NSMutableArray()
        arrMutable = contactArray.mutableCopy() as! NSMutableArray
        for i:Int in 0 ..< contactArray.count {
            let contactObj : Contact = contactArray.objectAtIndex(i) as! Contact
            if contactObj.active == "0" {
             arrMutable.removeObject(contactObj)
            }
        }
        contactArray = arrMutable as NSArray
        if contactArray.count == 0 {
            lblNoContact.hidden = false
         //   tblContact.hidden = true
        }else{
            lblNoContact.hidden = true
         //   tblContact.hidden = false
        }
        tblContact.reloadData()
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

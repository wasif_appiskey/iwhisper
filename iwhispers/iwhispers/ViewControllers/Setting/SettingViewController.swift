//
//  SettingViewController.swift
//  iwhispers
//
//  Created by Apple on 6/27/16.
//  Copyright © 2016 Appiskey. All rights reserved.
//

import UIKit

class SettingViewController: UIViewController {
    @IBOutlet var tblSetting: UITableView!
    let arrImage = ["account-icon.png", "chat-media-icon.png", "notification-icon.png" , "about-help-icon.png" , "contacts-icon.png"]
    let arrTitle = ["Accounts", "Chat & Media" , "Notifications" , "About & Help" ,  "Contacts"]

    @IBOutlet var lblNumber: UILabel!
    @IBOutlet var lblName: UILabel!
    var imgProfileAsync : AsyncImageView = AsyncImageView()
    @IBOutlet var imgProfile: UIImageView!
    
    override func viewWillAppear(animated: Bool) {
        self.populateView()
        rdv_tabBarController .setTabBarHidden(false, animated: true)
        
    }
    
    ///////////////******* Populate View ******////////////////////////
    
    func populateView(){
        let userObj : User = DatabaseHelper.getObjectIfExist("User", predicate: NSPredicate(format: "isLoggedIn == 1")) as! User
        lblName.text = userObj.name
        lblNumber.text = userObj.phone
        if userObj.userImage != nil {
            if userObj.userImage != kDefaultURL {
                imgProfileAsync.removeFromSuperview()
                /////Setting Profile Image///////
                imgProfileAsync = AsyncImageView(frame: CGRectMake(0, 0,imgProfile.frame.size.width, self.imgProfile.frame.size.width))
                imgProfileAsync.loadImageFromURL(NSURL(string: userObj.userImage!))
                imgProfile.layer.cornerRadius = CGRectGetWidth(imgProfile.frame)/2.0
                imgProfile.clipsToBounds = true
                imgProfile.addSubview(imgProfileAsync)
            }
        }
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.intializeTableView()
        self.settingNavigation()
        // Do any additional setup after loading the view.
    }
    /////////////////////**** Setting Up Navigation ******////////////
    
    func settingNavigation(){
        self.settingNavigationBg()
        self.settingNavigationTitle()
        
    }
    
    //////**** Setting Navigation Background ****//////
    
    func settingNavigationBg(){
        self.navigationController?.navigationBar.translucent = false
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 0.0/255.0, green: 115.0/255.0, blue: 181.0/255.0, alpha: 1.0)
        
    }
    
    //////**** Setting Navigation Title ****//////
    func settingNavigationTitle(){
        self.title = "Settings"
        self.navigationController!.navigationBar.titleTextAttributes = [
            NSFontAttributeName: UIFont(name: "OpenSans", size: 15.0)!,
            NSForegroundColorAttributeName :  UIColor.whiteColor()
            
            
        ]
    }
    

    
    
    ////////////////////***** Intialize TableView *******//////////
    
    func intializeTableView(){
        
        let nibOne = UINib(nibName: "SettingTableViewCell", bundle: nil)
        tblSetting.registerNib(nibOne, forCellReuseIdentifier: "SettingTableViewCellIdentifier")
        
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrImage.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat{
        return 53
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:SettingTableViewCell = tblSetting.dequeueReusableCellWithIdentifier("SettingTableViewCellIdentifier") as! SettingTableViewCell
        cell.selectionStyle = .None
        cell.lblSetting.text = arrTitle[indexPath.row]
        cell.imgSetting.image = UIImage(named: arrImage[indexPath.row])
        return cell
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
   //     rdv_tabBarController .setTabBarHidden(true, animated: true)
        if indexPath.row == 0 {
            var accountVC : AccountViewController = AccountViewController()
            accountVC = AccountViewController(nibName: "AccountViewController", bundle: nil)
            self.navigationController?.pushViewController(accountVC, animated: true)
        }else if indexPath.row == 1{
            var chatAndMediaVC : ChatAndMediaViewController = ChatAndMediaViewController()
            chatAndMediaVC = ChatAndMediaViewController(nibName: "ChatAndMediaViewController", bundle: nil)
            self.navigationController?.pushViewController(chatAndMediaVC, animated: true)
        }else if indexPath.row == 2{
            var conversationVC : ConversationViewController = ConversationViewController()
            conversationVC = ConversationViewController(nibName: "ConversationViewController", bundle: nil)
            self.navigationController?.pushViewController(conversationVC, animated: true)
        }else if indexPath.row == 4{
            self.rdv_tabBarController.selectedIndex = 1
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnActionEditProfile(sender: AnyObject) {
        var profileVC : ProfileViewController = ProfileViewController()
        profileVC = ProfileViewController(nibName: "ProfileViewController", bundle: nil)
        profileVC.isProfileEditing = true
        self.navigationController?.pushViewController(profileVC, animated: true)

    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

//
//  SettingTableViewCell.swift
//  iwhispers
//
//  Created by Apple on 6/27/16.
//  Copyright © 2016 Appiskey. All rights reserved.
//

import UIKit

class SettingTableViewCell: UITableViewCell {

    @IBOutlet var lblSetting: UILabel!
    @IBOutlet var imgSetting: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

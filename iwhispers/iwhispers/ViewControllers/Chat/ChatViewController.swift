    //
    //  ChatViewController.swift
    //  iwhispers
    //
    //  Created by Apple on 6/24/16.
    //  Copyright © 2016 Appiskey. All rights reserved.
    //
    
    import UIKit
    import Firebase
    import FirebaseInstanceID
    import FirebaseMessaging
    import Starscream
    
    class ChatViewController: UIViewController,BaseBLLDelegate,WebSocketDelegate,UISearchBarDelegate {
        
        @IBOutlet var tblChat: UITableView!
        @IBOutlet weak var searchBar: UISearchBar!
        @IBOutlet var lblNoContact: UILabel!
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        var tableData :  NSArray!
        override func viewDidLoad() {
            super.viewDidLoad()
            //  self.setTest()
            self.intializeTableView()
            self.settingNavigation()
            appDelegate.socket.delegate = self
            if appDelegate.socket.isConnected == false {
                appDelegate.socket.connect()
            }
            // Do any additional setup after loading the view.
        }
        
        
        //MARK : searchBar Delegates
        func searchBarSearchButtonClicked(searchBar: UISearchBar) {
            searchBar.resignFirstResponder()
            let strSearch = String(format: "%@*", searchBar.text!)
            tableData = DatabaseHelper.getAllData("Chat", offset: 0, predicate: NSPredicate(format: "name LIKE[c] %@",strSearch ), sortDescriptor: nil)
            
            tblChat.reloadData()
        }
        
        func searchBarCancelButtonClicked(searchBar: UISearchBar) {
            searchBar.resignFirstResponder()
            self.gettingChatArray()
            
        }
        
        // MARK: Websocket Delegate Methods.
        
        func websocketDidConnect(ws: WebSocket) {
            print("websocket is connected")
            if appDelegate.socket.isConnected == true {
                self.checkOnlineUserToSocket()
                
            }
        }
        
        func websocketDidDisconnect(ws: WebSocket, error: NSError?) {
            if let e = error {
                print("websocket is disconnected: \(e.localizedDescription)")
            } else {
                print("websocket disconnected")
            }
            appDelegate.socket.connect()
            
        }
        
        func websocketDidReceiveMessage(ws: WebSocket, text: String) {
            print("Received text: \(text)")
            let json : SBJSON = SBJSON()
            let dicJson : NSDictionary = json.objectWithString(text) as! NSDictionary
            var strStatus : String = String()
            if dicJson.valueForKey("status") != nil{
                strStatus = dicJson.valueForKey("status") as! String
            }
            var strCommad: String = String()
            if dicJson.valueForKey("command") != nil{
                strCommad = dicJson.valueForKey("command") as! String
            }
            if strStatus == "success" {
                var dataDic :NSDictionary = NSDictionary()
                if strCommad == "online" {
                    dataDic = dicJson.valueForKey("data") as! NSDictionary
                    var dataArr : NSArray = NSArray()
                    dataArr = dataDic.valueForKey("online") as! NSArray
                    appDelegate.udpateContactOnline(dataArr)
                    tblChat.reloadData()
                }
                
                if strCommad == "offline" {
                    var dataArr : NSArray = NSArray()
                    dataArr = dicJson.valueForKey("data")!.valueForKey("offline") as! NSArray
                    appDelegate.udpateContactOffline(dataArr)
                    tblChat.reloadData()
                }
                if strCommad == "last_seen" {
                    dataDic = dicJson.valueForKey("data") as! NSDictionary
                    var dataDic1 : NSDictionary = NSDictionary()
                    
                    dataDic1 = dataDic.valueForKey("last_seen") as! NSDictionary
                    appDelegate.udpateContactLastSeen(dataDic1)
                    tblChat.reloadData()
                }
                if strCommad == "typing" {
                    dataDic = dicJson.valueForKey("data") as! NSDictionary
                    var strUserID : String = String()
                    if ( dataDic.valueForKey("group_id") != nil) {
                        strUserID = dataDic.valueForKey("user_id") as! String
                        var strGroupID : String = String()
                        strGroupID = String(format:"%@",dataDic.valueForKey("group_id") as! NSNumber)
                        
                        let formatter = NSNumberFormatter()
                        formatter.numberStyle = NSNumberFormatterStyle.DecimalStyle;
                        if let number = formatter.numberFromString(strGroupID) {
                            let chatObj :  Chat = DatabaseHelper.getObjectIfExist("Chat", predicate: NSPredicate(format: "contactID == %@",number)) as! Chat
                            let contactObj  :  Contact = DatabaseHelper.getObjectIfExist("Contact", predicate: NSPredicate(format: "identifier == %@",strUserID)) as! Contact
                            print(chatObj)
                            for i:Int in 0 ..< tableData.count {
                                let chatObjTemp : Chat = tableData.objectAtIndex(i) as! Chat
                                if chatObj == chatObjTemp {
                                    chatObjTemp.typing = String(format:"%@ is typing...",contactObj.name)
                                    let indexPath : NSIndexPath = NSIndexPath(forRow: i, inSection: 0)
                                    tblChat.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
                                    let triggerTime = (Int64(NSEC_PER_SEC) * 3)
                                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, triggerTime), dispatch_get_main_queue(), { () -> Void in
                                        self.changeTypingStatus(chatObj)
                                    })
                                    break;
                                }
                            }
                        }
                        
                    }else{
                        strUserID = dataDic.valueForKey("user_id") as! String
                        let formatter = NSNumberFormatter()
                        formatter.numberStyle = NSNumberFormatterStyle.DecimalStyle;
                        if let number = formatter.numberFromString(strUserID) {
                            let chatObj :  Chat = DatabaseHelper.getObjectIfExist("Chat", predicate: NSPredicate(format: "contactID == %@",number)) as! Chat
                            print(chatObj)
                            for i:Int in 0 ..< tableData.count {
                                let chatObjTemp : Chat = tableData.objectAtIndex(i) as! Chat
                                if chatObj == chatObjTemp {
                                    chatObjTemp.typing = "typing..."
                                    let indexPath : NSIndexPath = NSIndexPath(forRow: i, inSection: 0)
                                    tblChat.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
                                    let triggerTime = (Int64(NSEC_PER_SEC) * 3)
                                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, triggerTime), dispatch_get_main_queue(), { () -> Void in
                                        self.changeTypingStatus(chatObj)
                                    })
                                    break;
                                }
                            }
                        }
                        
                        
                    }
                    
                    
                    tblChat.reloadData()
                }
                if strCommad == "authorize" {
                    self.checkOnlineUserToSocket()
                }
            }else{
                appDelegate.subscribeUserToSocket()
            }
        }
        
        func websocketDidReceiveData(ws: WebSocket, data: NSData) {
            print("Received data: \(data.length)")
        }
        
        
        func changeTypingStatus(chatObj : Chat) {
            for i:Int in 0 ..< tableData.count {
                var chatObjTemp : Chat = tableData.objectAtIndex(i) as! Chat
                if chatObj == chatObjTemp {
                    chatObjTemp.typing = ""
                    var indexPath : NSIndexPath = NSIndexPath(forRow: i, inSection: 0)
                    tblChat.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
                    break;
                }
            }
        }
        
        
        func checkOnlineUserToSocket(){
            let userObj : User = DatabaseHelper.getObjectIfExist("User", predicate: NSPredicate(format: "isLoggedIn == 1")) as! User
            if userObj.token != nil {
                tableData = [DatabaseHelper .getAllData("Chat", offset: 0, predicate: nil, sortDescriptor: NSSortDescriptor(key: "messageID", ascending: false))]
                tableData = tableData.objectAtIndex(0) as! NSArray
                let arrayContact : NSMutableArray = NSMutableArray()
                for i:Int in 0 ..< tableData.count {
                    let chatObj : Chat = tableData.objectAtIndex(i) as! Chat
                    if chatObj.contactID != 0 {
                        let f = NSNumberFormatter()
                        f.numberStyle = .DecimalStyle
                        if chatObj.chatType == NSNumber(int: 0) {
                            let contactObj : Contact = DatabaseHelper.getObjectIfExist("Contact", predicate: NSPredicate(format: "identifier == %@", f.stringFromNumber(chatObj.contactID)!)) as! Contact
                            arrayContact.addObject(contactObj.identifier)
                        }else{
                            
                            let arrMembers : NSArray = DatabaseHelper.getAllData("GroupMember", offset: 0, predicate: NSPredicate(format: "groupID == %@", f.stringFromNumber(chatObj.contactID)!), sortDescriptor: nil)
                            for j:Int in 0 ..< arrMembers.count {
                                let groupObj : GroupMember = arrMembers.objectAtIndex(j) as! GroupMember
                                let contactObj : Contact = DatabaseHelper.getObjectIfExist("Contact", predicate: NSPredicate(format: "identifier == %@",groupObj.contactID! )) as! Contact
                                arrayContact.addObject(contactObj.identifier)
                                
                            }
                        }
                    }
                }
                
                let dataDic : NSMutableDictionary = NSMutableDictionary()
                dataDic.setValue("online", forKey: "command")
                dataDic.setValue(userObj.userID , forKey: "requester")
                dataDic.setValue(arrayContact , forKey: "users")
                
                let json : SBJSON = SBJSON()
                let strJson : String = json.stringWithObject(dataDic)
                appDelegate.socket.writeString(strJson)
            }else{
                Common.showAlert("Sorry", message: "Please try again")
            }
        }
        
        
        override func viewWillAppear(animated: Bool) {
            self.gettingChatArray()
            
            appDelegate.socket.delegate = self
            if appDelegate.socket.isConnected == true {
                self.checkOnlineUserToSocket()
                
            }
            rdv_tabBarController .setTabBarHidden(false, animated: true)
            
        }
        
        override func viewDidAppear(animated: Bool) {
            appDelegate.socket.delegate = self
            if appDelegate.socket.isConnected == true {
                self.checkOnlineUserToSocket()
                
            }
        }
        
        
        func gettingChatArray(){
            tableData = [DatabaseHelper .getAllData("Chat", offset: 0, predicate: nil, sortDescriptor: NSSortDescriptor(key: "messageID", ascending: false))]
            tableData = tableData.objectAtIndex(0) as! NSArray
            for i:Int in 0 ..< tableData.count {
                let chatObj : Chat = tableData.objectAtIndex(i) as! Chat
                if chatObj.contactID != 0 && chatObj.contactID.intValue != 0{
                    
                    let f = NSNumberFormatter()
                    f.numberStyle = .DecimalStyle
                    if chatObj.chatType == NSNumber(int: 0) {
                        
                        
                        let contactObj : Contact = DatabaseHelper.getObjectIfExist("Contact", predicate: NSPredicate(format: "identifier == %@", f.stringFromNumber(chatObj.contactID)!)) as! Contact
                        if contactObj.phone == ""{
                            DatabaseHelper.deleteObject(contactObj)
                            let bllService : BLLServices = BLLServices()
                            bllService.delegate = self;
                            let parameter : NSMutableDictionary = NSMutableDictionary()
                            parameter.setValue(f.stringFromNumber(chatObj.contactID)!, forKey: "contactID")
                            bllService.getContactDetail(parameter)
                        }else{
                            if chatObj.name == "" {
                                chatObj.name = contactObj.name
                            }
                            if chatObj.name == nil {
                                chatObj.name = contactObj.name
                            }
                        }
                        
                    }else{
                        let groupObj : Group = DatabaseHelper.getObjectIfExist("Group", predicate: NSPredicate(format: "identifier == %@", f.stringFromNumber(chatObj.contactID)!)) as! Group
                        if groupObj.name == nil {
                            let bllService : BLLServices = BLLServices()
                            bllService.delegate = self;
                            let parameter : NSMutableDictionary = NSMutableDictionary()
                            parameter.setValue(f.stringFromNumber(chatObj.contactID)!, forKey: "groupID")
                            bllService.getGroupDetail(parameter)
                        }else{
                            if chatObj.name == "" {
                                chatObj.name = groupObj.name
                            }
                            if chatObj.name == nil {
                                chatObj.name = groupObj.name
                            }
                        }
                        
                    }
                }else{
                    DatabaseHelper.deleteObject(chatObj)
                }
                
            }
            if tableData.count == 0 {
                lblNoContact.hidden = false
                tblChat.hidden = true
            }else{
                
                if lblNoContact != nil {
                    lblNoContact.hidden = true
                    
                }
                
                if tblChat != nil {
                    tblChat.hidden = false
                    
                }
                
                
            }
            if lblNoContact != nil {
                tblChat.reloadData()
            }else{
                self.performSelector(#selector(ChatViewController.reloadView), withObject: self, afterDelay: 1.0)
            }
        }
        
        
        
        func reloadView()  {
            
            if lblNoContact != nil {
                lblNoContact.hidden = true
                
            }
            
            if tblChat != nil {
                tblChat.hidden = false
                
            }
            
            if lblNoContact != nil {
                tblChat.reloadData()
                
            }else{
                self.performSelector(#selector(ChatViewController.reloadView), withObject: self, afterDelay: 1.0)
                
            }
        }
        
        /////////////////////**** Setting Up Navigation ******////////////
        
        func settingNavigation(){
            self.settingNavigationBg()
            self.settingNavigationTitle()
            self.settingNavigationLeftButton()
            self.settingNavigationRightButton()
        }
        
        //////**** Setting Navigation Background ****//////
        
        func settingNavigationBg(){
            self.navigationController?.navigationBar.translucent = false
            self.navigationController?.navigationBar.barTintColor = UIColor(red: 0.0/255.0, green: 115.0/255.0, blue: 181.0/255.0, alpha: 1.0)
            
        }
        
        //////**** Setting Navigation Title ****//////
        func settingNavigationTitle(){
            self.title = "Chats"
            self.navigationController!.navigationBar.titleTextAttributes = [
                NSFontAttributeName: UIFont(name: "OpenSans", size: 15.0)!,
                NSForegroundColorAttributeName :  UIColor.whiteColor()
                
                
            ]
        }
        
        
        //////**** Setting Navigation Left Button ****//////
        
        func settingNavigationLeftButton(){
            let button: UIButton = UIButton(type:UIButtonType.Custom)
            button.setImage(UIImage(named: "new-message-icon.png"), forState: UIControlState.Normal)
            button.addTarget(self, action: #selector(HomeViewController.btnActionNewMessage), forControlEvents: UIControlEvents.TouchUpInside)
            button.frame = CGRectMake(0, 0, 20, 21)
            let barButton = UIBarButtonItem(customView: button)
            self.navigationItem.leftBarButtonItem = barButton
            
        }
        
        
        //////**** Setting New Message Button Action ****//////
        
        func btnActionNewMessage(){
            print("Menu Pressed")
            if (self.tblChat.editing == true) {
                self.tblChat.setEditing(false, animated: true)
            }else{
                self.tblChat.setEditing(true, animated: true)
                
            }
        }
        
        func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
            if editingStyle == .Delete {
                let chatObj : Chat = tableData.objectAtIndex(indexPath.row) as! Chat
                let arrMessage : NSArray = DatabaseHelper.getAllData("Message", offset: 0, predicate: NSPredicate(format: "chat_id == %@",String(format: "%@",chatObj.contactID)), sortDescriptor: nil)
                DatabaseHelper.deleteObject(chatObj)
                
                for i:Int in 0 ..< arrMessage.count {
                    let messageObj : Message = arrMessage.objectAtIndex(i) as! Message
                    DatabaseHelper.deleteObject(messageObj)
                    DatabaseHelper.commitChanges()
                }
                self.gettingChatArray()
                
                
            }
        }
        
        
        //////**** Setting Navigation Left Button ****//////
        
        func settingNavigationRightButton(){
            let buttonNewGroup: UIButton = UIButton(type:UIButtonType.Custom)
            buttonNewGroup.setImage(UIImage(named: "new-group-icon.png"), forState: UIControlState.Normal)
            buttonNewGroup.addTarget(self, action: #selector(HomeViewController.btnActionNewGroup), forControlEvents: UIControlEvents.TouchUpInside)
            buttonNewGroup.frame = CGRectMake(0, 0, 40, 48)
            let buttonNewChat: UIButton = UIButton(type:UIButtonType.Custom)
            buttonNewChat.setImage(UIImage(named: "new-chat-icon.png"), forState: UIControlState.Normal)
            buttonNewChat.addTarget(self, action: #selector(HomeViewController.btnActionNewChat), forControlEvents: UIControlEvents.TouchUpInside)
            buttonNewChat.frame = CGRectMake(42, 0, 40, 48)
            
            
            let viewRight: UIView = UIView(frame: CGRectMake(0, 0, 85, 48))
            viewRight.addSubview(buttonNewGroup)
            viewRight.addSubview(buttonNewChat)
            
            let barButton = UIBarButtonItem(customView: viewRight)
            self.navigationItem.rightBarButtonItem = barButton
        }
        
        //////**** Setting New Group Button Action ****//////
        
        func btnActionNewGroup(){
            rdv_tabBarController .setTabBarHidden(true, animated: true)
            var newGroupVC : NewGroupViewController = NewGroupViewController()
            newGroupVC = NewGroupViewController(nibName: "NewGroupViewController", bundle: nil)
            self.navigationController?.pushViewController(newGroupVC, animated: true)
        }
        
        //////**** Setting New Chat Button Action ****//////
        
        func btnActionNewChat(){
            self.rdv_tabBarController.selectedIndex = 1
        }
        
        
        ////////////////////***** Intialize TableView *******//////////
        
        func intializeTableView(){
            
            let nibOne = UINib(nibName: "ChatTableViewCell", bundle: nil)
            tblChat.registerNib(nibOne, forCellReuseIdentifier: "ChatTableViewCellIdentifier")
            
        }
        
        func numberOfSectionsInTableView(tableView: UITableView) -> Int {
            return 1
        }
        
        func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return tableData.count
        }
        
        func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat{
            return 65
        }
        
        func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
            let cell:ChatTableViewCell = tblChat.dequeueReusableCellWithIdentifier("ChatTableViewCellIdentifier") as! ChatTableViewCell
            cell.selectionStyle = .None
            /////Getting Chat Object
            let chatObj : Chat!
            if tableData.count > indexPath.row {
                chatObj  = tableData.objectAtIndex(indexPath.row) as! Chat
            }else{
                return UITableViewCell()
            }
            /////Getting Contact Object
            if chatObj.contactID.intValue == 0 {
                return UITableViewCell()
            }
            let strContactIdentifier : String = String(format: "%@",chatObj.contactID)
            let strMessageIdentifier : String = String(format: "%@",chatObj.messageID)
            let strNumberofUnreadMessage :  String = String(format: "%@", chatObj.numberOfUnreadMessages)
            
            if strNumberofUnreadMessage == "0" {
                cell.btnBadge.badgeString = nil
            }else{
                cell.btnBadge.badgeString = strNumberofUnreadMessage
            }
            cell.btnBadge.badgeTextColor = UIColor.whiteColor()
            cell.btnBadge.badgeBackgroundColor = UIColor.redColor()
            cell.btnBadge.badgeEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0)
            var strImageName : String = String()
            if chatObj.chatType == NSNumber(int: 1) {
                let groupObj : Group = DatabaseHelper.getObjectIfExist("Group", predicate: NSPredicate(format: "identifier == %@", strContactIdentifier)) as! Group
                if groupObj.name != nil {
                    cell.lblContactName.text = groupObj.name
                }
                if groupObj.image != nil {
                    strImageName  = String(format: "%@",groupObj.image! )
                }
                
            }else{
                let contactObj : Contact = DatabaseHelper.getObjectIfExist("Contact", predicate: NSPredicate(format: "identifier == %@", strContactIdentifier)) as! Contact
                if contactObj.name == "" {
                    cell.lblContactName.text = contactObj.serverName
                }else{
                    cell.lblContactName.text = contactObj.name
                }
                if contactObj.active == "offline"{
                    if contactObj.last_seen != nil {
                        //                    let dateLastSeen : NSDate = Common.convertStringToDate(contactObj.last_seen!)
                        //                    cell.lblLastMessage.text = String(format: "Last seen : %@", Common.formatADate(dateLastSeen))
                    }
                }else{
                    cell.lblLastMessage.text = contactObj.active
                    
                }
                
                strImageName  = String(format: "%@",contactObj.image_id )
                
            }
            
            ////Populate Cell View
            ////Contact  Name
            
            if strMessageIdentifier != "0"  && strMessageIdentifier != ""{
                /////Chat Last Message
                let messageObj : Message = DatabaseHelper.getObjectIfExist("Message", predicate: NSPredicate(format: "identifier == %@", strMessageIdentifier)) as! Message
                
                
                //if (messageObj.message_type == [NSNumber numberWithInt:2]){
                
                if messageObj.message_type == NSNumber(integer: 0) {
                    if messageObj.sender == NSNumber(integer: 0) {
                        cell.lblLastMessage.text = messageObj.message
                    }else{
                        let strAESDecrypted : String = AESCrypt.decrptyAESKey(messageObj.aesEncrypted, privateKey: KPrivateKey)
                        
                        let decryptMessage : String  = AESCrypt.decrypt(messageObj.text, password: strAESDecrypted)
                        cell.lblLastMessage.text = decryptMessage
                    }
                    
                }else if messageObj.message_type == NSNumber(integer: 1){
                    cell.lblLastMessage.text = "Image"
                    
                }else if messageObj.message_type == NSNumber(integer: 2){
                    cell.lblLastMessage.text = "Audio"
                    
                }else if messageObj.message_type == NSNumber(integer: 3){
                    cell.lblLastMessage.text = "Video"
                    
                }
                
                
                
                
                
                //   cell.lblLastMessage.text = messageObj.text
                
                /////Chat Last Message Time
                if messageObj.text.isEmpty == true {
                    cell.lblLastMessage.text = ""
                    DatabaseHelper.deleteObject(messageObj)
                }else{
                    
                    if messageObj.message_type == NSNumber(integer: 0) {
                        
                        
                        if messageObj.sender == NSNumber(integer: 0) {
                            cell.lblLastMessage.text = messageObj.message
                        }else{
                            let strAESDecrypted : String = AESCrypt.decrptyAESKey(messageObj.aesEncrypted, privateKey: KPrivateKey)
                            
                            let decryptMessage : String  = AESCrypt.decrypt(messageObj.text, password: strAESDecrypted)
                            cell.lblLastMessage.text = decryptMessage
                        }
                    }else if messageObj.message_type == NSNumber(integer: 1){
                        cell.lblLastMessage.text = "Image"
                        
                    }else if messageObj.message_type == NSNumber(integer: 2){
                        cell.lblLastMessage.text = "Audio"
                        
                    }else if messageObj.message_type == NSNumber(integer: 3){
                        cell.lblLastMessage.text = "Video"
                        
                    }
                    // cell.lblLastMessage.text = messageObj.text
                    
                    cell.lblMessageTime.text = Common.formatADate(messageObj.date)
                }
                
            }else{
                cell.lblLastMessage.text = ""
            }
            
            var imgProfileAsync : AsyncImageView = AsyncImageView()
            imgProfileAsync.removeFromSuperview()
            imgProfileAsync = AsyncImageView(frame: CGRectMake(0, 0,cell.imgContactImage.frame.size.width, cell.imgContactImage.frame.size.width))
            if chatObj.chatType == NSNumber(int: 1) {
                if !strImageName.containsString("http://appillions.com/group-images") {
                    strImageName = String(format: "http://appillions.com/group-images/%@", strImageName)
                }
                if strImageName.containsString("http://appillions.com/group-images/default.png") {
                    strImageName = "http://appillions.com/profile-images/default.png"
                    
                }
            }else{
                if !strImageName.containsString("http://appillions.com/profile-images") {
                    strImageName = String(format: "http://appillions.com/profile-images/%@", strImageName)
                }
                
                
            }
            
            imgProfileAsync.loadImageFromURL(NSURL(string: strImageName))
            cell.imgContactImage.layer.cornerRadius = CGRectGetWidth(cell.imgContactImage.frame)/2.0
            cell.imgContactImage.clipsToBounds = true
            cell.imgContactImage.addSubview(imgProfileAsync)
            cell.lblContactName.text = chatObj.name
            if chatObj.typing != "" && chatObj.typing != nil {
                cell.lblLastMessage.text = chatObj.typing
            }
            
            return cell
        }
        
        func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
            rdv_tabBarController .setTabBarHidden(true, animated: true)
            
            var messageVC : ChatDetailViewController = ChatDetailViewController()
            messageVC = ChatDetailViewController(nibName: "ChatDetailViewController", bundle: nil)
            /////Getting Chat Object
            let chatObj : Chat = tableData.objectAtIndex(indexPath.row) as! Chat
            /////Getting Contact Object
            let strContactIdentifier : String = String(format: "%@",chatObj.contactID)
            if chatObj.chatType == NSNumber(int: 0){
                let contactObj : Contact = DatabaseHelper.getObjectIfExist("Contact", predicate: NSPredicate(format: "identifier == %@", strContactIdentifier)) as! Contact
                messageVC.contactObj = contactObj
                
            }else{
                let groupObj : Group  = DatabaseHelper.getObjectIfExist("Group", predicate: NSPredicate(format: "identifier == %@", strContactIdentifier)) as! Group
                messageVC.groupObj = groupObj
            }
            
            messageVC.chat = tableData.objectAtIndex(indexPath.row) as! Chat
            self.navigationController?.pushViewController(messageVC, animated: true)
            
        }
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
        func successWithData(data: AnyObject!, msg successMsg: String!, tag: Int32) {
            let tagService = AuthenticationParserType(rawValue: UInt32(tag))
            if tagService == GetContactDetail {
                self.gettingChatArray()
                MBProgressHUD.hideHUDForView(self.view, animated: true)
            }else if tagService == GetGroupDetail{
                self.gettingChatArray()
                MBProgressHUD.hideHUDForView(self.view, animated: true)
            }
            // (UIApplication.sharedApplication().delegate as! AppDelegate).setupViewControllers()
        }
        //////***** Failure *****////////
        func requestFailure(failureMsg: String!, tag: Int32) {
            Common.showAlert("Sorry", message: failureMsg)
        }
        
        //////***** Uploading *****////////
        func uploadProgress(progress: Float) {
            
        }
        
        
        /*
         // MARK: - Navigation
         
         // In a storyboard-based application, you will often want to do a little preparation before navigation
         override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
         // Get the new view controller using segue.destinationViewController.
         // Pass the selected object to the new view controller.
         }
         */
        
    }

//
//  ProfileViewController.swift
//  iwhispers
//
//  Created by Apple on 7/28/16.
//  Copyright © 2016 Appiskey. All rights reserved.
//

import UIKit
import Firebase
import FirebaseInstanceID
import FirebaseMessaging

class ProfileViewController: UIViewController,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextFieldDelegate,BaseBLLDelegate,UIGestureRecognizerDelegate {

    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var btnProfileImage: UIButton!
    @IBOutlet var btnSubmit: UIButton!
    @IBOutlet var txtName: UITextField!
    var isImageChanged : Bool = Bool()
    var isProfileEditing : Bool = Bool()
    var imgProfileAsync : AsyncImageView = AsyncImageView()
    let picker = UIImagePickerController()

    @IBOutlet var viewName: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        isImageChanged = false
        txtName.returnKeyType = .Done
        if isProfileEditing == true {
            self.lblTitle.text = "Update Profile"
            btnSubmit.setTitle("Update", forState: .Normal)
        }else{
            self.lblTitle.text = "Profile"
            btnSubmit.setTitle("Next", forState: .Normal)
        }
        self.populateView()
        
        // Do any additional setup after loading the view.
    }
    
    
    ///////////////******* Populate View ******////////////////////////
    
    func populateView(){
        let userObj : User = DatabaseHelper.getObjectIfExist("User", predicate: NSPredicate(format: "isLoggedIn == 1")) as! User
        txtName.text = userObj.name
        if userObj.userImage != nil {
            if userObj.userImage != kDefaultURL {
                imgProfileAsync.removeFromSuperview()
                /////Setting Profile Image///////
                imgProfileAsync = AsyncImageView(frame: CGRectMake(0, 0,btnProfileImage.frame.size.width, self.btnProfileImage.frame.size.width))
                imgProfileAsync.userInteractionEnabled = true
                let tap = UITapGestureRecognizer(target: self, action: #selector(ProfileViewController.btnActionProfileImage(_:)))
                tap.delegate = self
                imgProfileAsync.addGestureRecognizer(tap)
                imgProfileAsync.loadImageFromURL(NSURL(string: userObj.userImage!))
                btnProfileImage.layer.cornerRadius = CGRectGetWidth(btnProfileImage.frame)/2.0
                btnProfileImage.clipsToBounds = true
                btnProfileImage.addSubview(imgProfileAsync)
            }
        }

    }
    
    ////////////*******UITextField Delegates ********///////////////
    func textFieldDidBeginEditing(textField: UITextField) {
        self.TextSlider(viewName.frame)
    }
    
    
    func textFieldShouldEndEditing(textField: UITextField) -> Bool {
        UIView.animateWithDuration(0.5, animations: { () -> Void in
            self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)
        })
        return true
    }
    
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        txtName.resignFirstResponder()
        return true
    }
    
    ////////////*****Touches began delegate function ******///////////
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        txtName.resignFirstResponder()
    }
    
    
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.navigationBarHidden = true
        if isProfileEditing == true{
            rdv_tabBarController .setTabBarHidden(true, animated: true)
   
        }

    }
    override func viewWillDisappear(animated: Bool) {
        self.navigationController?.navigationBarHidden = false
    }
    
    
    @IBAction func btnActionBack(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func btnActionProfileImage(sender: AnyObject) {
        let strCamera = "Take a picture"
        let strPhotoLibrary = "Upload photo from album"
        let strCancel = "Cancel"
        let actionSheet = UIActionSheet(title: "Select from below", delegate: self, cancelButtonTitle: strCancel, destructiveButtonTitle:nil, otherButtonTitles:strCamera ,strPhotoLibrary)
        actionSheet.showInView(self.view)
    }
    
    //////**** Validate Login *****///////
    
    func isValidate() -> Bool {
        
        if txtName.text == "" {
            Common.showAlert("Alert", message: "Please enter your name")
            return false
        }

        
        return true
    }

    
    ///////////******Get Profile Service **********///////////
    func getProfile()  {
        let bllService : BLLServices = BLLServices()
        bllService.delegate = self
        let dic : NSMutableDictionary = NSMutableDictionary()
        bllService.getProfile(dic)

    }
    
    @IBAction func btnActionNext(sender: AnyObject) {
        
//        if isProfileEditing == true{
//            self.navigationController?.popViewControllerAnimated(true)
//        }else{
//            (UIApplication.sharedApplication().delegate as! AppDelegate).setupViewControllers()
//            
//        }

        
        let isValidate : Bool = self.isValidate()
        if isValidate == false {
            return
        }
        if InternetCheck.IsConnected() {
            MBProgressHUD.showHUDAddedTo(self.view, animated: true)
            let bllService : BLLServices = BLLServices()
            bllService.delegate = self
            let dic : NSMutableDictionary = NSMutableDictionary()
            dic.setValue(txtName.text, forKey: "name")
            dic.setValue(KPublicKey, forKey: "public_key")
            let refreshedToken = FIRInstanceID.instanceID().token()!
            dic.setValue(refreshedToken, forKey: "fcm")
            let appDelegate : AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            let devToken = appDelegate.devToken
            if devToken == "" {
                dic.setValue("android", forKey: "device")

            }else{
                dic.setValue(devToken, forKey: "apns")
                dic.setValue("iPhone", forKey: "device")

            }

            if isImageChanged == true {
                dic.setValue("PUT", forKey: "_method")

                let imageData : NSData = UIImageJPEGRepresentation((btnProfileImage.imageView?.image)!, 0.35)!
                bllService.profileUpdate(dic, image: imageData)
            }else{
                bllService.profileUpdate(dic, image: nil)
            }
          
        }else{
            Common.showAlert("Alert", message: "Please check your internet connection");
        }
        
    }
    
    
    
    //// MARK:  ActionSheet Methods
    ////////////////////******* ActionSheet Delegates ******* ///////////////
    func actionSheet(actionSheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int)
    {
        switch buttonIndex{
            
        case 0:
            NSLog("Cancel");
            break;
        case 1:
            NSLog("Take Picture");
            self.openCamera()
            break;
        case 2:
            NSLog("Upload Photo");
            self.openGallary()
            break;
        default:
            NSLog("Default");
            break;
        }
    }
    //// MARK:  PhotoLibrary Methods
    ////////////////////******* UICollectionView Delegates ******* ///////////////
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera))
        {
            picker.sourceType = UIImagePickerControllerSourceType.Camera
            picker.delegate = self
            picker.allowsEditing = true
            self .presentViewController(picker, animated: true, completion: nil)
        }
        else
        {
            let alertWarning = UIAlertView(title:"Warning", message: "Error accessing Camera", delegate:nil, cancelButtonTitle:"OK", otherButtonTitles:"")
            alertWarning.show()
        }
    }
    func openGallary()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.SavedPhotosAlbum){
            picker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
            picker.delegate = self
            picker.allowsEditing = true
            self.presentViewController(picker, animated: true, completion: nil)
        } else
        {
            let alertWarning = UIAlertView(title:"Warning", message: "Error accessing Photo Library", delegate:nil, cancelButtonTitle:"OK", otherButtonTitles:"")
            alertWarning.show()
            
        }
    }
    
    func imagePickerController(picker: UIImagePickerController!, didFinishPickingImage image: UIImage!, editingInfo: NSDictionary!) {
        
        picker.dismissViewControllerAnimated(true, completion: nil)
        let tempImage : UIImage = editingInfo[UIImagePickerControllerOriginalImage] as! UIImage
        btnProfileImage.setImage(tempImage, forState: .Normal)
        btnProfileImage.layer.cornerRadius = CGRectGetWidth(btnProfileImage.frame)/2.0
        btnProfileImage.clipsToBounds = true
        isImageChanged = true
        imgProfileAsync.removeFromSuperview()
        
    }

    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /////////////////////****** Getting Keyboard Height *****////////////////////
    
    func getKeyboardHeight() ->(Int){
        var KeyboardHeight :Int
        
        KeyboardHeight = 273;
        
        return KeyboardHeight;
    }
    
    /////////////////////****** Setting Slider For UITextField *****////////////////////
    
    func TextSlider(viewFrame:CGRect) {
        
        let keyboardFrame = CGRectMake(0, 0, UIScreen.mainScreen().bounds.size.width, CGFloat(self.getKeyboardHeight()));
        
        var viewDifference : CGFloat;
        
        let keyBoaryYAxis:CGFloat = UIScreen.mainScreen().bounds.size.height - keyboardFrame.size.height
        
        viewDifference = keyBoaryYAxis - (viewFrame.origin.y + viewFrame.size.height)
        
        
        if (viewDifference<0) {
            UIView.animateWithDuration(0.5, animations: { () -> Void in
                self.view.frame = CGRectMake(0, viewDifference, self.view.frame.size.width, self.view.frame.size.height)
            })
        }
    }
    
    /////////////***** Base BLL Delegates ****/////////
    //////***** Success *****////////
    func successWithData(data: AnyObject!, msg successMsg: String!, tag: Int32) {
        let tagService = AuthenticationParserType(rawValue: UInt32(tag))

        if tagService == SetProfile {
            self.getProfile()
        }else if tagService == GetProfile{
            MBProgressHUD.hideHUDForView(self.view, animated: true)
            let userObj : User = DatabaseHelper.getObjectIfExist("User", predicate: NSPredicate(format: "isLoggedIn == 1")) as! User
            
            DatabaseHelper.commitChanges()
            if isProfileEditing == true{
                self.navigationController?.popViewControllerAnimated(true)
            }else{
                (UIApplication.sharedApplication().delegate as! AppDelegate).setupViewControllers()
   
            }


        }
       
    }
    //////***** Failure *****////////
    func requestFailure(failureMsg: String!, tag: Int32) {
        MBProgressHUD.hideHUDForView(self.view, animated: true)
        Common.showAlert("Sorry", message: failureMsg)
    }
    
    //////***** Uploading *****////////
    func uploadProgress(progress: Float) {
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

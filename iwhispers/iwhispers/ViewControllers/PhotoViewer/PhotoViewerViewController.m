//
//  PhotoViewerViewController.m
//  iwhispers
//
//  Created by Wasif Iqbal on 12/28/16.
//  Copyright © 2016 Appiskey. All rights reserved.
//

#import "PhotoViewerViewController.h"

@interface PhotoViewerViewController ()

@end

@implementation PhotoViewerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self populateData];
    
    self.mainScrollView.minimumZoomScale = 1.0;
    self.mainScrollView.maximumZoomScale = 3.0;
    self.mainScrollView.contentSize = self.imageView.frame.size;
    self.mainScrollView.delegate = self;
    
    
    UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTap:)];
    
    [doubleTap setNumberOfTapsRequired:2];
    
    [self.mainScrollView addGestureRecognizer:doubleTap];
    
    [self setupLeftButton];
    // Do any additional setup after loading the view from its nib.
}


- (void)handleDoubleTap:(UIGestureRecognizer *)gestureRecognizer {
    
    if(self.mainScrollView.zoomScale > self.mainScrollView.minimumZoomScale)
        [self.mainScrollView setZoomScale:self.mainScrollView.minimumZoomScale animated:YES];
    else
        [self.mainScrollView setZoomScale:self.mainScrollView.maximumZoomScale animated:YES];
    
}


- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return self.imageView;
}


- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(CGFloat)scale
{
}

-(void)setupLeftButton{
    UIButton *backBtn =  [UIButton buttonWithType:UIButtonTypeCustom];
    [backBtn setImage:[UIImage imageNamed:@"arrow-white.png"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backVC) forControlEvents:UIControlEventTouchUpInside];
    [backBtn setFrame:CGRectMake(0, 0,25, 25)];
    
    UIView *rightBarButtonItems = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    [rightBarButtonItems addSubview:backBtn];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightBarButtonItems];
    
}

-(void)backVC{
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)populateData{
    
    
    [self.mainImageView addSubview:self.imageView];
//    
//    VIPhotoView *photoImageViewer = [[VIPhotoView alloc]initWithFrame:self.view.bounds andImage:self.imageView];
//    photoImageViewer.autoresizingMask = (1 << 6) -1;
//    [self.view addSubview:photoImageViewer];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

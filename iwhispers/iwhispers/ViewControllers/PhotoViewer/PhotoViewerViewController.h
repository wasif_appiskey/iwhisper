//
//  PhotoViewerViewController.h
//  iwhispers
//
//  Created by Wasif Iqbal on 12/28/16.
//  Copyright © 2016 Appiskey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotoViewerViewController : UIViewController<UIScrollViewDelegate>
@property (weak, nonatomic) AsyncImageView *imageView;

@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;

@property (weak, nonatomic) IBOutlet UIImageView *mainImageView;

@end

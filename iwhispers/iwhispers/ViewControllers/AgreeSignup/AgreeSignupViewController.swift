//
//  AgreeSignupViewController.swift
//  iwhispers
//
//  Created by Apple on 6/20/16.
//  Copyright © 2016 Appiskey. All rights reserved.
//

import UIKit

class AgreeSignupViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(animated: Bool) {
        self.navigationController?.navigationBarHidden = true
    }
    
    override func viewWillDisappear(animated: Bool) {
        self.navigationController?.navigationBarHidden = false
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    ////////****** Button Action for Agree to Signup *****//////////
    
    @IBAction func btnActionAgree(sender: AnyObject) {

        ///// Going to View Controller Your Phone Number /////////
        var yourPhoneNumberVC : YourPhoneNumberViewController = YourPhoneNumberViewController()
        yourPhoneNumberVC = YourPhoneNumberViewController(nibName: "YourPhoneNumberViewController", bundle: nil)
        self.navigationController?.pushViewController(yourPhoneNumberVC, animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

//
//  AccountViewController.swift
//  iwhispers
//
//  Created by Apple on 6/30/16.
//  Copyright © 2016 Appiskey. All rights reserved.
//

import UIKit

class AccountViewController: UIViewController,UIAlertViewDelegate, BaseBLLDelegate{

    @IBOutlet var tblSetting: UITableView!
    let arrImage = ["change-number-icon.png", "delete-my-account-icon.png",]
    let arrTitle = ["Change Number", "Delete My Account" ]

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.intializeTableView()
        self.settingNavigation()
        // Do any additional setup after loading the view.
    }

    
    /////////////////////**** Setting Up Navigation ******////////////
    
    func settingNavigation(){
        self.settingNavigationBg()
        self.settingNavigationTitle()
        self.settingNavigationLeftButton()
    }
    
    //////**** Setting Navigation Background ****//////
    
    func settingNavigationBg(){
        self.navigationController?.navigationBar.translucent = false
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 0.0/255.0, green: 115.0/255.0, blue: 181.0/255.0, alpha: 1.0)
        
    }
    
    //////**** Setting Navigation Title ****//////
    func settingNavigationTitle(){
        self.title = "Account"
        self.navigationController!.navigationBar.titleTextAttributes = [
            NSFontAttributeName: UIFont(name: "OpenSans", size: 15.0)!,
            NSForegroundColorAttributeName :  UIColor.whiteColor()
            
            
        ]
    }
    
    
    //////**** Setting Navigation Left Button ****//////
    
    func settingNavigationLeftButton(){
        let button: UIButton = UIButton(type:UIButtonType.Custom)
        button.setImage(UIImage(named: "arrow-white.png"), forState: UIControlState.Normal)
        button.addTarget(self, action: #selector(HomeViewController.btnActionNewMessage), forControlEvents: UIControlEvents.TouchUpInside)
        button.frame = CGRectMake(0, 0, 20, 21)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    
    //////**** Setting New Message Button Action ****//////
    
    func btnActionNewMessage(){
        print("Menu Pressed")
        self.navigationController?.popViewControllerAnimated(true)
        
    }
    
    
    ////////////////////***** Intialize TableView *******//////////
    
    func intializeTableView(){
        
        let nibOne = UINib(nibName: "SettingTableViewCell", bundle: nil)
        tblSetting.registerNib(nibOne, forCellReuseIdentifier: "SettingTableViewCellIdentifier")
        
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrImage.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat{
        return 53
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:SettingTableViewCell = tblSetting.dequeueReusableCellWithIdentifier("SettingTableViewCellIdentifier") as! SettingTableViewCell
        cell.selectionStyle = .None
        cell.lblSetting.text = arrTitle[indexPath.row]
        cell.imgSetting.image = UIImage(named: arrImage[indexPath.row])
        return cell
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row == 0 {
            var yourPhoneNumberVC : YourPhoneNumberViewController = YourPhoneNumberViewController()
            yourPhoneNumberVC = YourPhoneNumberViewController(nibName: "YourPhoneNumberViewController", bundle: nil)
            yourPhoneNumberVC.isEditNumber = true
            self.navigationController?.pushViewController(yourPhoneNumberVC, animated: true)

        }else if indexPath.row == 1 {
            let alert : UIAlertView = UIAlertView(title: "", message: "Are you sure you want to delete your account ?", delegate: self, cancelButtonTitle: "No", otherButtonTitles: "Yes")
            alert.tag = 100
            alert.show()
        }
    }
    
    
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        if alertView.tag == 100 {
            let buttonTitle = alertView.buttonTitleAtIndex(buttonIndex)
            print("\(buttonTitle) pressed")
            if buttonTitle == "No" {
                
            }else if buttonTitle == "Yes"{
                self.deleteAccountService()
            }
        }
    }
    
    func deleteAccountService() {
        if InternetCheck.IsConnected() {
            MBProgressHUD.showHUDAddedTo(self.view, animated: true)
            let bllService : BLLServices = BLLServices()
            bllService.delegate = self
            let dic : NSMutableDictionary = NSMutableDictionary()
            dic.setValue("delete", forKey: "_method")
            bllService.accountDelete(dic)
        }else{
            Common.showAlert("Alert", message: "Please check your internet connection");
        }
        
    }
    
    
    /////////////***** Base BLL Delegates ****/////////
    //////***** Success *****////////
    func successWithData(data: AnyObject!, msg successMsg: String!, tag: Int32) {
        let tagService = AuthenticationParserType(rawValue: UInt32(tag))
        
        if tagService == DeleteAccount {
            MBProgressHUD.hideHUDForView(self.view, animated: true)

            DatabaseHelper.deleteAllObjectsOfEntity("Chat", predicate: nil)
            DatabaseHelper.deleteAllObjectsOfEntity("Message", predicate: nil)
            DatabaseHelper.deleteAllObjectsOfEntity("Group", predicate: nil)
            DatabaseHelper.deleteAllObjectsOfEntity("GroupMember", predicate: nil)
            DatabaseHelper.deleteAllObjectsOfEntity("Contact", predicate: nil)
            DatabaseHelper.deleteAllObjectsOfEntity("User", predicate: nil)
            (UIApplication.sharedApplication().delegate as! AppDelegate).gotoAgreeViewController()

        }
    }
    //////***** Failure *****////////
    func requestFailure(failureMsg: String!, tag: Int32) {
        MBProgressHUD.hideHUDForView(self.view, animated: true)
        Common.showAlert("Sorry", message: failureMsg)
    }
    
    //////***** Uploading *****////////
    func uploadProgress(progress: Float) {
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

//
//  InviteViewController.swift
//  iwhispers
//
//  Created by Wasif Iqbal on 12/27/16.
//  Copyright © 2016 Appiskey. All rights reserved.
//

import UIKit
import MessageUI

class InviteViewController: UIViewController,MFMessageComposeViewControllerDelegate {

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblPhoneNumber: UILabel!
    var contactObj:Contact!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupData()
        self.settingNavigation()
        // Do any additional setup after loading the view.
    }
    
    /////////////////////**** Setting Up Navigation ******////////////
    
    func settingNavigation(){
        self.settingNavigationBg()
        self.settingNavigationTitle()
        self.settingNavigationLeftButton()
    }
    
    //////**** Setting Navigation Background ****//////
    
    func settingNavigationBg(){
        self.navigationController?.navigationBar.translucent = false
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 0.0/255.0, green: 115.0/255.0, blue: 181.0/255.0, alpha: 1.0)
        
    }
    
    //////**** Setting Navigation Title ****//////
    func settingNavigationTitle(){
        self.title = "Contact Info"
        self.navigationController!.navigationBar.titleTextAttributes = [
            NSFontAttributeName: UIFont(name: "OpenSans", size: 15.0)!,
            NSForegroundColorAttributeName :  UIColor.whiteColor()
            
            
        ]
    }
    
    
    //////**** Setting Navigation Left Button ****//////
    
    func settingNavigationLeftButton(){
        let button: UIButton = UIButton(type:UIButtonType.Custom)
        button.setImage(UIImage(named: "arrow-white.png"), forState: UIControlState.Normal)
        button.addTarget(self, action: #selector(HomeViewController.btnActionNewMessage), forControlEvents: UIControlEvents.TouchUpInside)
        button.frame = CGRectMake(0, 0, 20, 21)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        
    }
    
    
    //////**** Setting New Message Button Action ****//////
    
    func btnActionNewMessage(){
      self.navigationController?.popViewControllerAnimated(true)
    }

    
    func messageComposeViewController(controller: MFMessageComposeViewController, didFinishWithResult result: MessageComposeResult) {
        //... handle sms screen actions
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    func setupData(){
        lblName.text = contactObj.name
        lblPhoneNumber.text = contactObj.phone
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnActionInvite(sender: AnyObject) {
        if (MFMessageComposeViewController.canSendText()) {
            let controller = MFMessageComposeViewController()
            controller.body = "Join iWhispers http://facebook.com"
            controller.recipients = [contactObj.phone]
            controller.messageComposeDelegate = self
            self.presentViewController(controller, animated: true, completion: nil)
        }
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

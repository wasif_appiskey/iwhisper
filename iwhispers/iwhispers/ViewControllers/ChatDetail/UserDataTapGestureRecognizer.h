//
//  UserDataTapGestureRecognizer.h
//  iwhispers
//
//  Created by Wasif Iqbal on 12/26/16.
//  Copyright © 2016 Appiskey. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserDataTapGestureRecognizer : UISwipeGestureRecognizer
@property (nonatomic, strong) id userData;
@end

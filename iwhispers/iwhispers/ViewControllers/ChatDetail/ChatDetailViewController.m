//
//  ChatDetailViewController.m
//  iwhispers
//
//  Created by Apple on 6/29/16.
//  Copyright © 2016 Appiskey. All rights reserved.
//

#import "ChatDetailViewController.h"
#import "ChatDetailTableViewCell.h"
#import "Inputbar.h"
#import "iwhispers-Swift.h"
#import "DAKeyboardControl.h"
#import "GroupChatDetalViewCell.h"
#import "BLLServices.h"
#import "SDAVAssetExportSession.h"
#import "MessageGateway.h"
#import "AudioMessageViewCell.h"
#import "iwhispers-Swift.h"
#import "VideoMessageViewCell.h"
#import "AZSoundManager.h"
#import "AESCrypt.h"
#import "NSData+Base64.h"
#import "NSData+CommonCrypto.h"
#import "NSString+Base64.h"
#import "RDVTabBarController.h"
#import "GroupAudioViewCell.h"
#import "GroupVideoViewCell.h"
#import "GroupMessageSeenViewController.h"
#import "UserDataTapGestureRecognizer.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "BRImagePicker.h"
#import "BR_ImageInfo.h"
#import "BRPreviewViewController.h"
#import "QBImagePicker.h"


@import Firebase;
@import FirebaseInstanceID;
@import FirebaseMessaging;
@import Starscream;

@interface ChatDetailViewController ()<WebSocketDelegate,InputbarDelegate,UITableViewDataSource,UITableViewDelegate,RDVTabBarDelegate,BaseBLLDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,QBImagePickerControllerDelegate>{
    NSTimer *timer;
    UIImagePickerController *imagePicker;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet Inputbar *inputbar;
@property (weak, nonatomic) AppDelegate *appDelegate;
@property (strong, nonatomic) MessageGateway *gateway;
@property (nonatomic, strong) NSArray *selectedAssetsArray;


@end


@implementation ChatDetailViewController

@synthesize appDelegate;




- (void)viewDidLoad {
    [super viewDidLoad];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    appDelegate.socket.delegate = self;
    [appDelegate subscribeUserToSocket];
    [self setInputbar];
    [self setTableView];
    [self setupNavigation];
    [self settingTapGesture];
    [self dismissNotification];
   
   //  [self setTest];
    // [[[[[self.view subviews]objectAtIndex:1]subviews]objectAtIndex:1]removeFromSuperview ];
    
    // Do any additional setup after loading the view from its nib.
}

-(void)checkOnlineUserToSocket{
    User *userObj = (User *)[DatabaseHelper getObjectIfExist:@"User" predicate:[NSPredicate predicateWithFormat:@"isLoggedIn == 1"]];
    if (userObj.token != nil) {
        NSMutableArray *arrContact = [[NSMutableArray alloc]init];
        NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
        f.numberStyle = NSNumberFormatterDecimalStyle;
        if ([_chat.chatType isEqualToNumber:[NSNumber numberWithInt:0]]) {
            [arrContact addObject:_contactObj.identifier];
        }else{
            BOOL isUserExist  = false;

            @try {
            
           // NSNumber *contactID = 0;
          //  contactID = [f numberFromString:_chat.identifier];
            NSArray *arrGroupMember = [DatabaseHelper getAllData:@"GroupMember" offset:0 predicate:[NSPredicate predicateWithFormat:@"groupID == %@",self.groupObj.identifier] sortDescriptor:nil];
            for (int i = 0; i<arrGroupMember.count; i++) {
                GroupMember *groupMemberObj = (GroupMember*)[arrGroupMember objectAtIndex:i];
                Contact *contactTempObj = (Contact*)[DatabaseHelper getObjectIfExist:@"Contact" predicate:[NSPredicate predicateWithFormat:@"identifier == %@",[NSString stringWithFormat:@"%@",groupMemberObj.contactID]]];
                if ([userObj.userID isEqualToNumber:groupMemberObj.contactID]) {
                    isUserExist = true;
                }
                [arrContact addObject:contactTempObj.identifier];
            }
                
            } @catch (NSException *exception) {
                NSLog(@"%@",exception);
            }
            if (isUserExist == false) {
                self.inputbar.hidden = true;
            }else{
                self.inputbar.hidden = false;
            }
        }
        
        NSMutableDictionary *dataDic = [[NSMutableDictionary alloc]init];
        [dataDic setValue:@"online" forKey:@"command"];
        [dataDic setValue:userObj.userID forKey:@"requester"];
        [dataDic setValue:arrContact forKey:@"users"];
        
        SBJSON *json = [[SBJSON alloc]init];
        NSString *strJson = [json stringWithObject:dataDic];
        appDelegate.socket.delegate = self;
        [appDelegate.socket writeString:strJson completion:nil];
    }else{
        [Common showAlert:@"Sorry" message:@"Please try again"];
    }
}





-(void)setupTimer{
    
    
    timer = [NSTimer scheduledTimerWithTimeInterval: 60.0 target: self
                                                    selector: @selector(timerFired:) userInfo: nil repeats: YES];

}

- (void)timerFired:(NSTimer*)theTimer{
    NSLog(@"Time is UP and AWAY");
    [self gettingUnSentMessage];
}

/////*****  Web Socket Delegate *******/////////////
-(void)websocketDidConnect:(WebSocket *)socket{
    if (appDelegate.socket.isConnected == YES) {
        [appDelegate subscribeUserToSocket];
    }
}

-(void)websocketDidDisconnect:(WebSocket *)socket error:(NSError *)error{
    if (error != nil) {
        NSLog(@"websocket is disconnected : %@",error.localizedDescription);
    }else{
        NSLog(@"websocket disconnected");
    }
    [appDelegate.socket connect];
}

-(void)websocketDidReceiveData:(WebSocket *)socket data:(NSData *)data{
    
}

-(void)websocketDidReceiveMessage:(WebSocket *)socket text:(NSString *)text{
    NSLog(text);
    SBJSON *json = [[SBJSON alloc]init];
    NSDictionary *dicJson = [[NSDictionary alloc]init];
    dicJson = (NSDictionary *)[json objectWithString:text];
    NSString *strStatus = [[NSString alloc]init];
    if ([dicJson valueForKey:@"status"] != nil) {
        strStatus = (NSString *)[dicJson valueForKey:@"status"];
    }
    NSString *strCommad = [[NSString alloc]init];
    if ([dicJson valueForKey:@"command"] != nil) {
        strCommad = (NSString *)[dicJson valueForKey:@"command"];
    }
    
    
    if ([strStatus isEqualToString:@"success"]) {
        NSDictionary *dataDic = [[NSDictionary alloc]init];
        if ([strCommad isEqualToString:@"online"]) {
            dataDic = (NSDictionary *)[dicJson valueForKey:@"data"];
            NSArray *dataArr = [[NSArray alloc]init];
            dataArr = (NSArray *)[dataDic valueForKey:@"online"];
            [appDelegate udpateContactOnline:dataArr];
            if ([self.chat.chatType isEqualToNumber:[NSNumber numberWithInt:0]]) {
                for (int i = 0; i<dataArr.count; i++) {
                    NSString *strContactID = (NSString *)[dataArr objectAtIndex:i];
                    if ([strContactID isEqualToString:self.contactObj.identifier]) {
                        [self settingTitleSubtitle:self.contactObj.name subtitle:@"online"];
                    }
                }
            }
            
        }
        if ([strCommad isEqualToString:@"offline"]) {
            dataDic = (NSDictionary *)[dicJson valueForKey:@"data"];
            NSArray *dataArr = [[NSArray alloc]init];
            dataArr = (NSArray *)[dataDic valueForKey:@"offline"];
            [appDelegate udpateContactOffline:dataArr];
            if ([self.chat.chatType isEqualToNumber:[NSNumber numberWithInt:0]]) {
                for (int i = 0; i<dataArr.count; i++) {
                    NSString *strContactID = (NSString *)[dataArr objectAtIndex:i];
                    if ([strContactID isEqualToString:self.contactObj.identifier]) {
                        [self setupNavigationTitle];
                    }
                }
            }
        }
        if ([strCommad isEqualToString:@"last_seen"]) {
            dataDic = (NSDictionary *)[dicJson valueForKey:@"data"];
            NSDictionary *dataDic1 = [[NSDictionary alloc]init];
            dataDic1 = (NSDictionary *)[dataDic valueForKey:@"last_seen"];
            [appDelegate udpateContactLastSeen:dataDic1];
            [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(setupNavigationTitle) object:nil];
            [self performSelector:@selector(setupNavigationTitle) withObject:self afterDelay:1.0];
            
        }
        if ([strCommad isEqualToString:@"typing"]) {
           dataDic = (NSDictionary *)[dicJson valueForKey:@"data"];
            NSString *strUserID = [[NSString alloc]init];
            strUserID = (NSString *)[dataDic valueForKey:@"user_id"];
            if ([self.chat.chatType isEqualToNumber: [NSNumber numberWithInt:0]]) {
            if ([self.contactObj.identifier  isEqualToString:strUserID]) {
                [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(setupNavigationTitle) object:nil];
                [self settingTitleSubtitle:self.contactObj.name subtitle:@"typing..."];
                [self performSelector:@selector(setupNavigationTitle) withObject:self afterDelay:1.0];

                NSLog(@"This user is typing");
            }
            }else{
                NSString *strGroupID = [[NSString alloc]init];
                NSString *strCurrentGroupID = [[NSString alloc]init];
                strCurrentGroupID = [self.chat.contactID stringValue];
                if ([dataDic valueForKey:@"group_id"] != nil) {
                    User *userObj = (User *)[DatabaseHelper getObjectIfExist:@"User" predicate:[NSPredicate predicateWithFormat:@"isLoggedIn == 1"]];

                    strGroupID = (NSString *)[[dataDic valueForKey:@"group_id"] stringValue];
                    if ([strCurrentGroupID  isEqualToString:strGroupID]) {
                        if (![[userObj.userID stringValue] isEqualToString:strUserID]) {
                            Contact *contactObjNew = (Contact *)[DatabaseHelper getObjectIfExist:@"Contact" predicate:[NSPredicate predicateWithFormat:@"identifier == %@",strUserID]];
                        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(setupNavigationTitle) object:nil];
                        [self settingTitleSubtitle:_groupObj.name subtitle:[NSString stringWithFormat:@"%@ is typing",contactObjNew.name]];
                        [self performSelector:@selector(setupNavigationTitle) withObject:self afterDelay:1.0];
                        NSLog(@"This user is typing");
                        }
                    }
                }

            }
        }
    }else{
        [appDelegate subscribeUserToSocket];
    }

}

-(void)settingTapGesture{
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(keyBoardDissmiss:)];
    [self.tableView addGestureRecognizer:tapRecognizer];
    self.tableView.tag=2;
}


-(void)dismissNotification{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveDissmissNotification:)
                                                 name:@"DismissKeyboard"
                                               object:nil];
}


- (void) receiveDissmissNotification:(NSNotification *) notification
{
    if ([[notification name] isEqualToString:@"DismissKeyboard"]){
        [self.inputbar.textView resignFirstResponder];

    }
}



-(void)keyBoardDissmiss:(UITapGestureRecognizer*)sender {
    {
        if(sender.view.tag==2) {
            [self.inputbar.textView resignFirstResponder];

        }
    }
}

//////////////**** Fetching Chat Object from Core Data ******/////////

-(void)gettingChatFromDB{
    /////Convert Contact Identifier from String to NSNumber
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    NSNumber *contactID = 0;
  
    //////Getting  Object for this Chat
    if (_contactObj != nil) {
        contactID = [f numberFromString:_contactObj.identifier];
        _chat =  (Chat *)[DatabaseHelper getObjectIfExist:@"Chat" predicate:[NSPredicate predicateWithFormat:@"contactID == %@",contactID]];
        Contact *contactObj = (Contact *)[DatabaseHelper getObjectIfExist:@"Contact" predicate:[NSPredicate predicateWithFormat:@"identifier == %@",contactID]];
        _chat.name = contactObj.name;
        [_chat setChatType:[NSNumber numberWithInt:0]];
    }else{
        _chat =  (Chat *)[DatabaseHelper getObjectIfExist:@"Chat" predicate:[NSPredicate predicateWithFormat:@"contactID == %@",_groupObj.identifier]];
        [_chat setChatType:[NSNumber numberWithInt:1]];
        _chat.name = _groupObj.name;
        contactID = _groupObj.identifier;
    }
    [_chat setNumberOfUnreadMessages:[NSNumber numberWithInt:0]];
    [_chat setContactID:contactID];
    [DatabaseHelper commitChanges];
    NSArray *allMessages = [DatabaseHelper getAllData:@"Message" offset:0 predicate:[NSPredicate predicateWithFormat:@"chat_id == %@",contactID] sortDescriptor:nil];
    for (int i = 0; i<allMessages.count; i++) {
        [self.tableArray addObject:(Message *)[allMessages objectAtIndex:i]];
    }
    [self.tableView reloadData];
    [self tableViewScrollToBottomAnimated:YES];
}

-(void)showAlert{

    NSLog(@"meow");
}

-(void)viewWillAppear:(BOOL)animated
{
    if ([_chat.chatType isEqualToNumber:[NSNumber numberWithInt:1]]) {
        [self callingGroupDetail];
    }else{
        [self callProfileInfo];
    }
    self.tableArray = [[TableArray alloc]init];
    [self gettingChatFromDB];
    [self settingMessagesToSeen];
    [self gettingUnSentMessage];
    [self setupTimer];
    appDelegate.socket.delegate = self;
    if (appDelegate.socket.isConnected != YES ) {
        [appDelegate.socket connect];
    }else{
        [self checkOnlineUserToSocket];

    }

}


-(void)gettingUnSentMessage{
    
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    NSNumber *contactID = 0;
    
    //////Getting  Object for this Chat
    if (_contactObj != nil) {
        contactID = [f numberFromString:_contactObj.identifier];
        _chat =  (Chat *)[DatabaseHelper getObjectIfExist:@"Chat" predicate:[NSPredicate predicateWithFormat:@"contactID == %@",contactID]];
        
        [_chat setChatType:[NSNumber numberWithInt:0]];
    }else{
        _chat =  (Chat *)[DatabaseHelper getObjectIfExist:@"Chat" predicate:[NSPredicate predicateWithFormat:@"contactID == %@",_groupObj.identifier]];
        [_chat setChatType:[NSNumber numberWithInt:1]];
        
        contactID = _groupObj.identifier;
    }

    
    NSArray *allFailedMessages = [DatabaseHelper getAllData:@"Message" offset:0 predicate:[NSPredicate predicateWithFormat:@"chat_id == %@ && status == 0",contactID] sortDescriptor:nil];
    
    NSArray *alMessages = [DatabaseHelper getAllData:@"Message" offset:0 predicate:[NSPredicate predicateWithFormat:@"chat_id == %@",contactID] sortDescriptor:nil];
    
    for (int i = 0; i<[self.tableArray numberOfSections]; i++) {
    
        for (int j = 0; j< [self.tableArray numberOfMessagesInSection:i]; j++) {
            
            NSIndexPath *indexPath = [[NSIndexPath alloc]init];
            indexPath = [NSIndexPath indexPathForRow:j inSection:i];
            Message *messageObj = (Message *)[self.tableArray objectAtIndexPath:indexPath];
            if ([messageObj.status isEqualToNumber: [NSNumber numberWithInt:0]]) {
            
            
            NSDate *startDate = messageObj.date;
            NSDate *endDate = [NSDate date];
            
            NSCalendar *gregorian = [[NSCalendar alloc]
                                     initWithCalendarIdentifier:NSGregorianCalendar];
            
            NSUInteger unitFlags = NSMonthCalendarUnit | NSDayCalendarUnit| NSMinuteCalendarUnit;
            
            NSDateComponents *components = [gregorian components:unitFlags
                                                        fromDate:startDate
                                                          toDate:endDate options:0];
            
            
            int minutes = [components minute];
                if ([messageObj.message_type isEqualToNumber: [NSNumber numberWithInt:0]]) {
                    if (minutes >= 1) {
                        [messageObj setStatus:[NSNumber numberWithInt:-1]];
                        [DatabaseHelper commitChanges];
                        [self updateMessageCell:messageObj];
                    }
                }else if (messageObj.message_type == [NSNumber numberWithInt:1]){
                    if (minutes >= 2) {
                        [messageObj setStatus:[NSNumber numberWithInt:-1]];
                        [DatabaseHelper commitChanges];
                        [self updateMessageCell:messageObj];
                    }

                }else if (messageObj.message_type == [NSNumber numberWithInt:2]){
                    if (minutes >= 1) {
                        [messageObj setStatus:[NSNumber numberWithInt:-1]];
                        [DatabaseHelper commitChanges];
                        [self updateMessageCell:messageObj];
                    }
                    
                }else if (messageObj.message_type == [NSNumber numberWithInt:3]){
                    if (minutes >= 2) {
                        [messageObj setStatus:[NSNumber numberWithInt:-1]];
                        [DatabaseHelper commitChanges];
                        [self updateMessageCell:messageObj];
                    }
                    
                }
                
                
         
            }
        }

    }

    
}



-(void)settingMessagesToSeen{
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    NSNumber *contactID = 0;
    
    //////Getting  Object for this Chat
    if (_contactObj != nil) {
        contactID = [f numberFromString:_contactObj.identifier];
        _chat =  (Chat *)[DatabaseHelper getObjectIfExist:@"Chat" predicate:[NSPredicate predicateWithFormat:@"contactID == %@",contactID]];
        
        [_chat setChatType:[NSNumber numberWithInt:0]];
    }else{
        _chat =  (Chat *)[DatabaseHelper getObjectIfExist:@"Chat" predicate:[NSPredicate predicateWithFormat:@"contactID == %@",_groupObj.identifier]];
        [_chat setChatType:[NSNumber numberWithInt:1]];
        
        contactID = _groupObj.identifier;
    }
    
    NSArray *arrMessages = [DatabaseHelper getAllData:@"Message" offset:0 predicate:[NSPredicate predicateWithFormat:@"chat_id == %@ AND status < 3  AND sender == 1",contactID] sortDescriptor:nil];
    NSMutableArray *arrMessageID = [[NSMutableArray alloc]init];
    
    for (int i = 0; i<arrMessages.count; i++) {
        Message *messageObj = (Message *)[arrMessages objectAtIndex:i];
        [arrMessageID addObject:messageObj];
    }
    if (arrMessageID.count > 0 ) {
        [appDelegate messageStatusUpdate:arrMessageID status:@"seen"];
    }
}


-(void)setupNavigation{
    [self  setupNavigationTitle];
    [self setupCallButton];
    [self setupLeftButton];
}

-(void)setupNavigationTitle{
    
    if (_contactObj != nil) {
        if ([self.contactObj.active isEqualToString:@"online"]) {
            [self settingTitleSubtitle:self.contactObj.name subtitle:self.contactObj.active];
        }else{
            [UIView animateWithDuration:0.5 animations:^{
                for (UIView *subUIView in self.navigationItem.titleView.subviews) {
                    [subUIView removeFromSuperview];
                }
                if (_contactObj.last_seen != nil) {
                    
                    [self settingTitleSubtitle:self.contactObj.name subtitle:[NSString stringWithFormat:@"Last seen %@",self.contactObj.last_seen]];
                    self.title = @"";
                }else{
                    [self settingTitleSubtitle:self.contactObj.name subtitle:@""];
                }

            }];

        }
        
    }else{
        self.title = self.groupObj.name;
        UIButton *btnTitle = [[UIButton alloc]initWithFrame:CGRectMake(self.navigationItem.titleView.frame.origin.x, self.navigationItem.titleView.frame.origin.y, self.navigationItem.titleView.frame.size.width, self.navigationItem.titleView.frame.size.height)];
        [btnTitle addTarget:self action:@selector(gotoGroupDetail) forControlEvents:UIControlEventTouchUpInside];
        [btnTitle setTitle:self.groupObj.name forState:UIControlStateNormal];
        [btnTitle setFont:[UIFont fontWithName:@"OpenSans" size:15.0]];
        for (UIView *subUIView in self.navigationItem.titleView.subviews) {
            [subUIView removeFromSuperview];
        }
        [UIView animateWithDuration:0.5 animations:^{
            for (UIView *subUIView in self.navigationItem.titleView.subviews) {
                [subUIView removeFromSuperview];
            }
            self.navigationItem.titleView = btnTitle;
            
        }];
        
    }
    [self.navigationController.navigationBar setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:
                                                                      [UIColor whiteColor], NSForegroundColorAttributeName,
                                                                      [UIFont fontWithName:@"OpenSans" size:15.0], NSFontAttributeName,nil]];
    
}



-(void)settingTitleSubtitle:(NSString *)title subtitle:(NSString*)status{
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.font = [UIFont fontWithName:@"OpenSans" size:15.0];
    titleLabel.text = title;
    [titleLabel sizeToFit];
    
    UILabel *subTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 22, 0, 0)];
    subTitleLabel.backgroundColor = [UIColor clearColor];
    subTitleLabel.textColor = [UIColor whiteColor];
    subTitleLabel.font = [UIFont fontWithName:@"OpenSans" size:10.0];
    subTitleLabel.text = status;
    [subTitleLabel sizeToFit];
    
    UIView *twoLineTitleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, MAX(subTitleLabel.frame.size.width, titleLabel.frame.size.width), 30)];
    [twoLineTitleView addSubview:titleLabel];
    [twoLineTitleView addSubview:subTitleLabel];
    
    float widthDiff = subTitleLabel.frame.size.width - titleLabel.frame.size.width;
    
    if (widthDiff > 0) {
        CGRect frame = titleLabel.frame;
        frame.origin.x = widthDiff / 2;
        titleLabel.frame = CGRectIntegral(frame);
    }else{
        CGRect frame = subTitleLabel.frame;
        frame.origin.x = abs(widthDiff) / 2;
        subTitleLabel.frame = CGRectIntegral(frame);
    }
   
    [UIView animateWithDuration:0.5 animations:^{
        for (UIView *subUIView in self.navigationItem.titleView.subviews) {
            [subUIView removeFromSuperview];
        }
        self.navigationItem.titleView = twoLineTitleView;

    }];
}


-(void)gotoGroupDetail{
    NSLog(@"detail");
    GroupListViewController *groupVC = [[GroupListViewController alloc]initWithNibName:@"GroupListViewController" bundle:nil];
    
    NSArray *arrMembers = [DatabaseHelper getAllData:@"GroupMember" offset:0 predicate:[NSPredicate predicateWithFormat:@"groupID == %@",self.groupObj.identifier] sortDescriptor:nil];
    
    groupVC.arrMembers = arrMembers;
    groupVC.groupObj = self.groupObj;
    groupVC.groupID = self.groupObj.identifier;
    [self.navigationController pushViewController:groupVC animated:YES];
    
}
-(void)setupCallButton{
    UIButton *callBtn =  [UIButton buttonWithType:UIButtonTypeCustom];
    [callBtn setImage:[UIImage imageNamed:@"call-icon.png"] forState:UIControlStateNormal];
    [callBtn addTarget:self action:@selector(callUser) forControlEvents:UIControlEventTouchUpInside];
    [callBtn setFrame:CGRectMake(15, 0,25, 25)];
    
    UIView *rightBarButtonItems = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 25 + 45, 25)];
    if (_groupObj == nil) {
        [rightBarButtonItems addSubview:callBtn];
    }
    AsyncImageView * imgProfileAsync = [[AsyncImageView alloc]initWithFrame:CGRectMake(45, -10, 40 , 40)];
    
    if (_contactObj != nil) {
        [imgProfileAsync loadImageFromURL:[NSURL URLWithString:self.contactObj.image_id]];
    }else{
        [imgProfileAsync loadImageFromURL:[NSURL URLWithString:self.groupObj.image]];
    }
    
    imgProfileAsync.layer.cornerRadius  = CGRectGetWidth(imgProfileAsync.frame) / 2.0;
    imgProfileAsync.clipsToBounds = YES;
    [rightBarButtonItems addSubview:imgProfileAsync];
    
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightBarButtonItems];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedOnInfoView)];
    [tap setNumberOfTouchesRequired:1];
    [tap setNumberOfTapsRequired:1];
    [imgProfileAsync addGestureRecognizer:tap];
    [imgProfileAsync setUserInteractionEnabled:YES];

    
}


-(void)tappedOnInfoView{
    AsyncImageView * imgProfileAsync = [[AsyncImageView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    [imgProfileAsync setStrTag:@"1.0"];
    if ([_chat.chatType isEqualToNumber:[NSNumber numberWithInt:0]]) {
        [imgProfileAsync loadImageFromURL:[NSURL URLWithString:self.contactObj.image_id]];
    }else{
        [imgProfileAsync loadImageFromURL:[NSURL URLWithString:self.groupObj.image]];
    }
    [self.imageView addSubview:imgProfileAsync];
    [self.viewMainImage setHidden:NO];
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"DismissKeyboard"
     object:self];
}


- (IBAction)btnActionClose:(id)sender {
    [self.viewMainImage setHidden:YES];
}

-(void)callUser{
    NSString *phoneNumber = [@"tel://" stringByAppendingString:self.contactObj.phone];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
}

-(void)setupLeftButton{
    UIButton *backBtn =  [UIButton buttonWithType:UIButtonTypeCustom];
    [backBtn setImage:[UIImage imageNamed:@"arrow-white.png"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backVC) forControlEvents:UIControlEventTouchUpInside];
    [backBtn setFrame:CGRectMake(0, 0,25, 25)];
    
    UIView *rightBarButtonItems = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    [rightBarButtonItems addSubview:backBtn];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightBarButtonItems];
    
}

-(void)backVC{
    [self.inputbar.voiceHud cancelRecording];
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    __weak Inputbar *inputbar = _inputbar;
    __weak UITableView *tableView = _tableView;
    __weak ChatDetailViewController *controller = self;
    
    self.view.keyboardTriggerOffset = inputbar.frame.size.height;
    
    [self.view addKeyboardPanningWithActionHandler:^(CGRect keyboardFrameInView, BOOL opening, BOOL closing) {
        /*
         Try not to call "self" inside this block (retain cycle).
         But if you do, make sure to remove DAKeyboardControl
         when you are done with the view controller by calling:
         [self.view removeKeyboardControl];
         */
        
        CGRect toolBarFrame = inputbar.frame;
        toolBarFrame.origin.y = keyboardFrameInView.origin.y - toolBarFrame.size.height;
        inputbar.frame = toolBarFrame;
        
        CGRect tableViewFrame = tableView.frame;
        tableViewFrame.size.height = toolBarFrame.origin.y;
        tableView.frame = tableViewFrame;
        
        [controller tableViewScrollToBottomAnimated:NO];
    }];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [self.view endEditing:YES];
    [self.view removeKeyboardControl];

}


-(void)viewWillDisappear:(BOOL)animated
{
    [timer invalidate];
    [self.inputbar.textView resignFirstResponder];

}

#pragma mark -

-(void)setInputbar
{
    self.inputbar.placeholder = @"Type message";
    self.inputbar.delegate = self;
    self.inputbar.leftButtonImage = [UIImage imageNamed:@"media-icon.png"];
    self.inputbar.rightButtonImage1 = [UIImage imageNamed:@"camera-icon.png"];
    self.inputbar.rightButtonImage = [UIImage imageNamed:@"mic-icon.png"];
}

- (void)tableView:(UITableView *)tableView
  willDisplayCell:(UITableViewCell *)cell
forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setBackgroundColor:[UIColor clearColor]];
}
-(void)inputbarDidPressVoiceCancel:(Inputbar *)inputbar{
    NSLog(@"Cancel mic");
}


-(void)inputbarDidTextChange:(Inputbar *)inputbar{
    [self sendTypingRequest];
}


-(void)sendTypingRequest{
    if (appDelegate.socket.isConnected != YES) {
        [appDelegate.socket connect];
    }else{
        User *userObj = (User *)[DatabaseHelper getObjectIfExist:@"User" predicate:[NSPredicate predicateWithFormat:@"isLoggedIn == 1"]];
        NSMutableDictionary *dataDic = [[NSMutableDictionary alloc]init];
        [dataDic setValue:@"typing" forKey:@"command"];
        [dataDic setValue:[NSString stringWithFormat:@"%@",userObj.userID] forKey:@"requester"];
        NSMutableArray *userArray = [[NSMutableArray alloc]init];
        
        if ([self.chat.chatType isEqualToNumber:[NSNumber numberWithInt:0]]) {
            [userArray addObject:self.contactObj.identifier];
        }else{
            NSArray *arrMember = [DatabaseHelper getAllData:@"GroupMember" offset:0 predicate:[NSPredicate predicateWithFormat:@"groupID == %@",self.chat.contactID] sortDescriptor:nil];
            [dataDic setValue:self.chat.contactID forKey:@"group_id"];
            
            for (int i = 0 ; i<arrMember.count; i++) {
                GroupMember *groupObj = (GroupMember *)[arrMember objectAtIndex:i];
                Contact *contactObj = (Contact *)[DatabaseHelper getObjectIfExist:@"Contact" predicate:[NSPredicate predicateWithFormat:@"identifier == %@",groupObj.contactID]];
                User *userObj = (User *)[DatabaseHelper getObjectIfExist:@"User" predicate:[NSPredicate predicateWithFormat:@"isLoggedIn == 1"]];
                NSString *strUserID = [NSString stringWithFormat:@"%@",userObj.userID];
                if (strUserID != contactObj.identifier) {
                    [userArray addObject:contactObj.identifier];
                    
                }
            }
        }
        
        [dataDic setValue:userArray forKey:@"reciever_ids"];
        SBJSON *json = [[SBJSON alloc]init];
        NSString *strJson = [[NSString alloc]init];
        strJson = [json stringWithObject:dataDic];
        appDelegate.socket.delegate = self;
        [appDelegate.socket writeString:strJson completion:nil];

    }
}


-(void)inputbarDidPressVoiceSend:(Inputbar *)inputbar{
    NSLog(@"send mic ");
    User *userObj = (User *)[DatabaseHelper getObjectIfExist:@"User" predicate:[NSPredicate predicateWithFormat:@"isLoggedIn == 1"]];
    NSString *strMessageID = [NSString stringWithFormat:@"%@",inputbar.textView.text];
    inputbar.textView.text = @"";
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.aac",strMessageID]];
    
    
    
    Message *messageObj = (Message *) [DatabaseHelper getObjectIfExist:@"Message" predicate:[NSPredicate predicateWithFormat:@"identifier == %@",strMessageID]];
    [messageObj setIdentifier:strMessageID];
    // Message *message = [[Message alloc] init];
    [messageObj setStrTag:[NSString stringWithFormat:@"%d",self.tableArray.numberOfMessages]];

    messageObj.text = @"Audio";
    messageObj.sender = [NSNumber numberWithInt:0];
    messageObj.status = [NSNumber numberWithInt:0];
    messageObj.date = [NSDate date];
    messageObj.senderID = userObj.userID;
    messageObj.message_type = [NSNumber numberWithInt:2];
    messageObj.file_link = path;
    messageObj.chat_id =  [NSString stringWithFormat:@"%@",_chat.contactID];
    [DatabaseHelper commitChanges];
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    _chat.messageID = strMessageID;
    [DatabaseHelper commitChanges];
    
    [self.tableArray addObject:messageObj];
    
    //Insert Message in UI
    NSIndexPath *indexPath = [self.tableArray indexPathForMessage:messageObj];
    [self.tableView beginUpdates];
    if ([self.tableArray numberOfMessagesInSection:indexPath.section] == 1)
        [self.tableView insertSections:[NSIndexSet indexSetWithIndex:indexPath.section]
                      withRowAnimation:UITableViewRowAnimationNone];
    [self.tableView insertRowsAtIndexPaths:@[indexPath]
                          withRowAnimation:UITableViewRowAnimationBottom];
    [self.tableView endUpdates];
    
    [self.tableView scrollToRowAtIndexPath:[self.tableArray indexPathForLastMessage]
                          atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    
    //    //Send message to server
    [self sendingAudioFromService:messageObj];
    [self.inputbar.textView setText:@""];
    [self tableViewScrollToBottomAnimated:YES];

}
-(void)inputbarDidPressRightButton:(Inputbar *)inputbar{
    NSLog(@"mic");
    [self.inputbar.textView resignFirstResponder];

    User *userObj = (User *)[DatabaseHelper getObjectIfExist:@"User" predicate:[NSPredicate predicateWithFormat:@"isLoggedIn == 1"]];
    
    
    NSTimeInterval timeInMiliseconds = [[NSDate date] timeIntervalSince1970];
    NSString *strMessageID = [NSString stringWithFormat:@"%0.lf%@",timeInMiliseconds,self.chat.contactID];
    self.inputbar.textView.text = strMessageID;

}

-(void)inputbarDidPressLeftButton:(Inputbar *)inputbar{
    [self btnActionMedia:nil];
    
}

-(void)inputbarDidPressRight1Button:(Inputbar *)inputbar{
    [self btnActionAddPhoto:nil];

}
-(void)inputbarDidPressSendButton:(Inputbar *)inputbar{
    User *userObj = (User *)[DatabaseHelper getObjectIfExist:@"User" predicate:[NSPredicate predicateWithFormat:@"isLoggedIn == 1"]];

    
    NSTimeInterval timeInMiliseconds = [[NSDate date] timeIntervalSince1970];
    NSString *strMessageID = [NSString stringWithFormat:@"%0.lf%@",timeInMiliseconds,self.chat.contactID];
    Message *messageObj = (Message *) [DatabaseHelper getObjectIfExist:@"Message" predicate:[NSPredicate predicateWithFormat:@"identifier == %@",strMessageID]];
    [messageObj setIdentifier:strMessageID];
    [messageObj setMessage:inputbar.textView.text];
   // Message *message = [[Message alloc] init];
    [messageObj setStrTag:[NSString stringWithFormat:@"%d",self.tableArray.numberOfMessages]];
    
    ////Generating AES Key
    NSString *strAESKey = [AESCrypt generateAESRandomKey];
    
    messageObj.aesEncrypted = strAESKey;

    ///Encrypt Message With AES Key
    NSString *encrpytMessage = [AESCrypt encrypt:inputbar.text password:strAESKey];
    messageObj.text = encrpytMessage;
    
    ////Generate Sha Key
    NSString *strSha = [AESCrypt generateSha:inputbar.textView.text];
    
    messageObj.sha = strSha;
    
    
    messageObj.sender = [NSNumber numberWithInt:0];
    messageObj.status = [NSNumber numberWithInt:0];
    messageObj.senderID = userObj.userID;
    
    messageObj.date = [NSDate date];
    messageObj.message_type = [NSNumber numberWithInt:0];
    
    messageObj.chat_id =  [NSString stringWithFormat:@"%@",_chat.contactID];
    [DatabaseHelper commitChanges];
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    _chat.messageID = strMessageID;
    [DatabaseHelper commitChanges];

    [self.tableArray addObject:messageObj];
    
    //Insert Message in UI
    NSIndexPath *indexPath = [self.tableArray indexPathForMessage:messageObj];
    [self.tableView beginUpdates];
    if ([self.tableArray numberOfMessagesInSection:indexPath.section] == 1)
        [self.tableView insertSections:[NSIndexSet indexSetWithIndex:indexPath.section]
                      withRowAnimation:UITableViewRowAnimationNone];
    [self.tableView insertRowsAtIndexPaths:@[indexPath]
                          withRowAnimation:UITableViewRowAnimationBottom];
    [self.tableView endUpdates];
    
    [self.tableView scrollToRowAtIndexPath:[self.tableArray indexPathForLastMessage]
                          atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    
    //Send message to server
    [self sendingMessageFromService:messageObj];
    [self.inputbar.textView setText:@""];
     [self tableViewScrollToBottomAnimated:YES];
}





-(void)addingIncomingMessages:(Message *)messageObj{
    
    
    [self.tableArray addObject:messageObj];
  
    //Insert Message in UI
    NSIndexPath *indexPath = [self.tableArray indexPathForMessage:messageObj];
    [self.tableView beginUpdates];
    if ([self.tableArray numberOfMessagesInSection:indexPath.section] == 1)
        [self.tableView insertSections:[NSIndexSet indexSetWithIndex:indexPath.section]
                      withRowAnimation:UITableViewRowAnimationNone];
    [self.tableView insertRowsAtIndexPaths:@[indexPath]
                          withRowAnimation:UITableViewRowAnimationBottom];
    [self.tableView endUpdates];
    
    [self.tableView scrollToRowAtIndexPath:[self.tableArray indexPathForLastMessage]
                          atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    

    [self tableViewScrollToBottomAnimated:YES];
    NSMutableArray *messageArr = [[NSMutableArray alloc]init];
    [messageArr addObject:messageObj];
    [appDelegate messageStatusUpdate:messageArr status:@"seen"];


}

-(void)callingGroupDetail{
    //[MBProgressHUD showHUDAddedTo:self.view animated:YES];
    BLLServices *bllService = [[BLLServices alloc]init];
    [bllService setDelegate:self];
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:self.chat.contactID forKey:@"groupID"];
    [bllService getGroupDetail:params];
}


-(void)callProfileInfo{
    //[MBProgressHUD showHUDAddedTo:self.view animated:YES];
    BLLServices *bllService = [[BLLServices alloc]init];
    [bllService setDelegate:self];
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:self.contactObj.identifier forKey:@"contactID"];
    [bllService getContactDetail:params];
}

-(void)sendingMessageFromService:(Message *)message
{
    message.status = [NSNumber numberWithInt:0];
    [DatabaseHelper commitChanges];
    [self updateMessageCell:message];
    BLLServices *bllService = [[BLLServices alloc]init];
    [bllService setDelegate:self];

    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    
    [params setObject:message.date forKey:@"message_time"];
    [params setObject:message.text forKey:@"message"];
    [params setObject:message.identifier forKey:@"message_id"];
    [params setObject:message.sha forKey:@"sha"];

    if (_groupObj != nil) {
        
        NSArray *arrMembers = [DatabaseHelper getAllData:@"GroupMember" offset:0 predicate:[NSPredicate predicateWithFormat:@"groupID == %@",self.groupObj.identifier] sortDescriptor:nil];
        NSMutableDictionary *dataMembers = [[NSMutableDictionary alloc]init];
        
        for (int  i = 0; i<arrMembers.count; i++) {
            GroupMember *groupMember = (GroupMember *)[arrMembers objectAtIndex:i];
            NSString *strContactID = [NSString stringWithFormat:@"%@", groupMember.contactID];
            User *userObj = (User *)[DatabaseHelper getObjectIfExist:@"User" predicate:[NSPredicate predicateWithFormat:@"isLoggedIn == 1"]];
            NSString *strUserID = [NSString stringWithFormat:@"%@", userObj.userID];

            if (![strUserID isEqualToString:strContactID]) {
                
                Contact *contactObjNew = (Contact *)[DatabaseHelper getObjectIfExist:@"Contact" predicate:[NSPredicate predicateWithFormat:@"identifier == %@",strContactID]];
                NSString *encryptedAES = @"";
                if (message.aesEncrypted.length == 24) {
                    encryptedAES = [AESCrypt encryptAESKey:message.aesEncrypted publicKey:contactObjNew.publicKey];
                }
//                NSString *encryptedAES = [AESCrypt encryptAESKey:message.aesEncrypted publicKey:contactObjNew.publicKey];
                
                [dataMembers setValue:encryptedAES forKey:strContactID];

            }
        }
        [params setObject:dataMembers forKey:@"keys"];
        [params setObject:self.chat.contactID forKey:@"group"];
        [bllService sendGroupMessage:params];
    }else{
        
        ////Encrypt AES Key with RSA
        NSString *encryptedAES = @"";
        if (message.aesEncrypted.length == 24) {
            encryptedAES = [AESCrypt encryptAESKey:message.aesEncrypted publicKey:_contactObj.publicKey];
        }
        message.aesEncrypted = encryptedAES;
        [params setObject:message.aesEncrypted  forKey:@"key"];
        [DatabaseHelper commitChanges];
        [params setObject:self.chat.contactID forKey:@"reciever"];
        [bllService sendChatMessage:params];
    }
}


-(void)sendingAudioFromService:(Message *)message
{
    message.status = [NSNumber numberWithInt:0];
    [DatabaseHelper commitChanges];
    [self updateMessageCell:message];
    BLLServices *bllService = [[BLLServices alloc]init];
    [bllService setDelegate:self];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
   
    [params setObject:message.date forKey:@"message_time"];
    [params setObject:message.text forKey:@"message"];
    [params setObject:message.identifier forKey:@"message_id"];
    [params setObject:@"AUDIO" forKey:@"type"];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.aac",message.identifier]];
    NSData *audioData = [[NSData alloc] initWithContentsOfFile:path];
    if (_groupObj != nil) {
        NSArray *arrMembers = [DatabaseHelper getAllData:@"GroupMember" offset:0 predicate:[NSPredicate predicateWithFormat:@"groupID == %@",self.groupObj.identifier] sortDescriptor:nil];
        NSMutableDictionary *dataMembers = [[NSMutableDictionary alloc]init];
        
        for (int  i = 0; i<arrMembers.count; i++) {
            GroupMember *groupMember = (GroupMember *)[arrMembers objectAtIndex:i];
            NSString *strContactID = [NSString stringWithFormat:@"%@", groupMember.contactID];
            User *userObj = (User *)[DatabaseHelper getObjectIfExist:@"User" predicate:[NSPredicate predicateWithFormat:@"isLoggedIn == 1"]];
            NSString *strUserID = [NSString stringWithFormat:@"%@", userObj.userID];
            
            if (![strUserID isEqualToString:strContactID]) {
                [dataMembers setValue:@"heyyyy" forKey:strContactID];
            }
            
        }
        [params setObject:dataMembers forKey:@"keys"];
        [params setObject:self.chat.contactID forKey:@"group"];
        [params setObject:@""forKey:@"sha"];
        [bllService sendGroupChatImage:params image:audioData];
    }else{
        [params setObject:self.chat.contactID forKey:@"reciever"];
        [params setObject:@""forKey:@"sha"];
        [bllService sendChatImage:params image:audioData];

    }
    
}

-(void)sendingPictureFromService:(Message *)message
{
    message.status = [NSNumber numberWithInt:0];
    [DatabaseHelper commitChanges];
    [self updateMessageCell:message];
    BLLServices *bllService = [[BLLServices alloc]init];
    [bllService setDelegate:self];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:message.date forKey:@"message_time"];
    [params setObject:message.text forKey:@"message"];
    [params setObject:message.identifier forKey:@"message_id"];
    [params setObject:@"IMAGE" forKey:@"type"];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",message.identifier]];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:path]];
    
    NSData *imageData = UIImageJPEGRepresentation(imageView.image, 0.35);
    
//    ////Generating AES Key
//    NSString *strAESKey = [AESCrypt generateAESRandomKey];
//    
//    message.aesEncrypted = strAESKey;
    
//    ///Encrypt Message With AES Key
//    NSData *encrpytMessage = [AESCrypt encryptData:imageData password:strAESKey];
//    
//    ////Generate Sha Key
//    NSData *dataSha = [AESCrypt generateShaData:imageData];
//    
//    NSLog(@"%@",[[NSString alloc] initWithData:dataSha encoding:NSUTF8StringEncoding]);
//    
//    [message setSha:[[NSString alloc] initWithData:dataSha encoding:NSUTF8StringEncoding]];

    
    [DatabaseHelper commitChanges];
    
    if (_groupObj != nil) {

        NSArray *arrMembers = [DatabaseHelper getAllData:@"GroupMember" offset:0 predicate:[NSPredicate predicateWithFormat:@"groupID == %@",self.groupObj.identifier] sortDescriptor:nil];
        NSMutableDictionary *dataMembers = [[NSMutableDictionary alloc]init];
        
        for (int  i = 0; i<arrMembers.count; i++) {
            GroupMember *groupMember = (GroupMember *)[arrMembers objectAtIndex:i];
            NSString *strContactID = [NSString stringWithFormat:@"%@", groupMember.contactID];
            User *userObj = (User *)[DatabaseHelper getObjectIfExist:@"User" predicate:[NSPredicate predicateWithFormat:@"isLoggedIn == 1"]];
            NSString *strUserID = [NSString stringWithFormat:@"%@", userObj.userID];
            
            if (![strUserID isEqualToString:strContactID]) {
                [dataMembers setValue:@"heyyyy" forKey:strContactID];
            }
            
        }
        [params setObject:dataMembers forKey:@"keys"];
        [params setObject:self.chat.contactID forKey:@"group"];
        [params setObject:@""forKey:@"sha"];
        [bllService sendGroupChatImage:params image:imageData];
    }else{
        [params setObject:self.chat.contactID forKey:@"reciever"];
        [params setObject:@""forKey:@"sha"];
        [bllService sendChatImage:params image:imageData];
    }
    
}


-(void)sendingVideoFromService:(Message *)message
{
    message.status = [NSNumber numberWithInt:0];
    message.date = [NSDate date];
    [DatabaseHelper commitChanges];
    [self updateMessageCell:message];
    BLLServices *bllService = [[BLLServices alloc]init];
    [bllService setDelegate:self];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:message.date forKey:@"message_time"];
    [params setObject:message.text forKey:@"message"];
    [params setObject:message.identifier forKey:@"message_id"];

    [params setObject:@"VIDEO" forKey:@"type"];
    
   // NSData *videoData = [[NSData alloc] initWithContentsOfFile:message.file_link];
//    NSURL *videoFileURL = [self convertVideoToLowQuailtyWithInputURL:[NSURL URLWithString:message.file_link] outputURL:[NSURL URLWithString:message.file_link]];
//    message.file_link = [NSString stringWithFormat:@"%@",videoFileURL];
    [DatabaseHelper commitChanges];
    NSData *videoData = [[NSData alloc]initWithContentsOfFile:message.file_link options:NSDataReadingMappedIfSafe error:nil];

    if (_groupObj != nil) {
        NSArray *arrMembers = [DatabaseHelper getAllData:@"GroupMember" offset:0 predicate:[NSPredicate predicateWithFormat:@"groupID == %@",self.groupObj.identifier] sortDescriptor:nil];
        NSMutableDictionary *dataMembers = [[NSMutableDictionary alloc]init];
        
        for (int  i = 0; i<arrMembers.count; i++) {
            GroupMember *groupMember = (GroupMember *)[arrMembers objectAtIndex:i];
            NSString *strContactID = [NSString stringWithFormat:@"%@", groupMember.contactID];
            User *userObj = (User *)[DatabaseHelper getObjectIfExist:@"User" predicate:[NSPredicate predicateWithFormat:@"isLoggedIn == 1"]];
            NSString *strUserID = [NSString stringWithFormat:@"%@", userObj.userID];
            
            if (![strUserID isEqualToString:strContactID]) {
                [dataMembers setValue:@"heyyyy" forKey:strContactID];
            }
            
        }
        [params setObject:dataMembers forKey:@"keys"];
        [params setObject:self.chat.contactID forKey:@"group"];
        [params setObject:@""forKey:@"sha"];
        [bllService sendGroupChatImage:params image:videoData];
    }else{
        [params setObject:self.chat.contactID forKey:@"reciever"];
        [params setObject:@""forKey:@"sha"];
        [bllService sendChatImage:params image:videoData];
    }
    
}


-(void)setTableView
{
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f,self.view.frame.size.width, 10.0f)];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = [UIColor clearColor];
    [self.tableView registerClass:[ChatDetailTableViewCell class] forCellReuseIdentifier: @"ChatDetailTableViewCell"];
}

#pragma mark - TableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.tableArray numberOfSections];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.tableArray numberOfMessagesInSection:section];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    Message *messageObj = (Message *)[self.tableArray objectAtIndexPath:indexPath];
    if (_groupObj != nil) {
        if (messageObj.message_type == [NSNumber numberWithInt:2]){
            @try {
                
                GroupAudioViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GroupAudioViewCell"];
                cell.tag = indexPath.row;
                
                if (!cell)
                {
                    cell = [[GroupAudioViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"GroupAudioViewCell"];
                    cell.tag = indexPath.row ;
                    cell.strTag = [NSString stringWithFormat:@"%ld",(long)indexPath.row ];
                    
                    
                }
                cell.navigationController = self.navigationController;
                
                NSString *strMesageID = [NSString stringWithFormat:@"%@", messageObj.identifier];
                
                NSString *strGroupID = [NSString stringWithFormat:@"%@", self.groupObj.identifier];

                UserDataTapGestureRecognizer *swipeLeftGesture =
                [[UserDataTapGestureRecognizer alloc] initWithTarget:self
                                                              action:@selector(handleSwipeGesture:)];
                [cell addGestureRecognizer:swipeLeftGesture];
                
                NSMutableDictionary *dicPassingData = [[NSMutableDictionary alloc]init];
                [dicPassingData setValue:messageObj forKey:@"message"];
                [dicPassingData setValue:strGroupID forKey:@"groupID"];
                
                swipeLeftGesture.userData = dicPassingData;
                cell.tag = indexPath.row;
                swipeLeftGesture.direction=UISwipeGestureRecognizerDirectionLeft;
                
                
                
                NSArray *arrMessageSeen = (NSArray *)[DatabaseHelper getAllData:@"GroupMessageSeen" offset:0 predicate:[NSPredicate predicateWithFormat:@"message_id == %@ && chat_id == %@",strMesageID,strGroupID] sortDescriptor:nil];
                
                cell.arrMessageSeen = arrMessageSeen;
                [cell setMessage:[self.tableArray objectAtIndexPath:indexPath]];
                cell.message.strTag = [NSString stringWithFormat:@"%ld",(long)indexPath.row ];
                if (cell.message.message_type == [NSNumber numberWithInt:2]){
                    
                    [cell.slider setTag:indexPath.row];
                    [cell.btnPlay setTag:indexPath.row];
                    
                    [cell.lblAudioTime setTag:indexPath.row];
                    if ([[[[cell subviews]objectAtIndex:0]subviews] count] > 6) {
                        [[[[[cell subviews]objectAtIndex:0]subviews]objectAtIndex:5] removeFromSuperview];
                        if ([[[[cell subviews]objectAtIndex:0]subviews] count] > 7) {
                            [[[[[cell subviews]objectAtIndex:0]subviews]objectAtIndex:6] removeFromSuperview];
                            
                        }
                        
                    }
                    cell.soundView.hidden = NO;
                    cell.textView.hidden = YES;
                    NSString *strImage = @"";
                    if (cell.message.sender == [NSNumber numberWithInt:1]) {
                        Contact *contact = (Contact *)[DatabaseHelper getObjectIfExist:@"Contact" predicate:[NSPredicate predicateWithFormat:@"identifier == %@",cell.message.senderID]];
                        strImage = contact.image_id;
                    }else{
                        User *userObj = (User *)[DatabaseHelper getObjectIfExist:@"User" predicate:[NSPredicate predicateWithFormat:@"isLoggedIn == 1"]];
                        strImage = userObj.userImage;
                    }
                    AsyncImageView * imgProfileAsync = [[AsyncImageView alloc]initWithFrame:CGRectMake(0, 0, cell.profileImage.frame.size.width, cell.profileImage.frame.size.height)];
                    [imgProfileAsync loadImageFromURL:[NSURL URLWithString:strImage]];
                    cell.profileImage.layer.cornerRadius  = CGRectGetWidth(cell.profileImage.frame) / 2.0;
                    cell.profileImage.clipsToBounds = YES;
                    [cell.profileImage addSubview:imgProfileAsync];
                    
                    
                    
                    
                }
                
                return cell;
                
            } @catch (NSException *exception) {
                NSLog(@"%@",exception);
                
            }
            
        }else if (messageObj.message_type == [NSNumber numberWithInt:3]){
            @try {
                
                GroupVideoViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GroupVideoViewCell"];
                if (!cell)
                {
                    cell = [[GroupVideoViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"GroupVideoViewCell"];
                    //  cell.bubbleX = 53.75;
                    
                }
//                
                
                cell.navigationController = self.navigationController;

                NSString *strMesageID = [NSString stringWithFormat:@"%@", messageObj.identifier];
                
                NSString *strGroupID = [NSString stringWithFormat:@"%@", self.groupObj.identifier];
                
                UserDataTapGestureRecognizer *swipeLeftGesture =
                [[UserDataTapGestureRecognizer alloc] initWithTarget:self
                                                              action:@selector(handleSwipeGesture:)];
                [cell addGestureRecognizer:swipeLeftGesture];
                
                NSMutableDictionary *dicPassingData = [[NSMutableDictionary alloc]init];
                [dicPassingData setValue:messageObj forKey:@"message"];
                [dicPassingData setValue:strGroupID forKey:@"groupID"];
                
                swipeLeftGesture.userData = dicPassingData;
                cell.tag = indexPath.row;
                swipeLeftGesture.direction=UISwipeGestureRecognizerDirectionLeft;
                
                
                
                NSArray *arrMessageSeen = (NSArray *)[DatabaseHelper getAllData:@"GroupMessageSeen" offset:0 predicate:[NSPredicate predicateWithFormat:@"message_id == %@ && chat_id == %@",strMesageID,strGroupID] sortDescriptor:nil];
                
                
                cell.arrMessageSeen = arrMessageSeen;
                

                
                cell.message = [self.tableArray objectAtIndexPath:indexPath];
                
                NSString *strImage = @"";
                if (cell.message.sender == [NSNumber numberWithInt:1]) {
                    Contact *contact = (Contact *)[DatabaseHelper getObjectIfExist:@"Contact" predicate:[NSPredicate predicateWithFormat:@"identifier == %@",cell.message.senderID]];
                    strImage = contact.image_id;
                }else{
                    User *userObj = (User *)[DatabaseHelper getObjectIfExist:@"User" predicate:[NSPredicate predicateWithFormat:@"isLoggedIn == 1"]];
                    strImage = userObj.userImage;
                }
                
                
                
                if (cell.message.message_type == [NSNumber numberWithInt:3]) {
                    
                    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                                         NSUserDomainMask, YES);
                    NSString *documentsDirectory = [paths objectAtIndex:0];
                    NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",cell.message.identifier]];
                    [cell.moviePlayer.view setHidden:NO];
                    cell.textView.hidden = YES;
                    
                    if ([cell.message.sender isEqualToNumber:[NSNumber numberWithInt:0]]) {
                        //[cell.moviePlayer loadImageFromNSBundle:path];
                    }else{
                        // [cell.messageImage loadImageFromURL:[NSURL URLWithString:cell.message.file_link]];
                        
                    }
                    
                    
                }else if ([cell.message.message_type isEqualToNumber:[NSNumber numberWithInt:0]]){
                    //  [cell.messageImage setHidden:YES];
                    cell.textView.hidden = NO;
                }
                
                return cell;
                

                
            } @catch (NSException *exception) {
                NSLog(@"%@",exception);
                
            }
            
        }else{
            GroupChatDetalViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GroupChatDetalViewCell"];
            if (!cell)
            {
                cell = [[GroupChatDetalViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"GroupChatDetalViewCell"];
                
            }
            NSString *strGroupID = [NSString stringWithFormat:@"%@", self.groupObj.identifier];

            UserDataTapGestureRecognizer *swipeLeftGesture =
            [[UserDataTapGestureRecognizer alloc] initWithTarget:self
                                                          action:@selector(handleSwipeGesture:)];
            [cell addGestureRecognizer:swipeLeftGesture];
            
            NSMutableDictionary *dicPassingData = [[NSMutableDictionary alloc]init];
            [dicPassingData setValue:messageObj forKey:@"message"];
            [dicPassingData setValue:strGroupID forKey:@"groupID"];
            
            swipeLeftGesture.userData = dicPassingData;
            cell.tag = indexPath.row;
            swipeLeftGesture.direction=UISwipeGestureRecognizerDirectionLeft;
            
            NSString *strMesageID = [NSString stringWithFormat:@"%@", messageObj.identifier];
            
            
            NSArray *arrMessageSeen = (NSArray *)[DatabaseHelper getAllData:@"GroupMessageSeen" offset:0 predicate:[NSPredicate predicateWithFormat:@"message_id == %@ && chat_id == %@",strMesageID,strGroupID] sortDescriptor:nil];
            
            cell.arrMessageSeen = arrMessageSeen;
            
            cell.message = [self.tableArray objectAtIndexPath:indexPath];

            
            cell.navigationController = self.navigationController;


            NSString *strImage = @"";
            if (cell.message.sender == [NSNumber numberWithInt:1]) {
                Contact *contact = (Contact *)[DatabaseHelper getObjectIfExist:@"Contact" predicate:[NSPredicate predicateWithFormat:@"identifier == %@",cell.message.senderID]];
                strImage = contact.image_id;
            }else{
                User *userObj = (User *)[DatabaseHelper getObjectIfExist:@"User" predicate:[NSPredicate predicateWithFormat:@"isLoggedIn == 1"]];
                strImage = userObj.userImage;
            }
            
            AsyncImageView * imgProfileAsync = [[AsyncImageView alloc]initWithFrame:CGRectMake(0, 0, cell.profileImage.frame.size.width, cell.profileImage.frame.size.height)];
            [imgProfileAsync loadImageFromURL:[NSURL URLWithString:strImage]];
            cell.profileImage.layer.cornerRadius  = CGRectGetWidth(cell.profileImage.frame) / 2.0;
            cell.profileImage.clipsToBounds = YES;
            [cell.profileImage addSubview:imgProfileAsync];
            
            if (cell.message.message_type == [NSNumber numberWithInt:1]) {
                [cell.messageImage setBackgroundColor:[UIColor whiteColor]];
                
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                                     NSUserDomainMask, YES);
                NSString *documentsDirectory = [paths objectAtIndex:0];
                NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",cell.message.identifier]];
                [cell.messageImage setHidden:NO];
                cell.textView.hidden = YES;
                
                if ([cell.message.sender isEqualToNumber:[NSNumber numberWithInt:0]]) {
                    [cell.messageImage loadImageFromNSBundle:path];
                }else{
                    User *userObj = (User *)[DatabaseHelper getObjectIfExist:@"User" predicate:[NSPredicate predicateWithFormat:@"isLoggedIn == 1"]];
                    if (userObj.saveMedia == [NSNumber numberWithInt:1]) {
                        cell.messageImage.saveImage = @"yes";
                    }
                    [cell.messageImage loadImageFromURL:[NSURL URLWithString:cell.message.file_link]];
                    
                }
                
                
            }else if ([cell.message.message_type isEqualToNumber:[NSNumber numberWithInt:0]]){
                [cell.messageImage setHidden:YES];
                cell.textView.hidden = NO;
            }
            return cell;
        }
        
    }else{
        
        if (messageObj.message_type == [NSNumber numberWithInt:2]){
            @try {
                
                AudioMessageViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AudioMessageViewCell"];
                cell.tag = indexPath.row;
                
                if (!cell)
                {
                    cell = [[AudioMessageViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"AudioMessageViewCell"];
                    cell.tag = indexPath.row ;
                    cell.strTag = [NSString stringWithFormat:@"%ld",(long)indexPath.row ];
                    
                    
                }
                cell.navigationController = self.navigationController;
                [cell setMessage:[self.tableArray objectAtIndexPath:indexPath]];
                cell.message.strTag = [NSString stringWithFormat:@"%ld",(long)indexPath.row ];
                if (cell.message.message_type == [NSNumber numberWithInt:2]){
                    
                    [cell.slider setTag:indexPath.row];
                    [cell.btnPlay setTag:indexPath.row];
                    
                    [cell.lblAudioTime setTag:indexPath.row];
                    cell.soundView.hidden = NO;
                    cell.textView.hidden = YES;
                    NSString *strImage = @"";
                    if (cell.message.sender == [NSNumber numberWithInt:1]) {
                        Contact *contact = (Contact *)[DatabaseHelper getObjectIfExist:@"Contact" predicate:[NSPredicate predicateWithFormat:@"identifier == %@",cell.message.senderID]];
                        strImage = contact.image_id;
                    }else{
                        User *userObj = (User *)[DatabaseHelper getObjectIfExist:@"User" predicate:[NSPredicate predicateWithFormat:@"isLoggedIn == 1"]];
                        strImage = userObj.userImage;
                    }
                    AsyncImageView * imgProfileAsync = [[AsyncImageView alloc]initWithFrame:CGRectMake(0, 0, cell.profileImage.frame.size.width, cell.profileImage.frame.size.height)];
                    [imgProfileAsync loadImageFromURL:[NSURL URLWithString:strImage]];
                    cell.profileImage.layer.cornerRadius  = CGRectGetWidth(cell.profileImage.frame) / 2.0;
                    cell.profileImage.clipsToBounds = YES;
                    [cell.profileImage addSubview:imgProfileAsync];
                    
                }
                
                return cell;
                
            } @catch (NSException *exception) {
                NSLog(@"%@",exception);
                
            }
            
        }else if (messageObj.message_type == [NSNumber numberWithInt:3]){
            @try {
                
                VideoMessageViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"VideoMessageViewCell"];
                if (!cell)
                {
                    cell = [[VideoMessageViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"VideoMessageViewCell"];
                    //  cell.bubbleX = 53.75;
                    
                }
                
                
                cell.navigationController = self.navigationController;
                cell.message = [self.tableArray objectAtIndexPath:indexPath];
                
                NSString *strImage = @"";
                if (cell.message.sender == [NSNumber numberWithInt:1]) {
                    Contact *contact = (Contact *)[DatabaseHelper getObjectIfExist:@"Contact" predicate:[NSPredicate predicateWithFormat:@"identifier == %@",cell.message.senderID]];
                    strImage = contact.image_id;
                }else{
                    User *userObj = (User *)[DatabaseHelper getObjectIfExist:@"User" predicate:[NSPredicate predicateWithFormat:@"isLoggedIn == 1"]];
                    strImage = userObj.userImage;
                }
                
                
                
                if (cell.message.message_type == [NSNumber numberWithInt:3]) {
                    
                    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                                         NSUserDomainMask, YES);
                    NSString *documentsDirectory = [paths objectAtIndex:0];
                    NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",cell.message.identifier]];
                    [cell.moviePlayer.view setHidden:NO];
                    cell.textView.hidden = YES;
                    
                    if ([cell.message.sender isEqualToNumber: [NSNumber numberWithInt:0]]) {
                        //[cell.moviePlayer loadImageFromNSBundle:path];
                    }else{
                        // [cell.messageImage loadImageFromURL:[NSURL URLWithString:cell.message.file_link]];
                        
                    }
                    
                    
                }else if ([cell.message.message_type isEqualToNumber: [NSNumber numberWithInt:0]]){
                    //  [cell.messageImage setHidden:YES];
                    cell.textView.hidden = NO;
                }
                
                return cell;
                
            } @catch (NSException *exception) {
                NSLog(@"%@",exception);
                
            }
            
        }else{
            ChatDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ChatDetailTableViewCell"];
            if (!cell)
            {
                cell = [[ChatDetailTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ChatDetailTableViewCell"];
                //  cell.bubbleX = 53.75;
                
            }
            
            
            cell.navigationController = self.navigationController;
            cell.message = [self.tableArray objectAtIndexPath:indexPath];
            
            NSString *strImage = @"";
            if (cell.message.sender == [NSNumber numberWithInt:1]) {
                Contact *contact = (Contact *)[DatabaseHelper getObjectIfExist:@"Contact" predicate:[NSPredicate predicateWithFormat:@"identifier == %@",cell.message.senderID]];
                strImage = contact.image_id;
            }else{
                User *userObj = (User *)[DatabaseHelper getObjectIfExist:@"User" predicate:[NSPredicate predicateWithFormat:@"isLoggedIn == 1"]];
                strImage = userObj.userImage;
            }
            
            AsyncImageView * imgProfileAsync = [[AsyncImageView alloc]initWithFrame:CGRectMake(0, 0, cell.profileImage.frame.size.width, cell.profileImage.frame.size.height)];
            [imgProfileAsync loadImageFromURL:[NSURL URLWithString:strImage]];
            cell.profileImage.layer.cornerRadius  = CGRectGetWidth(cell.profileImage.frame) / 2.0;
            cell.profileImage.clipsToBounds = YES;
            [cell.profileImage addSubview:imgProfileAsync];
            
            if (cell.message.message_type == [NSNumber numberWithInt:1]) {
                [cell.messageImage setBackgroundColor:[UIColor whiteColor]];
                
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                                     NSUserDomainMask, YES);
                NSString *documentsDirectory = [paths objectAtIndex:0];
                NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",cell.message.identifier]];
                [cell.messageImage setHidden:NO];
                cell.textView.hidden = YES;
                
                if ([cell.message.sender isEqualToNumber:[NSNumber numberWithInt:0]]) {
                    [cell.messageImage loadImageFromNSBundle:path];
                }else{
                    User *userObj = (User *)[DatabaseHelper getObjectIfExist:@"User" predicate:[NSPredicate predicateWithFormat:@"isLoggedIn == 1"]];
                    if (userObj.saveMedia == [NSNumber numberWithInt:1]) {
                        cell.messageImage.saveImage = @"yes";
                    }
                    [cell.messageImage loadImageFromURL:[NSURL URLWithString:cell.message.file_link]];
                    
                }
                
                
            }else if ([cell.message.message_type isEqualToNumber:[NSNumber numberWithInt:0]]){
                [cell.messageImage setHidden:YES];
                cell.textView.hidden = NO;
            }
            return cell;
        }
    }


    return nil;
}



-(void)handleSwipeGesture:(UserDataTapGestureRecognizer *) sender
{
    NSUInteger touches = sender.numberOfTouches;
  //  if (touches == 2)
  //  {
        if (sender.state == UIGestureRecognizerStateEnded)
        {
            [self.inputbar.voiceHud cancelRecording];
            
            NSLog(@"Swipe the helll out");
            GroupMessageSeenViewController *groupVC = [[GroupMessageSeenViewController alloc]initWithNibName:@"GroupMessageSeenViewController" bundle:nil];
            NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
            parameters = (NSMutableDictionary *)sender.userData;
            groupVC.messageObj = [parameters valueForKey:@"message"];
            groupVC.strGroupID = [parameters valueForKey:@"groupID"];
            [self.navigationController pushViewController:groupVC animated:YES];
            //Add view controller here
        }
   // }
}


- (void)tappedOnInfoView:(UIGestureRecognizer *)sender {
//    NSIndexPath *path = [NSIndexPath indexPathWithIndex:sender.view.tag];
//   // ChatDetailTableViewCell *cell = [self.tableView cellForRowAtIndexPath:path];
//    Message *messageObj = (Message *)[self.tableArray objectAtIndexPath:path];
//    NSLog(messageObj.file_link);
}




- (NSString*)convertTime:(NSInteger)time
{
    NSInteger minutes = time / 60;
    NSInteger seconds = time % 60;
    return [NSString stringWithFormat:@"%02ld:%02ld", (long)minutes, (long)seconds];
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Message *message = [self.tableArray objectAtIndexPath:indexPath];
    
    return [message.heigh floatValue]+ 5;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40.0;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [self.tableArray titleForSection:section];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    CGRect frame = CGRectMake(0, 0, tableView.frame.size.width, 40);
    
    UIView *view = [[UIView alloc] initWithFrame:frame];
    view.backgroundColor = [UIColor clearColor];
    view.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
    UILabel *label = [[UILabel alloc] init];
    label.text = [self tableView:tableView titleForHeaderInSection:section];
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont fontWithName:@"Helvetica" size:20.0];
    [label sizeToFit];
    label.center = view.center;
    label.font = [UIFont fontWithName:@"Helvetica" size:13.0];
    label.backgroundColor = [UIColor colorWithRed:207/255.0 green:220/255.0 blue:252.0/255.0 alpha:1];
    label.layer.cornerRadius = 10;
    label.layer.masksToBounds = YES;
    label.autoresizingMask = UIViewAutoresizingNone;
    [view addSubview:label];
    
    return view;
}


- (BOOL)textView:(UITextView *)textView shouldInteractWithURL:(NSURL *)URL inRange:(NSRange)characterRange{

    return FALSE;
}


- (void)tableViewScrollToBottomAnimated:(BOOL)animated
{
    NSInteger numberOfSections = [self.tableArray numberOfSections];
    NSInteger numberOfRows = [self.tableArray numberOfMessagesInSection:numberOfSections-1];
    if (numberOfRows)
    {
       // [_tableView scrollToRowAtIndexPath:[self.tableArray indexPathForLastMessage]
        //                  atScrollPosition:UITableViewScrollPositionBottom animated:animated];
        [self scrollToBottom];
    }
}

- (void)scrollToBottom
{
    CGFloat yOffset = 0;
    
    if (self.tableView.contentSize.height > self.tableView.bounds.size.height) {
        yOffset = self.tableView.contentSize.height - self.tableView.bounds.size.height;
    }
    
    [self.tableView setContentOffset:CGPointMake(0, yOffset +10) animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)successWithData:(id)data msg:(NSString *)successMsg tag:(int)tag{
    self.inputbar.sendButton.enabled = YES;
    if (tag == SendChatMessage || tag == SendChatImage) {
        
        NSMutableDictionary *dataDic = [[NSMutableDictionary alloc]init];
        dataDic = (NSMutableDictionary *)[data objectAtIndex:0];
        NSString *strMesageID = [NSString stringWithFormat:@"%@", [dataDic valueForKey:@"message_id"]];
        Message *messageObj = (Message *)[DatabaseHelper getObjectIfExist:@"Message" predicate:[NSPredicate predicateWithFormat:@"identifier == %@",strMesageID]];
        if ([messageObj.status intValue] < 1) {
            messageObj.status = [NSNumber numberWithInt:1];
        }
        if (_chat.chatType == [NSNumber numberWithInt:1]) {
            
        }
        
        [DatabaseHelper commitChanges];
        [self updateMessageCell:messageObj];
    }else if (tag == SendGroupMessage){
        NSMutableDictionary *dataDic = [[NSMutableDictionary alloc]init];
        dataDic = (NSMutableDictionary *)[data objectAtIndex:0];
        
        NSString *strMesageID = [NSString stringWithFormat:@"%@", [dataDic valueForKey:@"message_id"]];
        
        NSString *strGroupID = [NSString stringWithFormat:@"%@", self.groupObj.identifier];

        NSArray *arrMembers = [DatabaseHelper getAllData:@"GroupMember" offset:0 predicate:[NSPredicate predicateWithFormat:@"groupID == %@",self.groupObj.identifier] sortDescriptor:nil];

        for (int  i = 0; i<arrMembers.count; i++) {
            
            
            GroupMember *groupMember = (GroupMember *)[arrMembers objectAtIndex:i];
            NSString *strContactID = [NSString stringWithFormat:@"%@", groupMember.contactID];

            GroupMessageSeen *messageGroupSeen = (GroupMessageSeen *)[DatabaseHelper getObjectIfExist:@"GroupMessageSeen" predicate:[NSPredicate predicateWithFormat:@"message_id == %@ && chat_id == %@ && user_id == %@",strMesageID,strGroupID,strContactID]];
            
            [messageGroupSeen setChat_id:strGroupID];
            [messageGroupSeen setMessage_id:strMesageID];
            [messageGroupSeen setUser_id:strContactID];
            
            if ([messageGroupSeen.status intValue] < 1) {
                messageGroupSeen.status = [NSNumber numberWithInt:1];
            }
            
            
            [DatabaseHelper commitChanges];

        }
        Message *messageObj = (Message *)[DatabaseHelper getObjectIfExist:@"Message" predicate:[NSPredicate predicateWithFormat:@"identifier == %@",strMesageID]];
        if ([messageObj.status intValue] < 1) {
            messageObj.status = [NSNumber numberWithInt:1];
        }
        if (_chat.chatType == [NSNumber numberWithInt:1]) {
            
        }
        [self updateMessageCell:messageObj];

        
//        let messageGroupObj: GroupMessageSeen = (DatabaseHelper.getObjectIfExist("GroupMessageSeen", predicate: NSPredicate(format: "message_id == %@  &&  user_id == %@ && chat_id == %@", messageID,userID,groupID)) as! GroupMessageSeen)


    }else if (tag == GetGroupDetail){
       // [MBProgressHUD hideHUDForView:self.view animated:YES];

        appDelegate.socket.delegate = self;
        if (appDelegate.socket.isConnected != YES ) {
            [appDelegate.socket connect];
        }else{
            [self checkOnlineUserToSocket];
            
        }
    }else if(tag == GetContactDetail){
       // [MBProgressHUD hideHUDForView:self.view animated:YES];
        [self setupCallButton];
    }
}


-(void)updateMessageCell:(Message *)messageObj{
    @try {
    
    NSIndexPath *indexPath = [self.tableArray indexPathForMessage:messageObj];
   // [self.tableView beginUpdates];
    
    ChatDetailTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"ChatDetailTableViewCell"];
    if (!cell)
    {
        cell = [[ChatDetailTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ChatDetailTableViewCell"];
    }
    cell.message = [self.tableArray objectAtIndexPath:indexPath];
    [cell updateMessageStatus];
    NSArray *indexPathArray = [NSArray arrayWithObject:indexPath];
    //You can add one or more indexPath in this array...
    
    [self.tableView reloadRowsAtIndexPaths:indexPathArray withRowAnimation:UITableViewRowAnimationFade];
   // [self.tableView endUpdates];
        
    } @catch (NSException *exception) {
        NSLog(@"%@",exception);
    }
}


-(void)requestFailure:(NSString *)failureMsg tag:(int)tag{
    
}

-(void)uploadProgress:(float)progress{
    
}

/////////////////////****** Button Add Photo Action  *****////////////////////

-(void)btnActionAddPhoto:(UIButton *)sender{
    NSString *strCamera = @"Take a picture";
    NSString *strPhotoLibrary = @"Upload photo from album";
    NSString *strCancel = @"Cancel";
    UIActionSheet *actionSheet = [[UIActionSheet alloc]initWithTitle:@"Select from below" delegate:self cancelButtonTitle:strCancel destructiveButtonTitle:nil otherButtonTitles:strCamera,strPhotoLibrary, nil];
    [actionSheet showInView:self.view];
    [actionSheet setTag:1];
    [self.inputbar.textView resignFirstResponder];
}



-(void)btnActionMedia:(UIButton *)sender{
    NSString *strCamera = @"Take a picture";
    NSString *strPhotoLibrary = @"Upload photo from album";
    NSString *strVideoLibrary = @"Upload video from album";
    NSString *strVideoCamera = @"Make a video";

    NSString *strCancel = @"Cancel";
    UIActionSheet *actionSheet = [[UIActionSheet alloc]initWithTitle:@"Select your media" delegate:self cancelButtonTitle:strCancel destructiveButtonTitle:nil otherButtonTitles:strCamera,strPhotoLibrary,strVideoLibrary,strVideoCamera, nil];
    [actionSheet showInView:self.view];
    [actionSheet setTag:2];
    [self.inputbar.textView resignFirstResponder];
}




-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (actionSheet.tag == 2) {

    switch (buttonIndex) {
       
       
        case 0:
            NSLog(@"Take Picture");
            [self openCamera];
            break;
        case 1:
            NSLog(@"Upload Photo");
            [self openGallery];
            break;
        case 2:
            NSLog(@"Upload Video");
            [self openVideoGallery];
            break;
        case 3:
            NSLog(@"Upload Video");
            [self openVideoCamera];
            break;
        case 4:
            NSLog(@"Cancel");
            break;
        default:
            NSLog(@"Default");
            break;
    }
    }else if(actionSheet.tag == 1){
        switch (buttonIndex) {
            case 0:
                NSLog(@"Take Picture");
                [self openCamera];
                break;
            case 1:
                NSLog(@"Upload Photo");
                [self openGallery];
                break;
            case 2:
                NSLog(@"Cancel");
                break;
            default:
                NSLog(@"Default");
                break;
        }
    }
}


//// MARK:  PhotoLibrary Methods
////////////////////******* UICollectionView Delegates ******* ///////////////

-(void)openCamera{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        imagePicker = [[UIImagePickerController alloc]init];
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePicker.delegate = self;
        [self presentViewController:imagePicker animated:YES completion:nil];
    }else{
        UIAlertView *alertWarning = [[UIAlertView alloc]initWithTitle:@"Warning" message:@"Error accessing Camera" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:@"", nil];
        [alertWarning show];
    }
}


-(void)openGallery{
//    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
//        imagePicker = [[UIImagePickerController alloc]init];
//        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
//        imagePicker.delegate = self;
//        [self presentViewController:imagePicker animated:YES completion:nil];
//    }else{
//        UIAlertView *alertWarning = [[UIAlertView alloc]initWithTitle:@"Warning" message:@"Error accessing Photo Library" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:@"", nil];
//        [alertWarning show];
//    }
    
    QBImagePickerController *imagePickerController = [QBImagePickerController new];
    imagePickerController.delegate = self;
    imagePickerController.mediaType = QBImagePickerMediaTypeImage;
    imagePickerController.allowsMultipleSelection = YES;
    imagePickerController.showsNumberOfSelectedAssets = YES;

    imagePickerController.minimumNumberOfSelection = 1;
    imagePickerController.maximumNumberOfSelection = 3;

   // [imagePickerController.selectedAssets addObject:[PHAsset fetchAssetsWithOptions:nil].lastObject];

    [self presentViewController:imagePickerController animated:YES completion:NULL];
//    
//    BRImagePicker *imagePicker = [[BRImagePicker alloc] initWithPresentingController:self];
//    [imagePicker showPickerWithDataBlock:^(NSArray *data) {
//        
//        // Each object in returned Array will be of type  "BR_ImageInfo"
//        
//        self.selectedAssetsArray = data;
//        
//        for (int i = 0; i<self.selectedAssetsArray.count; i++) {
//            BR_ImageInfo *imageInfo = [self.selectedAssetsArray objectAtIndex:i];
//            [self sendImage:imageInfo.image];
//        }
    
    //    self.infoLabel.text = [NSString stringWithFormat:@"%lu  %@ selected.",(unsigned long)self.selectedAssetsArray.count, self.selectedAssetsArray.count==1 ? @"Asset" : @"Assets"];
        
     //   [self.assetsTableView reloadData];
 //   }];
    
    
}


#pragma mark - QBImagePickerControllerDelegate

- (void)qb_imagePickerController:(QBImagePickerController *)imagePickerController didFinishPickingAssets:(NSArray *)assets
{
    NSLog(@"Selected assets:");
    NSLog(@"%@", assets);
//    for (int i = 0; i<assets.count; i++) {
//    // [self sendImage:imageInfo.image];
//        PHAsset *asset = assets[i];
//        
//    }
    
    
 //   self.assets = [NSMutableArray arrayWithArray:assets];
    @autoreleasepool {
        
 
    PHImageRequestOptions *requestOptions = [[PHImageRequestOptions alloc] init];
    requestOptions.resizeMode   = PHImageRequestOptionsResizeModeExact;
    requestOptions.deliveryMode = PHImageRequestOptionsDeliveryModeHighQualityFormat;
    
    // this one is key
    requestOptions.synchronous = true;
    
    
    PHImageManager *manager = [PHImageManager defaultManager];
    NSMutableArray *images = [NSMutableArray arrayWithCapacity:[assets count]];
    
    // assets contains PHAsset objects.
    __block UIImage *ima;
    
    for (PHAsset *asset in assets) {
        // Do something with the asset
        
        [manager requestImageForAsset:asset
                           targetSize:PHImageManagerMaximumSize
                          contentMode:PHImageContentModeDefault
                              options:requestOptions
                        resultHandler:^void(UIImage *image, NSDictionary *info) {
                            ima = image;
                         //   [self sendImage:ima];

                            [images addObject:ima];
                        }];
        
        
    }
     [self dismissViewControllerAnimated:YES completion:NULL];
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            //Background Thread
            dispatch_async(dispatch_get_main_queue(), ^(void){
                //Run UI Updates
      
    [UIView animateWithDuration:0.5 animations:^{
        if (images.count > 0) {
            UIImage * tempImage = [images objectAtIndex:0];
            [self performSelector:@selector(sendImage:) withObject:tempImage afterDelay:0.0];
           // [self sendImage:tempImage];
        }
    } completion:^(BOOL finished) {
        
        [UIView animateWithDuration:0.5 animations:^{
            if (images.count > 1) {
                UIImage * tempImage = [images objectAtIndex:1];
                [self performSelector:@selector(sendImage:) withObject:tempImage afterDelay:1.0];

              //  [self sendImage:tempImage];
            }
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.5 animations:^{
                if (images.count > 2) {
                    UIImage * tempImage = [images objectAtIndex:2];
                    [self performSelector:@selector(sendImage:) withObject:tempImage afterDelay:2.0];

                 //   [self sendImage:tempImage];
                }
            } completion:^(BOOL finished) {
                [UIView animateWithDuration:0.5 animations:^{
                    if (images.count > 3) {
                        UIImage * tempImage = [images objectAtIndex:3];
                        [self performSelector:@selector(sendImage:) withObject:tempImage afterDelay:1.0];

                       // [self sendImage:tempImage];
                    }
                } completion:^(BOOL finished) {
                    [UIView animateWithDuration:0.5 animations:^{
                        if (images.count > 4) {
                            UIImage * tempImage = [images objectAtIndex:4];
                            [self performSelector:@selector(sendImage:) withObject:tempImage afterDelay:1.0];

                         //   [self sendImage:tempImage];
                        }
                    } completion:^(BOOL finished) {
                        [UIView animateWithDuration:0.5 animations:^{
                            if (images.count > 5) {
                                UIImage * tempImage = [images objectAtIndex:5];
                                [self performSelector:@selector(sendImage:) withObject:tempImage afterDelay:1.0];

                               // [self sendImage:tempImage];
                            }
                        } completion:^(BOOL finished) {
                            [UIView animateWithDuration:0.5 animations:^{
                                if (images.count > 6) {
                                    UIImage * tempImage = [images objectAtIndex:6];
                                    [self performSelector:@selector(sendImage:) withObject:tempImage afterDelay:1.0];

                                  //  [self sendImage:tempImage];
                                }
                            } completion:^(BOOL finished) {
                                
                            }];
                        }];
                    }];
                }];
            }];

        }];

       
    }];
            });
        });
  
    
  
//    
//    if (images.count > 2) {
//        UIImage * tempImage = [images objectAtIndex:2];
//        [self sendImage:tempImage];
//    }
//    
//    if (images.count > 3) {
//        UIImage * tempImage = [images objectAtIndex:3];
//        [self sendImage:tempImage];
//    }
//    
//    if (images.count > 4) {
//        UIImage * tempImage = [images objectAtIndex:4];
//        [self sendImage:tempImage];
//    }
//    
//    if (images.count > 5) {
//        UIImage * tempImage = [images objectAtIndex:5];
//        [self sendImage:tempImage];
//    }
//    
//    if (images.count > 6) {
//        UIImage * tempImage = [images objectAtIndex:6];
//        [self sendImage:tempImage];
//    }
//    

       }
}

- (void)qb_imagePickerControllerDidCancel:(QBImagePickerController *)imagePickerController
{
    NSLog(@"Canceled.");
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}


-(void)sendImage:(UIImage *)image{
    
    User *userObj = (User *)[DatabaseHelper getObjectIfExist:@"User" predicate:[NSPredicate predicateWithFormat:@"isLoggedIn == 1"]];
    NSTimeInterval timeInMiliseconds = [[NSDate date] timeIntervalSince1970];
    NSString *strMessageID = [NSString stringWithFormat:@"%0.lf%@",timeInMiliseconds,self.chat.contactID];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",strMessageID]];
    NSData* data = UIImageJPEGRepresentation(image, 0.15);
    [data writeToFile:path atomically:YES];
    
    UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);

    
    
    Message *messageObj = (Message *) [DatabaseHelper getObjectIfExist:@"Message" predicate:[NSPredicate predicateWithFormat:@"identifier == %@",strMessageID]];
    [messageObj setIdentifier:strMessageID];
    [messageObj setStrTag:strMessageID];
    // Message *message = [[Message alloc] init];
    messageObj.text = @"Picture";
    [messageObj setMessage:@"Picture"];
    messageObj.sender = [NSNumber numberWithInt:0];
    messageObj.status = [NSNumber numberWithInt:0];
    messageObj.date = [NSDate date];
    messageObj.senderID = userObj.userID;
    messageObj.message_type = [NSNumber numberWithInt:1];
    messageObj.file_link = path;
    messageObj.chat_id =  [NSString stringWithFormat:@"%@",_chat.contactID];
    [DatabaseHelper commitChanges];
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    _chat.messageID = strMessageID;
    [DatabaseHelper commitChanges];
    
    [self.tableArray addObject:messageObj];
    
    //Insert Message in UI
    NSIndexPath *indexPath = [self.tableArray indexPathForMessage:messageObj];
    [self.tableView beginUpdates];
    if ([self.tableArray numberOfMessagesInSection:indexPath.section] == 1)
        [self.tableView insertSections:[NSIndexSet indexSetWithIndex:indexPath.section]
                      withRowAnimation:UITableViewRowAnimationNone];
    [self.tableView insertRowsAtIndexPaths:@[indexPath]
                          withRowAnimation:UITableViewRowAnimationBottom];
    [self.tableView endUpdates];
    
    [self.tableView scrollToRowAtIndexPath:[self.tableArray indexPathForLastMessage]
                          atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    
    //    //Send message to server
    [self sendingPictureFromService:messageObj];
    [self.inputbar.textView setText:@""];
    [self tableViewScrollToBottomAnimated:YES];
}

-(void)openVideoGallery{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
        imagePicker = [[UIImagePickerController alloc]init];
        imagePicker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        imagePicker.videoQuality = UIImagePickerControllerQualityTypeMedium;
        imagePicker.mediaTypes = [NSArray arrayWithObject:(NSString *)kUTTypeMovie];
        imagePicker.delegate = self;
        [self presentViewController:imagePicker animated:YES completion:nil];
    }else{
        UIAlertView *alertWarning = [[UIAlertView alloc]initWithTitle:@"Warning" message:@"Error accessing Video Library" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:@"", nil];
        [alertWarning show];
    }
}

-(void)openVideoCamera{
    
    if (([UIImagePickerController isSourceTypeAvailable:
          UIImagePickerControllerSourceTypeCamera] == NO)){
        UIAlertView *alertWarning = [[UIAlertView alloc]initWithTitle:@"Warning" message:@"Error accessing Video Camera" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:@"", nil];
        [alertWarning show];
    }else{
        imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePicker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeMovie, nil];
        
        imagePicker.allowsEditing = NO;
        imagePicker.delegate = self;
        imagePicker.videoQuality = UIImagePickerControllerQualityTypeMedium;//you can change the quality here
        [self presentModalViewController:imagePicker animated:YES];
    }
  
}


- (void)compressVideoWithInputVideoUrl:(NSURL *) inputVideoUrl
{
    /* Create Output File Url */
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *finalVideoURLString = [documentsDirectory stringByAppendingPathComponent:@"compressedVideo.mp4"];
    NSURL *outputVideoUrl = ([[NSURL URLWithString:finalVideoURLString] isFileURL] == 1)?([NSURL URLWithString:finalVideoURLString]):([NSURL fileURLWithPath:finalVideoURLString]); // Url Should be a file Url, so here we check and convert it into a file Url
    
    
    SDAVAssetExportSession *compressionEncoder = [SDAVAssetExportSession.alloc initWithAsset:[AVAsset assetWithURL:inputVideoUrl]]; // provide inputVideo Url Here
    compressionEncoder.outputFileType = AVFileTypeMPEG4;
    compressionEncoder.outputURL = outputVideoUrl; //Provide output video Url here
    compressionEncoder.videoSettings = @
    {
    AVVideoCodecKey: AVVideoCodecH264,
    AVVideoWidthKey: @800,   //Set your resolution width here
    AVVideoHeightKey: @600,  //set your resolution height here
    AVVideoCompressionPropertiesKey: @
        {
        AVVideoAverageBitRateKey: @45000, // Give your bitrate here for lower size give low values
        AVVideoProfileLevelKey: AVVideoProfileLevelH264High40,
        },
    };
    compressionEncoder.audioSettings = @
    {
    AVFormatIDKey: @(kAudioFormatMPEG4AAC),
    AVNumberOfChannelsKey: @2,
    AVSampleRateKey: @44100,
    AVEncoderBitRateKey: @128000,
    };
    
    [compressionEncoder exportAsynchronouslyWithCompletionHandler:^
     {
         if (compressionEncoder.status == AVAssetExportSessionStatusCompleted)
         {
             NSLog(@"Compression Export Completed Successfully");
         }
         else if (compressionEncoder.status == AVAssetExportSessionStatusCancelled)
         {
             NSLog(@"Compression Export Canceled");
         }
         else
         {
             NSLog(@"Compression Failed");
             
         }
     }];
    
}


-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    [picker.presentingViewController dismissModalViewControllerAnimated:YES];
    NSString *type = [info objectForKey:UIImagePickerControllerMediaType];
    NSLog(@"type=%@",type);
    if ([type isEqualToString:(NSString *)kUTTypeVideo] ||
        [type isEqualToString:(NSString *)kUTTypeMovie])
    {
        NSURL *urlvideo = [info objectForKey:UIImagePickerControllerMediaURL];
        User *userObj = (User *)[DatabaseHelper getObjectIfExist:@"User" predicate:[NSPredicate predicateWithFormat:@"isLoggedIn == 1"]];
        NSTimeInterval timeInMiliseconds = [[NSDate date] timeIntervalSince1970];
        NSString *strMessageID = [NSString stringWithFormat:@"%0.lf%@",timeInMiliseconds,self.chat.contactID];
        NSString *moviePath = [urlvideo path];
        
        NSData *videoData = [[NSData alloc] initWithContentsOfFile:moviePath];

        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                             NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.MOV",strMessageID]];
        [videoData writeToFile:path atomically:YES];
//        if (UIVideoAtPathIsCompatibleWithSavedPhotosAlbum (moviePath)) {
//            UISaveVideoAtPathToSavedPhotosAlbum (moviePath, nil, nil, nil);
//        }
//        
        Message *messageObj = (Message *) [DatabaseHelper getObjectIfExist:@"Message" predicate:[NSPredicate predicateWithFormat:@"identifier == %@",strMessageID]];
        [messageObj setIdentifier:strMessageID];
        [messageObj setStrTag:strMessageID];
        // Message *message = [[Message alloc] init];
        messageObj.text = @"Video";
        messageObj.sender = [NSNumber numberWithInt:0];
        messageObj.status = [NSNumber numberWithInt:0];
        messageObj.date = [NSDate date];
        messageObj.senderID = userObj.userID;
        messageObj.message_type = [NSNumber numberWithInt:3];
        messageObj.file_link = [NSString stringWithFormat:@"%@",path];
        messageObj.chat_id =  [NSString stringWithFormat:@"%@",_chat.contactID];
        [DatabaseHelper commitChanges];
        NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
        f.numberStyle = NSNumberFormatterDecimalStyle;
        _chat.messageID = strMessageID;
        [DatabaseHelper commitChanges];
        
        [self.tableArray addObject:messageObj];
        
        //Insert Message in UI
        NSIndexPath *indexPath = [self.tableArray indexPathForMessage:messageObj];
        [self.tableView beginUpdates];
        if ([self.tableArray numberOfMessagesInSection:indexPath.section] == 1)
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:indexPath.section]
                          withRowAnimation:UITableViewRowAnimationNone];
        [self.tableView insertRowsAtIndexPaths:@[indexPath]
                              withRowAnimation:UITableViewRowAnimationBottom];
        [self.tableView endUpdates];
        
        [self.tableView scrollToRowAtIndexPath:[self.tableArray indexPathForLastMessage]
                              atScrollPosition:UITableViewScrollPositionBottom animated:YES];
        
        //    //Send message to server
       // [self compressVideoWithInputVideoUrl:[NSURL URLWithString:messageObj.file_link]];
        [self sendingVideoFromService:messageObj];
        [self.inputbar.textView setText:@""];
        [self tableViewScrollToBottomAnimated:YES];

        
        NSLog(@"%@",urlvideo);
    }else{
    UIImage *image = (UIImage *)info[UIImagePickerControllerOriginalImage];

    User *userObj = (User *)[DatabaseHelper getObjectIfExist:@"User" predicate:[NSPredicate predicateWithFormat:@"isLoggedIn == 1"]];
        NSTimeInterval timeInMiliseconds = [[NSDate date] timeIntervalSince1970];
        NSString *strMessageID = [NSString stringWithFormat:@"%0.lf%@",timeInMiliseconds,self.chat.contactID];
        
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",strMessageID]];
    NSData* data = UIImageJPEGRepresentation(image, 0.15);
    [data writeToFile:path atomically:YES];
        
        if (picker.sourceType == UIImagePickerControllerSourceTypeCamera) {
            UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
        }

    
    Message *messageObj = (Message *) [DatabaseHelper getObjectIfExist:@"Message" predicate:[NSPredicate predicateWithFormat:@"identifier == %@",strMessageID]];
    [messageObj setIdentifier:strMessageID];
    [messageObj setStrTag:strMessageID];
    // Message *message = [[Message alloc] init];
    messageObj.text = @"Picture";
    messageObj.sender = [NSNumber numberWithInt:0];
    messageObj.status = [NSNumber numberWithInt:0];
    messageObj.date = [NSDate date];
    messageObj.senderID = userObj.userID;
    messageObj.message_type = [NSNumber numberWithInt:1];
    messageObj.file_link = path;
    messageObj.chat_id =  [NSString stringWithFormat:@"%@",_chat.contactID];
    [DatabaseHelper commitChanges];
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    _chat.messageID = strMessageID;
    [DatabaseHelper commitChanges];
    
    [self.tableArray addObject:messageObj];
    
    //Insert Message in UI
    NSIndexPath *indexPath = [self.tableArray indexPathForMessage:messageObj];
    [self.tableView beginUpdates];
    if ([self.tableArray numberOfMessagesInSection:indexPath.section] == 1)
        [self.tableView insertSections:[NSIndexSet indexSetWithIndex:indexPath.section]
                      withRowAnimation:UITableViewRowAnimationNone];
    [self.tableView insertRowsAtIndexPaths:@[indexPath]
                          withRowAnimation:UITableViewRowAnimationBottom];
    [self.tableView endUpdates];
    
    [self.tableView scrollToRowAtIndexPath:[self.tableArray indexPathForLastMessage]
                          atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    
//    //Send message to server
    [self sendingPictureFromService:messageObj];
    [self.inputbar.textView setText:@""];
    [self tableViewScrollToBottomAnimated:YES];
    }
    
    
}

- (void)textViewDidChange:(UITextView *)textView{
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range
 replacementText:(NSString *)atext {
    NSLog(@"Meow");
    return YES;
}

- (NSString *)uniqueFileName
{
    CFUUIDRef theUniqueString = CFUUIDCreate(NULL);
    CFStringRef string = CFUUIDCreateString(NULL, theUniqueString);
    CFRelease(theUniqueString);
    return (NSString *)CFBridgingRelease(string) ;
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [picker.presentingViewController dismissModalViewControllerAnimated:YES];
}



/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end


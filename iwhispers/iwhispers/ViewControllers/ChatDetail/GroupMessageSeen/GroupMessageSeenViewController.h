//
//  GeoupMessageSeenViewController.h
//  iwhispers
//
//  Created by Wasif Iqbal on 12/23/16.
//  Copyright © 2016 Appiskey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Message.h"

@interface GroupMessageSeenViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tblGroupSeen;
@property (weak, nonatomic) Message *messageObj;
@property (weak, nonatomic) NSString *strGroupID;

@end


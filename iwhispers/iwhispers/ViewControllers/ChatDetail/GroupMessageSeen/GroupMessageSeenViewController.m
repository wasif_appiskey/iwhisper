//
//  GeoupMessageSeenViewController.m
//  iwhispers
//
//  Created by Wasif Iqbal on 12/23/16.
//  Copyright © 2016 Appiskey. All rights reserved.
//

#import "GroupMessageSeenViewController.h"
#import "GroupChatDetalViewCell.h"
#import "GroupAudioViewCell.h"
#import "GroupVideoViewCell.h"
#import "GroupMessageSeenViewCell.h"

@interface GroupMessageSeenViewController (){
    NSArray *arrMessageSeen;
    NSArray *arrMessageRead;
}

@end

@implementation GroupMessageSeenViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupNavigation];
    [self setupData];
    // Do any additional setup after loading the view from its nib.
}


-(void)setupData{
    [self settingMessageReadArr];
    [self settingMessageSeenArr];
    [_tblGroupSeen reloadData];
}


-(void)settingMessageSeenArr{
    
    NSString *strMesageID = [NSString stringWithFormat:@"%@", self.messageObj.identifier];
    NSNumber *statusSeen = [NSNumber  numberWithInt:3];

    arrMessageSeen = (NSArray *)[DatabaseHelper getAllData:@"GroupMessageSeen" offset:0 predicate:[NSPredicate predicateWithFormat:@"message_id == %@ && chat_id == %@ && status == %@",strMesageID,self.strGroupID,statusSeen] sortDescriptor:nil];

}


-(void)settingMessageReadArr{
    
    NSString *strMesageID = [NSString stringWithFormat:@"%@", self.messageObj.identifier];
    NSNumber *statusRead = [NSNumber  numberWithInt:2];
    arrMessageRead = (NSArray *)[DatabaseHelper getAllData:@"GroupMessageSeen" offset:0 predicate:[NSPredicate predicateWithFormat:@"message_id == %@ && chat_id == %@ && status == %@",strMesageID,self.strGroupID,statusRead] sortDescriptor:nil];

}

-(void)setupNavigation{
    [self  setupNavigationTitle];
    [self setupLeftButton];
}

-(void)setupNavigationTitle{
    self.title = @"Message Info";
}


-(void)setupLeftButton{
    UIButton *backBtn =  [UIButton buttonWithType:UIButtonTypeCustom];
    [backBtn setImage:[UIImage imageNamed:@"arrow-white.png"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backVC) forControlEvents:UIControlEventTouchUpInside];
    [backBtn setFrame:CGRectMake(0, 0,25, 25)];
    
    UIView *rightBarButtonItems = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    [rightBarButtonItems addSubview:backBtn];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightBarButtonItems];
    
}

-(void)backVC{
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        
        return [self.messageObj.heigh floatValue]+ 5;
        
    }else if (indexPath.section == 1){
        return 44;
    }else if (indexPath.section == 2){
        return 44;
    }
    return 0;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return 0;
    }else{
        return 30;
    }
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        
        return @"Message";
        
    }else if (section == 1){
        return @"SEEN BY";
    }else if (section == 2){
        return @"DELIVERED TO";
    }
    return @"";
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    CGRect frame = CGRectMake(0, 0, tableView.frame.size.width, 40);
    
    UIView *view = [[UIView alloc] initWithFrame:frame];
    
    
    view.backgroundColor = [UIColor lightGrayColor];
    view.alpha = 0.5;
    view.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 0,view.frame.size.width, view.frame.size.height)];
    label.text = [self tableView:tableView titleForHeaderInSection:section];
    label.textAlignment = NSTextAlignmentLeft;
    label.font = [UIFont fontWithName:@"OpenSans" size:12.0];
    [label sizeToFit];
    label.layer.masksToBounds = YES;
    label.autoresizingMask = UIViewAutoresizingNone;
    [view addSubview:label];
    
    return view;
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 1;
    }else if (section == 1){
        return arrMessageSeen.count + 1;
    }else if (section == 2){
        return arrMessageRead.count + 1;
    }
    return 0;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.section == 0) {
    
    
    if (self.messageObj.message_type == [NSNumber numberWithInt:2]){
        @try {
            
            GroupAudioViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GroupAudioViewCell"];
            cell.tag = indexPath.row;
            
            if (!cell)
            {
                cell = [[GroupAudioViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"GroupAudioViewCell"];
                cell.tag = indexPath.row ;
                cell.strTag = [NSString stringWithFormat:@"%ld",(long)indexPath.row ];
                
                
            }
            
            
            NSString *strMesageID = [NSString stringWithFormat:@"%@", self.messageObj.identifier];
            
        
            [cell setMessage:self.messageObj];
            cell.message.strTag = [NSString stringWithFormat:@"%ld",(long)indexPath.row ];
            if (cell.message.message_type == [NSNumber numberWithInt:2]){
                
                [cell.slider setTag:indexPath.row];
                [cell.btnPlay setTag:indexPath.row];
                
                [cell.lblAudioTime setTag:indexPath.row];
                if ([[[[cell subviews]objectAtIndex:0]subviews] count] > 6) {
                    [[[[[cell subviews]objectAtIndex:0]subviews]objectAtIndex:5] removeFromSuperview];
                    if ([[[[cell subviews]objectAtIndex:0]subviews] count] > 7) {
                        [[[[[cell subviews]objectAtIndex:0]subviews]objectAtIndex:6] removeFromSuperview];
                        
                    }
                    
                }
                cell.soundView.hidden = NO;
                cell.textView.hidden = YES;
                NSString *strImage = @"";
                if (cell.message.sender == [NSNumber numberWithInt:1]) {
                    Contact *contact = (Contact *)[DatabaseHelper getObjectIfExist:@"Contact" predicate:[NSPredicate predicateWithFormat:@"identifier == %@",cell.message.senderID]];
                    strImage = contact.image_id;
                }else{
                    User *userObj = (User *)[DatabaseHelper getObjectIfExist:@"User" predicate:[NSPredicate predicateWithFormat:@"isLoggedIn == 1"]];
                    strImage = userObj.userImage;
                }
                AsyncImageView * imgProfileAsync = [[AsyncImageView alloc]initWithFrame:CGRectMake(0, 0, cell.profileImage.frame.size.width, cell.profileImage.frame.size.height)];
                [imgProfileAsync loadImageFromURL:[NSURL URLWithString:strImage]];
                cell.profileImage.layer.cornerRadius  = CGRectGetWidth(cell.profileImage.frame) / 2.0;
                cell.profileImage.clipsToBounds = YES;
                [cell.profileImage addSubview:imgProfileAsync];
                
                
                
                
            }
            
            return cell;
            
        } @catch (NSException *exception) {
            NSLog(@"%@",exception);
            
        }
        
    }else if (self.messageObj.message_type == [NSNumber numberWithInt:3]){
        @try {
            
            GroupVideoViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GroupVideoViewCell"];
            if (!cell)
            {
                cell = [[GroupVideoViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"GroupVideoViewCell"];
                //  cell.bubbleX = 53.75;
                
            }
            

            cell.message = self.messageObj;
            
            NSString *strImage = @"";
            if (cell.message.sender == [NSNumber numberWithInt:1]) {
                Contact *contact = (Contact *)[DatabaseHelper getObjectIfExist:@"Contact" predicate:[NSPredicate predicateWithFormat:@"identifier == %@",cell.message.senderID]];
                strImage = contact.image_id;
            }else{
                User *userObj = (User *)[DatabaseHelper getObjectIfExist:@"User" predicate:[NSPredicate predicateWithFormat:@"isLoggedIn == 1"]];
                strImage = userObj.userImage;
            }
            
            
            
            if (cell.message.message_type == [NSNumber numberWithInt:3]) {
                
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                                     NSUserDomainMask, YES);
                NSString *documentsDirectory = [paths objectAtIndex:0];
                NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",cell.message.identifier]];
                [cell.moviePlayer.view setHidden:NO];
                cell.textView.hidden = YES;
                
                if (cell.message.sender == [NSNumber numberWithInt:0]) {
                    //[cell.moviePlayer loadImageFromNSBundle:path];
                }else{
                    // [cell.messageImage loadImageFromURL:[NSURL URLWithString:cell.message.file_link]];
                    
                }
                
                
            }else if (cell.message.message_type == [NSNumber numberWithInt:0]){
                //  [cell.messageImage setHidden:YES];
                cell.textView.hidden = NO;
            }
            
            return cell;
            
        } @catch (NSException *exception) {
            NSLog(@"%@",exception);
            
        }
        
    }else{
        GroupChatDetalViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GroupChatDetalViewCell"];
        if (!cell)
        {
            cell = [[GroupChatDetalViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"GroupChatDetalViewCell"];
            
        }
        
  
        
        NSString *strMesageID = [NSString stringWithFormat:@"%@", self.messageObj.identifier];
 
        
        
        NSArray *arrMessageSeenTemp = (NSArray *)[DatabaseHelper getAllData:@"GroupMessageSeen" offset:0 predicate:[NSPredicate predicateWithFormat:@"message_id == %@ && chat_id == %@",strMesageID,self.strGroupID] sortDescriptor:nil];
        
        cell.arrMessageSeen = arrMessageSeenTemp;
        
        
        cell.message = self.messageObj;
        
        
        NSString *strImage = @"";
        if (cell.message.sender == [NSNumber numberWithInt:1]) {
            Contact *contact = (Contact *)[DatabaseHelper getObjectIfExist:@"Contact" predicate:[NSPredicate predicateWithFormat:@"identifier == %@",cell.message.senderID]];
            strImage = contact.image_id;
        }else{
            User *userObj = (User *)[DatabaseHelper getObjectIfExist:@"User" predicate:[NSPredicate predicateWithFormat:@"isLoggedIn == 1"]];
            strImage = userObj.userImage;
        }
        
        AsyncImageView * imgProfileAsync = [[AsyncImageView alloc]initWithFrame:CGRectMake(0, 0, cell.profileImage.frame.size.width, cell.profileImage.frame.size.height)];
        [imgProfileAsync loadImageFromURL:[NSURL URLWithString:strImage]];
        cell.profileImage.layer.cornerRadius  = CGRectGetWidth(cell.profileImage.frame) / 2.0;
        cell.profileImage.clipsToBounds = YES;
        [cell.profileImage addSubview:imgProfileAsync];
        
        if (cell.message.message_type == [NSNumber numberWithInt:1]) {
            [cell.messageImage setBackgroundColor:[UIColor whiteColor]];
            
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                                 NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0];
            NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",cell.message.identifier]];
            [cell.messageImage setHidden:NO];
            cell.textView.hidden = YES;
            
            if (cell.message.sender == [NSNumber numberWithInt:0]) {
                [cell.messageImage loadImageFromNSBundle:path];
            }else{
                User *userObj = (User *)[DatabaseHelper getObjectIfExist:@"User" predicate:[NSPredicate predicateWithFormat:@"isLoggedIn == 1"]];
                if (userObj.saveMedia == [NSNumber numberWithInt:1]) {
                    cell.messageImage.saveImage = @"yes";
                }
                [cell.messageImage loadImageFromURL:[NSURL URLWithString:cell.message.file_link]];
                
            }
            
            
        }else if (cell.message.message_type == [NSNumber numberWithInt:0]){
            [cell.messageImage setHidden:YES];
            cell.textView.hidden = NO;
        }
        return cell;
    }
        return nil;
    }else if (indexPath.section == 1){


        
        
        GroupMessageSeenViewCell *cell = (GroupMessageSeenViewCell*)[tableView dequeueReusableCellWithIdentifier:[GroupMessageSeenViewCell cellIdentifier]];
        
        if (cell == nil) {
            cell = [GroupMessageSeenViewCell createCell];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        
        int totalCount = arrMessageSeen.count;
        if (indexPath.row == totalCount) {
            cell.imgMessageSeen.hidden = true;
            cell.lblMessageSeen.hidden = true;
            cell.imgBottom.hidden = false;
            cell.imgSeperator.hidden = true;
        }else{
            cell.imgMessageSeen.hidden = false;
            cell.lblMessageSeen.hidden = false;
            cell.imgBottom.hidden = true;
            cell.imgSeperator.hidden = false;
            GroupMessageSeen *messageSeenObj = (GroupMessageSeen *)[arrMessageSeen objectAtIndex:indexPath.row];
            
            
            NSString *strImage = @"";
            
            Contact *contact = (Contact *)[DatabaseHelper getObjectIfExist:@"Contact" predicate:[NSPredicate predicateWithFormat:@"identifier == %@",messageSeenObj.user_id]];
            strImage = contact.image_id;
            
            
            AsyncImageView * imgProfileAsync = [[AsyncImageView alloc]initWithFrame:CGRectMake(0, 0, cell.imgMessageSeen.frame.size.width, cell.imgMessageSeen.frame.size.height)];
            [imgProfileAsync loadImageFromURL:[NSURL URLWithString:strImage]];
            cell.imgMessageSeen.layer.cornerRadius  = CGRectGetWidth(cell.imgMessageSeen.frame) / 2.0;
            cell.imgMessageSeen.clipsToBounds = YES;
            [cell.imgMessageSeen addSubview:imgProfileAsync];
            if (contact.name != nil) {
                cell.lblMessageSeen.text  = contact.name;
                
            }else{
                cell.lblMessageSeen.text  = contact.serverName;
                
            }
        }


        return cell;
    }else if (indexPath.section == 2){
        
        GroupMessageSeenViewCell *cell = (GroupMessageSeenViewCell*)[tableView dequeueReusableCellWithIdentifier:[GroupMessageSeenViewCell cellIdentifier]];
        
        if (cell == nil) {
            cell = [GroupMessageSeenViewCell createCell];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        
        int totalCount = arrMessageRead.count;
        if (indexPath.row == totalCount) {
            cell.imgMessageSeen.hidden = true;
            cell.lblMessageSeen.hidden = true;
            cell.imgBottom.hidden = false;
            cell.imgSeperator.hidden = true;
        }else{
            cell.imgMessageSeen.hidden = false;
            cell.lblMessageSeen.hidden = false;
            cell.imgBottom.hidden = true;
            cell.imgSeperator.hidden = false;
        GroupMessageSeen *messageSeenObj = (GroupMessageSeen *)[arrMessageRead objectAtIndex:indexPath.row];

        
        NSString *strImage = @"";
        Contact *contact = (Contact *)[DatabaseHelper getObjectIfExist:@"Contact" predicate:[NSPredicate predicateWithFormat:@"identifier == %@",messageSeenObj.user_id]];
        strImage = contact.image_id;
        
        
        AsyncImageView * imgProfileAsync = [[AsyncImageView alloc]initWithFrame:CGRectMake(0, 0, cell.imgMessageSeen.frame.size.width, cell.imgMessageSeen.frame.size.height)];
        [imgProfileAsync loadImageFromURL:[NSURL URLWithString:strImage]];
        cell.imgMessageSeen.layer.cornerRadius  = CGRectGetWidth(cell.imgMessageSeen.frame) / 2.0;
        cell.imgMessageSeen.clipsToBounds = YES;
        [cell.imgMessageSeen addSubview:imgProfileAsync];
            if (contact.name != nil) {
                cell.lblMessageSeen.text  = contact.name;

            }else{
                cell.lblMessageSeen.text  = contact.serverName;

            }
        }
        
        return cell;
    }
    
    return nil;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

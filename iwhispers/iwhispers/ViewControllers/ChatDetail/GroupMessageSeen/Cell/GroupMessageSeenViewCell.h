//
//  GroupMessageSeenViewCell.h
//  iwhispers
//
//  Created by Wasif Iqbal on 12/26/16.
//  Copyright © 2016 Appiskey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GroupMessageSeenViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgMessageSeen;
@property (weak, nonatomic) IBOutlet UIImageView *imgBottom;
@property (weak, nonatomic) IBOutlet UIImageView *imgSeperator;
@property (weak, nonatomic) IBOutlet UILabel *lblMessageSeen;
+(NSString*)cellIdentifier;
+(id)createCell;
@end

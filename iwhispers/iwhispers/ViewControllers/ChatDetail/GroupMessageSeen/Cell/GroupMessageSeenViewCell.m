//
//  GroupMessageSeenViewCell.m
//  iwhispers
//
//  Created by Wasif Iqbal on 12/26/16.
//  Copyright © 2016 Appiskey. All rights reserved.
//

#import "GroupMessageSeenViewCell.h"

@implementation GroupMessageSeenViewCell

- (void)awakeFromNib {
    // Initialization code
}

+(NSString*)cellIdentifier {
    static NSString *cellIdentifier = @"GroupMessageSeenViewCellIdentifier";
    return cellIdentifier;
}

+(id)createCell {
    
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"GroupMessageSeenViewCell" owner:self options:nil];
    return [nib lastObject];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}


@end

//
//  ChatDetailViewController.h
//  iwhispers
//
//  Created by Apple on 6/29/16.
//  Copyright © 2016 Appiskey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TableArray.h"
#import "Chat.h"
#import "Group.h"



@interface ChatDetailViewController : UIViewController
@property (strong, nonatomic) Chat *chat;
@property (strong, nonatomic) Contact  *contactObj;
@property (strong, nonatomic) TableArray *tableArray;
@property (strong, nonatomic) Group *groupObj;
//@property (strong, nonatomic) AppDelegate *appDelegate;
@property (strong, nonatomic) IBOutlet UIView *viewMainImage;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;
- (IBAction)btnActionClose:(id)sender;

-(void)addingIncomingMessages:(Message *)message;
-(void)showAlert;
-(void)updateMessageCell:(Message *)messageObj;
-(void)sendingMessageFromService:(Message *)message;
-(void)sendingPictureFromService:(Message *)message;
-(void)sendingAudioFromService:(Message *)message;
-(void)sendingVideoFromService:(Message *)message;

@end

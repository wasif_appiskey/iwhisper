//
//  GroupChatDetalViewCell.m
//  iwhispers
//
//  Created by Apple on 9/8/16.
//  Copyright © 2016 Appiskey. All rights reserved.
//

#import "GroupChatDetalViewCell.h"
#import "AppConstants.h"
#import "DatabaseHelper.h"
#import "PhotoViewerViewController.h"
#import "ChatDetailViewController.h"

@interface GroupChatDetalViewCell ()<UIActionSheetDelegate>{
    
}
@property (strong, nonatomic) UILabel *timeLabel;
@property (strong, nonatomic) UIImageView *bubbleImage;
@property (strong, nonatomic) UIImageView *statusIcon;
@property (strong, nonatomic) UIImageView *imageFile;

@property (strong, nonatomic) AsyncImageView *imageViewFull;
@property (strong, nonatomic) UIButton *btnClose;

@property (strong, nonatomic) UIView *viewImageFull;



@end

@implementation GroupChatDetalViewCell

-(void)settingImageFullView{
    _viewImageFull = [[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    _imageViewFull = [[AsyncImageView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    self.imageViewFull.strTag = @"1.0";
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg",self.message.identifier]];
    if (self.message.sender == [NSNumber numberWithInt:0]) {
        [self.imageViewFull loadImageFromNSBundle:path];
    }else{
        [self.imageViewFull loadImageFromURL:[NSURL URLWithString:self.message.file_link]];
        
    }
    self.imageViewFull.contentMode = UIViewContentModeScaleAspectFit;
    _btnClose = [[UIButton alloc]initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width -100, 0, 100, 100)];
    [_btnClose setTitle:@"Close" forState:UIControlStateNormal];
    [_btnClose setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_viewImageFull setBackgroundColor:[UIColor blackColor]];
    [_btnClose addTarget:self action:@selector(closeImageView) forControlEvents:UIControlEventTouchUpInside];
//    [[[[UIApplication sharedApplication] delegate] window] addSubview:_viewImageFull];
//    [[[[UIApplication sharedApplication] delegate] window] addSubview:_imageViewFull];
//    [[[[UIApplication sharedApplication] delegate] window] addSubview:_btnClose];
    
    PhotoViewerViewController *photoVC = [[PhotoViewerViewController alloc]initWithNibName:@"PhotoViewerViewController" bundle:nil];
    photoVC.imageView = self.imageViewFull;
    [self.navigationController pushViewController:photoVC animated:YES];
    
    
}

-(void)closeImageView{
    [_viewImageFull removeFromSuperview];
    [_btnClose removeFromSuperview];
    [_imageViewFull removeFromSuperview];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}


-(CGFloat)height
{
    return _bubbleImage.frame.size.height;
}
-(void)updateMessageStatus
{
    [self buildCell];
    //Animate Transition
    _statusIcon.alpha = 0;
    [UIView animateWithDuration:.5 animations:^{
        _statusIcon.alpha = 1;
    }];
}

#pragma mark -

-(id)init
{
    self = [super init];
    if (self)
    {
        [self commonInit];
    }
    return self;
}
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        [self commonInit];
    }
    return self;
}
-(void)commonInit
{
    self.backgroundColor = [UIColor clearColor];
    self.contentView.backgroundColor = [UIColor clearColor];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.accessoryType = UITableViewCellAccessoryNone;
    _lblContactName = [[UILabel alloc] init];
    _textView = [[UITextView alloc] init];
    _bubbleImage = [[UIImageView alloc] init];
    _messageImage = [[AsyncImageView alloc]init];
    _timeLabel = [[UILabel alloc] init];
    _statusIcon = [[UIImageView alloc] init];
    _resendButton = [[UIButton alloc] init];
    _profileImage = [[UIImageView alloc] init];
    
    _resendButton.hidden = YES;
    
    [self.contentView addSubview:_bubbleImage];
    [_messageImage removeFromSuperview];
    [_bubbleImage addSubview:_messageImage];
    [self.contentView addSubview:_lblContactName];
    [self.contentView addSubview:_textView];
    [self.contentView addSubview:_timeLabel];
    [self.contentView addSubview:_statusIcon];
    [self.contentView addSubview:_resendButton];
    [self.contentView addSubview:_profileImage];
    // [self.contentView addSubview:_soundView];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedOnInfoView:)];
    [tap setNumberOfTouchesRequired:1];
    [tap setNumberOfTapsRequired:1];
    [self.messageImage addGestureRecognizer:tap];
    [self.messageImage setUserInteractionEnabled:YES];
    [self.bubbleImage setUserInteractionEnabled:YES];
    
    
}
-(void)prepareForReuse
{
    [super prepareForReuse];
    
    _textView.text = @"";
    _timeLabel.text = @"";
    _statusIcon.image = nil;
    _bubbleImage.image = nil;
    _profileImage.image = nil;
    _resendButton.hidden = YES;
}
-(void)setMessage:(Message *)message
{
    _message = message;
    [self buildCell];
    @try {
        
        message.heigh = [NSNumber numberWithFloat:self.height];
    } @catch (NSException *exception) {
        NSLog(@"%@",exception);
    }
    
}
-(void)buildCell
{
    @try {
        [self setTextView];
        [self setTimeLabel];
        [self setBubble];
        [self setNameLabel];
        [self addStatusIcon];
        [self setStatusIcon];
        [self setProfileImage];
        [self setFailedButton];
        
        if (_message.sender == [NSNumber numberWithInt:1]){
            if ([self alignmentForString:_textView.text] == YES) {
                _textView.frame = CGRectMake(_timeLabel.frame.origin.x+ _timeLabel.frame.size.width - _textView.frame.size.width + 10 , _textView.frame.origin.y, _textView.frame.size.width,  _textView.frame.size.height);
                _textView.textAlignment = NSTextAlignmentRight;
            }
        }else{
            if ([self alignmentForString:_textView.text] == YES) {
                _textView.textAlignment = NSTextAlignmentRight;
                
            }else{
                _textView.textAlignment = NSTextAlignmentLeft;
                
            }
            
        }
        
    } @catch (NSException *exception) {
        NSLog(@"%@",exception);
    }
    
    [self setNeedsLayout];
}



-(BOOL)alignmentForString:(NSString *)astring
{
    
    if (astring.length) {
        
        NSArray *rightLeftLanguages = @[@"ar",@"he",@"ur"];
        
        NSString *lang = CFBridgingRelease(CFStringTokenizerCopyBestStringLanguage((CFStringRef)astring,CFRangeMake(0,[astring length])));
        
        if ([rightLeftLanguages containsObject:lang]) {
            
            return YES;
            
        }
    }
    
    return NO;
    
}


- (void)tappedOnInfoView:(UIGestureRecognizer *)sender {
    [self settingImageFullView];
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"DismissKeyboard"
     object:self];
    NSLog(@"file %@",_message.file_link);
    
}


-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    for (UIView *view in self.contentView.subviews)
    {
        //  NSLog(@"%@",view);
        if ([view isKindOfClass:[UIImageView class]]) {
            //      NSLog(@"Image");
            
        }
    }
}

- (NSString*)convertTime:(NSInteger)time
{
    NSInteger minutes = time / 60;
    NSInteger seconds = time % 60;
    return [NSString stringWithFormat:@"%02ld:%02ld", (long)minutes, (long)seconds];
}

-(void)setProfileImage{
    
    _profileImage.frame = CGRectMake(0, 0, 0, 0);
    _profileImage.backgroundColor = [UIColor clearColor];
    
    
    
    CGFloat profileImage_x;
    CGFloat profileImage_y;
    CGFloat profileImage_w = _profileImage.frame.size.width;
    CGFloat profileImage_h = _profileImage.frame.size.height;
    UIViewAutoresizing autoresizing;
    
    if (_message.sender == [NSNumber numberWithInt:0])
    {
        autoresizing = UIViewAutoresizingFlexibleLeftMargin;
        
        //    [_profileImage setImage:[UIImage imageNamed:@"image1chat.png"]];
        profileImage_x = self.contentView.frame.size.width - profileImage_w - 2;
        profileImage_y = 0;
        
    }
    else
    {
        // [_profileImage setImage:[UIImage imageNamed:@"image2Chat.png"]];
        profileImage_x = 2;
        profileImage_y = 0;
        autoresizing = UIViewAutoresizingFlexibleRightMargin;
        
    }
    //    _profileImage.autoresizingMask = autoresizing;
    //
    //    _profileImage.frame = CGRectMake(profileImage_x, profileImage_y, profileImage_w, profileImage_h);
}

#pragma mark - TextView

-(void)setTextView
{
    CGFloat max_witdh = 0.7*self.contentView.frame.size.width;
    CGFloat max_height = 0.7*self.contentView.frame.size.height;
    
    _textView.frame = CGRectMake(0, 0, max_witdh, max_height);
    _textView.font = [UIFont fontWithName:@"OpenSans" size:15.0];
    _textView.backgroundColor = [UIColor clearColor];
    _textView.editable = NO;
    _textView.dataDetectorTypes = UIDataDetectorTypeLink;
    _textView.selectable = YES;
    _textView.textColor = [UIColor whiteColor];
    _textView.dataDetectorTypes = UIDataDetectorTypeLink;
    
    if (_message.sender == [NSNumber numberWithInt:0]) {

    
    _textView.text =  [AESCrypt decrypt:_message.text password:_message.aesEncrypted];
        
        
    }else{
        NSString *strAESDecrypted = [AESCrypt decrptyAESKey:_message.aesEncrypted privateKey:KPrivateKey];
        
        NSString *decryptMessage =  [AESCrypt decrypt:_message.text password:strAESDecrypted];
        
        _textView.text = decryptMessage;
    }
    //_textView.text =  _message.text;
    
    [_textView sizeToFit];
    if (_message.sender == [NSNumber numberWithInt:0]) {
        _textView.linkTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor],NSUnderlineStyleAttributeName: [NSNumber numberWithInt:NSUnderlineStyleSingle]};
        
    }else{
        _textView.linkTextAttributes = @{NSForegroundColorAttributeName : [UIColor blueColor],NSUnderlineStyleAttributeName: [NSNumber numberWithInt:NSUnderlineStyleSingle]};
    }

    CGFloat textView_x;
    CGFloat textView_y;
    CGFloat textView_w = _textView.frame.size.width;
    CGFloat textView_h = _textView.frame.size.height;
    UIViewAutoresizing autoresizing;
    
    if (_message.sender == [NSNumber numberWithInt:0])
    {
        textView_x = self.contentView.frame.size.width - textView_w - 5;
        textView_y = -3;
        autoresizing = UIViewAutoresizingFlexibleLeftMargin;
        textView_x -= [self isSingleLineCase]?65.0:0.0;
        textView_x -= [self isStatusFailedCase]?([self fail_delta]):0.0;
        _textView.textColor = [UIColor whiteColor];
        
    }
    else
    {
        textView_x = 5;
        textView_y = -1;
        autoresizing = UIViewAutoresizingFlexibleRightMargin;
        _textView.textColor = [UIColor blackColor];
        
    }
    
    _textView.autoresizingMask = autoresizing;
    _textView.frame = CGRectMake(textView_x, textView_y+10, textView_w, textView_h);
}

#pragma mark - TextView

-(void)setNameLabel
{
    CGFloat max_witdh = 0.7*self.contentView.frame.size.width;
    CGFloat max_height = 0.7*self.contentView.frame.size.height;
    
    _lblContactName.frame = CGRectMake(0, 0, max_witdh, max_height);
    _lblContactName.font = [UIFont fontWithName:@"Verdana-Italic" size:9.0];
 //   _lblContactName.backgroundColor = [UIColor yellowColor];
    _lblContactName.userInteractionEnabled = NO;
    _lblContactName.textColor = [UIColor whiteColor];
    
    //   _textView.text =  [AESCrypt decrypt:_message.text password:@"meow"];
    
    Contact *contactObj = (Contact *)[DatabaseHelper getObjectIfExist:@"Contact" predicate:[NSPredicate predicateWithFormat:@"identifier == %@",_message.senderID]];
    _lblContactName.text =  contactObj.name;
    _lblContactName.textAlignment = NSTextAlignmentRight;
    
    [_lblContactName sizeToFit];
    
    CGFloat textView_x;
    CGFloat textView_y;
    CGFloat textView_w = _bubbleImage.frame.size.width ;
    CGFloat textView_h = _lblContactName.frame.size.height;
    UIViewAutoresizing autoresizing;
    
    if (_message.sender == [NSNumber numberWithInt:0])
    {
        textView_x = self.contentView.frame.size.width - textView_w - 5;
        textView_y = -3;
        _lblContactName.textAlignment = NSTextAlignmentLeft;

        autoresizing = UIViewAutoresizingFlexibleLeftMargin;
        textView_x -= [self isSingleLineCase]?65.0:0.0;
        textView_x -= [self isStatusFailedCase]?([self fail_delta]):0.0;
        _lblContactName.textColor = [UIColor whiteColor];
        
    }
    else
    {
        textView_x = 5;
        textView_y = -1;
        _lblContactName.textAlignment = NSTextAlignmentRight;

        autoresizing = UIViewAutoresizingFlexibleRightMargin;
        _lblContactName.textColor = [UIColor blackColor];
        
    }
    
    _lblContactName.autoresizingMask = autoresizing;
    _lblContactName.frame = CGRectMake(textView_x+8, textView_y+5,textView_w-20, textView_h);
}

#pragma mark - TimeLabel

-(void)setTimeLabel
{
    _timeLabel.frame = CGRectMake(0, 0, 82, 14);
    _timeLabel.textColor = [UIColor whiteColor];
    _timeLabel.font = [UIFont fontWithName:@"OpenSans" size:8.0];
    _timeLabel.userInteractionEnabled = NO;
    _timeLabel.alpha = 0.7;
    _timeLabel.textAlignment = NSTextAlignmentRight;
    
    //Set Text to Label
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    df.timeStyle = NSDateFormatterShortStyle;
    df.dateStyle = NSDateFormatterNoStyle;
    df.doesRelativeDateFormatting = YES;
    self.timeLabel.text = [df stringFromDate:_message.date];
    
    //Set position
    CGFloat time_x;
    CGFloat time_y = _textView.frame.size.height - 10;
    
    if (_message.sender == [NSNumber numberWithInt:0])
    {
        time_x = _textView.frame.origin.x + _textView.frame.size.width - _timeLabel.frame.size.width - 10;
        _timeLabel.textColor = [UIColor colorWithRed:136.0/255.0 green:199.0/255.0 blue:236.0/255.0 alpha:1.0];
        
        
    }
    else
    {
        
        time_x = MAX(_textView.frame.origin.x + _textView.frame.size.width - _timeLabel.frame.size.width,
                     _textView.frame.origin.x);
        _timeLabel.textColor = [UIColor colorWithRed:152.0/255.0 green:152.0/255.0 blue:152.0/255.0 alpha:1.0];
        
        
        
    }
    
    if ([self isSingleLineCase])
    {
        time_x = _textView.frame.origin.x + _textView.frame.size.width - 5;
        time_y -= 10;
    }
    
    
    if (_message.sender == [NSNumber numberWithInt:0]){
        
        
        if (_message.message_type == [NSNumber numberWithInt:1]){
            
            time_y = 180;
            
            
            
        }
        
        
    }else{
        if (_message.message_type == [NSNumber numberWithInt:1]){
            ////Image////
            
            if (IS_IPHONE_6P) {
                time_x = 215;
                
            }else if (IS_IPHONE_6){
                time_x = 190;
            }else{
                time_x = 145;
                
            }
            time_y = 180;
            
            
            
        }
        
    }
    
    
    
    
    _timeLabel.frame = CGRectMake(time_x-10,
                                  time_y+10,
                                  _timeLabel.frame.size.width,
                                  _timeLabel.frame.size.height);
    
    _timeLabel.autoresizingMask = _textView.autoresizingMask;
}
-(BOOL)isSingleLineCase
{
    CGFloat delta_x = _message.sender == [NSNumber numberWithInt:0]?65.0:44.0;
    
    CGFloat textView_height = _textView.frame.size.height+50;
    CGFloat textView_width = _textView.frame.size.width;
    CGFloat view_width = self.contentView.frame.size.width;
    
    //Single Line Case
    return (textView_height <= 45 && textView_width + delta_x <= 0.8*view_width)?YES:NO;
}

#pragma mark - Bubble

- (void)setBubble
{
    //Margins to Bubble
    CGFloat marginLeft = 5;
    CGFloat marginRight = 2;
    
    //Bubble positions
    CGFloat bubble_x;
    CGFloat bubble_y = 0;
    CGFloat bubble_width;
    CGFloat bubble_height = MIN(_textView.frame.size.height + 8,
                                _timeLabel.frame.origin.y + _timeLabel.frame.size.height + 6);
    
    if (_message.sender == [NSNumber numberWithInt:0])
    {
        
        bubble_x = MIN(_textView.frame.origin.x -marginLeft,_timeLabel.frame.origin.x - 2*marginLeft + 40  );
        
        if (_message.message_type == [NSNumber numberWithInt:0]) {
            ////Text////
            bubble_width = self.contentView.frame.size.width - bubble_x - marginRight ;
            _messageImage.hidden = NO;
            _bubbleImage.autoresizingMask = _textView.autoresizingMask;
            
        }else if (_message.message_type == [NSNumber numberWithInt:1]){
            ////Image////
            bubble_height = 200;
            bubble_width = [UIScreen mainScreen].bounds.size.width/2 + [UIScreen mainScreen].bounds.size.width/4;
            
            bubble_x = [UIScreen mainScreen].bounds.size.width - (bubble_width) ;
            _messageImage.frame = CGRectMake(5, 5 + 10 , bubble_width-15, bubble_height-10);
            _messageImage.layer.cornerRadius = 5;
            _messageImage.layer.masksToBounds = YES;
            _messageImage.hidden = YES;
            if ([self isStatusFailedCase] == true) {
                bubble_x = [UIScreen mainScreen].bounds.size.width - (bubble_width) - 50 ;
                
            }
            _bubbleImage.autoresizingMask = _textView.autoresizingMask;
            
            
        }
        
        _bubbleImage.image = [[self imageNamed:@"bubbleMine"]
                              stretchableImageWithLeftCapWidth:21 topCapHeight:14];
        
        
        
        
         bubble_width -= [self isStatusFailedCase]?[self fail_delta]:0.0;
        _bubbleImage.frame = CGRectMake(bubble_x, bubble_y, bubble_width, bubble_height+10);
        
    }
    else
    {
        bubble_x = marginRight ;
        
        _bubbleImage.image = [[self imageNamed:@"bubbleSomeone"]
                              stretchableImageWithLeftCapWidth:21 topCapHeight:14];
        
        bubble_width = MAX(_textView.frame.origin.x + _textView.frame.size.width + marginLeft,
                           _timeLabel.frame.origin.x + _timeLabel.frame.size.width + 2*marginLeft);
        if (_message.message_type == [NSNumber numberWithInt:1]){
            ////Image////
            bubble_height = 200;
            bubble_width = [UIScreen mainScreen].bounds.size.width/2 + [UIScreen mainScreen].bounds.size.width/4 ;
            
            if (IS_IPHONE_6P) {
                bubble_x = [UIScreen mainScreen].bounds.size.width - (bubble_width) - 105;
            }else if (IS_IPHONE_6){
                bubble_x = [UIScreen mainScreen].bounds.size.width - (bubble_width) - 90;
            }else{
                bubble_x = [UIScreen mainScreen].bounds.size.width - (bubble_width) - 80;
                
            }
            
            _messageImage.frame = CGRectMake(10, 5 + 10, bubble_width-18, bubble_height-10);
            _messageImage.layer.cornerRadius = 5;
            _messageImage.layer.masksToBounds = YES;
            
        }
        //  _bubbleImage.autoresizingMask = _textView.autoresizingMask;
        
        _bubbleImage.frame = CGRectMake(bubble_x, bubble_y, bubble_width , bubble_height+10);
        
    }
    
}

#pragma mark - StatusIcon

-(void)addStatusIcon
{
    CGRect time_frame = _timeLabel.frame;
    CGRect status_frame = CGRectMake(0, 0, 15, 14);
    status_frame.origin.x = time_frame.origin.x + time_frame.size.width + 5;
    status_frame.origin.y = time_frame.origin.y;
    _statusIcon.frame = status_frame;
    _statusIcon.contentMode = UIViewContentModeLeft;
    _statusIcon.autoresizingMask = _textView.autoresizingMask;
}
-(void)setStatusIcon
{
    int minStatus = 0;

    User *userObj = (User *)[DatabaseHelper getObjectIfExist:@"User" predicate:[NSPredicate predicateWithFormat:@"isLoggedIn == 1"]];
    NSString *userID = [NSString stringWithFormat:@"%@",userObj.userID];
    for (int i = 0; i<_arrMessageSeen.count; i++) {
        GroupMessageSeen *groupMessage = (GroupMessageSeen *)[_arrMessageSeen objectAtIndex:i];
        NSString *groupUserID = [NSString stringWithFormat:@"%@",groupMessage.user_id];
        if (![groupUserID isEqualToString:userID]) {
            if (minStatus  == 0) {
                minStatus = [groupMessage.status integerValue];
            }
            
            int status = [groupMessage.status integerValue];

            if (minStatus > status ) {
                minStatus = status;
            }
        }else{
            if (minStatus  == 0) {
                minStatus = [groupMessage.status integerValue];
            }
        }
    }
    
    if (_arrMessageSeen.count == 0) {
        minStatus = [_message.status integerValue];
    }
    
    if (minStatus == 0)
        _statusIcon.image = [self imageNamed:@"status_sending"];
    else if (minStatus == 1)
        _statusIcon.image = [self imageNamed:@"status_sent"];
    else if (minStatus == 2)
        _statusIcon.image = [self imageNamed:@"status_notified"];
    else if (minStatus == 3)
        _statusIcon.image = [self imageNamed:@"status_read"];
    else if (self.message.status == [NSNumber numberWithInt:-1])
        _statusIcon.image = [self imageNamed:@"status_sending"];
    
//    if (self.message.status == [NSNumber numberWithInt:minStatus])
//        _statusIcon.image = nil;
//    
    _statusIcon.hidden = _message.sender == [NSNumber numberWithInt:1];
}

#pragma mark - Failed Case

//
// This delta is how much TextView
// and Bubble should shit left
//
-(NSInteger)fail_delta
{
    return 60;
}
-(BOOL)isStatusFailedCase
{
    return self.message.status == [NSNumber numberWithInt:-1];
}
-(void)setFailedButton
{
    NSInteger b_size = 22;
    
    CGRect frame;
    if (_message.sender == [NSNumber numberWithInt:0]){
        frame = CGRectMake(  [[UIScreen mainScreen]bounds].size.width - 45 ,
                           0,
                           b_size,
                           b_size);
    }else{
        frame = CGRectMake(  25 + _profileImage.frame.size.width +  _bubbleImage.frame.size.width,
                           (self.contentView.frame.size.height - b_size)/2,
                           b_size,
                           b_size);
    }
    _resendButton.frame = frame;
    _resendButton.hidden = ![self isStatusFailedCase];
    [_resendButton setImage:[self imageNamed:@"status_failed"] forState:UIControlStateNormal];
    [_resendButton addTarget:self action:@selector(btnActionResendMessage) forControlEvents:UIControlEventTouchUpInside];

}


-(void)btnActionResendMessage{
    NSString *strCamera = @"Resend Message";
    NSString *strCancel = @"Cancel";
    UIActionSheet *actionSheet = [[UIActionSheet alloc]initWithTitle:@"Message sending falied" delegate:self cancelButtonTitle:strCancel destructiveButtonTitle:nil otherButtonTitles:strCamera, nil];
    [actionSheet showInView:self];
    [actionSheet setTag:1];
    
}




-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(actionSheet.tag == 1){
        switch (buttonIndex) {
            case 0:
                NSLog(@"Take Picture");
                [self messageResend];
                break;
            default:
                NSLog(@"Default");
                break;
        }
    }
}


-(void)messageResend{
    
    
    ChatDetailViewController *chatDetailVC = (ChatDetailViewController *)[self.navigationController topViewController];
    if (self.message.message_type == [NSNumber numberWithInt:0]) {
        [chatDetailVC sendingMessageFromService:self.message];
    }else if (self.message.message_type == [NSNumber numberWithInt:1]){
        [chatDetailVC sendingPictureFromService:self.message];
        
    }
}

#pragma mark - UIImage Helper

-(UIImage *)imageNamed:(NSString *)imageName
{
    return [UIImage imageNamed:imageName
                      inBundle:[NSBundle bundleForClass:[self class]]
 compatibleWithTraitCollection:nil];
}



@end

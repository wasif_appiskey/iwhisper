//
//  GroupVideoViewCell.h
//  iwhispers
//
//  Created by Apple on 9/8/16.
//  Copyright © 2016 Appiskey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Message.h"
#import "AsyncImageView.h"
@import MediaPlayer;


@interface GroupVideoViewCell : UITableViewCell
@property (strong, nonatomic) UIButton *resendButton;
@property (strong, nonatomic) Message *message;
@property (strong, nonatomic) UIImageView *profileImage;
@property (strong, nonatomic) MPMoviePlayerController *moviePlayer;
@property (strong, nonatomic) UITextView *textView;
@property (strong, nonatomic) UIImageView *imgOverlay;
@property (strong, nonatomic) UILabel *lblContactName;
@property (strong, nonatomic) UINavigationController *navigationController;
@property (strong, nonatomic) NSArray *arrMessageSeen;

-(void)updateMessageStatus;

//Estimate BubbleCell Height
-(CGFloat)height;
-(CGFloat)bubbleX;
@end

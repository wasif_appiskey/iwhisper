//
//  GroupAudioViewCell.h
//  iwhispers
//
//  Created by Apple on 9/8/16.
//  Copyright © 2016 Appiskey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Message.h"
#import "AsyncImageView.h"
#import "AZSoundManager.h"

@interface GroupAudioViewCell : UITableViewCell

@property (strong, nonatomic) UIButton *resendButton;
@property (strong, nonatomic) Message *message;
@property (strong, nonatomic) NSArray *arrMessageSeen;
@property (strong, nonatomic) UIImageView *profileImage;
@property (strong, nonatomic) AsyncImageView *messageImage;
@property (strong, nonatomic) UITextView *textView;
@property (strong, nonatomic) UIView *soundView;
@property (strong,nonatomic) NSString *strTag;
@property (strong, nonatomic) UISlider *slider;
@property (strong, nonatomic) AZSoundManager *manager;
@property (strong, nonatomic) AZSoundItem *items;
@property (strong, nonatomic) UILabel *lblAudioTime;
@property (strong, nonatomic) UIButton *btnPlay;
@property (strong, nonatomic) UILabel *lblContactName;
@property (strong, nonatomic) UINavigationController *navigationController;

-(void)updateMessageStatus;

//Estimate BubbleCell Height
-(CGFloat)height;

@end

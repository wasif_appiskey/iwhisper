//
//  GroupChatListTableViewCell.swift
//  iwhispers
//
//  Created by Apple on 9/21/16.
//  Copyright © 2016 Appiskey. All rights reserved.
//

import UIKit

class GroupChatListTableViewCell: UITableViewCell {
    @IBOutlet var imgContactImage: UIImageView!
    @IBOutlet var lblContactName: UILabel!
    @IBOutlet var lblAdmin: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

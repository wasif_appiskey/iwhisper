//
//  GroupListViewController.swift
//  iwhispers
//
//  Created by Apple on 9/21/16.
//  Copyright © 2016 Appiskey. All rights reserved.
//

import UIKit

class GroupListViewController: UIViewController,BaseBLLDelegate {
    
    @IBOutlet var tblMember: UITableView!
    var arrMembers : NSMutableArray!
    var groupID :NSNumber!
    var isEditAble :Bool!

    var groupObj :Group!
    
    @IBOutlet var lblGroupName: UILabel!
    @IBOutlet var imgGroup: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.intializeTableView()
        self.settingNavigation()
        // Do any additional setup after loading the view.
    }
    
    func populateSubView() {
        
        
        for subview in imgGroup.subviews {
            subview.removeFromSuperview()
        }
        var strImageName : String = String()
        strImageName  = String(format: "%@",groupObj.image! )
        lblGroupName.text = groupObj.name
        var imgProfileAsync : AsyncImageView = AsyncImageView()
        imgProfileAsync.removeFromSuperview()
        imgProfileAsync = AsyncImageView(frame: CGRectMake(0, 0,imgGroup.frame.size.width, imgGroup.frame.size.width))
        imgProfileAsync.loadImageFromURL(NSURL(string: strImageName))
        imgGroup.layer.cornerRadius = CGRectGetWidth(imgGroup.frame)/2.0
        imgGroup.clipsToBounds = true
        imgGroup.addSubview(imgProfileAsync)
    }
    
    
    /////////////////////**** Setting Up Navigation ******////////////
    
    func settingNavigation(){
        self.settingNavigationBg()
        self.settingNavigationTitle()
        self.settingNavigationLeftButton()
        self.settingNavigationRightButton()
    }
    
    //////**** Setting Navigation Background ****//////
    
    func settingNavigationBg(){
        self.navigationController?.navigationBar.translucent = false
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 0.0/255.0, green: 115.0/255.0, blue: 181.0/255.0, alpha: 1.0)
        
    }
    
    //////**** Setting Navigation Title ****//////
    func settingNavigationTitle(){
        self.title = "Group Members"
        self.navigationController!.navigationBar.titleTextAttributes = [
            NSFontAttributeName: UIFont(name: "OpenSans", size: 15.0)!,
            NSForegroundColorAttributeName :  UIColor.whiteColor()
            
            
        ]
    }
    
    
    //////**** Setting Navigation Left Button ****//////
    
    func settingNavigationLeftButton(){
        let button: UIButton = UIButton(type:UIButtonType.Custom)
        button.setImage(UIImage(named: "arrow-white.png"), forState: UIControlState.Normal)
        button.addTarget(self, action: #selector(GroupListViewController.btnActionNewMessage), forControlEvents: UIControlEvents.TouchUpInside)
        button.frame = CGRectMake(0, 0, 20, 21)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        
    }
    
    
    //////**** Setting New Message Button Action ****//////
    
    func btnActionNewMessage(){
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    
    //////**** Setting Navigation Left Button ****//////
    
    func settingNavigationRightButton(){
        let button: UIButton = UIButton(type:UIButtonType.Custom)
        button.setTitle("+", forState: .Normal)
        button.titleLabel?.font = UIFont(name: "OpenSans", size: 19)
        button.addTarget(self, action: #selector(GroupListViewController.btnActionGotoAddMember), forControlEvents: UIControlEvents.TouchUpInside)
        button.frame = CGRectMake(0, 0, 20, 21)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
        
    }
    
    
    override func viewWillAppear(animated: Bool) {
        MBProgressHUD.hideHUDForView(self.view, animated: true)
        self.callingGroupDetail()
        self.populateView()
        self.populateSubView()
    }
    
    func callingGroupDetail(){
        MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        let bllService : BLLServices = BLLServices()
        bllService.delegate = self;
        let parameter : NSMutableDictionary = NSMutableDictionary()
        parameter.setValue(String(format: "%@",self.groupID), forKey: "groupID")
        bllService.getGroupDetail(parameter)
    }
    
    func populateView(){
        
        
        
        
        arrMembers = NSMutableArray()
        
        var  tempArr : NSArray!
        isEditAble = false
        tempArr = DatabaseHelper.getAllData("GroupMember", offset: 0, predicate: NSPredicate(format: "groupID == %@", self.groupObj.identifier! ), sortDescriptor: nil)
        arrMembers = tempArr.mutableCopy() as! NSMutableArray
        
        for i:Int in 0 ..< arrMembers.count {
            let userObj : User = DatabaseHelper.getObjectIfExist("User", predicate: NSPredicate(format: "isLoggedIn == 1")) as! User

            let groupMemberObj : GroupMember = arrMembers.objectAtIndex(i) as! GroupMember
            if groupMemberObj.isAdmin == "1"  {
                if userObj.userID == groupMemberObj.contactID {
                   isEditAble = true
                }
            }
        }
        
        tblMember.reloadData()
    }
    
    //////**** Setting New Message Button Action ****//////
    
    func btnActionAddMember(){
        
        var selectContact : SelectContactViewController = SelectContactViewController(nibName: "SelectContactViewController", bundle: nil)
        var strGroupID : String = String(format: "%@", groupID)
        selectContact.strGroupID = strGroupID
        selectContact.groupObj = groupObj
        selectContact.arrMembers = arrMembers
        self.navigationController?.pushViewController(selectContact, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    ////////////////////***** Intialize TableView *******//////////
    
    func intializeTableView(){
        
        let nibOne = UINib(nibName: "GroupChatListTableViewCell", bundle: nil)
        tblMember.registerNib(nibOne, forCellReuseIdentifier: "GroupChatListTableViewCellIdentifier")
        
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrMembers.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat{
        return 44
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:GroupChatListTableViewCell = tblMember.dequeueReusableCellWithIdentifier("GroupChatListTableViewCellIdentifier") as! GroupChatListTableViewCell
        cell.selectionStyle = .None
        
        var groupObj : GroupMember = arrMembers.objectAtIndex(indexPath.row) as! GroupMember
        let contactObj : Contact = DatabaseHelper.getObjectIfExist("Contact", predicate: NSPredicate(format: "identifier == %@",groupObj.contactID! )) as! Contact
        var strImageName : String = String()
        cell.lblContactName.text = contactObj.name
        strImageName  = String(format: "%@",contactObj.image_id )
        var imgProfileAsync : AsyncImageView = AsyncImageView()
        imgProfileAsync.removeFromSuperview()
        imgProfileAsync = AsyncImageView(frame: CGRectMake(0, 0,cell.imgContactImage.frame.size.width, cell.imgContactImage.frame.size.width))
        imgProfileAsync.loadImageFromURL(NSURL(string: strImageName))
        cell.imgContactImage.layer.cornerRadius = CGRectGetWidth(cell.imgContactImage.frame)/2.0
        cell.imgContactImage.clipsToBounds = true
        cell.imgContactImage.addSubview(imgProfileAsync)
        if groupObj.isAdmin  == "1" {
            cell.lblAdmin.alpha = 1
        }else{
            cell.lblAdmin.alpha = 0
        }
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        let groupObj : GroupMember = arrMembers.objectAtIndex(indexPath.row) as! GroupMember
        
        if groupObj.isAdmin  == "1" {
            return false
        }else{
            return isEditAble
        }
    }
    
    
    func tableView(tableView: UITableView!, commitEditingStyle editingStyle:   UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath!) {
        if (editingStyle == UITableViewCellEditingStyle.Delete) {
            let groupObj : GroupMember = arrMembers.objectAtIndex(indexPath.row) as! GroupMember
            MBProgressHUD.showHUDAddedTo(self.view, animated: true)
            self.removeGroupUser(groupObj)
            self.arrMembers.removeObject(groupObj)
            tblMember.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
            tblMember.endUpdates()
            
        }
    }
    
    func removeGroupUser(groupMember : GroupMember)  {
        
        let arrSelectedContact : NSMutableArray = NSMutableArray()
        
        arrSelectedContact.addObject(groupMember.contactID!)
        
        let params : NSMutableDictionary = NSMutableDictionary()
        params.setValue(arrSelectedContact, forKey: "members")
        params.setValue(String(format: "%@", self.groupObj.identifier!), forKey: "groupID")
        
        MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        let bllService : BLLServices = BLLServices()
        bllService.delegate = self;
        bllService.removeGroupMember(params)
    }
    
    func successWithData(data: AnyObject!, msg successMsg: String!, tag: Int32) {
        let tagService = AuthenticationParserType(rawValue: UInt32(tag))
        if tagService == RemoveGroupMember {
            MBProgressHUD.hideHUDForView(self.view, animated: true)
            self.callingGroupDetail()
        }else if tagService == GetGroupDetail{
            MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
            self.populateView()
        }
        // (UIApplication.sharedApplication().delegate as! AppDelegate).setupViewControllers()
    }
    //////***** Failure *****////////
    func requestFailure(failureMsg: String!, tag: Int32) {
        MBProgressHUD.hideHUDForView(self.view, animated: true)
        Common.showAlert("Sorry", message: failureMsg)
    }
    
    //////***** Uploading *****////////
    func uploadProgress(progress: Float) {
        
    }
    
    @IBAction func btnActionGotoAddMember(sender: AnyObject) {
        MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        self.performSelector(#selector(GroupListViewController.btnActionAddMember), withObject: self, afterDelay: 1.0)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

//
//  ChatDetailTableViewCell.h
//  iwhispers
//
//  Created by Apple on 6/29/16.
//  Copyright © 2016 Appiskey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Message.h"
#import "AsyncImageView.h"

@interface ChatDetailTableViewCell : UITableViewCell

@property (strong, nonatomic) UIButton *resendButton;
@property (strong, nonatomic) Message *message;
@property (strong, nonatomic) UIImageView *profileImage;
@property (strong, nonatomic) AsyncImageView *messageImage;
@property (strong, nonatomic) UITextView *textView;
@property (strong, nonatomic) UINavigationController *navigationController;

-(void)updateMessageStatus;

//Estimate BubbleCell Height
-(CGFloat)height;
-(CGFloat)bubbleX;


@end

//
//  AudioMessageViewCell.m
//  iwhispers
//
//  Created by Apple on 8/25/16.
//  Copyright © 2016 Appiskey. All rights reserved.
//

#import "AudioMessageViewCell.h"
#import "ChatDetailViewController.h"
#import "AppConstants.h"
@interface AudioMessageViewCell ()<UIActionSheetDelegate>{
    AZSoundManager *manager;
    AZSoundItem *items;
}
@property (strong, nonatomic) UILabel *timeLabel;
@property (strong, nonatomic) UIImageView *bubbleImage;
@property (strong, nonatomic) UIImageView *statusIcon;
@property (strong, nonatomic) UIImageView *imageFile;




@end

@implementation AudioMessageViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}


-(CGFloat)height
{
    return _bubbleImage.frame.size.height;
}
-(void)updateMessageStatus
{
    [self buildCell];
    //Animate Transition
    _statusIcon.alpha = 0;
    [UIView animateWithDuration:.5 animations:^{
        _statusIcon.alpha = 1;
    }];
}

#pragma mark -

-(id)init
{
    self = [super init];
    if (self)
    {
        [self commonInit];
    }
    return self;
}
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        [self commonInit];
    }
    return self;
}
-(void)commonInit
{
    self.backgroundColor = [UIColor clearColor];
    self.contentView.backgroundColor = [UIColor clearColor];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.accessoryType = UITableViewCellAccessoryNone;
    
    _textView = [[UITextView alloc] init];
    _bubbleImage = [[UIImageView alloc] init];
    _messageImage = [[AsyncImageView alloc]init];
    _timeLabel = [[UILabel alloc] init];
    _statusIcon = [[UIImageView alloc] init];
    _resendButton = [[UIButton alloc] init];
    _profileImage = [[UIImageView alloc] init];
    _soundView = [[UIView alloc] init];
    _btnPlay = [[UIButton alloc] init];
    _slider = [[UISlider alloc] init];
    _lblAudioTime = [[UILabel alloc] init];

    _resendButton.hidden = YES;
    
    [self.contentView addSubview:_bubbleImage];
    [_messageImage removeFromSuperview];
    
    [self.contentView addSubview:_textView];
    [self.contentView addSubview:_timeLabel];
    [self.contentView addSubview:_statusIcon];
    [self.contentView addSubview:_resendButton];
    [self.contentView addSubview:_profileImage];
    [_soundView removeFromSuperview];
    [self.contentView addSubview:_soundView];
    [_btnPlay removeFromSuperview];
    [_soundView addSubview:_btnPlay];
    [_slider removeFromSuperview];
    [_soundView addSubview:_slider];
    [_lblAudioTime removeFromSuperview];
    [_soundView addSubview:_lblAudioTime];
}
-(void)prepareForReuse
{
    [super prepareForReuse];
    
    _textView.text = @"";
    _timeLabel.text = @"";
    _statusIcon.image = nil;
    _bubbleImage.image = nil;
    _profileImage.image = nil;
    _resendButton.hidden = YES;
}
-(void)setMessage:(Message *)message
{
    _message = message;
    [self buildCell];
    @try {
        
        message.heigh = [NSNumber numberWithFloat:self.height];
    } @catch (NSException *exception) {
        NSLog(@"%@",exception);
    }
    
}
-(void)buildCell
{
    @try {
        [self setTimeLabel];
        [self setBubble];
        [self addStatusIcon];
        [self setStatusIcon];
        [self setProfileImage];
        [self setFailedButton];
        [self settingSoundView];
    } @catch (NSException *exception) {
        NSLog(@"%@",exception);
    }
    
    [self setNeedsLayout];
}

-(void)settingSoundView{
   // _soundView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width/2 + [UIScreen mainScreen].bounds.size.width/4,  40)];
  //  [_soundView setBackgroundColor:[UIColor whiteColor]];
   // [_soundView removeFromSuperview];
    
    if (_message.sender == [NSNumber numberWithInt:0]){
      
        if (IS_IPHONE_6P) {
            [_soundView setFrame:CGRectMake([UIScreen mainScreen].bounds.size.width/8 +60, 0, [UIScreen mainScreen].bounds.size.width/2 + [UIScreen mainScreen].bounds.size.width/4,  40)];
            
        }else if (IS_IPHONE_6){
            [_soundView setFrame:CGRectMake([UIScreen mainScreen].bounds.size.width/8+60, 0, [UIScreen mainScreen].bounds.size.width/2 + [UIScreen mainScreen].bounds.size.width/4,  40)];
        }else{
            [_soundView setFrame:CGRectMake([UIScreen mainScreen].bounds.size.width/8+40, 0, [UIScreen mainScreen].bounds.size.width/2 + [UIScreen mainScreen].bounds.size.width/4,  40)];
            
        }
        
    }else{
        if (IS_IPHONE_6P) {
            [_soundView setFrame:CGRectMake([UIScreen mainScreen].bounds.size.width/8 -40, 0, [UIScreen mainScreen].bounds.size.width/2 + [UIScreen mainScreen].bounds.size.width/4,  40)];
            
        }else if (IS_IPHONE_6){
            [_soundView setFrame:CGRectMake([UIScreen mainScreen].bounds.size.width/8-40, 0, [UIScreen mainScreen].bounds.size.width/2 + [UIScreen mainScreen].bounds.size.width/4,  40)];
        }else{
            [_soundView setFrame:CGRectMake(10, 0, [UIScreen mainScreen].bounds.size.width/2 + [UIScreen mainScreen].bounds.size.width/4,  40)];
            
        }
        
    }
    
    [_btnPlay setFrame:CGRectMake(5, 5, 25, 25)];
    [_btnPlay setBackgroundImage:[UIImage imageNamed:@"play.png"] forState:UIControlStateNormal];
    [_btnPlay setBackgroundImage:[UIImage imageNamed:@"pause.png"] forState:UIControlStateSelected];
    [_btnPlay addTarget:self action:@selector(play) forControlEvents:UIControlEventTouchUpInside];
   // [_btnPlay removeFromSuperview];
   
    [_slider setFrame:CGRectMake(35, 0, [UIScreen mainScreen].bounds.size.width/2, 35)];
    //[_slider removeFromSuperview];
    _slider.userInteractionEnabled = NO;
    
    [_slider setThumbImage:[UIImage new] forState:UIControlStateNormal];

    [_lblAudioTime setFrame:CGRectMake(_slider.frame.origin.x + _slider.frame.size.width + 1 , 4, 40, 25)];
    [_lblAudioTime setFont:[UIFont fontWithName:@"OpenSans" size:11]];
    [_lblAudioTime setTextColor:[UIColor whiteColor]];
   // [_lblAudioTime removeFromSuperview];
    
   
    
    if (_message.message_type == [NSNumber numberWithInt:0]){
        [_soundView setHidden:YES];
    }else if (_message.message_type == [NSNumber numberWithInt:1]){
        [_soundView setHidden:YES];
    }else if (_message.message_type == [NSNumber numberWithInt:2]){
        [_soundView setHidden:NO];
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                             NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.aac",self.message.identifier]];
        if (_message.sender == [NSNumber numberWithInt:0]) {
            items = [AZSoundItem soundItemWithContentsOfFile:path];
            
        }else{
            path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",self.message.file_link]];
            items = [AZSoundItem soundItemWithContentsOfFile:path];
            
        }
        
        // Sound item properties
 
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(slideValue:)
                                                     name:@"slideValue"
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(slideStop:)
                                                     name:@"slideStop"
                                                   object:nil];
        // Sound manager
        manager = [AZSoundManager sharedManager];
        [manager preloadSoundItem:items];
        // Get sound item info
        if (_message.strTag == nil) {
            [items setName:self.strTag];
        }else{
            [items setName: _message.strTag];
        }
        NSLog(@"itemname %@",items.name);
        NSLog(@"tag %ld",(long)self.tag);
        self.lblAudioTime.text = [NSString stringWithFormat:@"%@", [self convertTime:items.loadTime]];


        [manager getItemInfoWithProgressBlock:^(AZSoundItem *item) {
            @try {
                NSTimeInterval remainingTime = item.duration - item.currentTime;
                float value = item.currentTime /item.duration;
                NSMutableDictionary *datadic = [[NSMutableDictionary alloc]init];
                [datadic setValue:[NSString stringWithFormat:@"%f",value] forKey:@"time"];
                if (item.name == nil) {
                    [datadic setValue:[NSString stringWithFormat:@"0"] forKey:@"tag"];

                }else{
                    [datadic setValue:[NSString stringWithFormat:@"%@",item.name] forKey:@"tag"];
                }
                [datadic setValue:[NSString stringWithFormat:@"%f",remainingTime] forKey:@"interval"];
                
                NSLog(@"current time%f",item.currentTime);
                NSLog(@"duration %f",item.duration);
                [[NSNotificationCenter defaultCenter]
                 postNotificationName:@"slideValue"
                 object:self userInfo:datadic];
            } @catch (NSException *exception) {
                NSLog(@"%@",exception);
            }
            // self.slider.value = item.currentTime / item.duration;
        } finishBlock:^(AZSoundItem *item) {
            //  self.slider.value = 1;
            @try {
                
                
                int tag = [[item name] intValue];
                UISlider *tagSlider = [self.slider viewWithTag:tag];
                tagSlider.value = 0;
                UIButton *btnPlay = [self.btnPlay viewWithTag:tag];
                [btnPlay setSelected:NO];
                NSMutableDictionary *datadic = [[NSMutableDictionary alloc]init];
                [datadic setValue:[NSString stringWithFormat:@"%d",tag] forKey:@"tag"];
                [datadic setValue:[NSString stringWithFormat:@"%f",[item loadTime]] forKey:@"time"];

                [[NSNotificationCenter defaultCenter]
                 postNotificationName:@"slideStop"
                 object:self userInfo:datadic];
                NSLog(@"finish playing");
                
                
            } @catch (NSException *exception) {
                NSLog(@"%@",exception);
            }
        }];
        
        
        
    }
    
}



- (void) slideStop:(NSNotification *) notification
{
    if ([[notification name] isEqualToString:@"slideStop"]){
        @try {
            
            NSMutableDictionary *dataDic = [[NSMutableDictionary alloc]init];
            dataDic = (NSMutableDictionary *)[notification userInfo];
            int tag = [[dataDic valueForKey:@"tag"]intValue];
            NSTimeInterval duration = (NSTimeInterval)[[dataDic valueForKey:@"time"]intValue];

            UISlider *tagSlider = [self.slider viewWithTag:tag];
            tagSlider.value = 0;
            UILabel *tagLabel = [self.lblAudioTime viewWithTag:tag];
            tagLabel.text =  [NSString stringWithFormat:@"%@", [self convertTime:duration]];
            UIButton *btnPlay = [self.btnPlay viewWithTag:tag];
            [btnPlay setSelected:NO];
        } @catch (NSException *exception) {
            NSLog(@"%@",exception);
        }
    }
}

- (void) slideValue:(NSNotification *) notification
{
    if ([[notification name] isEqualToString:@"slideValue"]){
        @try {
            
            NSMutableDictionary *dataDic = [[NSMutableDictionary alloc]init];
            dataDic = (NSMutableDictionary *)[notification userInfo];
            float value = [[dataDic valueForKey:@"time"]floatValue];
            int tag = [[dataDic valueForKey:@"tag"]intValue];
            UISlider *tagSlider = [self.slider viewWithTag:tag];
            tagSlider.value = value;
            UILabel *tagLabel = [self.lblAudioTime viewWithTag:tag];
            NSTimeInterval remainingTime = [[dataDic valueForKey:@"interval"]intValue];
            
            tagLabel.text = [NSString stringWithFormat:@"%@", [self convertTime:remainingTime]];
            
            
        } @catch (NSException *exception) {
            NSLog(@"%@",exception);
        }
    }
}

-(void)play{
    if (_btnPlay.isSelected == YES) {
        [_btnPlay setSelected:NO];
        [manager pause];
    }else{
        [manager playSoundItem:items];
        
        [_btnPlay setSelected:YES];
        
    }
}




-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    for (UIView *view in self.contentView.subviews)
    {
        NSLog(@"%@",view);
    }
}

- (NSString*)convertTime:(NSInteger)time
{
    NSInteger minutes = time / 60;
    NSInteger seconds = time % 60;
    return [NSString stringWithFormat:@"%02ld:%02ld", (long)minutes, (long)seconds];
}

-(void)setProfileImage{
    
    _profileImage.frame = CGRectMake(0, 0, 0, 0);
    _profileImage.backgroundColor = [UIColor clearColor];
    
    
    
    CGFloat profileImage_x;
    CGFloat profileImage_y;
    CGFloat profileImage_w = _profileImage.frame.size.width;
    CGFloat profileImage_h = _profileImage.frame.size.height;
    UIViewAutoresizing autoresizing;
    
    if (_message.sender == [NSNumber numberWithInt:0])
    {
        autoresizing = UIViewAutoresizingFlexibleLeftMargin;
        
        //    [_profileImage setImage:[UIImage imageNamed:@"image1chat.png"]];
        profileImage_x = self.contentView.frame.size.width - profileImage_w - 2;
        profileImage_y = 0;
        
    }
    else
    {
        // [_profileImage setImage:[UIImage imageNamed:@"image2Chat.png"]];
        profileImage_x = 2;
        profileImage_y = 0;
        autoresizing = UIViewAutoresizingFlexibleRightMargin;
        
    }
   // _profileImage.autoresizingMask = autoresizing;
    
    _profileImage.frame = CGRectMake(profileImage_x, profileImage_y, profileImage_w, profileImage_h);
}

#pragma mark - TextView

-(void)setTextView
{
    CGFloat max_witdh = 0.7*self.contentView.frame.size.width;
    CGFloat max_height = 0.7*self.contentView.frame.size.height;
    
    _textView.frame = CGRectMake(0, 0, max_witdh, max_height);
    _textView.font = [UIFont fontWithName:@"OpenSans" size:13.0];
    _textView.backgroundColor = [UIColor clearColor];
    _textView.userInteractionEnabled = NO;
    _textView.textColor = [UIColor whiteColor];
    _textView.text = _message.text;
    [_textView sizeToFit];
    
    CGFloat textView_x;
    CGFloat textView_y;
    CGFloat textView_w = _textView.frame.size.width;
    CGFloat textView_h = _textView.frame.size.height;
    UIViewAutoresizing autoresizing;
    
    if (_message.sender == [NSNumber numberWithInt:0])
    {
        textView_x = self.contentView.frame.size.width - textView_w - 20 - 25 ;
        textView_y = -3;
        autoresizing = UIViewAutoresizingFlexibleLeftMargin;
        textView_x -= [self isSingleLineCase]?65.0:0.0;
        //  textView_x -= [self isStatusFailedCase]?([self fail_delta]-15):0.0;
        _textView.textColor = [UIColor whiteColor];
        
    }
    else
    {
        textView_x = 20 + 25;
        textView_y = -1;
        autoresizing = UIViewAutoresizingFlexibleRightMargin;
        _textView.textColor = [UIColor blackColor];
        
    }
    
    _textView.autoresizingMask = autoresizing;
    _textView.frame = CGRectMake(textView_x, textView_y, textView_w, textView_h);
}

#pragma mark - TimeLabel

-(void)setTimeLabel
{
    _timeLabel.frame = CGRectMake(0, 0, 82, 14);
    _timeLabel.textColor = [UIColor whiteColor];
    _timeLabel.font = [UIFont fontWithName:@"OpenSans" size:8.0];
    _timeLabel.userInteractionEnabled = NO;
    _timeLabel.alpha = 0.7;
    _timeLabel.textAlignment = NSTextAlignmentRight;
    
    //Set Text to Label
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    df.timeStyle = NSDateFormatterShortStyle;
    df.dateStyle = NSDateFormatterNoStyle;
    df.doesRelativeDateFormatting = YES;
    self.timeLabel.text = [df stringFromDate:_message.date];
    
    //Set position
    CGFloat time_x;
    CGFloat time_y = 25;
    
    if (_message.sender == [NSNumber numberWithInt:0])
    {
        if (IS_IPHONE_6P) {
            time_x = 310;
            
        }else if (IS_IPHONE_6){
            time_x = 270;
        }else{
            time_x = 210;
            
        }
        _timeLabel.textColor = [UIColor colorWithRed:136.0/255.0 green:199.0/255.0 blue:236.0/255.0 alpha:1.0];
        
        
    }
    else
    {
        if (IS_IPHONE_6P) {
            time_x = 235;
        }else if (IS_IPHONE_6){
            time_x = 205;
        }else{
            time_x = 160;
            
        }
        _timeLabel.textColor = [UIColor colorWithRed:152.0/255.0 green:152.0/255.0 blue:152.0/255.0 alpha:1.0];
        
    }
    _timeLabel.frame = CGRectMake(time_x,
                                  time_y,
                                  _timeLabel.frame.size.width,
                                  _timeLabel.frame.size.height);
    
    _timeLabel.autoresizingMask = _textView.autoresizingMask;
}
-(BOOL)isSingleLineCase
{
    CGFloat delta_x = _message.sender == [NSNumber numberWithInt:0]?65.0:44.0;
    
    CGFloat textView_height = _textView.frame.size.height+50;
    CGFloat textView_width = _textView.frame.size.width;
    CGFloat view_width = self.contentView.frame.size.width;
    
    //Single Line Case
    return (textView_height <= 45 && textView_width + delta_x <= 0.8*view_width)?YES:NO;
}

#pragma mark - Bubble

- (void)setBubble
{
    //Margins to Bubble
    CGFloat marginLeft = 5;
    CGFloat marginRight = 2;
    
    //Bubble positions
    CGFloat bubble_x;
    CGFloat bubble_y = 0;
    CGFloat bubble_width;
    CGFloat bubble_height = MIN(_textView.frame.size.height + 8,
                                _timeLabel.frame.origin.y + _timeLabel.frame.size.height + 6);
    
    if (_message.sender == [NSNumber numberWithInt:0])
    {
        
        bubble_x = MIN(_textView.frame.origin.x -marginLeft,_timeLabel.frame.origin.x - 2*marginLeft + 40 );
        
   
            ////Voice////
        
            bubble_width = [UIScreen mainScreen].bounds.size.width/2 + [UIScreen mainScreen].bounds.size.width/4 ;
            bubble_x = [UIScreen mainScreen].bounds.size.width - (bubble_width)  ;
            
            _messageImage.hidden = YES;
            _soundView.hidden = NO;
            
        
        _bubbleImage.image = [[self imageNamed:@"bubbleMine"]
                              stretchableImageWithLeftCapWidth:21 topCapHeight:14];
        
        
        
        
        //    bubble_width -= [self isStatusFailedCase]?[self fail_delta]:0.0;
        _bubbleImage.frame = CGRectMake(bubble_x, bubble_y, bubble_width, 40);
        
    }
    else
    {
        bubble_x = marginRight;
        
        _bubbleImage.image = [[self imageNamed:@"bubbleSomeone"]
                              stretchableImageWithLeftCapWidth:21 topCapHeight:14];
        
        bubble_width = MAX(_textView.frame.origin.x + _textView.frame.size.width + marginLeft,
                           _timeLabel.frame.origin.x + _timeLabel.frame.size.width + 2*marginLeft);
       
            ////Voice////
            bubble_width = [UIScreen mainScreen].bounds.size.width/2 + [UIScreen mainScreen].bounds.size.width/2.5 ;
        if (IS_IPHONE_6P) {
            bubble_x = [UIScreen mainScreen].bounds.size.width - (bubble_width) - 40 ;
            
        }else if (IS_IPHONE_6){
            bubble_x = [UIScreen mainScreen].bounds.size.width - (bubble_width) - 40 ;
        }else{
            bubble_x = [UIScreen mainScreen].bounds.size.width - (bubble_width)- 30 ;
            
        }
        
            _messageImage.hidden = YES;
            _soundView.hidden = NO;
            
        
        _bubbleImage.frame = CGRectMake(bubble_x, bubble_y, bubble_width - 40, 40);
        
    }
    
    _bubbleImage.autoresizingMask = _textView.autoresizingMask;
}

#pragma mark - StatusIcon

-(void)addStatusIcon
{
    CGRect time_frame = _timeLabel.frame;
    CGRect status_frame = CGRectMake(0, 0, 15, 14);
    status_frame.origin.x = time_frame.origin.x + time_frame.size.width + 5;
    status_frame.origin.y = time_frame.origin.y;
    _statusIcon.frame = status_frame;
    _statusIcon.contentMode = UIViewContentModeLeft;
    _statusIcon.autoresizingMask = _textView.autoresizingMask;
}
-(void)setStatusIcon
{
    if (self.message.status == [NSNumber numberWithInt:0])
        _statusIcon.image = [self imageNamed:@"status_sending"];
    else if (self.message.status == [NSNumber numberWithInt:1])
        _statusIcon.image = [self imageNamed:@"status_sent"];
    else if (self.message.status == [NSNumber numberWithInt:2])
        _statusIcon.image = [self imageNamed:@"status_notified"];
    else if (self.message.status == [NSNumber numberWithInt:3])
        _statusIcon.image = [self imageNamed:@"status_read"];
    if (self.message.status == [NSNumber numberWithInt:4])
        _statusIcon.image = nil;
    if (self.message.status == [NSNumber numberWithInt:-1])
        _statusIcon.image = [self imageNamed:@"status_sending"];
    
    _statusIcon.hidden = _message.sender == [NSNumber numberWithInt:1];
}

#pragma mark - Failed Case

//
// This delta is how much TextView
// and Bubble should shit left
//
-(NSInteger)fail_delta
{
    return 60;
}
-(BOOL)isStatusFailedCase
{
    return self.message.status == [NSNumber numberWithInt:-1];
}
-(void)setFailedButton
{
    NSInteger b_size = 22;
    
    CGRect frame;
    if (_message.sender == [NSNumber numberWithInt:0]){
        frame = CGRectMake(  [[UIScreen mainScreen]bounds].size.width - 25 - _profileImage.frame.size.width -  _bubbleImage.frame.size.width,
                           (self.contentView.frame.size.height - b_size)/2,
                           b_size,
                           b_size);
    }else{
        frame = CGRectMake(  25 + _profileImage.frame.size.width +  _bubbleImage.frame.size.width,
                           (self.contentView.frame.size.height - b_size)/2,
                           b_size,
                           b_size);
    }
    _resendButton.frame = frame;
    _resendButton.hidden = ![self isStatusFailedCase];
    [_resendButton setImage:[self imageNamed:@"status_failed"] forState:UIControlStateNormal];
    [_resendButton addTarget:self action:@selector(btnActionResendMessage) forControlEvents:UIControlEventTouchUpInside];
}




-(void)btnActionResendMessage{
    NSString *strCamera = @"Resend Message";
    NSString *strCancel = @"Cancel";
    UIActionSheet *actionSheet = [[UIActionSheet alloc]initWithTitle:@"Message sending falied" delegate:self cancelButtonTitle:strCancel destructiveButtonTitle:nil otherButtonTitles:strCamera, nil];
    [actionSheet showInView:self];
    [actionSheet setTag:1];
    
}




-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(actionSheet.tag == 1){
        switch (buttonIndex) {
            case 0:
                NSLog(@"Take Picture");
                [self messageResend];
                break;
            default:
                NSLog(@"Default");
                break;
        }
    }
}


-(void)messageResend{
    
    
    ChatDetailViewController *chatDetailVC = (ChatDetailViewController *)[self.navigationController topViewController];
    if (self.message.message_type == [NSNumber numberWithInt:2]) {
        [chatDetailVC sendingAudioFromService:self.message];
    }
}

#pragma mark - UIImage Helper

-(UIImage *)imageNamed:(NSString *)imageName
{
    return [UIImage imageNamed:imageName
                      inBundle:[NSBundle bundleForClass:[self class]]
 compatibleWithTraitCollection:nil];
}



@end

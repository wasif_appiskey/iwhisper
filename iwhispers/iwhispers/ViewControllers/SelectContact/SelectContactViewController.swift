//
//  SelectContactViewController.swift
//  iwhispers
//
//  Created by Apple on 8/24/16.
//  Copyright © 2016 Appiskey. All rights reserved.
//

import UIKit
import ContactsUI
import AddressBook
import Contacts

class SelectContactViewController: UIViewController,BaseBLLDelegate {
    
    var contactArray : NSArray = NSArray()
    @IBOutlet var tblContact: UITableView!
    var strGroupID : String =  String()
    var dataDictionary : NSDictionary =  NSDictionary()
    var phonesArray  = Array<String>()
    var nameArray  = Array<String>()
    @IBOutlet var lblNoContact: UILabel!
    var arrMembers : NSMutableArray!
    var groupObj : Group!
    
    var selectedPaths  : NSMutableArray = NSMutableArray()

    var arrAddedContact  : NSMutableArray = NSMutableArray()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.intializeTableView()
        self.getAddressBookNames()

        self.settingNavigation()
        // Do any additional setup after loading the view.
    }
    func getAddressBookNames() {
        let authorizationStatus = ABAddressBookGetAuthorizationStatus()
        if (authorizationStatus == ABAuthorizationStatus.NotDetermined)
        {
            NSLog("requesting access...")
            var emptyDictionary: CFDictionaryRef?
            let addressBook = (ABAddressBookCreateWithOptions(emptyDictionary, nil) == nil)
            ABAddressBookRequestAccessWithCompletion(addressBook,{success, error in
                if success {
                    self.processContactNames();
                }
                else {
                    NSLog("unable to request access")
                }
            })
        }
        else if (authorizationStatus == ABAuthorizationStatus.Denied || authorizationStatus == ABAuthorizationStatus.Restricted) {
            NSLog("access denied")
        }
        else if (authorizationStatus == ABAuthorizationStatus.Authorized) {
            NSLog("access granted")
            processContactNames()
        }
        
    }
    
    
    override func viewWillAppear(animated: Bool) {
        self.populateView()
        
    }
    
    func processContactNames()
    {
        var errorRef: Unmanaged<CFError>?
        let addressBook: ABAddressBookRef? = extractABAddressBookRef(ABAddressBookCreateWithOptions(nil, &errorRef))
        
        let contactList: NSArray = ABAddressBookCopyArrayOfAllPeople(addressBook).takeRetainedValue()
        print("records in the array \(contactList.count)")
        
        for record:ABRecordRef in contactList {
            processAddressbookRecord(record)
        }
        
        
        for i:Int in 0 ..< phonesArray.count {
            var contactObj : Contact = DatabaseHelper.getObjectIfExist("Contact", predicate: NSPredicate(format: "phone == %@",phonesArray[i])) as! Contact
            contactObj.phone = phonesArray[i]
            contactObj.name = nameArray[i]
            DatabaseHelper.commitChanges()
        }
        
        print("All Phones: \(phonesArray)")
        self.syncContactService()
        
        
    }
    
    func processAddressbookRecord(contactPerson: ABRecordRef) {
        let phonesRef: ABMultiValueRef = ABRecordCopyValue(contactPerson, kABPersonPhoneProperty).takeRetainedValue() as ABMultiValueRef
        for i:Int in 0 ..< ABMultiValueGetCount(phonesRef) {
            //  var label: String = ABMultiValueCopyLabelAtIndex(phonesRef, i).takeRetainedValue()  as! String
            var value: String = ABMultiValueCopyValueAtIndex(phonesRef, i).takeRetainedValue()  as! String
            
            //      value = value.stringByReplacingOccurrencesOfString("\u{00a0}", withString: "")
            //      value = value.stringByReplacingOccurrencesOfString(" ", withString: "")
            
            value = self.removeSpecialCharsFromString(value)
            
            let userObj : User = DatabaseHelper.getObjectIfExist("User", predicate: NSPredicate(format: "isLoggedIn == 1")) as! User
            
            
            print("Phone: = \(value)")
            ///// Removing "0" from start of phone number if Exists
            if value.hasPrefix("00")  {
                value = value.substringFromIndex(value.startIndex.advancedBy(1))
                value = value.substringFromIndex(value.startIndex.advancedBy(1))
                value = String(format: "+%@", value)
            }
            
            if value.hasPrefix("0")  {
                value = value.substringFromIndex(value.startIndex.advancedBy(1))
                value = String(format: "%@%@", userObj.countryCode!,value)
            }
            if !value.hasPrefix("+")  {
                value = String(format: "%@%@", userObj.countryCode!,value)
            }
            if let firstName = ABRecordCopyValue(contactPerson, kABPersonFirstNameProperty) {
                let fn:String = (firstName.takeRetainedValue() as? String) ?? ""
                nameArray.append(fn as String)
            }else{
                nameArray.append("" as String)
            }
            
            phonesArray.append(value as String)
        }
        
    }
    
    func removeSpecialCharsFromString(text: String) -> String {
        let okayChars : Set<Character> =
            Set("1234567890+".characters)
        return String(text.characters.filter {okayChars.contains($0) })
    }
    
    func extractABAddressBookRef(abRef: Unmanaged<ABAddressBookRef>!) -> ABAddressBookRef? {
        if let ab = abRef {
            return Unmanaged<NSObject>.fromOpaque(ab.toOpaque()).takeRetainedValue()
        }
        return nil
    }
    
    
    
    /////////////////////****Sync Contact With Service ******////////////////
    func syncContactService(){
        if InternetCheck.IsConnected() {
            
            //            let queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
            //            dispatch_async(queue) {
            //                MBProgressHUD.showHUDAddedTo(self.view, animated: true)
            //            }
            //
            let bllService : BLLServices = BLLServices()
            bllService.delegate = self;
            let parameter : NSMutableDictionary = NSMutableDictionary()
            parameter.setValue(phonesArray, forKey: "contacts")
            bllService.syncContact(parameter)
        }else{
            Common.showAlert("Alert", message: "Please check your internet connection");
        }
    }

    
    func  populateView() {
        contactArray = DatabaseHelper.getAllData("Contact", offset: 0, predicate: nil, sortDescriptor: nil)
        var arrRemoveObj : NSMutableArray = NSMutableArray()
        for i:Int in 0 ..< contactArray.count {
            let contactObj : Contact = contactArray.objectAtIndex(i) as! Contact
            if contactObj.phone == "" {
                DatabaseHelper.deleteObject(contactObj)
            }
            if contactObj.serverName == "" {
                DatabaseHelper.deleteObject(contactObj)
            }
            if contactObj.serverName == nil {
                DatabaseHelper.deleteObject(contactObj)
            }
            if contactObj.active == "1" {
                
            }
            
        }
        contactArray = DatabaseHelper.getAllData("Contact", offset: 0, predicate: nil, sortDescriptor: nil)
        
        var arrMutable : NSMutableArray = NSMutableArray()
        arrMutable = contactArray.mutableCopy() as! NSMutableArray
        for i:Int in 0 ..< contactArray.count {
            let contactObj : Contact = contactArray.objectAtIndex(i) as! Contact
            if contactObj.active == "0" {
                arrMutable.removeObject(contactObj)
            }
        }
        contactArray = arrMutable as NSArray

        if groupObj != nil {
            arrMutable = contactArray.mutableCopy() as! NSMutableArray

            for i:Int in 0 ..< arrMembers.count {
                let groupMemberObj : GroupMember = arrMembers.objectAtIndex(i) as! GroupMember
                for j:Int in 0 ..< arrMutable.count {
                    let contactObj : Contact = arrMutable.objectAtIndex(j) as! Contact
                    if contactObj.identifier == String(format: "%@", groupMemberObj.contactID!) {
                        arrMutable.removeObject(contactObj)
                        break
                    }
                }
            }
            contactArray = arrMutable as NSArray
        }
        
       

        if contactArray.count == 0 {
            lblNoContact.hidden = false
            //   tblContact.hidden = true
        }else{
            lblNoContact.hidden = true
            //   tblContact.hidden = false
        }
        tblContact.reloadData()
    }
    
    /////////////////////**** Setting Up Navigation ******////////////
    
    func settingNavigation(){
        self.settingNavigationBg()
        self.settingNavigationTitle()
        self.settingNavigationRightButton()
        if groupObj != nil {
            self.settingNavigationLeftButton()
        }else{
            self.navigationItem.leftBarButtonItem = nil;
            self.navigationItem.hidesBackButton = true;
        }
    }
    
    
    func settingNavigationLeftButton(){
        let button: UIButton = UIButton(type:UIButtonType.Custom)
        button.setImage(UIImage(named: "arrow-white.png"), forState: UIControlState.Normal)
        button.addTarget(self, action: #selector(HomeViewController.btnActionNewMessage), forControlEvents: UIControlEvents.TouchUpInside)
        button.frame = CGRectMake(0, 0, 20, 21)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    
    //////**** Setting New Message Button Action ****//////
    
    func btnActionNewMessage(){
        print("Menu Pressed")
        self.navigationController?.popViewControllerAnimated(true)
        
    }
    
    
    
    //////**** Setting Navigation Right Button ****//////
    
    func settingNavigationRightButton(){
        let button: UIButton = UIButton(type:UIButtonType.Custom)
        button.setTitle("Done", forState: .Normal)
        button.addTarget(self, action: #selector(SelectContactViewController.btnActionDone), forControlEvents: UIControlEvents.TouchUpInside)
        button.frame = CGRectMake(0, 0, 50, 21)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    
    //////**** Setting New Message Button Action ****//////
    
    func btnActionDone(){
        print("Menu Pressed")
        
        if selectedPaths.count == 0 {
            Common.showAlert("Sorry", message: "Please select atleast one contact")
            return
        }
        
        let arrSelectedContact : NSMutableArray = NSMutableArray()
        for i:Int in 0 ..< selectedPaths.count {
            
            let contactObj : Contact = contactArray.objectAtIndex(selectedPaths.objectAtIndex(i) as! Int) as! Contact
            arrSelectedContact.addObject(contactObj.identifier)
        }
        let params : NSMutableDictionary = NSMutableDictionary()
        params.setValue(arrSelectedContact, forKey: "members")
        params.setValue(strGroupID, forKey: "group_id")
        
        let bllService : BLLServices = BLLServices()
        MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        bllService.delegate = self
        bllService.addGroupMember(params)
    }
    
    
    //////**** Setting Navigation Background ****//////
    
    func settingNavigationBg(){
        self.navigationController?.navigationBar.translucent = false
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 0.0/255.0, green: 115.0/255.0, blue: 181.0/255.0, alpha: 1.0)
        
    }
    
    //////**** Setting Navigation Title ****//////
    func settingNavigationTitle(){
        self.title = "Select Contacts"
        self.navigationController!.navigationBar.titleTextAttributes = [
            NSFontAttributeName: UIFont(name: "OpenSans", size: 15.0)!,
            NSForegroundColorAttributeName :  UIColor.whiteColor()
            
        ]
    }
    
    ////////////////////***** Intialize TableView *******//////////
    
    func intializeTableView(){
        
        let nibOne = UINib(nibName: "AllContactTableViewCell", bundle: nil)
        tblContact.registerNib(nibOne, forCellReuseIdentifier: "AllContactTableViewCellIdentifier")
        
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contactArray.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat{
        return 65
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:AllContactTableViewCell = tblContact.dequeueReusableCellWithIdentifier("AllContactTableViewCellIdentifier") as! AllContactTableViewCell
        cell.selectionStyle = .None
        let contactObj : Contact = contactArray.objectAtIndex(indexPath.row) as! Contact
        cell.lblName.text = contactObj.name
        
        cell.lblLastMessage.text = ""
        cell.lblMessageTime.text = ""
        
        if selectedPaths.indexOfObject(indexPath.row) != NSNotFound {
            cell.accessoryType = .Checkmark
        }else{
            cell.accessoryType = .None
            
        }
        
        return cell
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        //        rdv_tabBarController .setTabBarHidden(true, animated: true)
        let cell:AllContactTableViewCell = tblContact.dequeueReusableCellWithIdentifier("AllContactTableViewCellIdentifier") as! AllContactTableViewCell
        if selectedPaths.indexOfObject(indexPath.row) != NSNotFound {
            cell.accessoryType = .None
            var inRemoveIndex : Int = Int()
            for i:Int in 0 ..< selectedPaths.count {
                if selectedPaths.objectAtIndex(i) as! Int == indexPath.row {
                    inRemoveIndex = i
                }
            }
            selectedPaths.removeObjectAtIndex(inRemoveIndex)
        }else{
            cell.accessoryType = .Checkmark
            selectedPaths.addObject(indexPath.row)
        }
        tblContact.reloadData()
        
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /////////////***** Base BLL Delegates ****/////////
    //////***** Success *****////////
    func successWithData(data: AnyObject!, msg successMsg: String!, tag: Int32) {
        let tagService = AuthenticationParserType(rawValue: UInt32(tag))
        if (tagService == AddGroupMember){
            if groupObj == nil {
                
                let f = NSNumberFormatter()
                f.numberStyle = .DecimalStyle
                let groupID = f.numberFromString(strGroupID)
                let groupObj : Group = DatabaseHelper.getObjectIfExist("Group", predicate: NSPredicate(format: "identifier == %@",groupID!)) as! Group
                groupObj.identifier = groupID
                groupObj.name = dataDictionary.valueForKey("name") as? String
                if dataDictionary.valueForKey("image") != nil {
                    groupObj.image = String(format: "%@%@", kGroupPictureURL,(dataDictionary.valueForKey("image") as? String)!)
                }else{
                    groupObj.image = kDefaultURL
                }
                DatabaseHelper.commitChanges()
                for i:Int in 0 ..< selectedPaths.count {
                    
                    let contactObj : Contact = contactArray.objectAtIndex(selectedPaths.objectAtIndex(i) as! Int) as! Contact
                    let groupMemberObj : GroupMember = DatabaseHelper.getObjectIfExist("GroupMember", predicate: NSPredicate(format: "contactID == %@",f.numberFromString(contactObj.identifier)!)) as! GroupMember
                    groupMemberObj.contactID = f.numberFromString(contactObj.identifier)
                    groupMemberObj.groupID = groupID
                    let userObj : User = DatabaseHelper.getObjectIfExist("User", predicate: NSPredicate(format: "isLoggedIn == 1")) as! User
                    
                    if userObj.userID == groupObj {
                        groupMemberObj.isAdmin = "1"
                    }
                    

                    DatabaseHelper.commitChanges()
                }
                let chat : Chat = (DatabaseHelper.getObjectIfExist("Chat", predicate: NSPredicate(format: "contactID == %@", groupID!)) as! Chat)
                chat.chatType = Int(1)
                chat.numberOfUnreadMessages = Int(0)
                chat.contactID = groupID!
                chat.name = groupObj.name
                DatabaseHelper.commitChanges()
                MBProgressHUD.hideHUDForView(self.view, animated: true)
                (UIApplication.sharedApplication().delegate as! AppDelegate).setupViewControllers()
            }else{
                let f = NSNumberFormatter()
                f.numberStyle = .DecimalStyle
                let bllService : BLLServices = BLLServices()
                bllService.delegate = self;
                let parameter : NSMutableDictionary = NSMutableDictionary()
                parameter.setValue(f.stringFromNumber(self.groupObj.identifier!)!, forKey: "groupID")
                bllService.getGroupDetail(parameter)
            }
        }else if (tagService == GetGroupDetail){
            self.navigationController?.popViewControllerAnimated(true);
        }else if (tagService == SyncContact){
            self.populateView()
        }
        
    }
    //////***** Failure *****////////
    func requestFailure(failureMsg: String!, tag: Int32) {
        MBProgressHUD.hideHUDForView(self.view, animated: true)
        Common.showAlert("Sorry", message: failureMsg)
    }
    
    //////***** Uploading *****////////
    func uploadProgress(progress: Float) {
        
    }
    
    
    func  removeAddedContacts()  {
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

//
//  NewGroupViewController.swift
//  iwhispers
//
//  Created by Apple on 7/1/16.
//  Copyright © 2016 Appiskey. All rights reserved.
//

import UIKit

class NewGroupViewController: UIViewController,BaseBLLDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    @IBOutlet var viewGroupName: UIView!
    let picker = UIImagePickerController()
    @IBOutlet var imgProfile: UIImageView!
    var isImageChanged : Bool = Bool()
    var groupObj : Group!
    @IBOutlet var txtGroupName: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        txtGroupName.returnKeyType = .Done
        
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func btnActionNext(sender: AnyObject) {
        if txtGroupName.text == "" {
            Common.showAlert("Sorry", message: "Please enter group name")
            return
        }
        
        if InternetCheck.IsConnected() {
            let bllService : BLLServices = BLLServices()
            bllService.delegate = self
            let params  : NSMutableDictionary = NSMutableDictionary()
            params.setValue(txtGroupName.text, forKey: "name")
            params.setValue("1231231231", forKey: "public_key")
            MBProgressHUD.showHUDAddedTo(self.view, animated: true)
            if isImageChanged == true{
                let imageData : NSData = UIImageJPEGRepresentation(imgProfile.image!, 0.35)!
                bllService.createGroup(params, image: imageData)
            }else{
                bllService.createGroup(params, image: nil)
            }
        }else{
            Common.showAlert("Alert", message: "Please check your internet connection");
        }
    }
    
    
    @IBAction func didEditBegin(sender: AnyObject) {
        self.TextSlider(viewGroupName.frame)
    }
    
    @IBAction func didEditEnd(sender: AnyObject) {
        UIView.animateWithDuration(0.5, animations: { () -> Void in
            self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)
        })
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        txtGroupName.resignFirstResponder()
        return true
    }
    
    @IBAction func btnActionback(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.navigationBarHidden = true
    }
    
    override func viewWillDisappear(animated: Bool) {
        self.navigationController?.navigationBarHidden = false
    }
    
    
    /////////////////////****** Getting Keyboard Height *****////////////////////
    
    func getKeyboardHeight() ->(Int){
        var KeyboardHeight :Int
        
        KeyboardHeight = 273;
        
        return KeyboardHeight;
    }
    
    /////////////////////****** Setting Slider For UITextField *****////////////////////
    
    func TextSlider(viewFrame:CGRect) {
        
        let keyboardFrame = CGRectMake(0, 0, UIScreen.mainScreen().bounds.size.width, CGFloat(self.getKeyboardHeight()));
        
        var viewDifference : CGFloat;
        
        let keyBoaryYAxis:CGFloat = UIScreen.mainScreen().bounds.size.height - keyboardFrame.size.height
        
        viewDifference = keyBoaryYAxis - (viewFrame.origin.y + viewFrame.size.height)
        
        
        if (viewDifference<0) {
            UIView.animateWithDuration(0.5, animations: { () -> Void in
                self.view.frame = CGRectMake(0, viewDifference, self.view.frame.size.width, self.view.frame.size.height)
            })
        }
    }
    
    //////////// Touch on UIView Delegate /////////////
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
        UIView.animateWithDuration(0.5, animations: { () -> Void in
            self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)
        })
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //// MARK:  ActionSheet Methods
    ////////////////////******* ActionSheet Delegates ******* ///////////////
    func actionSheet(actionSheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int)
    {
        switch buttonIndex{
            
        case 0:
            NSLog("Cancel");
            break;
        case 1:
            NSLog("Take Picture");
            self.openCamera()
            break;
        case 2:
            NSLog("Upload Photo");
            self.openGallary()
            break;
        default:
            NSLog("Default");
            break;
        }
    }
    //// MARK:  PhotoLibrary Methods
    ////////////////////******* UICollectionView Delegates ******* ///////////////
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera))
        {
            picker.sourceType = UIImagePickerControllerSourceType.Camera
            picker.delegate = self
            picker.allowsEditing = true
            self .presentViewController(picker, animated: true, completion: nil)
        }
        else
        {
            let alertWarning = UIAlertView(title:"Warning", message: "Error accessing Camera", delegate:nil, cancelButtonTitle:"OK", otherButtonTitles:"")
            alertWarning.show()
        }
    }
    func openGallary()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.SavedPhotosAlbum){
            picker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
            picker.delegate = self
            picker.allowsEditing = true
            self.presentViewController(picker, animated: true, completion: nil)
        } else
        {
            let alertWarning = UIAlertView(title:"Warning", message: "Error accessing Photo Library", delegate:nil, cancelButtonTitle:"OK", otherButtonTitles:"")
            alertWarning.show()
            
        }
    }
    func imagePickerController(picker: UIImagePickerController!, didFinishPickingImage image: UIImage!, editingInfo: NSDictionary!) {
        
        picker.dismissViewControllerAnimated(true, completion: nil)
        let tempImage : UIImage = editingInfo[UIImagePickerControllerOriginalImage] as! UIImage
        imgProfile.image = tempImage
        imgProfile.layer.cornerRadius = CGRectGetWidth(imgProfile.frame)/2.0
        imgProfile.clipsToBounds = true
        isImageChanged = true
        
    }
    func imagePickerControllerDidCancel(picker: UIImagePickerController)
    {
        picker.dismissViewControllerAnimated(true, completion: nil)
        print("picker cancel.")
    }
    
    @IBAction func btnActionProfileImage(sender: AnyObject) {
        let strCamera = "Take a picture"
        let strPhotoLibrary = "Upload photo from album"
        let strCancel = "Cancel"
        let actionSheet = UIActionSheet(title: "Select from below", delegate: self, cancelButtonTitle: strCancel, destructiveButtonTitle:nil, otherButtonTitles:strCamera ,strPhotoLibrary)
        actionSheet.showInView(self.view)
        
    }
    
    
    /////////////***** Base BLL Delegates ****/////////
    //////***** Success *****////////
    func successWithData(data: AnyObject!, msg successMsg: String!, tag: Int32) {
        let tagService = AuthenticationParserType(rawValue: UInt32(tag))
        if tagService == CreateGroup {
            MBProgressHUD.hideHUDForView(self.view, animated: true)
            
            let dataDictionary : NSDictionary = data.objectAtIndex(0) as! NSDictionary
            let groupID : NSNumber = dataDictionary.valueForKey("id") as! NSNumber
            let strGroupID : String = String(format: "%@", groupID)
            var selectContactVC : SelectContactViewController = SelectContactViewController()
            selectContactVC = SelectContactViewController(nibName: "SelectContactViewController", bundle: nil)
            selectContactVC.dataDictionary = dataDictionary
            selectContactVC.strGroupID = strGroupID
            self.navigationController?.pushViewController(selectContactVC, animated: true)
            
           
            
            //    (UIApplication.sharedApplication().delegate as! AppDelegate).setupViewControllers()

        }
    }
    //////***** Failure *****////////
    func requestFailure(failureMsg: String!, tag: Int32) {
        MBProgressHUD.hideHUDForView(self.view, animated: true)
        Common.showAlert("Sorry", message: failureMsg)
    }
    
    //////***** Uploading *****////////
    func uploadProgress(progress: Float) {
        
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

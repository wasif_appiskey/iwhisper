//
//  HomeViewController.swift
//  iwhispers
//
//  Created by Apple on 6/22/16.
//  Copyright © 2016 Appiskey. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    @IBOutlet var tabbar: UITabBar!
    @IBOutlet var btnChatBarBtn: UITabBarItem!
    @IBOutlet var btnContactBarBtn: UITabBarItem!
    @IBOutlet var btnSettingBarBtn: UITabBarItem!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //// Setting Tab Bar ////////////
        self.settingTabBar()
        //// Setting Navigation////////
        self.settingNavigation()
      //  self.view.addSubview(tabBarVC.view)
    }
    

    
    
    /////////////////////**** Setting Up Navigation ******////////////
    
    func settingNavigation(){
        self.settingNavigationBg()
        self.settingNavigationTitle()
        self.settingNavigationLeftButton()
        self.settingNavigationRightButton()
    }
    
    //////**** Setting Navigation Background ****//////
    
    func settingNavigationBg(){
        self.navigationController?.navigationBar.translucent = false
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 0.0/255.0, green: 115.0/255.0, blue: 181.0/255.0, alpha: 1.0)
       
    }
    
    //////**** Setting Navigation Title ****//////
    func settingNavigationTitle(){
        self.title = "My Jobs"
        self.navigationController!.navigationBar.titleTextAttributes = [
            NSFontAttributeName: UIFont(name: "OpenSans", size: 15.0)!,
            NSForegroundColorAttributeName :  UIColor.whiteColor()
            
            
        ]
    }
    
    
    //////**** Setting Navigation Left Button ****//////
    
    func settingNavigationLeftButton(){
        let button: UIButton = UIButton(type:UIButtonType.Custom)
        button.setImage(UIImage(named: "new-message-icon.png"), forState: UIControlState.Normal)
        button.addTarget(self, action: #selector(HomeViewController.btnActionNewMessage), forControlEvents: UIControlEvents.TouchUpInside)
        button.frame = CGRectMake(0, 0, 20, 21)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    
    //////**** Setting New Message Button Action ****//////
    
    func btnActionNewMessage(){
        print("Menu Pressed")
        
    }
    
    //////**** Setting Navigation Left Button ****//////
    
    func settingNavigationRightButton(){
        let buttonNewGroup: UIButton = UIButton(type:UIButtonType.Custom)
        buttonNewGroup.setImage(UIImage(named: "new-group-icon.png"), forState: UIControlState.Normal)
        buttonNewGroup.addTarget(self, action: #selector(HomeViewController.btnActionNewGroup), forControlEvents: UIControlEvents.TouchUpInside)
        buttonNewGroup.frame = CGRectMake(42, 0, 40, 48)
        let buttonNewChat: UIButton = UIButton(type:UIButtonType.Custom)
        buttonNewChat.setImage(UIImage(named: "new-chat-icon.png"), forState: UIControlState.Normal)
        buttonNewChat.addTarget(self, action: #selector(HomeViewController.btnActionNewChat), forControlEvents: UIControlEvents.TouchUpInside)
        buttonNewChat.frame = CGRectMake(0, 0, 40, 48)
        
        
        let viewRight: UIView = UIView(frame: CGRectMake(0, 0, 85, 48))
        viewRight.addSubview(buttonNewChat)
        viewRight.addSubview(buttonNewGroup)
        
        let barButton = UIBarButtonItem(customView: viewRight)
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    //////**** Setting New Group Button Action ****//////
    
    func btnActionNewGroup(){
     
    }
    
    //////**** Setting New Chat Button Action ****//////
    
    func btnActionNewChat(){
        
    }
    

    
    /////////////Setting Tab Bar ////////////
    
    func settingTabBar() {
        ///Tab Buttons////
        settingTabButons()
        ////Tab Appearence////
        settingTabTextAppearence()
        ////Tab Background/////
        settingTabBG()
    }
    
    
    /////////////Setting Chat Tab Bar Button ////////////

    func settingTabButons() {
        
        ////// Setting Chat Button///////////////
        let chat:UITabBarItem = UITabBarItem(title: "Chat", image: UIImage(named: "chat.png")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal), selectedImage: UIImage(named: "chat.png"))
        ///Title Position
        chat.titlePositionAdjustment = UIOffset(horizontal: -3, vertical: -4)
        
        
        ////// Setting Contact Button///////////////
        let contact :UITabBarItem = UITabBarItem(title: "Contact", image: UIImage(named: "contact.png")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal), selectedImage: UIImage(named: "contact.png"))
        ///Image Position
        contact.imageInsets = UIEdgeInsetsMake(-5, 1, 5, -1)
        ///Title Position
        contact.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: -4)
        
        
        ////// Setting setting Button///////////////
        let setting :UITabBarItem = UITabBarItem(title: "Setting", image: UIImage(named: "setting.png")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal), selectedImage: UIImage(named: "setting.png"))
        ///Title Position
        setting.titlePositionAdjustment = UIOffset(horizontal: -3, vertical: -4)
        
        ////// Setting Buttons to TabBar//////////////
        tabbar.setItems([chat,contact,setting], animated: false)

    }
    
    ///////// Chainging Text Color ////////////////////
    
    func settingTabTextAppearence(){
        for item in tabbar.items! as [UITabBarItem]
        {
            item.setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.whiteColor()], forState:UIControlState.Normal)
            
            item.setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.whiteColor()], forState:UIControlState.Disabled)
            item.setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.whiteColor()], forState:UIControlState.Selected)
        }
    }
    
    
    ///////////// Setting Tab Bar Background ///////////
    
    func settingTabBG(){
        //// Making Tab Bar Transparent
    //    UITabBar.appearance().barTintColor = UIColor(patternImage: UIImage(named: "transparent.png")!)
   //     tabbar.setValue("YES", forKeyPath: "_hidesShadow")
    //    UITabBar.appearance().shadowImage = UIImage()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

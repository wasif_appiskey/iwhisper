//
//  SMSVerifyViewController.swift
//  iwhispers
//
//  Created by Apple on 6/22/16.
//  Copyright © 2016 Appiskey. All rights reserved.
//

import UIKit
import Starscream

class SMSVerifyViewController: UIViewController,BaseBLLDelegate,UITextFieldDelegate,WebSocketDelegate,CAAnimationDelegate {
    
    @IBOutlet var txtCode: UITextField!
    @IBOutlet var lblSubtitle: UILabel!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var btnEditNumber: UIButton!

    var isEditNumber : Bool = false
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.settingKeyboard()
        self.populateView()
        self.settingToolbar()
        appDelegate.socket.delegate = self
        // Do any additional setup after loading the view.
    }
    
    func settingToolbar(){
        let numberToolbar = UIToolbar(frame: CGRectMake(0, 0, self.view.frame.size.width, 50))
        numberToolbar.barStyle = UIBarStyle.Default
        numberToolbar.items = [
            UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil),
            UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(SMSVerifyViewController.doneWithNumberPad))]
        numberToolbar.sizeToFit()
        txtCode.inputAccessoryView = numberToolbar
    }
    
    
    func doneWithNumberPad(){
        self.view.endEditing(true)
    }
    
    
    func subscribeUserToSocket(){
        let userObj : User = DatabaseHelper.getObjectIfExist("User", predicate: NSPredicate(format: "isLoggedIn == 1")) as! User
        if userObj.token != nil {
            var dataDic : NSMutableDictionary = NSMutableDictionary()
            dataDic.setValue("authorize", forKey: "command")
            dataDic.setValue(userObj.token , forKey: "token")
            let json : SBJSON = SBJSON()
            let strJson : String = json.stringWithObject(dataDic)
            appDelegate.socket.writeString(strJson)
        }else{
            Common.showAlert("Sorry", message: "Please try again")
        }
    }
    
    func settingKeyboard(){
        txtCode.returnKeyType = .Done
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        txtCode.resignFirstResponder()
        return true
    }
    
    func populateView() {
        let userObj : User = DatabaseHelper.getObjectIfExist("User", predicate: NSPredicate(format: "isLoggedIn == 1")) as! User

        lblTitle.text = userObj.phone
        lblSubtitle.text = String(format: "Wait to automatically detect an SMS sent %@ Wrong Number ?",userObj.phone!)
        if isEditNumber == true {
            btnEditNumber.hidden = false
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.navigationBarHidden = true
        appDelegate.socket.delegate = self
        if isEditNumber == true {
            rdv_tabBarController .setTabBarHidden(true, animated: true)
        }
    }
    
    override func viewWillDisappear(animated: Bool) {
        self.navigationController?.navigationBarHidden = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    ////////****** Button Action for Home *****//////////
    
    @IBAction func btnActionHome(sender: AnyObject) {
        
        
//        var profileVC : ProfileViewController = ProfileViewController()
//        profileVC = ProfileViewController(nibName: "ProfileViewController", bundle: nil)
//        self.navigationController?.pushViewController(profileVC, animated: true)
        let isValidate : Bool = self.isValidate()
        if isValidate == false {
            return
        }
        if InternetCheck.IsConnected() {
            MBProgressHUD.showHUDAddedTo(self.view, animated: true)
            let  bllSerives : BLLServices =  BLLServices()
            bllSerives.delegate = self
            let parameter : NSMutableDictionary =  NSMutableDictionary()
            let userObj : User = DatabaseHelper.getObjectIfExist("User", predicate: NSPredicate(format: "isLoggedIn == 1")) as! User
            
            lblTitle.text = userObj.phone
            parameter.setValue(txtCode.text, forKey: "code")
            parameter.setValue(userObj.phone, forKey: "phone")

            bllSerives.userVerify(parameter)
        }else{
            Common.showAlert("Alert", message: "Please check your internet connection");
        }
    }
    
    @IBAction func btnActionback(sender: AnyObject) {
        
        if self.navigationController?.viewControllers.count >= 2 {
            self.navigationController?.popViewControllerAnimated(true)
        }else{
            var yourPhoneVC = YourPhoneNumberViewController(nibName: "YourPhoneNumberViewController", bundle: nil)
            let userObj : User = DatabaseHelper.getObjectIfExist("User", predicate: NSPredicate(format: "isLoggedIn == 1")) as! User
            yourPhoneVC.strCountryCode = "+"+userObj.countryCode!
            var strPhone = userObj.phone!.stringByReplacingOccurrencesOfString(userObj.countryCode!, withString: "")
            yourPhoneVC.strCountryName = userObj.countryName
            yourPhoneVC.strPhoneNumber = strPhone

            var transition = CATransition()
            transition.duration = 0.45
            transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionDefault)
            transition.type = kCATransitionPush
            transition.subtype = kCATransitionFromLeft
            transition.delegate = self
            self.navigationController!.view.layer.addAnimation(transition, forKey: nil)
            self.navigationController!.navigationBarHidden = false
            self.navigationController!.pushViewController(yourPhoneVC, animated: false)
        }


    }
    
    //////////// Touch on UIView Delegate /////////////
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    //////**** Validate Login *****///////
    
    func isValidate() -> Bool {
        
        if txtCode.text == "" {
            Common.showAlert("Alert", message: "Please enter verification code")
            return false
        }

        return true
    }
    
    ////////****** Resend SMS ********///////////////
    func resendSMSService(){
        if InternetCheck.IsConnected() {
            MBProgressHUD.showHUDAddedTo(self.view, animated: true)
            let  bllSerives : BLLServices =  BLLServices()
            bllSerives.delegate = self
            let parameter : NSMutableDictionary =  NSMutableDictionary()
            let userObj : User = DatabaseHelper.getObjectIfExist("User", predicate: NSPredicate(format: "isLoggedIn == 1")) as! User
            
            parameter.setValue(userObj.phone, forKey: "phone")
            //// Callling Service
            bllSerives.userLogin(parameter)
        }else{
            Common.showAlert("Alert", message: "Please check your internet connection");
        }

    }
    ////////****** Button Action for Verify Number *****//////////
    
    @IBAction func btnActionResendSMS(sender: AnyObject) {
        
        self.resendSMSService()
    }
    /////////////***** Base BLL Delegates ****/////////
    //////***** Success *****////////
    func successWithData(data: AnyObject!, msg successMsg: String!, tag: Int32) {
        let tagService = AuthenticationParserType(rawValue: UInt32(tag))
        if (tagService == Login){
            MBProgressHUD.hideHUDForView(self.view, animated: true)
            Common.showAlert("", message: "Please check your mobile verifiction code is Resend")
        }else if tagService == Verify{
            ///// Going to View Controller Sms Verification /////////
            MBProgressHUD.hideHUDForView(self.view, animated: true)
            if isEditNumber == true {
                (UIApplication.sharedApplication().delegate as! AppDelegate).setupViewControllers()
            }else{
                self.subscribeUserToSocket()
                var profileVC : ProfileViewController = ProfileViewController()
                profileVC = ProfileViewController(nibName: "ProfileViewController", bundle: nil)
                self.navigationController?.pushViewController(profileVC, animated: true)
            }
        }
        
    }
    //////***** Failure *****////////
    func requestFailure(failureMsg: String!, tag: Int32) {
        MBProgressHUD.hideHUDForView(self.view, animated: true)
        Common.showAlert("Sorry", message: failureMsg)
    }
    
    //////***** Uploading *****////////
    func uploadProgress(progress: Float) {
        
    }
    

    
    
    
    
    
    // MARK: Websocket Delegate Methods.
    
    func websocketDidConnect(ws: WebSocket) {
        print("websocket is connected")
    }
    
    func websocketDidDisconnect(ws: WebSocket, error: NSError?) {
        if let e = error {
            print("websocket is disconnected: \(e.localizedDescription)")
        } else {
            print("websocket disconnected")
        }
        appDelegate.socket.connect()
    }
    
    func websocketDidReceiveMessage(ws: WebSocket, text: String) {
        print("Received text: \(text)")
        let json : SBJSON = SBJSON()
        let dicJson : NSDictionary = json.objectWithString(text) as! NSDictionary
        var strStatus : String = String()
        if dicJson.valueForKey("status") != nil{
            strStatus = dicJson.valueForKey("status") as! String
        }
        if strStatus == "success" {
            
            appDelegate.socket.delegate = appDelegate

        }
    }
    
    func websocketDidReceiveData(ws: WebSocket, data: NSData) {
        print("Received data: \(data.length)")
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

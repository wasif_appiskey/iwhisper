//
//  AES.h
//  iwhispers
//
//  Created by Wasif Iqbal on 2/14/17.
//  Copyright © 2017 Appiskey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonCrypto.h>

@interface AES : NSObject

+ (NSData *)Encrypt:(NSString *)data WithKey:(NSString *)key;
+ (NSString *)Decrypt:(NSData *)data WithKey:(NSString *)key;

+ (NSData *)AESOperation:(CCOperation)operation OnData:(NSData *)data key:(NSString *)key;



@end

//
//  AFNetworkingWrapper.h
//  Clippet
//
//  Created by Ammad on 11/4/13.
//  Copyright (c) 2013 Apppli. All rights reserved.
//

#import <Foundation/Foundation.h>

// Protocol for the importer to communicate with its delegate.
@protocol AFNetworkingWrapperDelegate <NSObject>

//asynchronous request finished
- (void)requestFinished:(NSString *)response tag:(int)tag;

//asynchronous request failed
- (void)requestFailed:(NSString *)response tag:(int)tag;

//get upload progress to display on home screen for upload items
-(void)uploadProgress:(float)uploadProgress;

@end


@interface AFNetworkingWrapper : NSObject
{
    __weak id <AFNetworkingWrapperDelegate> delegate;
    
    NSString *requestURL;
    NSString *postKey;
    NSString *extentionType;
    NSString *mimetype;
    NSString *rawData;

    NSMutableDictionary *postParams;
    NSData *postData;
    
    int tagService;
}

@property (nonatomic, weak) id <AFNetworkingWrapperDelegate> delegate;

@property (nonatomic, strong) NSString *requestURL;
@property (nonatomic, strong) NSString *postKey;
@property (nonatomic, strong) NSString *mimetype;
@property (nonatomic, strong) NSString *extentionType;
@property (nonatomic, strong) NSMutableDictionary *postParams;
@property (nonatomic, strong) NSData *postData;

@property (nonatomic, readwrite) int tagService;

-(id)initWithURL:(NSString *)url andPostParams:(NSMutableDictionary *)params;

-(void)startAsynchronous;
-(void)startAsynchronousPost;
-(void)startAsynchronousDelete;
-(void)startAsynchronousPut;
-(void)startAsynchronousPostWithData;
-(void)startAsynchronousPutWithData;

@end


//
//  AFNetworkingWrapper.m
//  Clippet
//
//  Created by Ammad on 11/4/13.
//  Copyright (c) 2013 Apppli. All rights reserved.
//

#import "AFNetworkingWrapper.h"
#import "AFHTTPRequestOperation.h"
#import "AppConstants.h"

#import "AFHTTPClient.h"

@implementation AFNetworkingWrapper
@synthesize delegate, postData, postParams, requestURL, tagService , postKey ,extentionType, mimetype;

-(id)initWithURL:(NSString *)url andPostParams:(NSMutableDictionary *)params
{
    if (self = [super init])
    {
        self.requestURL = url;
        self.postParams = params;
        
    }
    return self;
}


-(void)startAsynchronous
{
    @try {
        
        AFHTTPClient *httpClient = [AFHTTPClient clientWithBaseURL:[NSURL URLWithString:kAPIUrl]];
        id responseDelegate = self.delegate;
        [httpClient setParameterEncoding:AFFormURLParameterEncoding];
        NSMutableURLRequest *request = [httpClient requestWithMethod:@"GET"
                                                                path:self.requestURL
                                                          parameters:self.postParams];
        //[self addNetworkObserver];
        if (tagService != Login && tagService != Signup ) {
            User *userObj = (User *)[DatabaseHelper getObjectIfExist:@"User" predicate:[NSPredicate predicateWithFormat:@"isLoggedIn == 1"]];
            [request setValue:userObj.token  forHTTPHeaderField:@"authorization"];
        }
        
        
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            NSString *serviceResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
            [responseDelegate requestFinished:serviceResponse tag:self.tagService];
            
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Error: %@", error);
            NSDictionary *userInfo = [error userInfo];
            NSString *strError = [userInfo objectForKey:@"NSLocalizedRecoverySuggestion"];
            
            if (error.code == NSURLErrorTimedOut) {
                //time out error here
                //  [self connectionTimeOut];
                strError = @"Connection Timeout";
            }else if (error.code == NSURLErrorNotConnectedToInternet ||error.code == NSURLErrorCannotConnectToHost){
                strError = @"The Internet connection appears to be offline.";
            }else if (error.code == NSURLErrorBadServerResponse){
                strError = @"Server Error";
            }
            if (strError != nil && [strError isEqualToString:@""]){
                strError = [userInfo objectForKey:@"NSLocalizedDescription"];
            }
            if (![strError isEqualToString:@""]) {
                [responseDelegate requestFailed:strError tag:self.tagService];
            }else{
                [responseDelegate requestFailed:@"fail" tag:self.tagService];
            }
        }];
        [operation start];
    }
    @catch (NSException *exception) {
        NSLog(@"NSException startAsynchronousPost");
        
    }
    
}


-(void)startAsynchronousPut
{
    @try {
        
        AFHTTPClient *httpClient = [AFHTTPClient clientWithBaseURL:[NSURL URLWithString:kAPIUrl]];
        id responseDelegate = self.delegate;
        
        [httpClient setParameterEncoding:AFFormURLParameterEncoding];
        
        
        NSMutableURLRequest *request = [httpClient requestWithMethod:@"PUT"
                                                                path:self.requestURL
                                                          parameters:self.postParams];
        
        if (tagService != Login) {
            User *userObj = (User *)[DatabaseHelper getObjectIfExist:@"User" predicate:[NSPredicate predicateWithFormat:@"isLoggedIn == 1"]];
            [request setValue:userObj.token  forHTTPHeaderField:@"authorization"];
            NSLog(@"%@",userObj.token);
        }
        
        [httpClient registerHTTPOperationClass:[AFHTTPRequestOperation class]];
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            NSString *serviceResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
            [responseDelegate requestFinished:serviceResponse tag:self.tagService];
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSDictionary *userInfo = [error userInfo];
            NSString *strError = [userInfo objectForKey:@"NSLocalizedRecoverySuggestion"];
            
            if (error.code == NSURLErrorTimedOut) {
                //time out error here
                //  [self connectionTimeOut];
                strError = @"Connection Timeout";
            }else if (error.code == NSURLErrorNotConnectedToInternet ||error.code == NSURLErrorCannotConnectToHost){
                strError = @"The Internet connection appears to be offline.";
            }else if (error.code == NSURLErrorBadServerResponse){
                strError = @"Server Error";
            }
            
            
            if (![strError isEqualToString:@""]) {
                [responseDelegate requestFailed:strError tag:self.tagService];
            }else{
                [responseDelegate requestFailed:@"fail" tag:self.tagService];
            }
        }];
        [operation start];
        
        
    }
    @catch (NSException *exception) {
        if (isDebug) {
            NSLog(@"NSException startAsynchronousPost");
        }
    }
    
}


-(void)startAsynchronousPostFCM
{
    @try {
        
        AFHTTPClient *httpClient = [AFHTTPClient clientWithBaseURL:[NSURL URLWithString:kAPIUrl]];
        id responseDelegate = self.delegate;
        
        [httpClient setParameterEncoding:AFFormURLParameterEncoding];
        
        
        NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                                path:self.requestURL
                                                          parameters:self.postParams];
        
        
        if (tagService != Login && tagService != Signup ) {
            User *userObj = (User *)[DatabaseHelper getObjectIfExist:@"User" predicate:[NSPredicate predicateWithFormat:@"isLoggedIn == 1"]];
            [request setValue:userObj.token  forHTTPHeaderField:@"authorization"];
            NSLog(@"%@",userObj.token);
        }
        
        [httpClient registerHTTPOperationClass:[AFHTTPRequestOperation class]];
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            NSString *serviceResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
            [responseDelegate requestFinished:serviceResponse tag:self.tagService];
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSDictionary *userInfo = [error userInfo];
            NSString *strError = [userInfo objectForKey:@"NSLocalizedRecoverySuggestion"];
            
            if (error.code == NSURLErrorTimedOut) {
                //time out error here
                //  [self connectionTimeOut];
                strError = @"Connection Timeout";
            }else if (error.code == NSURLErrorNotConnectedToInternet ||error.code == NSURLErrorCannotConnectToHost){
                strError = @"The Internet connection appears to be offline.";
            }else if (error.code == NSURLErrorBadServerResponse){
                strError = @"Server Error";
            }
            
            if (![strError isEqualToString:@""]) {
                [responseDelegate requestFailed:strError tag:self.tagService];
            }else{
                [responseDelegate requestFailed:@"fail" tag:self.tagService];
            }
        }];
        [operation start];
        
        
    }
    @catch (NSException *exception) {
        if (isDebug) {
            NSLog(@"NSException startAsynchronousPost");
        }
    }
    
}



-(void)startAsynchronousPost
{
    @try {
        
        AFHTTPClient *httpClient = [AFHTTPClient clientWithBaseURL:[NSURL URLWithString:kAPIUrl]];
        id responseDelegate = self.delegate;
        
        [httpClient setParameterEncoding:AFFormURLParameterEncoding];
        
        
        NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                                path:self.requestURL
                                                          parameters:self.postParams];
        
        
        if (tagService != Login && tagService != Signup ) {
            User *userObj = (User *)[DatabaseHelper getObjectIfExist:@"User" predicate:[NSPredicate predicateWithFormat:@"isLoggedIn == 1"]];
            [request setValue:userObj.token  forHTTPHeaderField:@"authorization"];
            NSLog(@"%@",userObj.token);
        }
        
        [httpClient registerHTTPOperationClass:[AFHTTPRequestOperation class]];
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            NSString *serviceResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
            [responseDelegate requestFinished:serviceResponse tag:self.tagService];
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSDictionary *userInfo = [error userInfo];
            NSString *strError = [userInfo objectForKey:@"NSLocalizedRecoverySuggestion"];
            
            if (error.code == NSURLErrorTimedOut) {
                //time out error here
                //  [self connectionTimeOut];
                strError = @"Connection Timeout";
            }else if (error.code == NSURLErrorNotConnectedToInternet ||error.code == NSURLErrorCannotConnectToHost || error.code == NSURLErrorNetworkConnectionLost){
                strError = @"Network connection was lost please try again";
            }else if (error.code == NSURLErrorBadServerResponse){
                strError = @"Server Error";
            }
            
            if (![strError isEqualToString:@""]) {
                [responseDelegate requestFailed:strError tag:self.tagService];
            }else{
                [responseDelegate requestFailed:@"fail" tag:self.tagService];
            }
        }];
        [operation start];
        
        
    }
    @catch (NSException *exception) {
        if (isDebug) {
            NSLog(@"NSException startAsynchronousPost");
        }
    }
    
}



-(void)startAsynchronousDelete
{
    @try {
        
        AFHTTPClient *httpClient = [AFHTTPClient clientWithBaseURL:[NSURL URLWithString:kAPIUrl]];
        id responseDelegate = self.delegate;
        
        [httpClient setParameterEncoding:AFFormURLParameterEncoding];
        
        
        NSMutableURLRequest *request = [httpClient requestWithMethod:@"DELETE"
                                                                path:self.requestURL
                                                          parameters:self.postParams];
        
        //        if (tagService != Login && tagService != Signup ) {
        //            User *userObj = (User *)[DatabaseHelper getObjectIfExist:@"User" predicate:[NSPredicate predicateWithFormat:@"isLoggedIn == 1"]];
        //            [request setValue:userObj.token  forHTTPHeaderField:@"X-Authorization"];
        //        }
        
        [httpClient registerHTTPOperationClass:[AFHTTPRequestOperation class]];
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            NSString *serviceResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
            [responseDelegate requestFinished:serviceResponse tag:self.tagService];
            
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSDictionary *userInfo = [error userInfo];
            NSString *strError = [userInfo objectForKey:@"NSLocalizedRecoverySuggestion"];
            
            if (error.code == NSURLErrorTimedOut) {
                //time out error here
                //  [self connectionTimeOut];
                strError = @"Connection Timeout";
            }else if (error.code == NSURLErrorNotConnectedToInternet ||error.code == NSURLErrorCannotConnectToHost){
                strError = @"The Internet connection appears to be offline.";
            }else if (error.code == NSURLErrorBadServerResponse){
                strError = @"Server Error";
            }
            
            if (![strError isEqualToString:@""]) {
                [responseDelegate requestFailed:strError tag:self.tagService];
            }else{
                [responseDelegate requestFailed:@"fail" tag:self.tagService];
            }
        }];
        [operation start];
        
        
    }
    @catch (NSException *exception) {
        if (isDebug) {
            NSLog(@"NSException startAsynchronousPost");
        }
    }
    
}

-(void)startAsynchronousPostWithData
{
    id responseDelegate = self.delegate;
    AFHTTPClient *client= [AFHTTPClient clientWithBaseURL:[NSURL URLWithString:kAPIUrl]];
    
    
    
    
    NSMutableURLRequest *request = [client multipartFormRequestWithMethod:@"POST" path:self.requestURL parameters:self.postParams constructingBodyWithBlock: ^(id <AFMultipartFormData>formData) {
            User *userObj = (User *)[DatabaseHelper getObjectIfExist:@"User" predicate:[NSPredicate predicateWithFormat:@"isLoggedIn == 1"]];
        
            [formData appendPartWithFileData:self.postData name:postKey fileName:[NSString stringWithFormat:@"%@.%@",userObj.token,extentionType] mimeType:mimetype];
    }];
    
    
        if (tagService != Login && tagService != Signup ) {
            User *userObj = (User *)[DatabaseHelper getObjectIfExist:@"User" predicate:[NSPredicate predicateWithFormat:@"isLoggedIn == 1"]];
            [request setValue:userObj.token  forHTTPHeaderField:@"authorization"];
        }
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSString *serviceResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         [responseDelegate requestFinished:serviceResponse tag:self.tagService];
         
     }
        failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         if (isDebug) {
             NSLog(@"Error: %@", error);
         }
         NSDictionary *userInfo = [error userInfo];
         NSString *strError = [userInfo objectForKey:@"NSLocalizedRecoverySuggestion"];
         
         if (error.code == NSURLErrorTimedOut) {
             //time out error here
             //  [self connectionTimeOut];
             strError = @"Connection Timeout";
         }else if (error.code == NSURLErrorNotConnectedToInternet ||error.code == NSURLErrorCannotConnectToHost){
             strError = @"The Internet connection appears to be offline.";
         }else if (error.code == 404){
             strError = @"Server Error";
         }
         
         if (![strError isEqualToString:@""]) {
             [responseDelegate requestFailed:strError tag:self.tagService];
         }else{
             [responseDelegate requestFailed:@"fail" tag:self.tagService];
         }
     }];
    
    [operation start];
    
    
}

-(void)startAsynchronousPutWithData
{
    id responseDelegate = self.delegate;
    AFHTTPClient *client= [AFHTTPClient clientWithBaseURL:[NSURL URLWithString:kAPIUrl]];
    
    
    
    
    NSMutableURLRequest *request = [client multipartFormRequestWithMethod:@"PUT" path:self.requestURL parameters:self.postParams constructingBodyWithBlock: ^(id <AFMultipartFormData>formData) {
        User *userObj = (User *)[DatabaseHelper getObjectIfExist:@"User" predicate:[NSPredicate predicateWithFormat:@"isLoggedIn == 1"]];
        
        [formData appendPartWithFileData:self.postData name:postKey fileName:[NSString stringWithFormat:@"%@.%@",userObj.token,extentionType] mimeType:mimetype];
    }];
    
    
    if (tagService != Login) {
        User *userObj = (User *)[DatabaseHelper getObjectIfExist:@"User" predicate:[NSPredicate predicateWithFormat:@"isLoggedIn == 1"]];
        [request setValue:userObj.token  forHTTPHeaderField:@"authorization"];
    }
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSString *serviceResponse = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         [responseDelegate requestFinished:serviceResponse tag:self.tagService];
         
     }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         if (isDebug) {
             NSLog(@"Error: %@", error);
         }
         NSDictionary *userInfo = [error userInfo];
         NSString *strError = [userInfo objectForKey:@"NSLocalizedRecoverySuggestion"];
         
         if (error.code == NSURLErrorTimedOut) {
             //time out error here
             //  [self connectionTimeOut];
             strError = @"Connection Timeout";
         }else if (error.code == NSURLErrorNotConnectedToInternet ||error.code == NSURLErrorCannotConnectToHost){
             strError = @"The Internet connection appears to be offline.";
         }else if (error.code == 404){
             strError = @"Server Error";
         }
         
         if (![strError isEqualToString:@""]) {
             [responseDelegate requestFailed:strError tag:self.tagService];
         }else{
             [responseDelegate requestFailed:@"fail" tag:self.tagService];
         }
     }];
    
    [operation start];
    
    
}


- (void)addNetworkObserver
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(HTTPOperationDidFinish:)
                                                 name:AFNetworkingOperationDidFinishNotification
                                               object:nil];
}

- (void)HTTPOperationDidFinish:(NSNotification *)notification
{
    AFHTTPRequestOperation *operation = (AFHTTPRequestOperation *)[notification object];
    if (![operation isKindOfClass:[AFHTTPRequestOperation class]]) {
        return;
    }
    if (operation.error) {
        
        NSLog(@"connection error");
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection error"
                                                        message:@"Missing connection to the internet"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
    
}


-(void)connectionTimeOut{
    UIViewController * currentVC = (UIViewController *)[self getCurrentViewController];
    if (currentVC)
    {
        NSLog(@"currentVC :%@",currentVC);
    }
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:currentVC.view animated:YES];
    hud.mode = MBProgressHUDModeAnnularDeterminate;
    hud.label.text = @"";
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Timeout"
                                                    message:@""
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
    
    
}

-(id)getCurrentViewController
{
    id WindowRootVC = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
    
    id currentViewController = [self findTopViewController:WindowRootVC];
    
    return currentViewController;
}

-(id)findTopViewController:(id)inController
{
    /* if ur using any Customs classes, do like this.
     * Here SlideNavigationController is a subclass of UINavigationController.
     * And ensure you check the custom classes before native controllers , if u have any in your hierarchy.
     if ([inController isKindOfClass:[SlideNavigationController class]])
     {
     return [self findTopViewController:[inController visibleViewController]];
     }
     else */
    if ([inController isKindOfClass:[UITabBarController class]])
    {
        return [self findTopViewController:[inController selectedViewController]];
    }
    else if ([inController isKindOfClass:[UINavigationController class]])
    {
        return [self findTopViewController:[inController visibleViewController]];
    }
    else if ([inController isKindOfClass:[UIViewController class]])
    {
        return inController;
    }
    else
    {
        NSLog(@"Unhandled ViewController class : %@",inController);
        return nil;
    }
}



@end

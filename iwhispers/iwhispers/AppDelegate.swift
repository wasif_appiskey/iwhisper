//
//  AppDelegate.swift
//  iwhispers
//
//  Created by Apple on 6/17/16.
//  Copyright © 2016 Appiskey. All rights reserved.
//

import UIKit
import CoreData
import Firebase
import FirebaseInstanceID
import FirebaseMessaging
import Starscream
import AVFoundation
import Fabric
import Crashlytics
import Heimdall

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, WebSocketDelegate,BaseBLLDelegate {
    var socket = WebSocket(url: NSURL(string: kAPIUrlSocket)!, protocols: nil)
    
    var window: UIWindow?
    
    var devToken : String  = ""
    
    /// Setting Navigation /////////////
    var navController:UINavigationController?
    
    ///// Setting Agree Signup VC ///////////
    var agreeSignupVC :AgreeSignupViewController?
    
    ///// Setting SMS Verify ///////////
    var smsVerifyVC :SMSVerifyViewController?
    
    ///// Setting SMS Verify ///////////
    var profileVC :ProfileViewController?
    
    ///// Setting Home VC ///////////
    var homeVC :HomeViewController?
    
    var viewController: UIViewController?
    
    var pianoSound = NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("message", ofType: "mp3")!)
    var audioPlayer = AVAudioPlayer()
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        // Register for remote notifications
        //                DatabaseHelper.deleteAllObjectsOfEntity("Message", predicate: nil)
        //            DatabaseHelper.deleteAllObjectsOfEntity("Chat", predicate: nil)
        //            DatabaseHelper.deleteAllObjectsOfEntity("Group", predicate: nil)
        //
        //              DatabaseHelper.deleteAllObjectsOfEntity("Contact", predicate: nil)
        //                DatabaseHelper.deleteAllObjectsOfEntity("User", predicate: nil)
        
        UIApplication.sharedApplication().applicationIconBadgeNumber = 0
        
        //[AESCrypt encrypt:inputbar.text password:@"meow"];
//        
//        print(AESCrypt.generateAESRandomKey())
//        
//        
//        var strMessage: String = AESCrypt.encrypt("hi", password: "meow")
//        print(strMessage)
//        
//        var shaMessage: String = AESCrypt.generateSha("hi")
//        print(shaMessage)
//
//     //   var decryptStrMessage: String = AESCrypt.decrypt(strMessage, password: "meow")
//     //   print(decryptStrMessage)
//        
//       // AESCrypt.generate()
//        
//        var rsaManager :  RSAManager = RSAManager()
//        let publicKey = rsaManager.getPublicKeyRef()
//        let privateKey = rsaManager.getPrivateKeyRef()

        Fabric.with([Crashlytics.self])
        //
        //        for familyName in UIFont.familyNames() {
        //            for fontName in UIFont.fontNamesForFamilyName(familyName as! String) {
        //                print("\(familyName) : \(fontName)")
        //            }
        //        }
        //
        
        ////Socket Commection
        self.connectingSocket()
        let CatSound = NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("message", ofType: "mp3")!)
        
        //    audioPlayer = AVAudioPlayer(contentsOfURL: pianoSound, error: nil)
        do {
            audioPlayer = try AVAudioPlayer(contentsOfURL: CatSound)
            audioPlayer.prepareToPlay()
        } catch {
            print("Problem in getting File")
        }
        
        if #available(iOS 8.0, *) {
            // [START register_for_notifications]
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(forTypes: [.Alert, .Badge, .Sound], categories: nil)
            application.registerUserNotificationSettings(settings)
            application.registerForRemoteNotifications()
            // [END register_for_notifications]
        } else {
            // Fallback
            let types: UIRemoteNotificationType = [.Alert, .Badge, .Sound]
            application.registerForRemoteNotificationTypes(types)
        }
        
        FIRApp.configure()
        
        // Add observer for InstanceID token refresh callback.
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.tokenRefreshNotification),
                                                         name: kFIRInstanceIDTokenRefreshNotification, object: nil)
        
        
        ////Setting UIWindow/////////
        window = UIWindow(frame: UIScreen.mainScreen().bounds)
        
        
        ////Intializing AgreeSignupViewController /////////////
        agreeSignupVC = AgreeSignupViewController(nibName: "AgreeSignupViewController", bundle: nil);
        
        ////Intializing HomeViewController /////////////
        homeVC = HomeViewController(nibName: "HomeViewController", bundle: nil);
        ////Intializing SMSVerifyViewController /////////////
        smsVerifyVC = SMSVerifyViewController(nibName: "SMSVerifyViewController", bundle: nil);
        ////Intializing ProfileViewController /////////////
        profileVC = ProfileViewController(nibName: "ProfileViewController", bundle: nil);
        
        ////// Setting VC to Navigation////////////
        //navController = UINavigationController(rootViewController: agreeSignupVC!);
        let userObj : User = DatabaseHelper.getObjectIfExist("User", predicate: NSPredicate(format: "isLoggedIn == 1")) as! User
        if userObj.isLoggedIn == nil{
            navController = UINavigationController(rootViewController: agreeSignupVC!);
            /////// Setting Navigation to RootViewController //////////
            window?.rootViewController = navController;
            
            ////////// Setting UIWindow Visible ////////////
            window?.makeKeyAndVisible()
            
        }else{
            if userObj.isVerified == nil{
                navController = UINavigationController(rootViewController: smsVerifyVC!);
                /////// Setting Navigation to RootViewController //////////
                window?.rootViewController = navController;
                ////////// Setting UIWindow Visible ////////////
                window?.makeKeyAndVisible()
                
            }else{
                if userObj.name == nil {
                    navController = UINavigationController(rootViewController: profileVC!);
                    /////// Setting Navigation to RootViewController //////////
                    window?.rootViewController = navController;
                    ////////// Setting UIWindow Visible ////////////
                    window?.makeKeyAndVisible()
                }else{
                    self.setupViewControllers()
                }
            }
        }
        
        return true
    }
    
    
    
    func connectingSocket(){
        socket.connect()
        socket.delegate = self
        
    }
    
    
    func subscribeUserToSocket(){
        let userObj : User = DatabaseHelper.getObjectIfExist("User", predicate: NSPredicate(format: "isLoggedIn == 1")) as! User
        if userObj.token != nil {
            var dataDic : NSMutableDictionary = NSMutableDictionary()
            dataDic.setValue("authorize", forKey: "command")
            dataDic.setValue(userObj.token , forKey: "token")
            let json : SBJSON = SBJSON()
            let strJson : String = json.stringWithObject(dataDic)
            socket.writeString(strJson)
        }
    }
    
    
    func gotoAgreeViewController() {
        agreeSignupVC = AgreeSignupViewController(nibName: "AgreeSignupViewController", bundle: nil);
        navController = UINavigationController(rootViewController: agreeSignupVC!);
        /////// Setting Navigation to RootViewController //////////
        window?.rootViewController = navController;
        
        ////////// Setting UIWindow Visible ////////////
        window?.makeKeyAndVisible()
        
    }

    
    
    
    // MARK: Websocket Delegate Methods.
    
    func websocketDidConnect(ws: WebSocket) {
        print("websocket is connected")
        self.subscribeUserToSocket()
        
        
        
    }
    
    func websocketDidDisconnect(ws: WebSocket, error: NSError?) {
        
        if let e = error {
            print("websocket is disconnected: \(e.localizedDescription)")
        } else {
            print("websocket disconnected")
        }
        self.socket.connect()
        
    }
    
    func websocketDidReceiveMessage(ws: WebSocket, text: String) {
        print("Received text: \(text)")
        let json : SBJSON = SBJSON()
        let dicJson : NSDictionary = json.objectWithString(text) as! NSDictionary
        var strStatus : String = String()
        if dicJson.valueForKey("status") != nil{
            strStatus = dicJson.valueForKey("status") as! String
        }
        var strCommad: String = String()
        if dicJson.valueForKey("command") != nil{
            strCommad = dicJson.valueForKey("command") as! String
        }
        if strStatus == "success" {
            
            if strCommad == "online" {
                var dataArr : NSArray = NSArray()
                dataArr = dicJson.valueForKey("data")!.valueForKey("online") as! NSArray
                self.udpateContactOnline(dataArr)
            }
            if strCommad == "offline" {
                var dataArr : NSArray = NSArray()
                dataArr = dicJson.valueForKey("data")!.valueForKey("offline") as! NSArray
                self.udpateContactOffline(dataArr)
            }
            if strCommad == "last_seen" {
                var dataDic : NSDictionary = NSDictionary()
                dataDic = dicJson.valueForKey("data")!.valueForKey("last_seen") as! NSDictionary
                self.udpateContactLastSeen(dataDic)
            }
            
        }else{
            if strCommad == "authorize" {
                self.subscribeUserToSocket()
            }
        }
        
    }
    
    
    func udpateContactOnline(dataArr: NSArray){
        for i:Int in 0 ..< dataArr.count {
            var strContactID : String = String()
            strContactID =  dataArr.objectAtIndex(i) as! String
            let contactObj : Contact = DatabaseHelper.getObjectIfExist("Contact", predicate: NSPredicate(format: "identifier == %@",strContactID )) as! Contact
            contactObj.active = "online"
            DatabaseHelper.commitChanges()
            
        }
    }
    
    func udpateContactOffline(dataArr: NSArray){
        for i:Int in 0 ..< dataArr.count {
            var strContactID : String = String()
            strContactID = dataArr.objectAtIndex(i) as! String
            let contactObj : Contact = DatabaseHelper.getObjectIfExist("Contact", predicate: NSPredicate(format: "identifier == %@",strContactID )) as! Contact
            contactObj.active = "offline"
            DatabaseHelper.commitChanges()
            
        }
    }
    
    func udpateContactLastSeen(dataDic: NSDictionary){
        var arrKey : NSArray = NSArray()
        arrKey = dataDic.allKeys
        
        var arrValue : NSArray = NSArray()
        arrValue = dataDic.allValues
        for i:Int in 0 ..< arrValue.count {
            
            var strContactID : String = String()
            strContactID = arrKey.objectAtIndex(i) as! String
            var strLastSeen : String = String()
            strLastSeen = arrValue.objectAtIndex(i) as! String
            let contactObj : Contact = DatabaseHelper.getObjectIfExist("Contact", predicate: NSPredicate(format: "identifier == %@",strContactID )) as! Contact
            if strLastSeen != "0000-00-00 00:00:00" {
                let dateLastSeen : NSDate = Common.convertStringToDate(strLastSeen)
                let strSeen : String = Common.convertDateToString(dateLastSeen)
                contactObj.last_seen = String(format: "%@",strSeen)
            }
            contactObj.active = "offline"
            DatabaseHelper.commitChanges()
        }
    }
    
    
    func websocketDidReceiveData(ws: WebSocket, data: NSData) {
        print("Received data: \(data.length)")
    }
    
    
    
    func silentNotification() {
        
        if #available(iOS 8.0, *) {
            // [START register_for_notifications]
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(forTypes: [.Alert, .Badge], categories: nil)
            UIApplication.sharedApplication().registerUserNotificationSettings(settings)
            UIApplication.sharedApplication().registerForRemoteNotifications()
            // [END register_for_notifications]
        } else {
            // Fallback
            let types: UIRemoteNotificationType = [.Alert, .Badge]
            UIApplication.sharedApplication().registerForRemoteNotificationTypes(types)
        }
    }
    
    
    func soundNotification() {
        
        if #available(iOS 8.0, *) {
            // [START register_for_notifications]
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(forTypes: [.Alert, .Badge, .Sound], categories: nil)
            UIApplication.sharedApplication().registerUserNotificationSettings(settings)
            UIApplication.sharedApplication().registerForRemoteNotifications()
            // [END register_for_notifications]
        } else {
            // Fallback
            let types: UIRemoteNotificationType = [.Alert, .Badge, .Sound]
            UIApplication.sharedApplication().registerForRemoteNotificationTypes(types)
        }
    }
    
    
    
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        // for development
        
        let tokenChars = UnsafePointer<CChar>(deviceToken.bytes)
        var tokenString = ""
        
        for i in 0..<deviceToken.length {
            tokenString += String(format: "%02.2hhx", arguments: [tokenChars[i]])
        }
        devToken = tokenString
        //  FIRInstanceID.instanceID().setAPNSToken(deviceToken, type: FIRInstanceIDAPNSTokenType.Prod)
        //  FIRInstanceID.instanceID().setAPNSToken(deviceToken, type: FIRInstanceIDAPNSTokenType.Sandbox)
        print("tokenString: \(tokenString)")
        print(deviceToken)
        // for production
        //     [[FIRInstanceID instanceID] setAPNSToken:deviceToken type:FIRInstanceIDAPNSTokenTypeProd];
        // for production
        //     [[FIRInstanceID instanceID] setAPNSToken:deviceToken type:FIRInstanceIDAPNSTokenTypeProd];
    }
    
    // [START receive_message]
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject],
                     fetchCompletionHandler completionHandler: (UIBackgroundFetchResult) -> Void) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // Print message ID.
        // print("Message ID: \(userInfo["gcm.message_id"]!)")
        // FIRMessaging.messaging().appDidReceiveMessage(userInfo)
        // Print full message.
        if let rootViewController = UIApplication.topViewController() {
            
            var dataDictionary : NSDictionary = NSDictionary()
            dataDictionary = userInfo as NSDictionary
            NetworkMessageWrapper.incomingMessage(rootViewController, data: dataDictionary as! [NSObject : AnyObject])
//            var isAudio : Bool = false
//            var isMessageExist : Bool = false
//            var isGroupUpdate : Bool = false
//            var isGroupMessage : Bool = false
//            var isAPNS : Bool = false
//            
//            var isMessageRecieved : Bool = false
//            
//            if dataDictionary.valueForKey("custom data")?.valueForKey("isapns") != nil{
//                print("IS APNS")
//                dataDictionary = dataDictionary.valueForKey("custom data") as! NSDictionary
//                isAPNS = true
//                
//            }
//            if dataDictionary.valueForKey("group_id") != nil{
//                isGroupMessage = true
//                print("IS Group")
//                //   return
//            }
//            
//            let f: NSNumberFormatter = NSNumberFormatter()
//            f.numberStyle = .DecimalStyle
//            var contactID : NSNumber = NSNumber()
//            var chat: Chat = (DatabaseHelper.getObjectIfExist("Chat", predicate: NSPredicate(format: "contactID == %@", 0)) as! Chat)
//            var groupObj: Group = (DatabaseHelper.getObjectIfExist("Group", predicate: NSPredicate(format: "identifier == %@", 0)) as! Group)
//            
//            if dataDictionary.valueForKey("command") != nil{
//                let strMessageType : String = dataDictionary.valueForKey("command") as! String
//                if strMessageType == "group_message" ||  strMessageType == "file_group_message" {
//                    contactID  = f.numberFromString(dataDictionary.valueForKey("group_id") as! String)!
//                    DatabaseHelper.deleteObject(chat)
//                    DatabaseHelper.commitChanges()
//                    chat = (DatabaseHelper.getObjectIfExist("Chat", predicate: NSPredicate(format: "contactID == %@", contactID)) as! Chat)
//                    
//                    chat.chatType = NSNumber(int: 1)
//                    groupObj = (DatabaseHelper.getObjectIfExist("Group", predicate: NSPredicate(format: "identifier == %@", contactID)) as! Group)
//                    groupObj.identifier = contactID
//                    DatabaseHelper.commitChanges()
//                    isMessageRecieved = true
//                    chat.contactID = contactID
//                    chat.name = groupObj.name
//                    DatabaseHelper.commitChanges()
//                }else if strMessageType == "group_updated"{
//                    contactID  = f.numberFromString(dataDictionary.valueForKey("group_id") as! String)!
//                    DatabaseHelper.deleteObject(chat)
//                    DatabaseHelper.commitChanges()
//                    chat = (DatabaseHelper.getObjectIfExist("Chat", predicate: NSPredicate(format: "contactID == %@", contactID)) as! Chat)
//                    chat.chatType = NSNumber(int: 1)
//                    groupObj = (DatabaseHelper.getObjectIfExist("Group", predicate: NSPredicate(format: "identifier == %@", contactID)) as! Group)
//                    groupObj.identifier = contactID
//                    chat.name = groupObj.name
//                    DatabaseHelper.commitChanges()
//                    isGroupUpdate = true
//                    chat.contactID = contactID
//                    DatabaseHelper.commitChanges()
//                    
//                }else if strMessageType == "message_ack"{
//                    DatabaseHelper.deleteObject(chat)
//                    DatabaseHelper.commitChanges()
//                }else if strMessageType == "group_message_ack"{
//                    DatabaseHelper.deleteObject(chat)
//                    DatabaseHelper.commitChanges()
//                }else if strMessageType == "group_members_removed"{
//                    DatabaseHelper.deleteObject(chat)
//                    DatabaseHelper.commitChanges()
//                    return
//                }else{
//                    if isAPNS == true {
//                        contactID  = dataDictionary.valueForKey("sender_id") as! NSNumber
//                        
//                    }else{
//                        contactID  = f.numberFromString(dataDictionary.valueForKey("sender_id") as! String)!
//                        
//                    }
//                    DatabaseHelper.deleteObject(chat)
//                    DatabaseHelper.commitChanges()
//                    chat = (DatabaseHelper.getObjectIfExist("Chat", predicate: NSPredicate(format: "contactID == %@", contactID)) as! Chat)
//                    chat.chatType = NSNumber(int: 0)
//                    let contactObj : Contact = DatabaseHelper.getObjectIfExist("Contact", predicate: NSPredicate(format: "identifier == %@", contactID)) as! Contact
//                    chat.name = contactObj.name
//                    isMessageRecieved = true
//                    chat.contactID = contactID
//                    DatabaseHelper.commitChanges()
//                    
//                }
//                
//            }
//            
//            
//            
//            
//            
//            
//            if isMessageRecieved == true {
//                
//                audioPlayer.play()
//                let arrChat: NSArray = DatabaseHelper.getAllData("Message", offset: 0, predicate: NSPredicate(format: "chat_id == %@", contactID), sortDescriptor: nil)
//                
//                
//                var notificationID : String = ""
//                notificationID =   String(format: "%@",dataDictionary.valueForKey("message_id") as! String)
//                
//             //   let messageObj1: Message = (DatabaseHelper.getObjectIfExist("Message", predicate: NSPredicate(format: "identifier == %@", notificationID)) as! Message)
//                
//                
//                let userObj: User = (DatabaseHelper.getObjectIfExist("User", predicate: NSPredicate(format: "isLoggedIn == 1")) as! User)
//                let strMessageID: String = String(format: "%@-%@%@",notificationID,chat.contactID,userObj.userID!)
//                
//                let messageObj: Message = (DatabaseHelper.getObjectIfExist("Message", predicate: NSPredicate(format: "identifier == %@", strMessageID)) as! Message)
//                if messageObj.text != "" {
//                    isMessageExist = true
//                    return
//                }
//                messageObj.identifier = strMessageID
//                // Message *message = [[Message alloc] init];
//                if dataDictionary.valueForKey("message") !=  nil{
//                    
//                    messageObj.text = dataDictionary.valueForKey("message") as! String
//                }
//                
//                
//                if dataDictionary.valueForKey("key") !=  nil{
//                    
//                    messageObj.aesEncrypted = dataDictionary.valueForKey("key") as! String
//                }
//                
//                if dataDictionary.valueForKey("sha") !=  nil{
//                    
//                    messageObj.sha = dataDictionary.valueForKey("sha") as! String
//                }
//                
//                messageObj.sender = Int(1)
//                messageObj.status = Int(2)
//                messageObj.date = NSDate()
//                if isGroupMessage == true {
//                    ///   var strSenderID : String = String(format: "%@", dataDictionary.valueForKey("sender_id") as! NSNumber )
//                    
//                    var senderID : NSNumber = NSNumber()
//                    senderID =  f.numberFromString(dataDictionary.valueForKey("sender_id") as! String)!
//                    
//                    messageObj.senderID   = senderID
//                }else{
//                    messageObj.senderID = contactID
//                    
//                }
//                
//                if dataDictionary.valueForKey("fileType") !=  nil{
//                    let strFileType : String = dataDictionary.valueForKey("fileType") as! String
//                    let strFileName : String = dataDictionary.valueForKey("file") as! String
//                    
//                    if strFileType == "IMAGE" {
//                        messageObj.message_type = NSNumber(int: 1)
//                        messageObj.file_link = String(format: "%@%@",kFilesURL,strFileName )
//                        messageObj.text = "Picture"
//                    }else if strFileType == "AUDIO"{
//                        isAudio = true
//                        messageObj.message_type = NSNumber(int: 2)
//                        messageObj.file_link = strFileName
//                        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {() -> Void in
//                            print("Downloading Started")
//                            var urlToDownload = String(format: "%@%@",kFilesURL,strFileName )
//                            var url = NSURL(string: urlToDownload)!
//                            var urlData = NSData(contentsOfURL: url)!
//                            var paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
//                            var documentsDirectory = paths[0]
//                            var filePath : String =  String(format: "%@/%@", documentsDirectory,strFileName)
//                            //saving is done on main thread
//                            dispatch_async(dispatch_get_main_queue(), {() -> Void in
//                                urlData.writeToFile(filePath, atomically: true)
//                                print("File Saved !")
//                                messageObj.text = "Audio"
//                                messageObj.chat_id = "\(chat.contactID)"
//                                DatabaseHelper.commitChanges()
//                                let fa: NSNumberFormatter = NSNumberFormatter()
//                                fa.numberStyle = .DecimalStyle
//                                chat.messageID = strMessageID
//                                DatabaseHelper.commitChanges()
//                                
//                                
//                                if rootViewController.isKindOfClass(RDVTabBarController){
//                                    
//                                    let rdvTabVC :  RDVTabBarController = rootViewController as! RDVTabBarController
//                                    print("%@",rdvTabVC.selectedViewController)
//                                    let navigationVC : UINavigationController = rdvTabVC.selectedViewController as! UINavigationController
//                                    let VC : UIViewController = navigationVC.topViewController! as UIViewController
//                                    
//                                    if VC.isKindOfClass(ChatDetailViewController) {
//                                        //code
//                                        print("good to go")
//                                        let chatDetailVC : ChatDetailViewController = VC as! ChatDetailViewController
//                                        if chatDetailVC.chat ==  chat {
//                                            chatDetailVC.addingIncomingMessages(messageObj)
//                                        }else{
//                                            var score: Int?
//                                            score = chat.numberOfUnreadMessages.integerValue  + 1
//                                            if isMessageExist == false{
//                                                chat.numberOfUnreadMessages = NSNumber(integer: score!)
//                                            }
//                                            DatabaseHelper.commitChanges()
//                                        }
//                                    }else if VC.isKindOfClass(ChatViewController){
//                                        //    chat.numberOfUnreadMessages = chat.numberOfUnreadMessages + NSNumber(integer: 1)
//                                        var score: Int?
//                                        
//                                        score = chat.numberOfUnreadMessages.integerValue  + 1
//                                        if isMessageExist == false{
//                                            chat.numberOfUnreadMessages = NSNumber(integer: score!)
//                                        }
//                                        
//                                        DatabaseHelper.commitChanges()
//                                        
//                                        let chatVC : ChatViewController = VC as! ChatViewController
//                                        chatVC.tblChat.reloadData()
//                                        print("good to go ")
//                                        
//                                    }else{
//                                        var score: Int?
//                                        score = chat.numberOfUnreadMessages.integerValue  + 1
//                                        if isMessageExist == false{
//                                            chat.numberOfUnreadMessages = NSNumber(integer: score!)
//                                        }
//                                        DatabaseHelper.commitChanges()
//                                    }
//                                    
//                                }
//                                
//                            })
//                        })
//                        
//                    }else if strFileType == "VIDEO"{
//                        messageObj.message_type = NSNumber(int: 3)
//                        messageObj.file_link = String(format: "%@%@",kFilesURL,strFileName )
//                        messageObj.text = "Video"
//                        
//                    }
//                }
//                
//                if dataDictionary.valueForKey("group_id") !=  nil{
//                    messageObj.chat_id = String(format: "%@", dataDictionary.valueForKey("group_id") as! String)
//                }
//                
//                
//                var messageArr : NSMutableArray = NSMutableArray()
//                messageArr.addObject(messageObj)
//                DatabaseHelper.commitChanges()
//                self.messageStatusUpdate(messageArr, status: "received")
//                if isAudio == false {
//                    
//                    
//                    messageObj.chat_id = "\(chat.contactID)"
//                    DatabaseHelper.commitChanges()
//                    let fa: NSNumberFormatter = NSNumberFormatter()
//                    fa.numberStyle = .DecimalStyle
//                    chat.messageID = strMessageID
//                    DatabaseHelper.commitChanges()
//                    
//                    
//                    if rootViewController.isKindOfClass(RDVTabBarController){
//                        
//                        
//                        let rdvTabVC :  RDVTabBarController = rootViewController as! RDVTabBarController
//                        print("%@",rdvTabVC.selectedViewController)
//                        let navigationVC : UINavigationController = rdvTabVC.selectedViewController as! UINavigationController
//                        let VC : UIViewController = navigationVC.topViewController! as UIViewController
//                        
//                        if VC.isKindOfClass(ChatDetailViewController) {
//                            //code
//                            print("good to go")
//                            let chatDetailVC : ChatDetailViewController = VC as! ChatDetailViewController
//                            if chatDetailVC.chat ==  chat {
//                                chatDetailVC.addingIncomingMessages(messageObj)
//                            }else{
//                                var score: Int?
//                                score = chat.numberOfUnreadMessages.integerValue  + 1
//                                
//                                chat.numberOfUnreadMessages = NSNumber(integer: score!)
//                                DatabaseHelper.commitChanges()
//                            }
//                        }else if VC.isKindOfClass(ChatViewController){
//                            //    chat.numberOfUnreadMessages = chat.numberOfUnreadMessages + NSNumber(integer: 1)
//                            var score: Int?
//                            score = chat.numberOfUnreadMessages.integerValue  + 1
//                            chat.numberOfUnreadMessages = NSNumber(integer: score!)
//                            
//                            DatabaseHelper.commitChanges()
//                            let chatVC : ChatViewController = VC as! ChatViewController
//                            if chatVC.isOnScreen {
//                                chatVC.gettingChatArray()
//                                chatVC.tblChat.reloadData()
//                            }
//                            
//                            print("good to go ")
//                            
//                        }else{
//                            var score: Int?
//                            score = chat.numberOfUnreadMessages.integerValue  + 1
//                            chat.numberOfUnreadMessages = NSNumber(integer: score!)
//                            
//                            DatabaseHelper.commitChanges()
//                        }
//                        print("%@",navigationVC.topViewController)
//                        
//                    }else{
//                        var score: Int?
//                        score = chat.numberOfUnreadMessages.integerValue  + 1
//                        chat.numberOfUnreadMessages = NSNumber(integer: score!)
//                        
//                        DatabaseHelper.commitChanges()
//                    }
//                }
//            }
//            
//            
//            
//            
//            
//            else{
//                if isGroupUpdate == true {
//                    if rootViewController.isKindOfClass(RDVTabBarController){
//                        
//                        
//                        let rdvTabVC :  RDVTabBarController = rootViewController as! RDVTabBarController
//                        print("%@",rdvTabVC.selectedViewController)
//                        let navigationVC : UINavigationController = rdvTabVC.selectedViewController as! UINavigationController
//                        let VC : UIViewController = navigationVC.topViewController! as UIViewController
//                        
//                        if VC.isKindOfClass(ChatDetailViewController) {
//                            //code
//                            print("good to go")
//                            let chatDetailVC : ChatDetailViewController = VC as! ChatDetailViewController
//                            if chatDetailVC.chat ==  chat {
//                                
//                            }
//                        }else if VC.isKindOfClass(ChatViewController){
//                            //    chat.numberOfUnreadMessages = chat.numberOfUnreadMessages + NSNumber(integer: 1)
//                            
//                            DatabaseHelper.commitChanges()
//                            let chatVC : ChatViewController = VC as! ChatViewController
//                            chatVC.gettingChatArray()
//                            chatVC.tblChat.reloadData()
//                            print("good to go ")
//                            
//                        }else{
//                            
//                        }
//                        print("%@",navigationVC.topViewController)
//                        
//                    }
//                }
//                let strMessageType : String = dataDictionary.valueForKey("command") as! String
//                if strMessageType == "message_ack" {
//                    
//                    var messageIDs : NSArray = NSArray()
//                    var strMessage : String = String()
//                    strMessage = dataDictionary.valueForKey("messages_ids") as! String
//                    strMessage = strMessage.stringByReplacingOccurrencesOfString("[", withString: "")
//                    strMessage = strMessage.stringByReplacingOccurrencesOfString("]", withString: "")
//                    messageIDs = strMessage.componentsSeparatedByString(",")
//                    for i in 0  ..< messageIDs.count  {
//                        
//                        
//                        
//                        var messageID : String = ""
//                        messageID =   String(format: "%@",messageIDs.objectAtIndex(i) as! String)
//                        messageID = messageID.stringByReplacingOccurrencesOfString("\\", withString: "")
//                        messageID = messageID.stringByReplacingOccurrencesOfString("\"", withString: "")
//                        
//                        let messageObj: Message = (DatabaseHelper.getObjectIfExist("Message", predicate: NSPredicate(format: "identifier == %@", messageID)) as! Message)
//                        
//                        var strAckMessageType : String = String()
//                        strAckMessageType = dataDictionary.valueForKey("ack_type") as! String
//                        if strAckMessageType == "received" {
//                            if messageObj.status.intValue < 2{
//                                messageObj.status = Int(2)
//                            }
//                        }else{
//                            if messageObj.status.intValue < 3{
//                                messageObj.status = Int(3)
//                            }
//                        }
//                        DatabaseHelper.commitChanges()
//                    }
//                    
//                    
//                    if rootViewController.isKindOfClass(RDVTabBarController){
//                        
//                        
//                        
//                        let rdvTabVC :  RDVTabBarController = rootViewController as! RDVTabBarController
//                        print("%@",rdvTabVC.selectedViewController)
//                        let navigationVC : UINavigationController = rdvTabVC.selectedViewController as! UINavigationController
//                        let VC : UIViewController = navigationVC.topViewController! as UIViewController
//                        
//                        if VC.isKindOfClass(ChatDetailViewController) {
//                            //code
//                            print("good to go")
//                            let chatDetailVC : ChatDetailViewController = VC as! ChatDetailViewController
//                            
//                            for i in 0  ..< messageIDs.count  {
//                                
//                                var messageID : String = ""
//                                messageID =   String(format: "%@",messageIDs.objectAtIndex(i) as! String)
//                                messageID = messageID.stringByReplacingOccurrencesOfString("\\", withString: "")
//                                messageID = messageID.stringByReplacingOccurrencesOfString("\"", withString: "")
//                                let messageObj: Message = (DatabaseHelper.getObjectIfExist("Message", predicate: NSPredicate(format: "identifier == %@", messageID)) as! Message)
//                                
//                                if messageObj.senderID != 0{
//                                    chatDetailVC.updateMessageCell(messageObj)
//                                }
//                            }
//                        }
//                    }
//                }else if strMessageType == "group_message_ack"{
//                    var messageIDs : NSArray = NSArray()
//                    var strMessage : String = String()
//                    strMessage = dataDictionary.valueForKey("messages_ids") as! String
//                    strMessage = strMessage.stringByReplacingOccurrencesOfString("[", withString: "")
//                    strMessage = strMessage.stringByReplacingOccurrencesOfString("]", withString: "")
//                    messageIDs = strMessage.componentsSeparatedByString(",")
//                    for i in 0  ..< messageIDs.count  {
//                        
//                        var messageID : String = ""
//                        var userID : String = ""
//                        var groupID : String = ""
//                        
//                        if dataDictionary.valueForKey("group_id") !=  nil{
//                            groupID = String(format: "%@", dataDictionary.valueForKey("group_id") as! String)
//                            
//                        }
//                        
//                        if dataDictionary.valueForKey("sender_id") !=  nil{
//                            userID = String(format: "%@", dataDictionary.valueForKey("sender_id") as! String)
//                            
//                        }
//                        
//                        
//                        messageID =   String(format: "%@",messageIDs.objectAtIndex(i) as! String)
//                        messageID = messageID.stringByReplacingOccurrencesOfString("\\", withString: "")
//                        messageID = messageID.stringByReplacingOccurrencesOfString("\"", withString: "")
//                        let messageObj: Message = (DatabaseHelper.getObjectIfExist("Message", predicate: NSPredicate(format: "identifier == %@", messageID)) as! Message)
//                        
//                        let messageGroupObj: GroupMessageSeen = (DatabaseHelper.getObjectIfExist("GroupMessageSeen", predicate: NSPredicate(format: "message_id == %@  &&  user_id == %@ && chat_id == %@", messageID,userID,groupID)) as! GroupMessageSeen)
//                        messageGroupObj.message_id = messageID
//                        messageGroupObj.chat_id = groupID
//                        messageGroupObj.user_id = userID
//                        
//                        var strAckMessageType : String = String()
//                        strAckMessageType = dataDictionary.valueForKey("ack_type") as! String
//                        if strAckMessageType == "received" {
//                            if messageGroupObj.status!.intValue < 2{
//                                messageGroupObj.status = Int(2)
//                            }
//                        }else{
//                            if messageGroupObj.status!.intValue < 3{
//                                messageGroupObj.status = Int(3)
//                            }
//                        }
//                        DatabaseHelper.commitChanges()
//                    }
//                    if rootViewController.isKindOfClass(RDVTabBarController){
//                        
//                        let rdvTabVC :  RDVTabBarController = rootViewController as! RDVTabBarController
//                        print("%@",rdvTabVC.selectedViewController)
//                        let navigationVC : UINavigationController = rdvTabVC.selectedViewController as! UINavigationController
//                        let VC : UIViewController = navigationVC.topViewController! as UIViewController
//                        
//                        if VC.isKindOfClass(ChatDetailViewController) {
//                            //code
//                            print("good to go")
//                            let chatDetailVC : ChatDetailViewController = VC as! ChatDetailViewController
//                            
//                            for i in 0  ..< messageIDs.count  {
//                                
//                                var messageID : String = ""
//                                messageID =   String(format: "%@",messageIDs.objectAtIndex(i) as! String)
//                                messageID = messageID.stringByReplacingOccurrencesOfString("\\", withString: "")
//                                messageID = messageID.stringByReplacingOccurrencesOfString("\"", withString: "")
//                                let messageObj: Message = (DatabaseHelper.getObjectIfExist("Message", predicate: NSPredicate(format: "identifier == %@", messageID)) as! Message)
//                                
//                                if messageObj.text.isEmpty == false {
//                                    chatDetailVC.updateMessageCell(messageObj)
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//            
//            print("%@", userInfo)
            
            
            
        }
    }
    ///messages_ids
    //ack_type = seen / recieved
    //reciever =
    
    func messageStatusUpdate(messageArr: NSMutableArray, status type : String){
        var messageIDs : NSMutableArray = NSMutableArray()
        var messageUserIDs : NSMutableArray = NSMutableArray()
        var strGroupID : String = String()
        for i in 0  ..< messageArr.count  {
            let messageObj : Message = messageArr.objectAtIndex(i) as! Message
            if messageObj.senderID?.intValue != nil {
                let strArr : NSArray = messageObj.identifier.componentsSeparatedByString("-")
                let strMessage : String = strArr.objectAtIndex(0) as! String
                messageIDs.addObject(strMessage)
                messageUserIDs.addObject(messageObj.senderID!)
                
                let f: NSNumberFormatter = NSNumberFormatter()
                f.numberStyle = .DecimalStyle
                strGroupID = String(format: "%@", messageObj.chat_id)
            }else{
                DatabaseHelper.deleteObject(messageObj)
                DatabaseHelper.commitChanges()
            }

        }
        
        var params : NSMutableDictionary = NSMutableDictionary()
        
        if strGroupID != "" {
            let someString = strGroupID
            let myInteger = Int(someString)
            let myNumber = NSNumber(integer:myInteger!)
            let chatObj : Chat = DatabaseHelper.getObjectIfExist("Chat", predicate: NSPredicate(format: "contactID == %@",myNumber )) as! Chat
            if chatObj.chatType == NSNumber(int: 1) {
                params.setValue(strGroupID, forKey: "group_id")
            }
        }
        
        
        var bllService : BLLServices = BLLServices()
        bllService.delegate = self
        
        params.setValue(messageIDs, forKey: "messages_ids")
        params.setValue(type, forKey: "ack_type")
        
        params.setValue(String(format: "%@",messageUserIDs.objectAtIndex(0) as! NSNumber), forKey: "receiver")
        if type == "seen" {
            bllService.messageSeen(params)
        }else{
            bllService.messageRecieved(params)
        }
        
    }
    
    
    //////***** Success *****////////
    func successWithData(data: AnyObject!, msg successMsg: String!, tag: Int32) {
        let tagService = AuthenticationParserType(rawValue: UInt32(tag))
        if tagService == MessageSeen {
            //            if (messageObj.status.intValue < 3 ) {
            //                messageObj.status = [NSNumber numberWithInt:3];
            //                [DatabaseHelper commitChanges];
            //            }
            //            NSMutableDictionary *dataDic = [[NSMutableDictionary alloc]init];
            //            dataDic = (NSMutableDictionary *)[data objectAtIndex:0];
            //            NSString *strMesageID = [NSString stringWithFormat:@"%@", [dataDic valueForKey:@"message_id"]];
            //            Message *messageObj = (Message *)[DatabaseHelper getObjectIfExist:@"Message" predicate:[NSPredicate predicateWithFormat:@"identifier == %@",strMesageID]];
            //            if ([messageObj.status intValue] < 1) {
            //                messageObj.status = [NSNumber numberWithInt:1];
            //            }
            //            [DatabaseHelper commitChanges];
            //            [self updateMessageCell:messageObj];
            
            var dataDic : NSMutableDictionary = NSMutableDictionary()
            dataDic = data.objectAtIndex(0) as! NSMutableDictionary
            dataDic.valueForKey("message_ids")
            var arrMesageID : NSMutableArray  = NSMutableArray()
            arrMesageID = dataDic.valueForKey("messages_ids") as! NSMutableArray
            for i in 0  ..< arrMesageID.count  {
                var strMessageID : String = String(format: "%@",arrMesageID.objectAtIndex(i) as! String)
                let messageObj: Message = (DatabaseHelper.getObjectIfExist("Message", predicate: NSPredicate(format: "identifier == %@", strMessageID)) as! Message)
                messageObj.status = NSNumber(int: 3)
                DatabaseHelper.commitChanges()
                
            }
            print("Seen")
        }else if tagService == MessageRecieved{
            print("Recieved")
            
        }
    }
    //////***** Failure *****////////
    func requestFailure(failureMsg: String!, tag: Int32) {
        print(failureMsg)
        //  Common.showAlert("Sorry", message: failureMsg)
    }
    
    //////***** Uploading *****////////
    func uploadProgress(progress: Float) {
        
    }
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
        print("\(userInfo)")
    }
    // [END receive_message]
    
    // [START refresh_token]
    func tokenRefreshNotification(notification: NSNotification) {
        // let refreshedToken = FIRInstanceID.instanceID().token()!
        // print("InstanceID token: \(refreshedToken)")
        
        // Connect to FCM since connection may have failed when attempted before having a token.
        connectToFcm()
    }
    // [END refresh_token]
    
    // [START connect_to_fcm]
    func connectToFcm() {
        FIRMessaging.messaging().connectWithCompletion { (error) in
            if (error != nil) {
                print("Unable to connect with FCM. \(error)")
            } else {
                print("Connected to FCM.")
                //  let refreshedToken = FIRInstanceID.instanceID().token()!
                // print("InstanceID token: \(refreshedToken)")
                
                
            }
        }
    }
    // [END connect_to_fcm]
    
    
    
    func applicationDidBecomeActive(application: UIApplication) {
        UIApplication.sharedApplication().applicationIconBadgeNumber = 0
        connectToFcm()
        self.connectingSocket()
    }
    
    // [START disconnect_from_fcm]
    func applicationDidEnterBackground(application: UIApplication) {
        FIRMessaging.messaging().disconnect()
        print("Disconnected from FCM.")
    }
    // [END disconnect_from_fcm]
    
    func setupViewControllers() {
        var firstViewController: UIViewController = ChatViewController(nibName: "ChatViewController", bundle: nil);
        var firstNavigationController: UIViewController = UINavigationController(rootViewController: firstViewController)
        var secondViewController: UIViewController = AllContactViewController(nibName: "AllContactViewController", bundle: nil);
        var secondNavigationController: UIViewController = UINavigationController(rootViewController: secondViewController)
        var thirdViewController: UIViewController = SettingViewController(nibName: "SettingViewController", bundle: nil);
        var thirdNavigationController: UIViewController = UINavigationController(rootViewController: thirdViewController)
        var tabBarController: RDVTabBarController = RDVTabBarController()
        tabBarController.viewControllers = [firstNavigationController, secondNavigationController, thirdNavigationController]
        self.viewController = tabBarController
        self.customizeTabBarForController(tabBarController)
    }
    
    func customizeTabBarForController(tabBarController: RDVTabBarController) {
        var finishedImage: UIImage = UIImage(named: "transparent.png")!
        var unfinishedImage: UIImage = UIImage(named: "transparent.png")!
        var tabBarItemImages: [AnyObject] = ["first", "second", "third"]
        var tabBarItemTitle: [AnyObject] = ["Chats", "Contacts", "Settings"]
        
        var index: Int = 0
        
        var bgImage : UIImageView = UIImageView(image: UIImage(named: "tab bar"))
        bgImage.autoresizingMask = [.FlexibleRightMargin, .FlexibleLeftMargin, .FlexibleBottomMargin] ;
        tabBarController.tabBar.backgroundView.addSubview(bgImage)
        
        for item: RDVTabBarItem in tabBarController.tabBar.items as! Array {
            item.setBackgroundSelectedImage(finishedImage, withUnselectedImage: unfinishedImage)
            var selectedimage: UIImage = UIImage(named: "\(tabBarItemImages[index])_normal")!
            var unselectedimage: UIImage = UIImage(named: "\(tabBarItemImages[index])_normal")!
            item.setFinishedSelectedImage(selectedimage, withFinishedUnselectedImage: unselectedimage)
            item.title = tabBarItemTitle[index] as! String
            index++
        }
        window?.rootViewController = self.viewController;
        
        ////////// Setting UIWindow Visible ////////////
        window?.makeKeyAndVisible()
    }
    
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }
    
    
    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }
    
    
    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }
    
    // MARK: - Core Data stack
    
    lazy var applicationDocumentsDirectory: NSURL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "com.appiskey.iwhispers" in the application's documents Application Support directory.
        let urls = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
        return urls[urls.count-1]
    }()
    
    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = NSBundle.mainBundle().URLForResource("iwhispers", withExtension: "momd")!
        return NSManagedObjectModel(contentsOfURL: modelURL)!
    }()
    
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.URLByAppendingPathComponent("SingleViewCoreData.sqlite")
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: url, options: nil)
        } catch {
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data"
            dict[NSLocalizedFailureReasonErrorKey] = failureReason
            
            dict[NSUnderlyingErrorKey] = error as NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }
        
        return coordinator
    }()
    
    lazy var managedObjectContext: NSManagedObjectContext = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .MainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                abort()
            }
        }
    }
    
    // $dataBuilder->addData
    //    (['notification_id'=>$notification_id,
    //    'sender_id' => $request->user->id,
    //    'message' =>$request->message,
    //    'fragement'=>"No",
    //    'message_time' => $request->message_time,
    //    'messageType'=>'chat_message']);
    
}


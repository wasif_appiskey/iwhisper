//
//  ContactsImporter.swift
//  iwhispers
//
//  Created by Apple on 7/27/16.
//  Copyright © 2016 Appiskey. All rights reserved.
//

import Foundation
import AddressBook
import UIKit

class ContactsImporter {
    
    private class func extractABAddressBookRef(abRef: Unmanaged<ABAddressBookRef>!) -> ABAddressBookRef? {
        if let ab = abRef {
            return Unmanaged<NSObject>.fromOpaque(ab.toOpaque()).takeUnretainedValue()
        }
        return nil
    }
    
    class func importContacts() {
        if(ABAddressBookGetAuthorizationStatus() == ABAuthorizationStatus.Denied || ABAddressBookGetAuthorizationStatus() == ABAuthorizationStatus.Restricted) {
            let alert = UIAlertView(title: "Address Book Access Denied", message: "Please grant us access to your Address Book in Settings -> Privacy -> Contacts", delegate: nil, cancelButtonTitle: "OK")
            alert.show()
            return
        }
        else if (ABAddressBookGetAuthorizationStatus() == ABAuthorizationStatus.NotDetermined) {
            var errorRef: Unmanaged<CFError>? = nil
            var addressBook: ABAddressBookRef? = extractABAddressBookRef(ABAddressBookCreateWithOptions(nil, &errorRef))
            ABAddressBookRequestAccessWithCompletion(addressBook, { (accessGranted: Bool, error: CFError!) -> Void in
                if(accessGranted) {
                    let contacts = self.copyContacts()
                    //callback(contacts)
                }
            })
        }
        else if (ABAddressBookGetAuthorizationStatus() == ABAuthorizationStatus.Authorized) {
            let contacts = self.copyContacts()
          //  callback(contacts)
        }
    }
    
    private class func copyContacts() -> Array<Contacts> {
        var errorRef: Unmanaged<CFError>? = nil
        var addressBook: ABAddressBookRef? = extractABAddressBookRef(ABAddressBookCreateWithOptions(nil, &errorRef))
        var contactsList: NSArray = ABAddressBookCopyArrayOfAllPeople(addressBook).takeRetainedValue() as NSArray
        print("\(contactsList.count) records in the array")
        
        var importedContacts = Array<Contacts>()
        
        for record:ABRecordRef in contactsList {
            var contactPerson: ABRecordRef = record
            var firstName: String = ABRecordCopyValue(contactPerson, kABPersonFirstNameProperty).takeRetainedValue() as! NSString as String
            let lastName: String = ABRecordCopyValue(contactPerson, kABPersonLastNameProperty).takeRetainedValue() as! NSString as String
            
            print("-------------------------------")
            print("\(firstName) \(lastName)")
            
            var phonesRef: ABMultiValueRef = ABRecordCopyValue(contactPerson, kABPersonPhoneProperty).takeRetainedValue() as ABMultiValueRef
            var phonesArray  = Array<Dictionary<String,String>>()
            for var i:Int = 0; i < ABMultiValueGetCount(phonesRef); i++ {
                var label: String = ABMultiValueCopyLabelAtIndex(phonesRef, i).takeRetainedValue() as NSString as String
                let value: String = ABMultiValueCopyValueAtIndex(phonesRef, i).takeRetainedValue() as! NSString as String
                
                print("Phone: \(label) = \(value)")
                
                var phone = [label: value]
                phonesArray.append(phone)
            }
            
            print("All Phones: \(phonesArray)")
            
            var emailsRef: ABMultiValueRef = ABRecordCopyValue(contactPerson, kABPersonEmailProperty).takeRetainedValue() as ABMultiValueRef
            var emailsArray = Array<Dictionary<String, String>>()
            for var i:Int = 0; i < ABMultiValueGetCount(emailsRef); i++ {
                let label: String = ABMultiValueCopyLabelAtIndex(emailsRef, i).takeRetainedValue() as NSString as String
                let value: String = ABMultiValueCopyValueAtIndex(emailsRef, i).takeRetainedValue() as! NSString as String
                
                print("Email: \(label) = \(value)")
                
                var email = [label: value]
                emailsArray.append(email)
            }
            
            print("All Emails: \(emailsArray)")
            
            var birthday: NSDate? = ABRecordCopyValue(contactPerson, kABPersonBirthdayProperty).takeRetainedValue() as? NSDate
            
            print ("Birthday: \(birthday)")
            
            var thumbnail: NSData? = nil
            var original: NSData? = nil
            if ABPersonHasImageData(contactPerson) {
                thumbnail = ABPersonCopyImageDataWithFormat(contactPerson, kABPersonImageFormatThumbnail).takeRetainedValue() as NSData
                original = ABPersonCopyImageDataWithFormat(contactPerson, kABPersonImageFormatOriginalSize).takeRetainedValue() as NSData
            }
            
            let currentContact = Contacts(firstName: firstName, lastName: lastName, birthday: birthday)
            currentContact.phonesArray = phonesArray
            currentContact.emailsArray = emailsArray
            currentContact.thumbnailImage = thumbnail
            currentContact.originalImage = original
            
            importedContacts.append(currentContact)
        }
        
        return importedContacts
    }
    
}
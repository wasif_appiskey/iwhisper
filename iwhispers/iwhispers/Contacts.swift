//
//  Contacts.swift
//  iwhispers
//
//  Created by Apple on 7/27/16.
//  Copyright © 2016 Appiskey. All rights reserved.
//

import Foundation
import UIKit

class Contacts: NSObject {
    var firstName : String
    var lastName : String
    var birthday: NSDate?
    var thumbnailImage: NSData?
    var originalImage: NSData?
    
    // these two contain emails and phones in <label> = <value> format
    var emailsArray: Array<Dictionary<String, String>>?
    var phonesArray: Array<Dictionary<String, String>>?
    
    override var description: String { get {
        return "\(firstName) \(lastName) \nBirthday: \(birthday) \nPhones: \(phonesArray) \nEmails: \(emailsArray)\n\n"}
    }
    
    init(firstName: String, lastName: String, birthday: NSDate?) {
        self.firstName = firstName
        self.lastName = lastName
        self.birthday = birthday
    }
}
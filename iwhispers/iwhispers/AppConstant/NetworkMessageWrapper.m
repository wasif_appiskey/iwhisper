//
//  NetworkMessageWrapper.m
//  iwhispers
//
//  Created by Wasif Iqbal on 2/16/17.
//  Copyright © 2017 Appiskey. All rights reserved.
//

#import "NetworkMessageWrapper.h"
#import "Chat.h"
#import "DatabaseHelper.h"
#import "Group.h"
#import "RDVTabBarController.h"
#import "iwhispers-Swift.h"
#import "ChatDetailViewController.h"

@implementation NetworkMessageWrapper


+ (void)incomingMessage:(UIViewController *)viewController data:(NSDictionary *)dataDic{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    @try {
    
    
    BOOL isAudio = false;
    BOOL isMessageExist = false;
    BOOL isGroupUpdate = false;
    BOOL isGroupMessage = false;
    BOOL isAPNS = false;
    
    BOOL isMessageRecieved = false;
    
    
    
    if ([[dataDic valueForKey:@"custom data"] valueForKey:@"isapns"] != nil && [[dataDic valueForKey:@"custom data"] valueForKey:@"isapns"] != [NSNull null]) {
        dataDic = (NSDictionary *)[dataDic valueForKey:@"custom data"];
        isAPNS = true;
    }
    //////////////////
    if ([dataDic valueForKey:@"group_id"]) {
        isGroupMessage = true;
    }
    //////////////////
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    [f setNumberStyle:NSNumberFormatterDecimalStyle];
    
    NSNumber *contactID = [[NSNumber alloc]init];
    Chat *chat = (Chat *)[DatabaseHelper getObjectIfExist:@"Chat" predicate:[NSPredicate predicateWithFormat:@"contactID == %@",0]];
    
    Group *groupObj = (Group *)[DatabaseHelper getObjectIfExist:@"Group" predicate:[NSPredicate predicateWithFormat:@"identifier == %@",0]];
    
    //////////////////
    
    if ([dataDic valueForKey:@"command"] != nil && [dataDic valueForKey:@"command"]  != [NSNull null]) {
        NSString *strMessageType = (NSString*)[dataDic valueForKey:@"command"];
        if ([strMessageType isEqualToString:@"group_message"] || [strMessageType isEqualToString:@"file_group_message"]) {
            contactID = [f numberFromString:(NSString *)[dataDic valueForKey:@"group_id"]];
            [DatabaseHelper deleteObject:chat];
            [DatabaseHelper commitChanges];
            chat = (Chat *) [DatabaseHelper getObjectIfExist:@"Chat" predicate:[NSPredicate predicateWithFormat:@"contactID == %@",contactID]];
            chat.chatType = [NSNumber numberWithInt:1];
            
            groupObj = (Group *) [DatabaseHelper getObjectIfExist:@"Group" predicate:[NSPredicate predicateWithFormat:@"identifier == %@",contactID]];
            groupObj.identifier = contactID;
            [DatabaseHelper commitChanges];
            isMessageRecieved = true;
            chat.contactID = contactID;
            chat.name  = groupObj.name;
            [DatabaseHelper commitChanges];
        }else if ([strMessageType isEqualToString:@"group_updated"]){
            contactID = [f numberFromString:(NSString *)[dataDic valueForKey:@"group_id"]];
            [DatabaseHelper deleteObject:chat];
            [DatabaseHelper commitChanges];
            chat = (Chat *) [DatabaseHelper getObjectIfExist:@"Chat" predicate:[NSPredicate predicateWithFormat:@"contactID == %@",contactID]];
            chat.chatType = [NSNumber numberWithInt:1];
            groupObj = (Group *)[DatabaseHelper getObjectIfExist:@"Group" predicate:[NSPredicate predicateWithFormat:@"identifier == %@",contactID]];
            groupObj.identifier = contactID;
            chat.name = groupObj.name;
            [DatabaseHelper commitChanges];
            isGroupUpdate = true;
            chat.contactID = contactID;
            [DatabaseHelper commitChanges];
        }else if ([strMessageType isEqualToString:@"message_ack"]){
            [DatabaseHelper deleteObject:chat];
            [DatabaseHelper commitChanges];
        }else if ([strMessageType isEqualToString:@"group_message_ack"]){
            [DatabaseHelper deleteObject:chat];
            [DatabaseHelper commitChanges];
        }else if ([strMessageType isEqualToString:@"group_message_ack"]){
            [DatabaseHelper deleteObject:chat];
            [DatabaseHelper commitChanges];
        }else{
            if (isAPNS == true) {
                contactID = (NSNumber *)[dataDic valueForKey:@"sender_id"];
            }else{
                contactID = [f numberFromString:(NSString *)[dataDic valueForKey:@"sender_id"]];
            }
            [DatabaseHelper deleteObject:chat];
            [DatabaseHelper commitChanges];
            chat = (Chat *)[DatabaseHelper getObjectIfExist:@"Chat" predicate:[NSPredicate predicateWithFormat:@"contactID == %@",contactID]];
            chat.chatType = [NSNumber numberWithInt:0];
            Contact *contactObj = (Contact *)[DatabaseHelper getObjectIfExist:@"Contact" predicate:[NSPredicate predicateWithFormat:@"identifier == %@",contactID]];
            chat.name = contactObj.name;
            isMessageRecieved = true;
            chat.contactID = contactID;
            [DatabaseHelper commitChanges];
        }
    }
    
    //////////////////////
    
    
    
    if ( isMessageRecieved == true) {
        NSArray *arrChat = [DatabaseHelper getAllData:@"Message" offset:0 predicate:[NSPredicate predicateWithFormat:@"chat_id == %@",contactID] sortDescriptor:nil];
        NSString *notificationID = @"";
        notificationID = [NSString stringWithFormat:@"%@",[dataDic valueForKey:@"message_id"]];
        
        User *userObj = (User *)[DatabaseHelper getObjectIfExist:@"User" predicate:[NSPredicate predicateWithFormat:@"isLoggedIn == 1"]];
        NSString *strMessageID = [NSString stringWithFormat:@"%@-%@%@",notificationID,chat.contactID,userObj.userID];
        Message *messageObj = (Message *)[DatabaseHelper getObjectIfExist:@"Message" predicate:[NSPredicate predicateWithFormat:@"identifier == %@",strMessageID]];
        
        if (messageObj.text != nil ) {
            isMessageExist = true;
            return;
        }
        
        if ([dataDic valueForKey:@"fileType"] != nil && [dataDic valueForKey:@"fileType"] != [NSNull null] ) {
            NSString *strFileTypeTemp = (NSString *)[dataDic valueForKey:@"fileType"];

            if ([strFileTypeTemp isEqualToString:@"AUDIO"]){
                isAudio = true;
                if (messageObj.file_link != nil ) {
                    isMessageExist = true;
                    return;
                }
            }else if ([strFileTypeTemp isEqualToString:@"IMAGE"]) {
                if (messageObj.file_link != nil ) {
                    isMessageExist = true;
                    return;
                }
            }else if ([strFileTypeTemp isEqualToString:@"VIDEO"]){
                if (messageObj.file_link != nil ) {
                    isMessageExist = true;
                    return;
                }
            }
        }
        
        
        [appDelegate.audioPlayer play];

        
       
        messageObj.identifier = strMessageID;
        if ([dataDic valueForKey:@"message"] != nil || [dataDic valueForKey:@"message"] != [NSNull null] ) {
            NSString *strMessageTrim  = (NSString *)[dataDic valueForKey:@"message"];
            strMessageTrim = [[strMessageTrim componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@""];
            messageObj.text = strMessageTrim;
        }
//        
        if ([dataDic valueForKey:@"key"] != nil || [dataDic valueForKey:@"key"] != [NSNull null] ) {
            NSString *strAESTrim  = (NSString *)[dataDic valueForKey:@"key"];
            strAESTrim = [[strAESTrim componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@""];
            messageObj.aesEncrypted = strAESTrim;
        }
//
//        if ([dataDic valueForKey:@"sha"] != nil || [dataDic valueForKey:@"key"] != [NSNull null] ) {
//            messageObj.sha = (NSString *)[dataDic valueForKey:@"sha"];
//        }
        
        messageObj.sender = [NSNumber numberWithInt:1];
        messageObj.status = [NSNumber numberWithInt:2];
        messageObj.date = [NSDate date];
        
        if (isGroupMessage == true) {
            NSNumber *senderID = [[NSNumber alloc]init];
            NSString *strSenderID = [NSString stringWithFormat:@"%@",[dataDic valueForKey:@"sender_id"]];
            senderID = [f numberFromString:strSenderID];
            messageObj.senderID = senderID;
        }else{
            messageObj.senderID = contactID;
        }
        
        if ([dataDic valueForKey:@"fileType"] != nil && [dataDic valueForKey:@"fileType"] != [NSNull null] ) {
            
            NSString *strFileType = (NSString *)[dataDic valueForKey:@"fileType"];
            NSString *strFileName = (NSString *)[dataDic valueForKey:@"file"];
            if ([strFileType isEqualToString:@"IMAGE"]) {
                messageObj.message_type = [NSNumber numberWithInt:1];
                messageObj.file_link = [NSString stringWithFormat:@"%@%@",kFilesURL,strFileName];
                messageObj.text = @"Picture";
            }else if ([strFileType isEqualToString:@"AUDIO"]){
                isAudio = true;
                messageObj.message_type = [NSNumber numberWithInt:2];
                messageObj.file_link = strFileName;
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    NSString *urlToDownload = [NSString stringWithFormat:@"%@%@",kFilesURL,strFileName];
                    NSURL *url = [NSURL URLWithString:urlToDownload];
                    NSData *urlData = [[NSData alloc]initWithContentsOfURL:url];
                    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                                         NSUserDomainMask, YES);
                    NSString *documentsDirectory = [paths objectAtIndex:0];
                    NSString * filePath = [NSString stringWithFormat:@"%@/%@",documentsDirectory,strFileName];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [urlData writeToFile:filePath atomically:true];
                        messageObj.text = @"Audio";
                        messageObj.chat_id = [NSString stringWithFormat:@"%@",chat.contactID];
                        [DatabaseHelper commitChanges];
                        NSNumberFormatter *fa = [[NSNumberFormatter alloc]init];
                        [fa setNumberStyle:NSNumberFormatterDecimalStyle];
                        chat.messageID = strMessageID;
                        [DatabaseHelper commitChanges];
                        if ([viewController isKindOfClass:[RDVTabBarController class]]) {
                            RDVTabBarController *rdvTabVC = (RDVTabBarController *)viewController;
                            UINavigationController *navigationVC = (UINavigationController *)[rdvTabVC selectedViewController];
                            UIViewController *VC = (UIViewController *)[navigationVC topViewController];
                            if ([VC isKindOfClass:[ChatDetailViewController class]]) {
                                ChatDetailViewController *chatDetailVC = (ChatDetailViewController *)VC;
                                if (chatDetailVC.chat == chat) {
                                    [chatDetailVC addingIncomingMessages:messageObj];
                                }else{
                                    int score;
                                    score = [chat.numberOfUnreadMessages intValue] + 1;
                                    if (isMessageExist == false) {
                                        chat.numberOfUnreadMessages = [NSNumber numberWithInt:score];
                                    }
                                    [DatabaseHelper commitChanges];
                                }
                                
                                
                            }else if ([VC isKindOfClass:[ChatViewController class]]){
                                int score;
                                score = [chat.numberOfUnreadMessages intValue] + 1;
                                if (isMessageExist == false) {
                                    chat.numberOfUnreadMessages = [NSNumber numberWithInt:score];
                                }
                                [DatabaseHelper commitChanges];
                                ChatViewController *chatVC = (ChatViewController *)VC;
                                [chatVC.tblChat reloadData];
                            }else{
                                int score;
                                score = [chat.numberOfUnreadMessages intValue] + 1;
                                if (isMessageExist == false) {
                                    chat.numberOfUnreadMessages = [NSNumber numberWithInt:score];
                                }
                                [DatabaseHelper commitChanges];
                            }
                        }
                        
                    });
                    
                });
            }else if ([strFileType isEqualToString:@"VIDEO"]){
                messageObj.message_type = [NSNumber numberWithInt:3];
                messageObj.file_link = [NSString stringWithFormat:@"%@%@",kFilesURL,strFileName];
            }
        }
        if ([dataDic valueForKey:@"group_id"] != nil
            && [dataDic valueForKey:@"group_id"] != [NSNull null]) {
            messageObj.chat_id = [NSString stringWithFormat:@"%@",(NSString *)[dataDic valueForKey:@"group_id"]];
        }
        NSMutableArray *messageArr = [[NSMutableArray alloc]init];
        [messageArr addObject:messageObj];
        [DatabaseHelper commitChanges];
        [appDelegate messageStatusUpdate:messageArr status:@"received"];
        if (isAudio == false) {
            messageObj.chat_id = [NSString stringWithFormat:@"%@",chat.contactID];
            [DatabaseHelper commitChanges];
            NSNumberFormatter *fa  = [[NSNumberFormatter alloc]init];
            fa.numberStyle = NSNumberFormatterDecimalStyle;
            chat.messageID = strMessageID;
            [DatabaseHelper commitChanges];
            if ([viewController isKindOfClass:[RDVTabBarController class]]) {
                
                RDVTabBarController *rdvTabVC = (RDVTabBarController *)viewController;
                UINavigationController *navigationVC = (UINavigationController *)[rdvTabVC selectedViewController];
                UIViewController *VC = (UIViewController *)[navigationVC topViewController];
                if ([VC isKindOfClass:[ChatDetailViewController class]]) {
                    ChatDetailViewController *chatDetailVC = (ChatDetailViewController *)VC;
                    if (chatDetailVC.chat == chat) {
                        [chatDetailVC addingIncomingMessages:messageObj];
                    }else{
                        int score;
                        score = [chat.numberOfUnreadMessages intValue] + 1;
                        if (isMessageExist == false) {
                            chat.numberOfUnreadMessages = [NSNumber numberWithInt:score];
                        }
                        [DatabaseHelper commitChanges];
                    }
                }else if ([VC isKindOfClass:[ChatViewController class]]){
                    int score;
                    score = [chat.numberOfUnreadMessages intValue] + 1;
                    chat.numberOfUnreadMessages = [NSNumber numberWithInt:score];
                    [DatabaseHelper commitChanges];
                    ChatViewController *chatVC = (ChatViewController *)VC;
                    if (chatVC.isOnScreen) {
                        [chatVC gettingChatArray];
                        [chatVC.tblChat reloadData];
                    }
                }else{
                    int score;
                    score = [chat.numberOfUnreadMessages intValue] + 1;
                    chat.numberOfUnreadMessages = [NSNumber numberWithInt:score];
                    [DatabaseHelper commitChanges];
                }
            }else{
                int score;
                score = [chat.numberOfUnreadMessages intValue] + 1;
                chat.numberOfUnreadMessages = [NSNumber numberWithInt:score];
                [DatabaseHelper commitChanges];
            }
            /////////////////////////////
        }
        
    }else{
        if (isGroupUpdate == true) {
            
            if ([viewController isKindOfClass:[RDVTabBarController class]]) {
                RDVTabBarController *rdvTabVC = (RDVTabBarController *)viewController;
                UINavigationController *navigationVC = (UINavigationController*)[rdvTabVC selectedViewController];
                UIViewController *VC = (UIViewController *)[navigationVC topViewController];
                if ([VC isKindOfClass:[ChatDetailViewController class]]) {
                    
                }else if ([VC isKindOfClass:[ChatViewController class]]){
                    ChatViewController *chatVC = (ChatViewController *)VC;
                    [chatVC gettingChatArray];
                    [chatVC.tblChat reloadData];
                }else{
                    
                }
            }
        }
        NSString *strMessageType = [NSString stringWithFormat:@"%@",[dataDic valueForKey:@"command"]];
        if ([strMessageType isEqualToString:@"message_ack"]) {
            NSArray *messageIDs = [[NSArray alloc]init];
            NSString *strMessage = [[NSString alloc]init];
            strMessage = (NSString *)[dataDic valueForKey:@"messages_ids"];
            strMessage = [strMessage stringByReplacingOccurrencesOfString:@"[" withString:@""];
            strMessage = [strMessage stringByReplacingOccurrencesOfString:@"]" withString:@""];
            messageIDs = [strMessage componentsSeparatedByString:@","];
            for (int i = 0; i< messageIDs.count; i++) {
                NSString *messageID = [[NSString alloc]init];
                messageID = [NSString stringWithFormat:@"%@",[messageIDs objectAtIndex:i]];
                messageID = [messageID stringByReplacingOccurrencesOfString:@"\\" withString:@""];
                messageID = [messageID stringByReplacingOccurrencesOfString:@"\"" withString:@""];
                Message *messageObj = (Message *)[DatabaseHelper getObjectIfExist:@"Message" predicate:[NSPredicate predicateWithFormat:@"identifier == %@",messageID]];
                NSString *strAckMessageType = [[NSString alloc]init];
                strAckMessageType = [dataDic valueForKey:@"ack_type"];
                if ([strAckMessageType isEqualToString:@"received"]) {
                    if ([messageObj.status intValue] < 2) {
                        messageObj.status = [NSNumber numberWithInt:2];
                    }
                }else{
                    if ([messageObj.status intValue] < 3) {
                        messageObj.status = [NSNumber numberWithInt:3];
                    }
                }
                [DatabaseHelper commitChanges];
            }
            
            if ([viewController isKindOfClass:[RDVTabBarController class]]) {
                RDVTabBarController *rdvTabVC = (RDVTabBarController *)viewController;
                UINavigationController *navigationVC = (UINavigationController*)[rdvTabVC selectedViewController];
                UIViewController *VC = (UIViewController *)[navigationVC topViewController];
                if ([VC isKindOfClass:[ChatDetailViewController class]]) {
                    ChatDetailViewController *chatDetailVC = (ChatDetailViewController *)VC;
                    for (int i = 0; i<messageIDs.count; i++) {
                        NSString *messageID = [[NSString alloc]init];
                        messageID = [NSString stringWithFormat:@"%@",[messageIDs objectAtIndex:i]];
                        messageID = [messageID stringByReplacingOccurrencesOfString:@"\\" withString:@""];
                        messageID = [messageID stringByReplacingOccurrencesOfString:@"\"" withString:@""];
                        Message *messageObj = (Message *)[DatabaseHelper getObjectIfExist:@"Message" predicate:[NSPredicate predicateWithFormat:@"identifier == %@",messageID]];
                        if (messageObj.text != nil) {
                            [chatDetailVC updateMessageCell:messageObj];
                        }

                       
                    }
                }
            }
        }
        else if ([strMessageType isEqualToString:@"group_message_ack"]){
            NSArray *messageIDs = [[NSArray alloc]init];
            NSString *strMessage = [[NSString alloc]init];
            strMessage = (NSString *)[dataDic valueForKey:@"messages_ids"];
            strMessage = [strMessage stringByReplacingOccurrencesOfString:@"[" withString:@""];
            strMessage = [strMessage stringByReplacingOccurrencesOfString:@"]" withString:@""];
            messageIDs = [strMessage componentsSeparatedByString:@","];
            for (int i = 0; i< messageIDs.count; i++) {
                NSString *messageID = [[NSString alloc]init];
                NSString *userID = [[NSString alloc]init];
                NSString *groupID = [[NSString alloc]init];
                if ([dataDic valueForKey:@"group_id"] != nil && [dataDic valueForKey:@"group_id"] != [NSNull null]) {
                    groupID = [NSString stringWithFormat:@"%@",[dataDic valueForKey:@"group_id"]];
                }
                
                if ([dataDic valueForKey:@"sender_id"] != nil && [dataDic valueForKey:@"sender_id"] != [NSNull null]) {
                    userID = [NSString stringWithFormat:@"%@",[dataDic valueForKey:@"sender_id"]];
                }
                messageID = [NSString stringWithFormat:@"%@",[messageIDs objectAtIndex:i]];
                messageID = [messageID stringByReplacingOccurrencesOfString:@"\\" withString:@""];
                messageID = [messageID stringByReplacingOccurrencesOfString:@"\"" withString:@""];
        //        Message *messageObj = (Message *)[DatabaseHelper getObjectIfExist:@"Message" predicate:[NSPredicate predicateWithFormat:@"identifier == %@",messageID]];
                GroupMessageSeen *messageGroupObj = (GroupMessageSeen *)[DatabaseHelper getObjectIfExist:@"GroupMessageSeen" predicate:[NSPredicate predicateWithFormat:@"message_id == %@  &&  user_id == %@ && chat_id == %@",messageID,userID,groupID]];
                messageGroupObj.message_id = messageID;
                messageGroupObj.chat_id = groupID;
                messageGroupObj.user_id = userID;
                NSString * strAckMessageType = [[NSString alloc]init];
                strAckMessageType = (NSString *)[dataDic valueForKey:@"ack_type"];
                if ([strAckMessageType isEqualToString:@"received"]) {
                    if ([messageGroupObj.status intValue] < 2) {
                        messageGroupObj.status = [NSNumber numberWithInt:2];
                    }
                }else{
                    if ([messageGroupObj.status intValue] < 3) {
                        messageGroupObj.status = [NSNumber numberWithInt:3];
                    }
                }
                [DatabaseHelper commitChanges];
            }
            if ([viewController isKindOfClass:[RDVTabBarController class]]) {
                RDVTabBarController *rdvTabVC = (RDVTabBarController *)viewController;
                UINavigationController *navigationVC = (UINavigationController*)[rdvTabVC selectedViewController];
                UIViewController *VC = (UIViewController *)[navigationVC topViewController];
                if ([VC isKindOfClass:[ChatDetailViewController class]]) {
                    ChatDetailViewController *chatDetailVC = (ChatDetailViewController *)VC;
                    for (int i = 0; messageIDs.count; i++) {
                        NSString *messageID = [[NSString alloc]init];
                        messageID = [NSString stringWithFormat:@"%@",[messageIDs objectAtIndex:i]];
                        messageID = [messageID stringByReplacingOccurrencesOfString:@"\\" withString:@""];
                        messageID = [messageID stringByReplacingOccurrencesOfString:@"\"" withString:@""];
                        Message *messageObj = (Message *)[DatabaseHelper getObjectIfExist:@"Message" predicate:[NSPredicate predicateWithFormat:@"identifier == %@",messageID]];
                        if (messageObj.text != nil) {
                            [chatDetailVC updateMessageCell:messageObj];
                        }
                    }
                    
                }
            }
        }
    }
        
    } @catch (NSException *exception) {
        NSLog(@"%@",exception);
    }
}




@end

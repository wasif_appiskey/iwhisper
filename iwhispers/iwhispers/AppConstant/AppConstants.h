//
//  AppConstants.h
//  danone
//
//  Created by Wasif Iqbal on 3/20/15.
//  Copyright (c) 2015 Amani Systems. All rights reserved.
//

#ifndef solution_AppConstants_h
#define solution_AppConstants_h


//Base URL


/////Appiskey Server


//#define kAPIUrl @"http://iwhispers.com.almadani01.nine.ch/iwhisper/public/"
//#define kProfilePictureURL @"http://iwhispers.com.almadani01.nine.ch/iwhisper/public/profile-images/"
//#define kGroupPictureURL @"http://iwhispers.com.almadani01.nine.ch/iwhisper/public/group-images/"
//#define kDefaultURL @"http://iwhispers.com.almadani01.nine.ch/iwhisper/public/profile-images/default.png"
//#define kFilesURL @"http://iwhispers.com.almadani01.nine.ch/iwhisper/public/chat-files/"

///////Staging Server
//
#define kAPIUrl @"http://appillions.com/"
#define kAPIUrlSocket @"ws://appillions.com:3000/"
#define kProfilePictureURL @"http://appillions.com/profile-images/"
#define kGroupPictureURL @"http://appillions.com/group-images/"
#define kDefaultURL @"http://appillions.com/profile-images/default.png"
#define kFilesURL @"http://appillions.com/chat-files/"


////////Developement Server

//#define kAPIUrl @"http://192.168.99.171/iwhispiii/iwhisper/public/"
//#define kAPIUrlSocket @"http://192.168.99.171:8080/"
//#define kProfilePictureURL @"http://192.168.99.171/iwhispiii/iwhisper/public/profile-images/"
//#define kGroupPictureURL @"http://192.168.99.171/iwhispiii/iwhisper/public/group-images/"
//#define kDefaultURL @"http://192.168.99.171/iwhispiii/iwhisper/public/profile-images/default.png"
//#define kFilesURL @"http://192.168.99.171/iwhispiii/iwhisper/public/chat-files/"

#define DEGREES_TO_RADIANS(d) (d * M_PI / 180)

//#define kTitleColor @"#A50E21"
#define kTitleColor @"#e5007d"
#define kGoogleAPI @"AIzaSyDDrLazWlUQHCtmrj04Gk52vmFPJTdOsAo"
#define KMapsServerAPI @"AIzaSyBUq8MNDM1Arl4rG4leqLprv9mFXrusF0Y"
#define UIColorFromRGB(rgbValue) \
[UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0x00FF00) >>  8))/255.0 \
blue:((float)((rgbValue & 0x0000FF) >>  0))/255.0 \
alpha:1.0]
//Font Name
#define KFontBold @"CircularStd-Medium"
#define KFontBoldItalic @"PTSans-BoldItalic"
#define KFontRegular @"Montserrat-Regular"
#define KFontLight @"Muli-Light"

#define KFontItalic @"Muli-Italic"

#define KHTMLBODY @"<style>body{font-family: 'Muli-Light';font-size: 10px;color:white;font-weight:bold}</style>"

//to determine if its iPhone 4 or 5
#define Is_4InchScreen (([[UIScreen mainScreen] bounds].size.height - 568) ? NO : YES)
#define Is_NOTRetina (([[UIScreen mainScreen] respondsToSelector:@selector(scale)] && [[UIScreen mainScreen] scale] == 2) ? NO : YES)

#define screenWidth() [[UIScreen mainScreen] bounds].size.width
#define screenHeight() [[UIScreen mainScreen] bounds].size.height
#define screenCenterX() ([UIScreen mainScreen].bounds.size.width)/2 - 15
#define screenCenterY() ([UIScreen mainScreen].bounds.size.height)/2 - 15
#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))

#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)

#define TimeStamp [NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970] * 1000]

//Set Debug Mode
#define isDebug YES




/*
 
 Service Urls
 
 */

#define KLogin @"account"
#define KSignUp @"account/register"
#define KVerify @"account/phone/verify"
#define KSyncContact @"contact"
#define KSendChatMessage @"chat"
#define KGetCerts @"certs"
#define KGetJobs @"jobs"
#define kCreateGroup @"group"
#define kRemoveMember @"remove/member"
#define KAddGroupMember @"add/member"
#define KGetPromoCode @"account/promo"
#define KPostFeedback @"feedback"
#define KGetCategories @"categories"
#define KGetFavourite @"contractor/favourites"
#define KPostFavourite @"contractor/favourite"
#define KWorkerNearBy @"nearby"
#define KJobsAttachment @"attachments"
#define kProfilePicture @"profile/picture"
#define KProfile @"profile"
#define KGetMessages @"messages"
#define KChatMedia @"http://demo.appiskey.com/hireup/messages"
#define KChatImages @"/images"
#define KChatAudios @"/audios"
#define KChatVideos @"/videos"
#define KChatDocs @"/docs"
#define KSendGroupMessage @"group/chat"
#define KBidsDelete @"bids"
#define KInvite @"invites"
#define KLogout @"account/logout"
#define KCredits @"account/credits"
#define KMessageSeen @"chat/seen"

#define KGetFAQ @"faqs"
#define KGetPrivacyPolicy @"privacy"
#define KGetTermsAndCondition @"terms"
#define kRateUser @"review"
#define kCompleteJob @"jobs"
#define kChangeEmail @"account/email"
#define kChangeNumber @"account/phone"
#define kChangePassword @"account/password"
#define kForgetPassword @"account/forgot"

#define KGetLocation @"locations"

#define KGetContentInsta @"getInstagramImages"
#define KGetPageKey @"getPageByKey"
#define KEventReservation @"reservation"
#define KEventReservationNonAPI @"reservationForm"
#define KEventReservationAvailibility @"checkReservationAvailability"


//////Verification Keys
#define NothingVerified @"0"
#define EmailVerified @"1"
#define SmsVerified @"2"
#define BothVerified @"3"


/////Role Keys
#define Contractor @"contractor"
#define Worker @"worker"


////Job Type
#define RunningJobs @"running"
#define PostedJobs @"posted"
#define CompletedJobs @"completed"

////Default Image Url

#define  DefaultImage  @"http://demo.whatthepsd.com/appiskey/hireup/images/users/pictures/default.png"


//1 = Badminton
//2 = Multi Purpose Court       
//3 = Snooker
//4 = Squash
//5 = Tennis

//API return codes
#define kAPIResponseCodeSuccess @"0" //success
#define kAPIResponseCodeFailure @"1001"//e.g. when wrong password provided for signup
#define kAPIResponseCodeError @"1002"//when service was not called properly
#define KAPINotVerifiedAccountCode @"1064"

//API return codes
#define kAPIResponseCodeUpdated @"1801"//success
//Alert Message
#define AlertTitleServer @"Server busy"
#define AlertTitleOps @"Oops"
#define AlertTitleSorry @"Sorry"
#define AlertTitleThanks @"Thanks"
#define AlertTitleOnSearch @"Search"
#define AlertMessageOnSearch @"Search result not found"
#define AlertMessageNoDataFound @"No result found"
#define AlertMessageNoGalleryExist @"No result found"
#define AlertMessage @"Please try again later"



#define KMindBodySourceName @"OliveBeanStudio"
#define KMindBodySourcePassword @"p2x8ROobDjL1XLc8V3CI/LUA6oE="
#define KSiteID 98458


#define KPublicKey @"MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCyEKN2mmZPkvs6mTltu9K3fm7cMl3JPUlNorNQbQxEEjBOLv8/KD9mp+lpOIqez2xh/iCunARV3M9/2A+KhygaUsGqPknZIwA2viuz++L9TrID1/vff6bLFjZrqFUyEEBU9ME3WCRMf6BK2ffCgj+fVIMBeegdlbdAvA2QMjD4dQIDAQAB"

#define KPrivateKey @"MIICeAIBADANBgkqhkiG9w0BAQEFAASCAmIwggJeAgEAAoGBALIQo3aaZk+S+zqZOW270rd+btwyXck9SU2is1BtDEQSME4u/z8oP2an6Wk4ip7PbGH+IK6cBFXcz3/YD4qHKBpSwao+SdkjADa+K7P74v1OsgPX+99/pssWNmuoVTIQQFT0wTdYJEx/oErZ98KCP59UgwF56B2Vt0C8DZAyMPh1AgMBAAECgYAazi3pYhNiScBXoS7XwdVWCaqzK8LaE0eIcCR5yDy3N5y9fE4w6gRnVgIpdJlLkHKiqtCtMG/npBK5jmNCiZej5IWv6/mobKew0cumGj8807mCILlE4lUqU6doUzaCVwEj1UEX68WD6KHdxTzNcCn8kMBFjm5+sSnAnwBj7iHzaQJBANfgaEjqvz4rBAxFz8uo92qDqcbb6nAikK2mQrCVcfuJVjQmil87keKmLHlPeFHbtdVBw2I6mgd/4m30Ivaafs0CQQDTKR+CAMZlnIwEGMQI2qrYlHZiLV0AJ1/S/Z+sAOsSDJVg+tyXTlaKHlAKP1mfZ+H6l3RYYPGNkarzLrZtDhBJAkEAlO7vQ48gzaUjr8viJmG2ADhBwz4pqZ9PLV5NGEKiVqVTYbtNL5KLfVCj0jWvjtoDwtMSFwzTA2WJM86A6J2B6QJBALVtjuHPMqeZgv/+G2kUIFin7rNaEJ8SNZX+TlOmElNWtn417JSgPWC8vL6hQarhwdriiJdq7ABcGUqKZzl6LOkCQQDGTnMa0uMq1PxAEwGtvjJ3gyL71g7Rg+1qd2JYfRwDNb0fGNIinKHB0z/FU2UXjJm4DqqzlhCMleTQch9QjEPI"


#endif

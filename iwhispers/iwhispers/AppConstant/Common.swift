//
//  Common.swift
//  Hire Up
//
//  Created by Apple on 2/3/16.
//  Copyright © 2016 AppIsKey. All rights reserved.
//

import UIKit
import AudioToolbox
import AVFoundation

class Common: NSObject {

     static func setUserDefaultValue (value : String , key : String){
        NSUserDefaults.standardUserDefaults().setObject(value, forKey: key)
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
     static func getUserDefaultValue (key : String) -> AnyObject {
        let returnValue = NSUserDefaults.standardUserDefaults().objectForKey(key)
        if (returnValue != nil){
            return returnValue!
        }
        return "0"
    }
    
    //////**** Show Alert View ****//////
    
    static func showAlert (title : String , message : String){
        let alert : UIAlertView = UIAlertView(title: title, message: message, delegate: self, cancelButtonTitle: "Ok")
        alert.show()
        
    }

    
    static func validateNumber(number : String) -> Bool{
        
       // let value = "[+\\-\\0-9]{9,15}"
        let index = number.startIndex.advancedBy(1)

        
        let strFirstValue : String = number.substringToIndex(index)
      //  if strFirstValue != "+"{
        //    return false
    //    }
        let strEndValue : String = number.substringFromIndex(index)


      
        
        return self.isNumeric(strEndValue)
    }
    
    static func validateEmail(email : String) -> Bool{
        
        let value = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format: "SELF MATCHES %@", value)
        
        let result =  emailTest.evaluateWithObject(email)
        
        return result
    }
    
    
    static func formatADate(d : NSDate) -> String {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        var s = dateFormatter.stringFromDate(d)
        if self.checkIFCurrentDate(s) == false {
            dateFormatter.dateFormat = "HH:mm"
            s = dateFormatter.stringFromDate(d)
        }
        return s
    }
    
    
    static func convertStringToDate(str : String)-> NSDate{
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = NSTimeZone(name: "GMT")
        let date = dateFormatter.dateFromString(str)
        return date!
    }
    
    
    static func convertDateToString(date : NSDate)-> String{
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = NSTimeZone(name: "GMT")
        dateFormatter.timeZone = NSTimeZone.localTimeZone()

        let strDate = dateFormatter.stringFromDate(date)
        return strDate
    }
    
    static func  isNumeric(s : String) -> Bool
    {
        let PHONE_REGEX = "\\d+"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluateWithObject(s)
        return result
    }
    
    static func checkIFCurrentDate(s: String) -> Bool{
        //Ref date
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        let someDate = dateFormatter.dateFromString(s)
        
        //Get calendar
        let calendar = NSCalendar.currentCalendar()
        
        //Get just MM/dd/yyyy from current date
        let flags : NSCalendarUnit = [NSCalendarUnit.Day , NSCalendarUnit.Month , NSCalendarUnit.Year]
        let components = calendar.components(flags, fromDate: NSDate())
        
        //Convert to NSDate
        let today = calendar.dateFromComponents(components)
        
        if someDate!.timeIntervalSinceDate(today!).isSignMinus {
            return true
        } else {
            return false
        }
    }
    
    
    static func jsonStringWithJSONObject(jsonObject: AnyObject) throws -> String? {
        let data: NSData? = try? NSJSONSerialization.dataWithJSONObject(jsonObject, options: NSJSONWritingOptions.PrettyPrinted)
        
        var jsonStr: String?
        if data != nil {
            jsonStr = String(data: data!, encoding: NSUTF8StringEncoding)
        }
        
        return jsonStr
    }

    
    static func  randomAlphabet(length:Int) -> String {
        let wantedCharacters:NSString="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXZY0123456789"
        let s=NSMutableString(capacity: length)
        for (var i:Int = 0; i < length; i += 1) {
            let r:UInt32 = arc4random() % UInt32( wantedCharacters.length)
            let c:UniChar = wantedCharacters.characterAtIndex( Int(r) )
            s.appendFormat("%C", c)
        }
        return s as String

    }
    
    static func  isAlphabets(s : String) -> Bool
    {
        let ALPHABET_REGEX = "^[a-zA-Z ]+$"
        let alphabetTest = NSPredicate(format: "SELF MATCHES %@", ALPHABET_REGEX)
        let result =  alphabetTest.evaluateWithObject(s)
        return result
    }

    static func play() {
        var badumSound = NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("message", ofType: "mp3")!)
        var audioPlayer = AVAudioPlayer()
        do {
            audioPlayer = try AVAudioPlayer(contentsOfURL: badumSound)
        } catch {
            print("No sound found by URL:\(badumSound)")
        }
        audioPlayer.prepareToPlay()
        audioPlayer.play()
    }

    
}



extension UIViewController{
    var isOnScreen: Bool{
        return self.isViewLoaded() && view.window != nil
    }
}


//
//  Enums.h
//  danone
//
//  Created by Wasif Iqbal on 3/20/15.
//  Copyright (c) 2015 Amani Systems. All rights reserved.
//


#ifndef danone_Enums_h
#define danone_Enums_h


typedef enum CommentsViewEnum
{
    LikesTabs = 0,
    DisLikeTabs = 1,
    CommentTabs = 2
} CommentsViewEnum;


typedef enum NotificationDetailEnum
{
    NotificationLikesTabs = 0,
    NotificationDisLikeTabs = 1,
    NotificationCommentTabs = 2
} NotificationDetailEnum;

typedef enum SocialNetworkType {
    
    FacebookAccount =0,
    LinkedInAccount =1,
    TwitterAccount =2
    
} SocialNetworkType;


typedef enum
{
	Login = 1,
    Signup = 2,
    GetOccupation = 3,
    GetCerts = 4,
    ProfilePicture = 5,
    SetProfile = 6,
    Account = 7,
    GetProfile = 8,
    Verify = 9,
    SyncContact = 10,
    SendChatMessage = 11,
    SendChatImage = 12,
    CreateGroup = 13,
    AddGroupMember = 14,
    GetContactDetail = 15,
    GetGroupDetail = 16,
    SendGroupMessage = 17,
    DeleteAccount = 18,
    RemoveGroupMember = 19,
    MessageSeen = 20,
    MessageRecieved = 21,
    InviteAccept = 22,
    InviteReject = 23,
    DeleteBid = 24,
    Faq = 25,
    PrivacyPolicy = 26,
    RateUser = 27,
    CompleteJob = 28,
    ChangeEmail = 29,
    ChangePassword = 30,
    ForgetPassword = 31,
    ChangeNumber = 32,
    TermsAndCondition = 33,
    UserProfile = 34,
    BidDetail = 35,
    Favourite = 36,
    GetCategory = 37,
    PostFavourite = 38,
    DeleteFavourite = 39,
    PostCredits = 40,
    PromoCode = 41,
    PostFeedback = 42,
    Logout = 43
    
}AuthenticationParserType;


typedef enum JourneyRideType
{
    RideTypeSolo = 0,
    RideTypeShare = 1,
    RideTypeShareFallback=2,
    RideTypeShareNonInitiated=3,
    RideTypeShareCabAssigned=4,
    RideTypeShareFallbackCabAssigned=5
    
} JourneyRideType;

typedef enum DriverState
{
    DriverNotAvailable = 0,
    DriverAvailable = 1,
    DriverPOB = 2,
    DriverPostCheckIn = 3,
    
} DriverState;

typedef NS_ENUM(NSUInteger, AnimationDirectionType) {
    kForward,
    kReverse,
    kCancel,
    kReverseCancel,
    kDriverArrived,
    kCancelShare,
};

typedef enum NotificationState
{
    MessageRecieve = 0, // Message Recieved
    InviteRecieved = 1, // Invite Recieved
    InviteAccepted = 2, // Invite Accepted
    ProposalAccepted = 3, // Proposal Accepted
    JobComplete  = 4,  // Job Completed
} NotificationState;


#endif

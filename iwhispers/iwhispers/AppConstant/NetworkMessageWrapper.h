//
//  NetworkMessageWrapper.h
//  iwhispers
//
//  Created by Wasif Iqbal on 2/16/17.
//  Copyright © 2017 Appiskey. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NetworkMessageWrapper : NSObject

+ (void)incomingMessage:(UIViewController *)viewController data:(NSDictionary *)dataDic;

@end
